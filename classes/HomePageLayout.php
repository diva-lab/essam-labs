<?php 
require_once 'CRUD.php'; 
class HomePageLayout extends CRUD{ 
   //calss attributes 
   public $id; 
   public $title;
   public $position; 
   public $status;    
   public $plugin; 
   public $header_title;
   public $plugin_value;
   //push attributes for relational tables 
   public function enable_relation(){ 
		 array_push(static::$primary_fields); 
   }    
   //define table name and fields 
	protected static $table_name = 'home_page_layout'; 
	protected static $primary_fields = array('id', 'title','position','status','plugin','header_title','plugin_value'); 
	public static function get_position($position){
		$sql = "SELECT * FROM home_page_layout WHERE `position`='$position' AND status = 'publish'";
		 $result_array = static::find_by_sql($sql); 
         return !empty($result_array)? array_shift($result_array) : false; 
	}
	 
	   
} 
?> 
