<?php  
	require_once("../layout/initialize.php"); 
	$define_class = new SocialComments(); 
	$define_class->enable_relation(); 
	$records = $define_class->comment_data("inserted_date", "DESC",null,$general_setting_info->translate_lang_id); 
	require_once("../layout/header.php"); 
?> <!--header end--> 
      <!--sidebar start--> 
       <?php require_once("../layout/navigation.php");?> 
      <!--sidebar end--> 
      <!--main content start--> 
      <section id="main-content"> 
     <section class="wrapper site-min-height"> 
      <h4>Social Comments  Module</h4> 
              <section class="panel"> 
                  <header class="panel-heading"> 
                      View Comments 
                  </header> 
                  <div class="panel-body"> 
                      <div class="adv-table editable-table "> 
                          <!--<div class="clearfix"> 
                              <div class="btn-group"> 
                                  <button id="editable-sample_new" class="btn green"> 
                                      Add New <i class="icon-plus"></i> 
                                  </button> 
                              </div> 
                              <div class="btn-group pull-right"> 
                                  <button class="btn dropdown-toggle" data-toggle="dropdown">Tools <i class="icon-angle-down"></i> 
                                  </button> 
                                  <ul class="dropdown-menu pull-right"> 
                                      <li><a href="#">Print</a></li> 
                                      <li><a href="#">Save as PDF</a></li> 
                                      <li><a href="#">Export to Excel</a></li> 
                                  </ul> 
                              </div> 
                          </div>--> 
                          <div class="space15"></div> 
                          <table class="table  table-hover" > 
                          <thead> 
                            <tr> 
                               <th>#</th> 
                              <th><i class=""></i> User Name</th> 
                              <th><i class=""></i> Comment Title</th> 
                              <th><i class=""></i> Node Title</th> 
                              <th>Node Type</th> 
                              <th><i class=""></i> Created  Date</th> 
                              <th><i class=""></i> Status</th> 
                              <th>Action</th> 
                            </tr> 
                          </thead> 
                          <tbody> 
                              <?php  
							      $serialize = 1;  
							     foreach($records as $record){ 
									 
								 echo "<tr>"; 
								  echo" <td>{$serialize}</td> 
								   <td>{$record->user_name}</td> 
								   <td><a href='full_info.php?id={$record->id}'>{$record->title}</a></td> 
								   <td>{$record->node_title}</td> 
								   <td>{$record->node_type}</td> 
								   <td>{$record->inserted_date}</td> 
								   <td>{$record->status}</td> 
								    <td> "; 
					$module_name = $opened_url_parts[count($opened_url_parts) - 2]; 
					include('../layout/btn_control.php'); 
									echo " 
									</td> 
								</tr>";   //delete dialoge  
						 echo   "<div class='modal fade' id='my{$record->id}' tabindex='-1' role='dialog' aria-labelledby='myModalLabel' aria-hidden='true'> 
								<div class='modal-dialog'> 
									<div class='modal-content'> 
										<div class='modal-header'> 
											<button type='button' class='close' data-dismiss='modal' aria-hidden='true'>&times;</button> 
											<h4 class='modal-title'>Delete</h4> 
										</div> 
										<div class='modal-body'> 
										 <p> Are you sure you want delete  $record->title??</p> 
										</div> 
										<div class='modal-footer'> 
											<button class='btn btn-warning' type='button' onClick=\"window.location.href = 'data_model/delete.php?task=delete&id={$record->id}'\"/> 
											 Confirm</button><button data-dismiss='modal' class='btn btn-default' type='button'>Close</button> 
										</div> 
									</div> 
								</div> 
							</div>"; 
														 $serialize++; 
													   }?> 
                             </tbody> 
                        </table> 
                      </div> 
                  </div> 
              </section> 
              <!-- page end--> 
          </section> 
      </section> 
      <!--main content end--> 
      <!--footer start--> 
     <?php require_once("../layout/footer.php");?>