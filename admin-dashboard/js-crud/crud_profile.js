$(document).ready(function (){	
//if form button clicked
$("#form_crud").submit(function (){
	//disable button
	$('#submit').attr("disabled", "true");
	var type = 'POST';
	var url = $('#form_crud').attr('action');
	//send json data
	var data = {	
		task: $('#process_type').val(),
		record: $('#record').val(),
		title: $('#title').val(), 
		modules: $('select#my_multi_select1').val(),
		global_edit:$('input[name=global_edit]:checked').val(),
		global_delete:$('input[name=global_delete]:checked').val(),
		developer_mode:$('input[name=developer_mode]:checked').val(),
		profile_turn_off:$('input[name=profile_turn_off]:checked').val(),
		post_publishing:$('input[name=post_publishing]:checked').val(),
		page_publishing:$('input[name=page_publishing]:checked').val(),
		event_publishing:$('input[name=event_publishing]:checked').val()
	};
	
	$.ajax({
		type: type,
		url: url,
		data: data,
		beforeSend: function(){
		//show laoding 
		$('#loading_data').html('<img src="../../img/loading.gif"/>&nbsp Loading ....');
		},
		success: function(data){
			if(data.status == 'work'){
				 if($('#process_type').val() == 'update'){
					window.location.href = "../profile_pages/update.php?id="+$('#record').val();
				 }else{
					window.location.href = "../profile_pages/update.php?id="+data.id;			 
				 }
			 }else if(data.status == 'valid_error'){
			 	$('#loading_data').html(data.fileds);
			 	$('#loading_data').css('color', 'red');
				 $('#submit').removeAttr('disabled');	 
			 }else{
				$('#loading_data').html('Error In Process');
			 }
		}
	});
 //do not go to any where
  return false;     
 });
});
 
 




