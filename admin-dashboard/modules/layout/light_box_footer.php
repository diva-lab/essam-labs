    <!--footer end--> 
</section> 
<!-- js placed at the end of the document so the pages load faster --> 
<!--- outo suggest post--> 
<script src="../../js/bootstrap.min.js"></script> 
<script class="include" type="text/javascript" src="../../js/jquery.dcjqaccordion.2.7.js"></script> 
<script src="../../js/jquery.scrollTo.min.js"></script> 
<script src="../../js/jquery.nicescroll.js" type="text/javascript"></script> 
<script type="text/javascript" src="../../assets/data-tables/jquery.dataTables.js"></script> 
<script type="text/javascript" src="../../assets/data-tables/DT_bootstrap.js"></script> 
<script src="../../js/respond.min.js" ></script> 
<script src="../../js/gritter.js" type="text/javascript"></script> 
<script src="../../js/jquery.fastLiveFilter.js"></script> 
<script>
    $(function() {
        $('#search_input').fastLiveFilter('#search_list');
    });
</script>
<!--this page plugins--> 
<!-- for datatime picker--> 
<script type="text/javascript" src="../../js/jquery.datetimepicker.js"></script> 
<script> 
$('#start_time').datetimepicker({ 
    format:'Y-m-d H:i' 
}); 
$('#end_time').datetimepicker({ 
    format:'Y-m-d H:i' 
}); 
$('#start_date').datetimepicker({ 
    format:'Y-m-d H:i' 
}); 
$('#end_date').datetimepicker({ 
    format:'Y-m-d H:i'	 
}); 
$('#end').datetimepicker({ 
    format:'Y-m-d', 
    timepicker:false 
}); 
$('#start').datetimepicker({ 
    format:'Y-m-d', 
    timepicker:false 
}); 

$('.store_time').datetimepicker({
	datepicker:false,
	format:'H:i'
});
</script> 
<!-- js placed at the end of the document so the pages load faster --> 
<!--<script src="js/jquery.js"></script>--> 
</body> 
</html> 
<?php 
//close connection 
if(isset($database)){ 
$database->close_connection(); 
} 
?>