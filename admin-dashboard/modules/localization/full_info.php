<?php 
	require_once("../layout/initialize.php"); 
	if(isset($_GET['id']) && is_numeric($_GET['id'])){ 
		$record_id = $_GET['id']; 
		$record_info = Localization::find_by_id($record_id); 
		//check id access 
		if(empty($record_info->id)){ 
			redirect_to("view.php");	 
		} 
	}else{ 
		redirect_to("view.php");	 
	} 
	require_once("../layout/header.php"); 
?> 
 <!--header end-->  
  <!--sidebar start--> 
  <?php require_once("../layout/navigation.php");?> 
  <!--sidebar end-->  
  <!--main content start--> 
  <section id="main-content"> 
    <section class="wrapper site-min-height">  
       <h4>Localization Module </h4>  
      <!-- page start--> 
      <div class="row"> 
        <aside class="profile-info col-lg-9"> 
          <section> 
            <div class="panel  "> 
              <div class="panel-heading"> Language Info</div> 
              <div class="panel-body"> 
                <form class="form-horizontal tasi-form" role="form" id="form_crud"> 
                <input type="hidden" id="process_type" value="insert"> 
                  <div class="form-group"> 
                    <label  class="col-lg-2 ">Name:</label> 
                    <div class="col-lg-6" > 
                    <?php echo $record_info->name?> 
                    </div> 
                  </div> 
                  <div class="form-group"> 
                    <label  class="col-lg-2 ">Label:</label> 
                    <div class="col-lg-6" > 
                    <?php echo $record_info->label?> 
                    </div> 
                  </div> 
                  <div class="form-group"> 
                  <div class="col-lg-offset-2 col-lg-10"> 
                    <button type="button" class="btn btn-info"   
                        onClick="window.location.href = 'update.php?id='+<?php echo $record_id?>" > <li class="icon-edit-sign"></li> Update </button> 
                        </div> 
                  </div> 
                   
                </form> 
              </div> 
            </div> 
          </section> 
        </aside> 
      </div> 
       
      <!-- page end-->  
    </section> 
  </section> 
  <!--main content end-->  
  <!--footer start--> 
   
  <?php require_once("../layout/footer.php");?>