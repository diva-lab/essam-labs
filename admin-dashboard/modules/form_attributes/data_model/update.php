<?php
require_once('../../../../classes/Session.php');
require_once('../../../../classes/Functions.php');
require_once('../../../../classes/MysqlDatabase.php');
require_once('../../../../classes/FormAttributes.php');
require_once('../../../../classes/Users.php');
require_once('../../../../classes/Profile.php');
//check  session user  log in
if($session->is_logged() == false){
	redirect_to("../../../index.php");
}
// get user profile  
$user_data = Users::find_by_id($session->user_id);
// get user profile data
$user_profile  = Profile::Find_by_id($user_data->user_profile);
// check if the user profile block
if($user_profile->profile_block == "yes"){
   redirect_to("../../../index.php");	
}
if(!empty($_POST["task"]) && $_POST["task"] == "update"){
	//get data
	 $id = $_POST["record"];
	 $edit = FormAttributes::find_by_id($id);
	if($user_profile->globel_edit != 'all_records' && $edit->inserted_by != $session->user_id  ){	
		   redirect_to("../view.php");
	}else{
		  header('Content-Type: application/json');
		  //validite required required
		  $required_fields = array( 'attribute_label'=>'- Insert label');
		  $check_required_fields = check_required_fields($required_fields);
		  if(count($check_required_fields) == 0){
			//update	
				      $edit->form_id = $_POST["form_id"];
					  $edit->required = $_POST["required"];
					  $edit->sorting = $_POST["sorting"];
			          $edit->attribute_label = $_POST["attribute_label"];
		              $edit->attribute_id = $_POST["attribute_id"];
			          $edit->attribute_values = $_POST["attribute_values"];
					  $edit->update_by=$session->user_id;
					  $edit->last_update = date_now();
					  $update = $edit->update();
					  if($update){
						  $data  = array("status"=>'work');
						  echo json_encode($data);
					  }else{
						  $data  = array("status"=>"error");
						  echo json_encode($data);
					  }
					
		   
		}else{
			  //validation error
			  $comma_separated = implode("<br>", $check_required_fields);
			  $data  = array("status"=>"valid_error", "fileds"=>$comma_separated);
			  echo json_encode($data);
		}
  }	
}
//close connection
if(isset($database)){
	$database->close_connection();
}

?>