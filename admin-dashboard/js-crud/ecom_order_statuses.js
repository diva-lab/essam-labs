$(document).ready(function (){
$("#form_crud").submit(function (){
	 //disable button
	 $('#submit').attr("disabled", "true");
	 var type = 'POST';
	 var url = $('#form_crud').attr('action');
	 //send json data
	 var data = {	
			task: $('#process_type').val(),
			record: $('#record').val(),
			name: $('#name').val(), 
			color: $('#color').val(), 
			notify_customer: $('input[name=notify_customer]:checked').val(),  
			email_subject: $('#email_subject').val(), 
			email_header: tinymce.get('email_header').getContent()
		};
	$.ajax({
	 type: type,
	 url: url,
	 data: data,
	 beforeSend: function(){
		//show laoding 
		$('#loading_data').html('<img src="../../img/loading.gif"/>&nbsp Loading ....');
	 },
	 success: function(data){
		if(data.status == 'work'){
			 if($('#process_type').val() == 'update'){
				window.location.href = "full_info.php?id="+$('#record').val();
			 }else{
				window.location.href = "full_info.php?id="+data.inserted_id;
			 }
		}else if(data.status == 'valid_error'){
		 	$('#loading_data').html(data.fileds);
		 	$('#loading_data').css('color', 'red');
		 	$('#submit').removeAttr('disabled');
		}else{
			$('#loading_data').html('Error In Process');
		}
	 }
})
 //do not go to any where
  return false;     
 })    
})