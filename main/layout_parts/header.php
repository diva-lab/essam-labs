<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="" />
    <meta name="Classification" content="" />
    <meta name="RESOURCE-TYPE" content="DOCUMENT" />
    <meta name="DISTRIBUTION" content="GLOBAL" />
    <meta name="robots" content="all" />
    <meta name="revisit-after" content="5 days" />
    <meta name="rating" content="general" />
    <meta http-equiv="Content-Language" content="" />
    <meta name="author" content=""/>
  <?php
   $main = "";
  if(isset($_GET["alias"])){
    $node_alias = $_GET["alias"];
    if($opened_url_file == "post_details.php"){
        //  $post_info = $define_node->front_node_data(null,'post',$node_alias, null,$lang_info->id, null, null, null, null,  null,'one');
         $post_info = $define_node->front_node_data(null,'post',$node_alias,null,$lang_info->id,null,null,null,null,null,null,null,'one',null,null);

         if(!empty($post_info)){
           $main = $post_info;
           if(!empty($post_info->cover_image)){
                  $image = $post_info->cover_image;
                  $parts = explode('/',$image);
                  $image_cover = $parts[count($parts)-1];
                  $folder = $parts[count($parts)-2];
                }else{
                  $folder = "";
                  $image_cover = "";
                }
                $website_new_title = $main->title;
                echo "  <meta property='og:url' content='{$define_general_setting->site_url}post_details.php?lang=$lang&alias=$post_info->alias' />
                <meta property'og:image' content='{$define_general_setting->site_url}/media-library-thumb/$folder/large/$image_cover '/>
                <meta property='og:description' content='".mb_substr(strip_tags($post_info->summary),0,110,'utf-8')."'/>
                <meta property='og:title' content='$post_info->title' />";
              }

          }else if($opened_url_file == "content.php"){
        $page_info =  $define_node->front_node_data(null,'page',$node_alias,null,$lang_info->id,null,null,null,null,null,null,null,'one',null,null);
            if(!empty($page_info)){
              $main = $page_info;
              if($page_info->cover_image){
                $image = $page_info->cover_image;
                $parts = explode('/',$image);
                $image_cover = $parts[count($parts)-1];
                $folder = $parts[count($parts)-2];
              }else{
                $folder = "";
                $image_cover = "";
              }
                $website_new_title = $main->title;
                echo "<meta property='og:url' content='{$define_general_setting->site_url}content.php?lang=$lang&alias=$page_info->alias' />
                  <meta property'og:image' content='{$define_general_setting->site_url}/media-library-thumb/$folder/large/$image_cover '/>
                  <meta property='og:description' content='".mb_substr(strip_tags($page_info->summary),0,110,'utf-8')."'/>
                  <meta property='og:title' content='$page_info->title' />";
              }
      }

  }else{
    $website_new_title = $website_home_title ;
    ?>
        <meta property="og:type" content="website" />
        <meta property="og:title" content="<?php echo $website_new_title;?>" />
        <meta property="og:description" content="<?php echo $define_general_setting->description;?>" />
        <meta property="og:url" content="<?php echo $define_general_setting->site_url;?>" />
        <meta property="og:site_name" content="<?php  echo $website_new_title ; ?>" />
  <?php }?>

    <!--fontAwesome-sheet-->
    <link rel="stylesheet" href="main/css/font-awesome.min.css" />
    <link rel="stylesheet" href="main/css/flexslider.css">
    <!--bootstrap-sheet-->
    <link rel="stylesheet" href="main/css/bootstrap.css" />
    <!--style-sheet-->
    <?php $lang = $_GET['lang']; ?>
    <link rel="stylesheet" href="main/css/style-<?php echo $lang ;  ?>.css" />
    <title><?php echo $website_new_title;  ?></title>

  </head>
  <body>

    <div class='container clearfix'><!--start of container-->
      <header class='col-md-12'>
        <div class='col-md-12 wrapping1 clearfix'><!--wrapping1-->
          <div class='col-md-12 logo'><!--logo-->
            <a href='index.php?lang=<?php echo $lang ;  ?>'><img src='main/images/logo.png' alt=''></a>
          </div><!--end logo-->


          <ul class='col-md-12 social-icons'><!--social-icons-->
          <?php
          // start implementation of social menu links  and listing all of them
           $social_menu = $define_menu_link->menu_submenu_front_data('sorting','ASC','social_menu',0,$lang_info->id);
              if(count($social_menu)){
              foreach($social_menu as $link){
                $path_link_data = $define_node->get_node_content($link->path,$lang_info->id);
                  $path = "";
                  if($link->path_type == "page"){
                    $path = "content.php";
                  }else if($link->path_type == "post"){
                    $path = "post_details.php";
                  }else if($link->path_type == "event"){
                    $path = "event_details.php";
                  }
                  if($link->path_type == "external"){
                    echo "<li><a href='$link->external_path' target='__blank' class='btn fa $link->icon'></a></li>";

                  }else{
                     // echo "<li><a href='$path?alias=$path_link_data->alias&lang=$lang'>$link->title</a></li>";
                  }
               }
          }
          // end of social menu links  implementation is done
          ?>
         <li><a href="<?php if($lang=='en'){echo $url_ar ;}else{echo $url_en ;} ?>" class="btn language"><?php echo $translate_lang ; ?></a></li>
          </ul><!--end social-icons-->


        </div><!--end wrapping1-->

        <nav class='col-md-12 header-nav clearfix'>
          <ul class='col-md-7'>
            <li><a href="index.php?lang=<?php echo $lang ; ?>"><?php if($lang=='ar'){echo "الرئيسيه";}else{echo "Home" ;  } ?></a></li>
          <?php

          // start implementation of social menu links  and listing all of them
           $main_menu = $define_menu_link->menu_submenu_front_data('sorting','ASC','main_menu',0,$lang_info->id);
              if(count($main_menu)){
              foreach($main_menu as $link){
                $path_link_data = $define_node->get_node_content($link->path,$lang_info->id);
                  $path = "";
                  if($link->path_type == "page"){
                    $path = "content.php";
                  }else if($link->path_type == "post"){
                    $path = "post_details.php";
                  }else if($link->path_type == "event"){
                    $path = "event_details.php";
                  }
                  if($link->path_type == "external"){
                    echo "<li><a href='$link->external_path' target='__blank' class='btn fa $link->icon'></a></li>";

                  }else{
                     echo "<li><a href='$path?alias=$path_link_data->alias&lang=$lang'>$link->title</a></li>";
                  }
               }
          }
          // end of social menu links  implementation is done


          ?>
          <!--   <li><a href=''>Home</a></li>
            <li><a href=''>About us</a></li>
            <li><a href=''>Services</a></li>
            <li><a href=''>FAQ</a></li>
            <li><a href=''>Contact us</a></li> -->

          </ul>
           <form class='col-md-5 our-search' action="content.php" method="get">
              <?php $search_info = $define_node->get_node_content(5,$lang_info->id);  ?>
              <div class='input-group'><!--input-group-->
                <input type='text' class='form-control bg-grey search-bar' placeholder='<?php echo $search ;  ?>'>
                 <input type="hidden" class="form-control"  name="lang" value="<?php echo $lang; ?>">
                 <input type="hidden" class="form-control"  name="alias" value="<?php echo $search_info->alias; ?>">
                <span class='input-group-btn'>
                  <button class='btn btn-secondary fa fa-search bg-red search-btn' type='button'></button>
                </span>
              </div><!--end input-group-->
            </form>
        </nav><!--end header-nav-->
      </header>
