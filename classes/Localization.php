<?php 
require_once 'CRUD.php'; 
class Localization extends CRUD{ 
	//calss attributes 
	public $id; 
	public $name; 
	public $label; 
	public $sorting;
	public $status;
	 
	//relation table attribute 
	 
	//push attributes for relational tables 
	public function enable_relation(){ 
		array_push(static::$primary_fields , ''); 
	}    
	//define table name and fields 
	protected static $table_name = 'localization'; 
	protected static $primary_fields = array('id', 'name', 'sorting','status','label'); 
	 
	public function get_file_content($lang_id,$file_name){ 
		$record_info = self::find_by_id($lang_id); 
		$path = "../../../localization/"; 
		$file_path = $path.$record_info->label."/"; 
		$file_directory = $file_path.$file_name.".php"; 
		if(file_exists($file_directory)){ 
			$fh = fopen($file_directory,"r"); 
			$size = filesize($file_directory); 
			if($size!=0){ 
				$data = fread($fh,$size); 
				echo $data; 
			} 
		} 
	}  
	 
} 
?>