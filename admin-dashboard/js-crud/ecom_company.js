$(document).ready(function (){
$("#form_crud").submit(function (){
	var selected_type=[];
	//get company type
	$('input:checkbox[name="company_type[]"]:checked').each(function(){
		   selected_type.push($(this).val());
	});
	 //disable button
	 $('#submit').attr("disabled", "true");
	 var type = 'POST';
	 var url = $('#form_crud').attr('action');
	 //send json data
	 var data = {	
			task: $('#process_type').val(),
			record: $('#record').val(),
			name: $('#name').val(), 
			alias: $('#alias').val(), 
			email: $('#email').val(),
			website_url:  $('#website_url').val(),
			phone:  $('#phone').val(),
			mobile:  $('#mobile').val(),
			fax:  $('#fax').val(),
			address: $('#address').val(),
			description :tinymce.get('description').getContent(),
			company_type: selected_type
		};
	$.ajax({
	 type: type,
	 url: url,
	 data: data,
	 beforeSend: function(){
		//show laoding 
		$('#loading_data').html('<img src="../../img/loading.gif"/>&nbsp Loading ....');
	 },
	 success: function(data){
		 		if(data.status == 'work'){
			 if($('#process_type').val() == 'update'){
				 window.location.href = "full_info.php?id="+$('#record').val();
			 }else{
 				window.location.href = "view.php";			 
			 }
		 }else if(data.status == 'valid_error'){
			 $('#loading_data').html(data.fileds);
			 $('#loading_data').css('color', 'red');
			 $('#submit').removeAttr('disabled');	 
		 }else{
			$('#loading_data').html('Error In Process');
		 }
	 }
})
 //do not go to any where
  return false;     
 })    
})