<?php  
	require_once("../layout/initialize.php"); 
	if(isset($_GET['id']) && is_numeric($_GET['id'])){ 
		$record_id = $_GET['id'];	 
		$record_info = SocialSuggestionTopics::find_by_id($record_id); 
		//check id access 
		if(empty($record_info->id)){ 
			redirect_to("view.php");	 
		} 
	}else{ 
		redirect_to("view.php");	 
	} 
	require_once("../layout/header.php"); 
?>   
  <!--header end-->  
  <!--sidebar start--> 
  <?php require_once("../layout/navigation.php");?> 
  <!--sidebar end-->  
  <!--main content start--> 
  <section id="main-content"> 
   <section class="wrapper site-min-height"> 
      <h4> Social Suggestion Topics Module</h4> 
      <!-- page start--> 
      <div class="row"> 
        <aside class="profile-info col-lg-7"> 
          <section> 
            <div class="panel"> 
              <div class="panel-heading"> Suggestion Topic Info</div> 
              <div class="panel-body"> 
                <form class="form-horizontal tasi-form" role="form" > 
                   
                <div class="form-group"> 
                    <label  class="col-lg-2 control-label">User Name:</label> 
                    <div class="col-lg-6" style="margin-top:7px"> 
                    <?php echo $record_info->user_name?> 
                    </div> 
                  </div> 
                  <div class="form-group"> 
                    <label  class="col-lg-2 control-label">Topic Title:</label> 
                    <div class="col-lg-6" style="margin-top:7px"> 
                    <?php echo $record_info->title?> 
                    </div> 
                  </div> 
                 <div class="form-group"> 
                    <label  class="col-lg-2 control-label">Email:</label> 
                    <div class="col-lg-6" style="margin-top:7px"> 
                    <?php echo $record_info->email?> 
                    </div> 
                  </div> 
                  <div class="form-group"> 
                    <label  class="col-lg-2 control-label">Body:</label> 
                    <div class="col-lg-6" style="margin-top:7px"> 
                    <?php echo $record_info->body?> 
                    </div> 
                  </div> 
                  
                </form> 
              </div> 
            </div> 
          </section> 
        </aside> 
        <div class="col-lg-4"> 
          <section class="panel panel-primary"> 
            <header class="panel-heading"> Entry Information: </header> 
            <div class="panel-body"> 
              <div id="list_info"> 
                <ul> 
                  <li><span style="color:#428bca">> Created Date:</span> <?php echo $record_info->inserted_date?></li> 
                   
                </ul> 
              </div> 
            </div> 
          </section> 
           
        </div> 
      </div> 
       
      <!-- page end-->  
    </section> 
  </section> 
  <!--main content end-->  
  <!--footer start--> 
  <?php require_once("../layout/footer.php");?>