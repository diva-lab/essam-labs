jQuery(document).ready(function($) {
	// process the form
	$('#user_register').submit(function() {
		$('#submit').attr("disabled", "true");
		var formData = {
			lang: $('#lang').val(), 
			first_name: $('#first_name').val(), 
			last_name: $('#last_name').val(),
			email:  $('#email').val(),
			password: $('#password').val(), 
			password_compare:$('#password_compare').val(),
			city:$('#city').val(), 
			area:$('#area').val(), 
			street:$('#street').val() , 
			gender:$('#gender').val() , 
			birth_day: $('#datetimepicker').val()
		};
		// process the form
		$.ajax({
			type 		: 'POST', // define the type of HTTP verb we want to use (POST for our form)
			url 		: 'main/data_model/user_register.php', // the url where we want to POST
			data 		: formData, // our data object
			success : function(data) {
				if(data.status == 'work'){
					$("#validation_message_reg").html("<div class='alert alert-block alert-success'>"+data.message+"</div >");
					$('#user_register')[0].reset();
				}else if (data.status == 'not_work'){
					$("#validation_message_reg").html("<div class='alert alert-block alert-danger'>"+data.message+"</div >");
					$('#submit').removeAttr('disabled'); 
				}else if(data.status == 'email_exists'){
					$("#validation_message_reg").html("<div class='alert alert-block alert-danger'>"+data.message+"</div >");
					$('#submit').removeAttr('disabled'); 
				}else if(data.status == 'pass_length'){
					$("#validation_message_reg").html("<div class='alert alert-block alert-danger'>"+data.message+"</div >");
					$('#submit').removeAttr('disabled');
				}else{
					$("#validation_message_reg").html("<div class='alert alert-block alert-danger'>"+data.message+"</div >");
				}
				//alert(data);
			}
		});
	return false;
	});
	
	
		
});