jQuery(document).ready(function($) {

	// process the form

	$('#general_info').submit(function() {

		$('#save_general').attr('disabled', 'disabled'); 

		var formData = {

			lang: $('#lang').val(), 

			customer_id : $('#customer_id').val(),  

			first_name: $('#first_name').val(), 

			last_name: $('#last_name').val(),

			birthday :  $('#datetimepicker').val(),

			gender: $('#gender').val(), 

			countries :$('#countries').val(),

			colleage :$('#colleage').val(), 

			mobile:$('#mobile').val(), 

			other_mobile:$('#other_mobile').val() , 

			email:$('#email').val() , 

			city: $('#city').val() ,

			area : $('#area').val(), 

			street:$('#street').val() 

		


		};

		// process the form

		$.ajax({

			type 		: 'POST', // define the type of HTTP verb we want to use (POST for our form)

			url 		: 'main/data_model/insert_general_info.php', // the url where we want to POST

			data 		: formData, // our data object

			success : function(data) {
				 

				if(data.status == 'work'){

					$("#general_info_notify").html("<div class='alert alert-block alert-success'>"+data.message+"</div >");
					window.setTimeout(function(){location.reload();},220);
					//$('#general_info')[0].reset();

				}else if (data.status == 'not_work'){
					$("#general_info_notify").html("<div class='alert alert-block alert-danger'>"+data.message+"</div >");
					$('#save_general').removeAttr('disabled'); 
				}else if(data.status == 'email_exists'){
					$("#general_info_notify").html("<div class='alert alert-block alert-danger'>"+data.message+"</div >");
					$('#save_general').removeAttr('disabled'); 
				}else if(data.status == 'pass_length'){
					$("#general_info_notify").html("<div class='alert alert-block alert-danger'>"+data.message+"</div >");
					$('#save_general').removeAttr('disabled');
				}else{

					$("#general_info_notify").html("<div class='alert alert-block alert-danger'>"+data.message+"</div >");

				}
			 

				

			}

		});

	return false;

	});

	

	

		

});