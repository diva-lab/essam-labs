
jQuery(document).ready(function($) { 
	
	// process the form
	$('#form_email_product').submit(function(event) {
	var formData = {
					
					message:$('#pqmessage').val(),
					id:$("#product").val(),
					lang:$('#lang').val()
					
					
					
		};
		// process the form
		$.ajax({
			type 		: 'POST', // define the type of HTTP verb we want to use (POST for our form)
			url 		: 'main/data_model/insert_message_p.php', // the url where we want to POST
			data 		: formData, // our data object
			dataType 	: 'json', // what type of data do we expect back from the server
                        encode          : true
		})
			// using the done promise callback
			.done(function(data) {
                 if(data.status == 'work'){
			       
			       $("#fromEmailHolderMessagep").html('<div class="alert alert-success alert-block fade in"><span style ="font-size:16px"> Your message has been sent  </span></div>');
				  
				   $('#form_email_product')[0].reset();
                    setTimeout(function() { $('#fromEmailHolderMessagep').hide();}, 5000);
				   
					
				    $('#pqsubmit').removeAttr('disabled');
				}else if(data.status == 'Wrong_email'){
					$("#fromEmailHolderMessagep").show();
			        $("#fromEmailHolderMessagep").html('<div class="alert alert-block alert-danger fade in"><span style ="font-size:16px"> E-mail is incorrect </span></div>');
					 $('#pqsubmit').removeAttr('disabled');
					
				}else if(data.status == 'valid_error'){
					 $("#fromEmailHolderMessagep").show();
				 $('#fromEmailHolderMessagep').html('<div class="alert alert-block alert-danger fade in"><span style ="font-size:16px">'+data.fileds+'</span></div>');
				 $('#pqsubmit').removeAttr('disabled');
				 
				}else{ 
			      
			     $("#fromEmailHolderMessage").html('<div class="alert alert-block alert-danger fade in"><span style ="font-size:16px">Please enter your data</span></div>');
			     $("#fromEmailHolderMessage").show();
				 $('#form_email')[0].reset();
			     }
			});
		// stop the form from submitting the normal way and refreshing the page
		event.preventDefault();
	});
	
});