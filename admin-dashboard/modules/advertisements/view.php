<?php
	require_once("../layout/initialize.php");
	$translate_lang = new GeneralSettings();
	$translate_lang->enable_relation();
    $translate_lang_data = $translate_lang->general_settings_data();
	$define_class = new Advertisements();
	$define_class->enable_relation();
	$records = $define_class->adv_data('id','ASC',null,null,$main_lang_id);
	
	//localization 
	//get lang 
	$languages = Localization::find_all();
	require_once("../layout/header.php");
?>
<!--header end-->
<!--sidebar start-->
<?php require_once("../layout/navigation.php");?>
<!--sidebar end--> 
<!--main content start-->
<section id="main-content">
  <section class="wrapper site-min-height">
    <h4> Advertisements  Module</h4>
    <div class="row">
      <div class="col-lg-12">
        <section class="panel">
          <header class="panel-heading">Show Advertisements</header>
          <br/>
          <button type="button" class="btn btn-danger" style="margin-left:15px" onClick="window.location.href = 'insert.php'" <?php
		  $module_name = $opened_url_parts[count($opened_url_parts) - 2];
		  $opened_module_page_insert = $module_name.'/insert';
		  if(!in_array($opened_module_page_insert, $user_allowed_page_array)){
			echo "disabled";
		  }
		  ?>>
          <li class="icon-plus-sign"></li>
          Add New Advertisement </button>
          <br>
          <br/>
            <div class="panel-body"><div class="adv-table editable-table "><table class="table table-striped table-hover table-bordered" id="editable-sample">
            <thead>
              <tr>
                <th>#</th>
                <th><i class=""></i>Title One</th>
                <th><i class=""></i>Status </th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              <?php 
			  $serialize = 1; 
			  foreach($records as $record){
				echo "<tr>
				   <td>{$serialize}</td>
				   <td><a href='full_info.php?id={$record->id}'>{$record->title}</a></td>
				   <td>{$record->status}</td>";
				 
			echo "</td><td>";
					
					  include('../layout/btn_control.php');
				echo "</td>
			</tr>";
			$serialize++;
				?>
                <!--delete dialoge-->
            <div class="modal fade" id="my<?php echo $record->id?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Delete</h4>
                  </div>
                  <div class="modal-body">
                    <p> Are you sure you want delete  <?php echo $record->title?>??</p>
                  </div>
                  <div class="modal-footer">
                    <button class="btn btn-warning" type="button" 
					onClick="window.location.href = 'data_model/delete.php?task=delete&id='+<?php  echo $record->id?>"> Confirm</button>
                    <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
                  </div>
                </div>
              </div>
            </div>
            <?php   }?>
              </tbody>
            
          </table></div></div>
        </section>
      </div>
    </div>
    <!-- page end--> 
  </section>
</section>
<!--main content end--> 
<!--footer start-->
<?php require_once("../layout/footer.php");?>