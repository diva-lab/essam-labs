<?php 
require_once('../../../../classes/Session.php'); 
require_once('../../../../classes/Functions.php'); 
require_once('../../../../classes/MysqlDatabase.php'); 
require_once('../../../../classes/Themes.php'); 
require_once('../../../../classes/ThemesLayouts.php'); 
//call zip class 
$zipArchive = new ZipArchive(); 
//send notfy by json 
//header('Content-Type: application/json');  
//open file 
$file_name = $_POST["file_name"]; 
$file_root = '../../../../theme_temp/'; 
$extract_root = '../../../../'; 
//send notify by json 
if(file_exists($file_root.$file_name)){  
	$open_file = $zipArchive->open($file_root.$file_name);  
	if($open_file){ 
		//get content of zipfile (get name of main folder name) 
		$stat = $zipArchive->statIndex(0);  
		$compresed_file = trim(basename($stat['name']).PHP_EOL);  
		//check flder not exist 
		if(file_exists($extract_root.$compresed_file)){ 
			//delete source 
			unlink($file_root.$file_name); 
			$data  = array("status"=>"plugin_exist"); 
			echo json_encode($data); 
		}else{ 
			//extract to directory 
			$zipArchive->extractTo($extract_root); 
			//close class 
			$zipArchive ->close(); 
			//array hold file info 
			$plugin_info = array(); 
			$plugin_info['source'] = $compresed_file; 
			//load theme config 
			$theme_config = fopen($extract_root.$compresed_file."/theme_config", "r"); 
			if($theme_config){ 
				//loop to the end of file 
				while(!feof($theme_config)){ 
					//each line 
					$line = fgets($theme_config); 
					//do not handle with empty line 
					if(trim($line) != ""){ 
						$line = explode("=>", $line); 
						$plugin_info[$line[0]] = trim($line[1]); 
					} 
				} 
				fclose($theme_config); 
			}else{ 
				$data  = array("status"=>"config_not_exist"); 
				echo json_encode($data);				 
			}			 
			//insert configs to db 
			$add_theme = new Themes(); 
			$add_theme->source = $plugin_info["source"]; 
			$add_theme->name = $plugin_info["name"]; 
			$add_theme->version = $plugin_info["version"]; 
			$add_theme->author = $plugin_info["author"]; 
			$add_theme->uploaded_date = date_now(); 
			$add_theme->uploaded_by = $session->user_id; 
			$insert = $add_theme->insert(); 
			$theme_id = $add_theme->id; 
			if($insert){ 
				$data  = array("status"=>"inserted_theme_done"); 
				echo json_encode($data);		 
			}else{ 
				$data  = array("status"=>"inserted_theme_error"); 
				echo json_encode($data);	 
			} 
			//load theme layout  
			$layout_config = fopen($extract_root.$compresed_file."/layout_config", "r"); 
			if($layout_config){ 
				//loop to the end of file 
				while(!feof($layout_config)){ 
					//each line 
					$line = fgets($layout_config); 
					//do not handle with empty line 
					if(trim($line) != ""){ 
						$line = explode("|", $line); 
						//insert theme 
						$add_layout = new ThemesLayouts(); 
						$add_layout->theme_id = $theme_id; 
						$add_layout->version = $line[0]; 
						$add_layout->author = $line[1]; 
						$add_layout->layout_name = $line[2]; 
						$add_layout->layout_cells = $line[3]; 
						$add_layout->source = $compresed_file; 
						$add_layout->uploaded_date = date_now(); 
						$add_layout->uploaded_by = $session->user_id; 
						$add_layout->insert(); 
					} 
				} 
				fclose($layout_config); 
			} 
			//delete source 
			$data  = array("status"=>"inserted_done"); 
			echo json_encode($data);	 
			unlink($file_root.$file_name); 
		} 
	}else{ 
		$data  = array("status"=>"not_read"); 
		echo json_encode($data); 
	} 
}else{ 
	$data  = array("status"=>"not_exist"); 
	echo json_encode($data); 
} 
?> 
