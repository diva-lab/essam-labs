<?php 
require_once 'CRUD.php'; 
class Themes extends CRUD{ 
   //calss attributes 
   public $id; 
   public $name; 
   public $version; 
   public $author;    
   public $source; 
   public $uploaded_date; 
   public $uploaded_by;   
   //relation table attribute 
    //push attributes for relational tables 
   public function enable_relation(){ 
		array_push(static::$primary_fields , 'user_name'); 
   }    
   //define table name and fields 
	protected static $table_name = 'themes'; 
	protected static $primary_fields = array('id', 'name', 'version', 'source', 'author', 'uploaded_date', 'uploaded_by'); 
	 
	public function themes_data($sort_filed = null, $order_by = null){ 
		$sql = "SELECT themes.id AS id, themes.version AS version, themes.source AS source, themes.name AS name, themes.author AS author, 
				users.user_name AS uploaded_by, themes.uploaded_date AS uploaded_date  
				FROM themes 
				LEFT JOIN users ON themes.uploaded_by = users.id"; 
		if(!empty($sort_filed) && !empty($order_by)){ 
			$sql .= " ORDER BY ".$sort_filed." ".$order_by;  
		 } 
		return self::find_by_sql($sql);  			 
	} 
} 
?> 
