<?php 
 	require_once("../layout/initialize.php"); 
	if(isset($_GET['id']) && is_numeric($_GET['id'])){ 
	   $record_id = $_GET['id']; 
	   $record_info = HomePageLayout::find_by_id($record_id); 
	   if(empty($record_info->id)){ 
		  redirect_to("view.php");	 
	   }
   }else{ 
	   redirect_to("view.php"); 
   } 
   $home_page_plugin = Plugins::find_all_by_custom_filed("enable_index","yes");
 	require_once("../layout/header.php"); 
 ?> 
  <!--header end-->  
  <!--sidebar start--> 
  <?php require_once("../layout/navigation.php");?> 
  <!--sidebar end-->  
  <!--main content start-->  
  <script src="../../js-crud/index_layout.js"></script> 
  <section id="main-content"> 
    <section class="wrapper site-min-height"> 
      <h4>Home Page Layout</h4> 
      <div class="row"> 
        <aside class="col-lg-8"> 
          <section> 
            <div class="panel "> 
              <div class="panel-heading">Add Home layout</div> 
              <div class="panel-body"> 
                <form class="form-horizontal tasi-form" role="form" id="form_crud" action="data_model/update.php"> 
                  <input type="hidden" id="process_type" value="update"> 
                  <input type="hidden" id="record" value="<?php echo $record_id; ?>"> 
                  <div class="form-group"> 
                      <label  class="col-lg-2">Title:</label> 
                      <div class="col-lg-8"> 
                       <input type="text" class="form-control" id="title" placeholder=" " autocomplete="off" value="<?php echo $record_info->title ?>"> 
                      </div> 
                    </div> 
                    <div class="form-group"> 
                      <label  class="col-lg-2">Postion:</label> 
                      <div class="col-lg-8"> 
                         <select class="form-control" id="postion"> 
                        <option value=""> Select Postion</option> 
                        <?php  
						for($i=1;$i<12;$i++){ 
							echo "<option value='$i' "; 
							if($i == $record_info->position){ 
								echo "selected"; 
							} 
							echo ">$i ";
							if($i<= 7){
								echo "Left";
						    }else{
								echo "Right";
							}
							echo"</option>"; 
						} 
						 
						?> 
                        </select> 
                       </div> 
                    </div> 
                    <div class="form-group"> 
                      <label  class="col-lg-2">Status:</label> 
                      <div class="col-lg-8"> 
                        <label class="checkbox-inline"> 
                          <input type="radio" name="status" class="radio " value="publish" <?php if($record_info->status == 'publish'){echo "checked";} ?> > 
                          Publish</label> 
                        <label class="checkbox-inline"> 
                          <input type="radio"  name="status" class="radio " value="draft" <?php   if($record_info->status == 'draft'){echo "checked";}?>> 
                          Draft</label> 
                      </div> 
                    </div> 
                     
                     <div class="form-group  " > 
                    <label  class="col-lg-2">Plugin:</label> 
                    <div class="col-lg-8"> 
                      <select class="form-control" id="plugin"> 
                      <option value="">Select Plugin</option>
                      <?php
					  foreach($home_page_plugin as $pulgin){
						  echo "<option value='$pulgin->id'";
						  if($pulgin->id == $record_info->plugin){
							  echo "selected";
						  }
						  
						  echo">$pulgin->name</option>";
					  }
					  
					   ?>
                       
                      </select> 
                      
                    </div> 
                  </div> 
                  <div class="form-group"> 
                      <label  class="col-lg-2"> Header Title:</label> 
                      <div class="col-lg-8"> 
                       <input type="text" class="form-control" id="header_title" placeholder="English Title || Arabic Title " autocomplete="off" value="<?php echo $record_info->header_title ?>"> 
                      </div> 
                    </div>
                    <div class="form-group"> 
                      <label  class="col-lg-2">Plugin value:</label> 
                      <div class="col-lg-8"> 
                       <input type="text" class="form-control" id="plugin_value" placeholder=" " autocomplete="off" value="<?php echo $record_info->plugin_value ?>"> 
                      </div> 
                    </div>
                    
                  
                    <div class="form-group"> 
                      <div class="col-lg-offs	et-2 col-lg-10"> 
                        <button type="submit" class="btn btn-info" id="submit">Save</button> 
                        <button type="reset" class="btn btn-default">Cancel</button> 
                        <div id="loading_data"></div> 
                      </div> 
                    </div> 
                </form> 
              </div> 
            </div> 
          </section> 
        </aside> 
        <section class='col-lg-4'> 
        <img  src="index-layout.jpg" style="  width: 350px;height: 481px;"/> 
         
         
        </section> 
      </div> 
      <!-- page end-->  
    </section> 
  </section> 
  <!--main content end-->  
  <!--footer start--> 
   
  <?php require_once("../layout/footer.php");?> 
 
