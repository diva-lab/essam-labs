<?php    
	require_once("../layout/initialize.php"); 
	require_once("../layout/header.php"); 
	$poll_id = $_GET['poll_id']; 
?> 
<script type="text/javascript" src="../../js-crud/poll_questions_options.js"></script> 
  <!--header end-->  
  <!--sidebar start--> 
  <?php require_once("../layout/navigation.php");?> 
  <!--sidebar end-->  
  <!--main content start--> 
  <section id="main-content"> 
    <section class="wrapper site-min-height"> 
      <h4>Poll Options  Module</h4> 
      <!-- page start--> 
      <div class="row"> 
        <aside class="col-lg-7"> 
          <section> 
            <div class="panel"> 
              <div class="panel-heading"> Add Poll Option</div> 
              <div class="panel-body"> 
                <form class="form-horizontal tasi-form" role="form" id="form_crud" action="data_model/insert.php"> 
                <input type="hidden" id="process_type" value="insert"> 
                 <input type="hidden" id="poll_id" value="<?php echo $poll_id;?>"> 
                  
                  <div class="form-group"> 
                    <label  class="col-lg-2 ">Option:</label> 
                    <div class="col-lg-8"> 
                       <input type="text" class="form-control" id="option" placeholder=" " autocomplete="off"> 
                    </div> 
                  </div> 
                  <div class="form-group"> 
                   <div class="col-lg-offset-2 col-lg-10"> 
                      <button type="submit" class="btn btn-info" id="submit">Save</button> 
                      <button type="reset" class="btn btn-default">Cancel</button> 
                    <div id="loading_data"></div> 
                   </div> 
                  </div> 
                </form> 
              </div> 
            </div> 
          </section> 
        </aside> 
      </div> 
      <!-- page end-->  
    </section> 
  </section> 
  <!--main content end-->  
  <!--footer start--> 
  <?php require_once("../layout/footer.php");?>