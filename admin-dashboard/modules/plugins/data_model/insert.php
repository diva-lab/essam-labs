<?php 
require_once('../../../../classes/Session.php'); 
require_once('../../../../classes/Functions.php'); 
require_once('../../../../classes/MysqlDatabase.php'); 
require_once('../../../../classes/Plugins.php'); 
//call zip class 
$zipArchive = new ZipArchive(); 
//send notfy by json 
header('Content-Type: application/json');  
//open file 
$file_name = $_POST["file_name"]; 
$file_root = '../../../../plugins_temp/'; 
$extract_root = '../../../../plugins/'; 
//send notify by json 
if (file_exists($file_root.$file_name)){  
	$open_file = $zipArchive->open($file_root.$file_name);  
	if($open_file){ 
		//get content of zipfile (get name of main folder name) 
		$stat = $zipArchive->statIndex(0);  
		$compresed_file = trim(basename($stat['name']).PHP_EOL);  
		//check flder not exist 
		if(file_exists($extract_root.$compresed_file)){ 
			//delete source 
			unlink($file_root.$file_name); 
			$data  = array("status"=>"plugin_exist"); 
			echo json_encode($data); 
		}else{ 
			//extract to directory 
			$zipArchive->extractTo($extract_root); 
			//close class 
			$zipArchive ->close(); 
			//array hold file info 
			$plugin_info = array(); 
			$plugin_info['source'] = $compresed_file; 
			//load and read file 
			$handle = fopen($extract_root.$compresed_file."/config", "r"); 
			if($handle){ 
				//loop to the end of file 
				while(!feof($handle)){ 
					//each line 
					$line = fgets($handle); 
					//do not handle with empty line 
					if(trim($line) != ""){ 
						$line = explode("=>", $line); 
						$plugin_info[$line[0]] = trim($line[1]); 
					} 
				} 
				fclose($handle); 
			}else{ 
					$data  = array("status"=>"config_not_exist"); 
					echo json_encode($data);				 
			} 
			//delete source 
			unlink($file_root.$file_name); 
			//fetch plugin info and save to db 
			$add = new Plugins(); 
			$add->source = $plugin_info["source"]; 
			$add->name = $plugin_info["name"]; 
			$add->description = $plugin_info["description"]; 
			$add->version = $plugin_info["version"]; 
			$add->author = $plugin_info["author"]; 
			$add->enable_options = $plugin_info["options"]; 
			$add->enable_translate = $plugin_info["translate"]; 
			$add->enable_event = $plugin_info["event"]; 
			$add->enable_post = $plugin_info["post"]; 
			$add->enable_page = $plugin_info["page"]; 
			$add->enable_index = $plugin_info["index"]; 
			$add->position = $plugin_info["position"]; 
			$add->uploaded_date = date_now(); 
			$add->uploaded_by = $session->user_id; 
			$insert = $add->insert(); 
			if($insert){ 
				$data  = array("status"=>"inserted_done"); 
				echo json_encode($data);		 
			}else{ 
				$data  = array("status"=>"inserted_error"); 
				echo json_encode($data);	 
			} 
		} 
	}else{ 
		$data  = array("status"=>"not_read"); 
		echo json_encode($data); 
	} 
}else{ 
	$data  = array("status"=>"not_exist"); 
	echo json_encode($data); 
} 
?> 
