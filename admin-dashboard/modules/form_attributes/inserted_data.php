<?php
	require_once("../layout/initialize.php");
	if(isset($_GET['form_id'])){
	$form_id = $_GET['form_id'];
	$form_name = Forms::find_by_id($form_id);
   }else{
	 redirect_to('../forms/view.php');	
   }  
	$define_class = new FormInsertedData();
	$define_class->enable_relation();
	$records = $define_class->form_inserted_data($form_id);
	require_once("../layout/header.php");
?>
<!--header end-->
<!--sidebar start-->
<?php require_once("../layout/navigation.php");?>
<!--sidebar end--> 
<!--main content start-->
<section id="main-content">
  <section class="wrapper site-min-height">
    <h4>Forms</h4>
    <section class="panel">
      <header class="panel-heading"> View Forms Attributes </header>
      <div class="panel-body">
        <div class="adv-table editable-table "> 
          <!--<div class="clearfix">
                              <div class="btn-group">
                                  <button id="editable-sample_new" class="btn green">
                                      Add New <i class="icon-plus"></i>
                                  </button>
                              </div>
                              <div class="btn-group pull-right">
                                  <button class="btn dropdown-toggle" data-toggle="dropdown">Tools <i class="icon-angle-down"></i>
                                  </button>
                                  <ul class="dropdown-menu pull-right">
                                      <li><a href="#">Print</a></li>
                                      <li><a href="#">Save as PDF</a></li>
                                      <li><a href="#">Export to Excel</a></li>
                                  </ul>
                              </div>
                          </div>-->
          <div class="space15"></div>
          <br/>
          <br/>
          <table class="table table-striped table-hover table-bordered" id="editable-sample">
            <thead>
              <tr>
                <th>#</th>
                <th><i class=""></i> Label</th>
                <th><i class=""></i> value </th>
                <th class=""><i class=""></i>Inserted Date</th>
               
              </tr>
            </thead>
            <tbody>
              <?php 
							   $serialize=1;
							   foreach($records as $record){
								 echo "<tr>
								  <td>{$serialize}</td>
                                 <td>{$record->label}</td>
								  <td>{$record->value}</td>
								  <td>{$record->inserted_date}</td>
							 </tr>";  
                             
			   $serialize++;
             }?>
            </tbody>
          </table>
        </div>
      </div>
    </section>
    <!-- page end--> 
  </section>
</section>
<!--main content end--> 
<!--footer start-->
<?php require_once("../layout/footer.php");?>
