// JavaScript Document
//load layout type
$(".type").click(function() {
	$('#type_id').html('');
	var type = $(this).val();
	if( type == 'category'){
		 $('#cat_div').removeClass('hide');
		 $('#product_div').addClass('hide');
		var path_data = {	
				type: $(this).val()
				
		 }	
		//load path by path type
		 $.ajax({
			 type: 'POST',
			 url: 'data_model/get_layout_type.php',
			 dataType: 'JSON',
			 data: path_data,
			 beforeSend: function(){
			//show laoding 
			   $('#type_loading_data').html('<img src="../../img/loading.gif"/>&nbsp Loading ....');
			 },
			 success: function(data){
					 
				  $('#type_loading_data').html('');
				  $('#cat_id').append('<option value="">Select category </option>');
				   for(var x=0; x < data.length; x++){
					 $('#cat_id').append('<option value="' + data[x].id + '">' + data[x].title + '</option>');
					
				}
				 
			   }
			 });
		 }else{
	         $('#product_div').removeClass('hide');	
			 $('#cat_div').addClass('hide')
			  
			 
	     }
	 
	
	//load layout type
})