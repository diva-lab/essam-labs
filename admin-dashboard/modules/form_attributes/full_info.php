<?php	
	require_once("../layout/initialize.php");
    if(isset($_GET['id']) && is_numeric($_GET['id'])){
		$record_id = $_GET['id'];
		$define_class= new FormAttributes();
		$define_class->enable_relation();
	    $record_info = $define_class->form_attribute_data('inserted_date', 'DESC',$record_id);
		//check id access
		if(empty($record_info->id)){
			redirect_to("view.php");	
		}
	}else{
		redirect_to("view.php");	
	}
	require_once("../layout/header.php");	
?>
<!--header end-->
<!--sidebar start-->
<?php require_once("../layout/navigation.php");?>
<!--sidebar end--> 
<!--main content start-->
<section id="main-content">
  <section class="wrapper site-min-height">
    <h4> Forms</h4>
    <div class="row">
      <aside class="col-lg-8">
        <section>
          <div class="panel">
            <div class="panel-heading">Forms Attributes Info</div>
            <div class="panel-body">
              <form class="form-horizontal tasi-form" role="form" id="form_crud">
                <input type="hidden" id="process_type" value="insert">
                <div class="form-group">
                  <label  class="col-lg-2 ">Attribute :</label>
                  <div class="col-lg-8" > <?php echo $record_info->attribute_type?> </div>
                </div>
                 <div class="form-group">
                  <label  class="col-lg-2 ">Attribute Label:</label>
                  <div class="col-lg-8" > <?php echo $record_info->attribute_label?> </div>
                </div>
                <div class="form-group">
                  <label  class="col-lg-2 ">Form Name:</label>
                  <div class="col-lg-8" > <?php echo $record_info->form_name;?> </div>
                </div>
                <div class="form-group">
                  <label  class="col-lg-2 ">Required:</label>
                  <div class="col-lg-8" > <?php echo $record_info->required;?> </div>
                </div>
                 <div class="form-group">
                  <label  class="col-lg-2 ">Values:</label>
                  <div class="col-lg-8" > <?php echo $record_info->attribute_values;?> </div>
                </div>
                <div class="form-group">
                  <div class="col-lg-offset-2 col-lg-10">
                    <button type="button" class="btn btn-info"  
                        onClick="window.location.href = 'update.php?id='+<?php echo $record_id?>" >
                    <li class="icon-edit-sign"></li>
                    Update </button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </section>
      </aside>
      <div class="col-lg-4">
          <section class="panel panel-primary">
            <header class="panel-heading"> Entry Info: </header>
            <div class="panel-body">
              <div id="list_info">
                <ul>
                    
                      <li><span style="color:#428bca">> Created By:</span> <?php echo $record_info->inserted_by?></li>
                      <li><span style="color:#428bca">> Created Date:</span> <?php echo $record_info->inserted_date?></li>
                      <li><span style="color:#428bca">> Last Update By:</span>
                        <?php if($record_info->update_by!=""){
					  echo $record_info->update_by;
				      }else{
						  echo "--";}
					  ?>
                      </li>
                      <li><span style="color:#428bca">> Last Update Date:</span>
                        <?php if($record_info->last_update!=""){
					  echo $record_info->last_update;
				      }else{
						  echo "--";
					 }
					  ?>
                      </li>
                   
                    
                
                </ul>
              </div>
            </div>
          </section>
          
        </div>
    </div>
    
    <!-- page end--> 
  </section>
</section>
<!--main content end--> 
<!--footer start-->

<?php require_once("../layout/footer.php");?>