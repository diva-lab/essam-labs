<?php
require_once('../../classes/FrontSession.php'); 
require_once('../../classes/Functions.php'); 
require_once('../../classes/MysqlDatabase.php'); 
require_once('../../classes/CustomerSelections.php'); 
require_once('../../localization/'.$_POST["lang"].'/validation_label.php');
header('Content-Type: application/json');
if(!empty($_POST["item_id"])){ 
//check if exist
	$item_exist = CustomerSelections::front_check_item_exsit($front_session->user_id,$_POST["item_id"]);
	 if(empty($item_exist)){
		$add_item = new CustomerSelections(); 
		$add_item->customer_id = $front_session->user_id; 
		$add_item->product_id =  $_POST["item_id"];
		$insert = $add_item->insert();
		$insert_id = $add_item->id;
		if($insert){
			$data = array("status"=>"work","action"=>"yes");
			echo json_encode($data);					
			
		}else{
			$data = array("status"=>"not_work","message"=>$error_from_system);
			echo json_encode($data);	
		}
	 
	}else{
		   
		   $find_selection = CustomerSelections::find_by_id($item_exist->id);
		   $delete = $find_selection->delete();
		    $data = array("status"=>"work","action"=>"no");
			echo json_encode($data);					
	}
  }else{
	  $data = array("status"=>"not_work","message"=>$error_from_system);
			echo json_encode($data);
}

?>