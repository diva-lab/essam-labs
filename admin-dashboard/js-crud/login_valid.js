$(document).ready(function (){
//get email val and trim it
var username = $('#username');

//get pass val and trim it
var password = $('#password');

//when button clicked call vaild email and password function
$(".form-signin").submit(function (){
  if(valid_username() && valid_password()){
         //disable button
         $('#submit').attr("disabled", "true");
		 //show laoding 
         var type = $('.form-signin').attr('method');
         var url = 'login_processing.php';
		 		 //send json data
         var data = {	
		 				task: 'login',
                        username: username.val(), 
                        password: password.val()
                    };
		 $.ajax({
             type: type,
             url: url,
             data: data,
			 dataType:"JSON",
			 beforeSend: function(){
				  $('#username_error').text('');
				  $('#password_error').text(''); 
       			  $('#loading_data').html('<img src="img/loading.gif"/>&nbspLoading..');
			 },
             success: function(data){
				 if(data.status == "work"){
					window.location.href = "modules/home/";
				 }else if(data.status == "block"){
					 $('#loading_data').css("color","red").html('Sorry  Your profile is blocked');
					
					 
				 }else{
					 $('#loading_data').css("color","red").html('The data you entered are incorrect');
					 $('#submit').removeAttr("disabled");
					  
				 }
             }
         })
           return false;
      }else{
         valid_username();
         valid_password();
         return false;
      } 
 })    

//function valid email
  function valid_username(){
       if(username.val() == ''){
           $('#username_error').css("color","red").text('Insert User Name');
           return false;
       }else{
           $('#username_error').text('');
           return true;
       }
   }
   
 //function valid password
   function valid_password(){
        if(password.val() ==''){
           $('#password_error').css("color","red").text('Insert Your Password');
           return false;
       }else{
           $('#password_error').text('');
           return true;
       }
   }  
})