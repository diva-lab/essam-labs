<?php  
	require_once("../layout/initialize.php"); 
	require_once("../layout/header.php"); 
?> 
<script type="text/javascript" src="../../js-crud/crud_media_directory.js"></script> 
  <!--header end-->  
  <!--sidebar start--> 
<?php require_once("../layout/navigation.php");?> 
  <!--sidebar end-->  
  <!--main content start--> 
  <section id="main-content"> 
    <section class="wrapper site-min-height"> 
      <h4>Media Librar Module</h4> 
      <!-- page start--> 
      <div class="row"> 
        <aside class="profile-info col-lg-8"> 
          <section> 
            <div class="panel"> 
              <div class="panel-heading"> Add Media Directory</div> 
              <div class="panel-body"> 
                <form class="form-horizontal tasi-form" role="form" id="form_crud" action="data_model/insert.php"> 
                <input type="hidden" id="process_type" value="insert"> 
                  
                  <div class="form-group"> 
                    <label  class="col-lg-2">Title:</label> 
                    <div class="col-lg-8"> 
                      <input type="text" class="form-control" id="title" placeholder=" " autocomplete="off"> 
                    </div> 
                  </div> 
                 
                  <div class="form-group"> 
                    <div class="col-lg-offset-2 col-lg-10"> 
                      <button type="submit" class="btn btn-info" id="submit">Save</button> 
                      <button type="reset" class="btn btn-default">Cancel</button> 
                      <div id="loading_data"></div> 
                    </div> 
                  </div> 
                </form> 
              </div> 
            </div> 
          </section> 
        </aside> 
      </div> 
       <!-- page end-->  
    </section> 
  </section> 
  <!--main content end-->  
  <!--footer start--> 
  <?php require_once("../layout/footer.php");?>