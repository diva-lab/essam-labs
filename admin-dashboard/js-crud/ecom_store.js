$(document).ready(function (){
	var main_content = {};
	var address_main_content = {};
	var selected_categories = [];
	//delete selected categories
	

$("#form_crud").submit(function (){
	//get selected categories
	$('input:checkbox[name="category[]"]:checked').each(function(){
		   selected_categories.push($(this).val());
	});
	
	//get all main content values
	$(".main_content").each(function(){
		if($(this).is("textarea")){
			var textarea_value = tinymce.get($(this).attr("id")).getContent()
			main_content[$(this).attr("id")] = textarea_value; 
		}else{
			 main_content[$(this).attr("id")] = $(this).val(); 
		}
	});	 
	//get all address main content values
	$(".address_main_content").each(function(){
		if($(this).is("textarea")){
			var textarea_value = tinymce.get($(this).attr("id")).getContent()
			address_main_content[$(this).attr("id")] = textarea_value; 
		}else{
			 address_main_content[$(this).attr("id")] = $(this).val(); 
		}
	});	 
	 //disable button
	 $('#submit').attr("disabled", "true");
	 var type = 'POST';
	 var url = $('#form_crud').attr('action');
	 //send json data
	 var data = {	
			task: $('#process_type').val(),
			record: $('#record').val(),
			email: $('#email').val(),
			address_email: $('#address_email').val(),
			imageVal:$('#imageVal').val(),
			website_url:  $('#website_url').val(),
		    phone: $('#phone').val(),
			state:$('#state').val(),
			open_time: $('#open_time').val(),
			close_time: $('#close_time').val(),
			main_content: main_content,
			enable_shipping:$('input[name=enable_shippng]:checked').val(),
			enable_comments:$('input[name=comments]:checked').val(),
		    front_page:$('input[name=show_in_front]:checked').val(),
		    slide_show:$('input[name=show_in_slide]:checked').val(),
			imageVal_slider:$('#imageVal_slider').val(),
			categories:selected_categories,
			enable_stipe:$('input[name=enable_stipe]:checked').val(),
			enable_pay_pal:$('input[name=enable_pay_pal]:checked').val(),
			enable_master_card:$('input[name=enable_master_card]:checked').val(),
			enable_visa:$('input[name=enable_visa]:checked').val(),
			address_main_content:address_main_content,
			fax: $('#fax').val()
		};
	$.ajax({
	 type: type,
	 url: url,
	 data: data,
	 beforeSend: function(){
		//show laoding 
		$('#loading_data').html('<img src="../../img/loading.gif"/>&nbsp Loading ....');
	 },
	 success: function(data){
		 if(data.status == 'work'){
			 if($('#process_type').val() == 'update'){
				 window.location.href = "full_info.php?id="+$('#record').val();
			 }else{
 				window.location.href = "view.php";			 
			 }
		 }else if(data.status == 'valid_error'){
			 $('#loading_data').html(data.fileds);
			 $('#loading_data').css('color', 'red');
			 $('#submit').removeAttr('disabled');	 
		 }else{
			$('#loading_data').html('Error In Process');
		 }
	 }
})
 //do not go to any where
  return false;     
 })    
})