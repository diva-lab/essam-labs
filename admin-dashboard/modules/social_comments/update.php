<?php 
	require_once("../layout/initialize.php"); 
	if(isset($_GET['id']) && is_numeric($_GET['id'])){ 
		$record_id = $_GET['id']; 
		// get the record info 
		$define_class = new SocialComments; 
		$define_class->enable_relation(); 
		$record_info = $define_class->comment_data(null,null,$record_id); 
		//check id access 
		if(empty($record_info->id)){ 
			redirect_to('view.php');	 
		} 
	}else{ 
		redirect_to('view.php');	 
	} 
	require_once("../layout/header.php"); 
?> 
  <script type="text/javascript" src="../../js-crud/crud_social_comments.js"></script>  
  <!--header end-->  
  <!--sidebar start--> 
  <?php require_once("../layout/navigation.php");?> 
  <!--sidebar end-->  
  <!--main content start--> 
  <section id="main-content"> 
    <section class="wrapper site-min-height"> 
      <h4> Comments Module</h4> 
      <!-- page start--> 
      <div class="row"> 
        <aside class="profile-info col-lg-7"> 
          <section> 
            <div class="panel"> 
              <div class="panel-heading"> Edit Comment</div> 
              <div class="panel-body"> 
                <form class="form-horizontal tasi-form" role="form" id="form_crud" action="data_model/update.php"> 
                  <input type="hidden" id="process_type" value="update"> 
                  <input type="hidden" id="record" value="<?php echo $record_id; ?>"> 
                  
                  <div class="form-group"> 
                    <label  class="col-lg-2 "> Comment Title:</label> 
                    <div class="col-lg-8"> 
                      <input type="text" class="form-control" id="title" placeholder=" "   
                      value="<?php echo $record_info->title;?>"autocomplete="off"> 
                    </div> 
                    </div> 
                    <div class="form-group"> 
                    <label  class="col-lg-2 "> Post Title:</label> 
                    <div class="col-lg-8"> 
                    <p><?php echo $record_info->node_title;?> </p> 
                    </div> 
                  </div> 
                   
                   <div class="form-group"> 
                    <label  class="col-lg-2 ">User Name:</label> 
                    <div class="col-lg-8"> 
                   <p><?php echo $record_info->user_name?></p> 
                    </div> 
                  </div> 
                   <div class="form-group"> 
                    <label  class="col-lg-2 ">Email:</label> 
                    <div class="col-lg-8"> 
                      <p><?php echo $record_info->email?></p> 
                    </div> 
                  </div> 
                  <div class="form-group"> 
                    <label  class="col-lg-2 ">body:</label> 
                    <div class="col-lg-8"> 
                      <textarea class=" form-control" id="comment_body" placeholder=" " 
                        rows="6"><?php echo $record_info->body; ?></textarea> 
                    </div> 
                  </div> 
                  <div class="form-group"> 
                    <div class="col-lg-offset-2 col-lg-10"> 
                      <button type="submit" class="btn btn-info" id="submit">Save</button> 
                      <button type="button" class=" btn btn-info "   
                    onClick="window.location.href = 'full_info.php?id='+<?php echo $record_id?>" > <i class="icon-info-sign"></i> View Full Info </button> 
                      <div id="loading_data"></div> 
                    </div> 
                  </div> 
                </form> 
              </div> 
            </div> 
          </section> 
        </aside> 
        <div class="col-lg-4"> 
          <section class="panel panel-primary"> 
            <header class="panel-heading"> Publish Options: </header> 
            <div class="panel-body"> 
              <form class="form-horizontal tasi-form" role="form"> 
                  <div class="form-group "> 
                <label class="col-lg-5">Status:</label> 
                <div class="col-lg-6"> 
                  <label class="checkbox-inline"> 
                    <input type="radio" name="shadow" class="radio" value="draft" <?php if($record_info->status=="draft") echo 'checked'?>> 
                    Draft</label> 
                  <label class="checkbox-inline"> 
                    <input type="radio"  name="shadow" class="radio" value="publish" <?php if($record_info->status=="publish") echo 'checked'?>> 
                   Publish</label> 
                </div> 
              </div>      
              </form> 
            </div> 
          </section> 
        </div> 
       </div> 
       
      <!-- page end-->  
    </section> 
  </section> 
  <!--main content end-->  
  <!--footer start-->  
  
  <?php require_once("../layout/footer.php");?>