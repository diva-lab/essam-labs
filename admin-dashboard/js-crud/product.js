$(document).ready(function (){
var main_content = {};
var selected_categories = [];
var selected_image_gallery = {};
var selected_tages = [];

//delete selected categories
$('.DeleteCategory').live('click', function() {
	var li_id = $(this).closest('li').attr("id");
	$(this).closest('li').remove();
});

//insert and update ajex 	

$("#form_crud").submit(function (){
	//get selected categories
	$(".selected_category li").each(function() {
		selected_categories.push($(this).attr("id"));
	});
	//image gallery 
	//get selected image
	$('.selected_image_gallery').each(function() { 
		 selected_image_gallery[$(this).attr("id")] = $(this).find('input.image_gallery_name').val()+','+$(this).find('input.image_gallery_sort').val()+','+$(this).find('input.image_gallery_caption').val();
	})	
	//get all main content values
	$(".main_content").each(function(){
		if($(this).is("textarea")){
			var textarea_value = tinymce.get($(this).attr("id")).getContent()
			main_content[$(this).attr("id")] = textarea_value; 
		}else{
			 main_content[$(this).attr("id")] = $(this).val(); 
		}
	});
	//get selected tages
   $(".tages").each(function() {
		selected_tages.push($(this).attr("name"));
	});

	//disable button
	$('#submit').attr("disabled", "true");
	var type = 'POST';
	var url = $('#form_crud').attr('action');
	//send json data
	var data = {	
		task: $('#process_type').val(),
		record: $('#record').val(),
		main_content: main_content,
		shadow: $('input[name=shadow]:checked').val(), 
		lang: $('#lang').val(),
		model: $('#model').val(),
		author_id:$('#author').val(),
		enable_comments:$('input[name=comments]:checked').val(),
		enable_summary:$('input[name=enable_summary]:checked').val(),
		front_page:$('input[name=show_in_front]:checked').val(),
		slide_show:$('input[name=show_in_slide]:checked').val(),
		categories:selected_categories,
		tags: selected_tages,
		imageVal:$('#imageVal').val(),
		imageVal_slider:$('#imageVal_slider').val(),
		selected_image_gallery: selected_image_gallery,
		start_time: $('#start_time').val(),
		end_time: $('#end_time').val()			
	};
	$.ajax({
	type: type,
	url: url,
	data: data,
	beforeSend: function(){
	//show laoding 
	$('#loading_data').html('<img src="../../img/loading.gif"/>&nbsp Loading ....');
	},
	success: function(data){
	 if(data.status == 'work'){
		 if($('#process_type').val() == 'update'){
			window.location.href = "full_info.php?id="+$('#record').val();
		 }else{
			window.location.href = "full_info.php?id="+data.inserted_id;
		 }
	 }else if(data.status == 'valid_error'){
		 $('#loading_data').html(data.fileds);
		 $('#loading_data').css('color', 'red');
		 $('#submit').removeAttr('disabled');
	 }else{
		$('#loading_data').html('Error In Process');
	 }
	}
	})
//do not go to any where
return false; 

}) ;
//delete ajex
$('.confirm_delete').click(function(){
	var getUrl = window.location.pathname;
    var hostName=window.location.hostname;
   	var task = $("#task_type").val();
	var del_id = $(this).attr('id');
	var data = {
		 task : task ,
		 id : del_id
		};
	var url = "data_model/delete.php";
	var type = "GET";
	$.ajax({
		url : url,
		type : type,
		data : data,
		success : function(data){
			$('#post_'+del_id).remove();
			var rowsCount = $('#myTable tr').length;
			var urlVars=getUrlVars();
			if(urlVars ['page'] == undefined) {
				urlVars ['page'] = 1;
				}
	        var currentpage = urlVars['page'];
            if(rowsCount < 1){
			  if(((currentpage)-1) == 0){
			   window.location.replace("http://"+hostName+getUrl);
			  }else{
				window.location.replace("http://"+hostName+getUrl+"?page="+((currentpage)-1));
			  }
		 }	
	   }
    });
});
// Read a page's GET URL variables and return them as an associative array.
function getUrlVars(){
    var vars = [], hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for(var i = 0; i < hashes.length; i++)
    {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
    }
    return vars;
}
});