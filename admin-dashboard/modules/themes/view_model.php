<!DOCTYPE html> 
<html lang="en"> 
  <head> 
    <meta charset="utf-8"> 
    <meta name="viewport" content="width=device-width, initial-scale=1.0"> 
    <meta name="description" content=""> 
    <meta name="author" content="Mosaddek"> 
    <meta name="keyword" content="FlatLab, Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina"> 
    <!-- Bootstrap core CSS --> 
    <link href="../../css/bootstrap.min.css" rel="stylesheet"> 
    <link href="../../css/bootstrap-reset.css" rel="stylesheet"> 
    <!--external css--> 
    <link rel="stylesheet" type="text/css" href="../../assets/gritter/css/jquery.gritter.css" /> 
    <link href="../../assets/font-awesome/css/font-awesome.css" rel="stylesheet" /> 
     <link href="../../assets/advanced-datatable/media/css/demo_page.css" rel="stylesheet" /> 
    <link href="../../assets/advanced-datatable/media/css/demo_table.css" rel="stylesheet" /> 
    <link rel="stylesheet" type="text/css" href="../../js/fancybox/jquery.fancybox-1.3.4.css" media="screen" /> 
     <link href="../../css/style.css" rel="stylesheet"> 
    <link href="../../css/style-responsive.css" rel="stylesheet" /> 
   <?php  
    require_once("../../../classes/ThemeLayoutModel.php"); 
	require_once("../../../classes/ThemeLayoutModelPlugin.php"); 
	require_once("../../../classes/Plugins.php"); 
	$theme = $_GET['theme']; 
	$layout = $_GET["layout"]; 
	?> 
    <!-- Custom styles for this template --> 
     
 </head> 
 <body style="background:#fff"> 
  <section class="panel"> 
   <header class="panel-heading">Show Model </header> 
   <br /> 
    <a href='../plugin_controller/customize_layout_plugin.php?id=<?php echo $layout?>&theme=<?php echo $theme?>&type=post'    
       class="btn btn-default btn-info "> <i class="icon-filter"></i>&nbsp;&nbsp;ADD Post Model</a> 
       <a href='../plugin_controller/customize_layout_plugin.php?id=<?php echo $layout?>&theme=<?php echo $theme?>&type=page'   
       class="btn btn-default  btn-info"> <i class="icon-filter"></i>&nbsp;&nbsp;ADD Page Model</a> 
        <a href='../plugin_controller/customize_layout_plugin.php?id=<?php echo $layout?>&theme=<?php echo $theme?>&type=event' 
       class="btn btn-default  btn-info"> <i class="icon-filter"></i>&nbsp;&nbsp;ADD Event Model</a> 
       <a href='../plugin_controller/customize_layout_plugin.php?id=<?php echo $layout?>&theme=<?php echo $theme?>&type=product' 
       class="btn btn-default  btn-info"> <i class="icon-filter"></i>&nbsp;&nbsp;ADD Product Model</a> 
        <div class="panel-body"> 
    
    
<div class="panel-body"> 
 <div class="adv-table"> 
  <table  class="display table table-bordered table-striped" id="example2"> 
     
           <thead> 
               <tr> 
				  <th>#</th> 
				  <th><i class=""></i> Name</th> 
                   <th><i class=""></i> Type</th> 
				  <th><i class=""></i> Plugins</th> 
                  <th><i class=""></i> Action</th> 
                    
			 </tr> 
              </thead> 
              <tbody> 
                <?php  
				$models = ThemeLayoutModel::find_all(); 
				 
				 
				 $serialize = 1; 
				 foreach($models as $model){ 
					 //get model plugin 
				$model_plugins = ThemeLayoutModelPlugin::find_all_by_custom_filed('model_id', $model->id,'sorting','DESC'); 
				 echo "<tr> 
				  <td>{$serialize}</td> 
				  <td>{$model->name}</td> 
				   <td>{$model->type}</td> 
				  <td>";  
					foreach($model_plugins as $plugin){ 
						$plugin_data = Plugins::find_by_id($plugin->plugin_id); 
						echo " <strong>Plugin Name</strong> :{$plugin_data->name},<strong>Position</strong>:{$plugin->position} <br/>"; 
					} 
				  echo "</td> 
				 <td> 
				 <a href='../plugin_controller/update_customize_layout_plugin.php?id=$model->id' id='update_model{$model->id}'> Update Model </a> 
				  
				 </td> 
				 </tr>"; 
				  $serialize++;?> 
<script> 
	<!-- for update model--> 
$(document).ready(function() { 
	$("#update_model<?php echo $record->id?>").fancybox({ 
		'width'	      : '100%', 
		'height'         : '100%', 
		'autoScale'      : 'true', 
		'transitionIn'   : 'none', 
		'transitionOut'  : 'none', 
		 
		 
	}); 
}); 
</script> 
                   
				 <?php  }?> 
                   
            </tbody> 
      </table> 
     </div> 
     </div> 
      </div> 
     <!-- END JAVASCRIPTS --> 
      <script src="../../js/jquery.js"></script> 
    <script src="../../js/bootstrap.min.js"></script> 
    <script class="include" type="text/javascript" src="../../js/jquery.dcjqaccordion.2.7.js"></script> 
    <script src="../../js/jquery.scrollTo.min.js"></script> 
    <script src="../../js/jquery.nicescroll.js" type="text/javascript"></script> 
    <script src="../../js/respond.min.js" ></script> 
     <script type="text/javascript" src="../../assets/gritter/js/jquery.gritter.js"></script> 
    <script type="text/javascript" language="javascript" src="../../assets/advanced-datatable/media/js/jquery.dataTables.js"></script> 
    <script type="text/javascript" src="../../js/fancybox/jquery.fancybox-1.3.4.pack.js"></script> 
      <script type="text/javascript" src="../../js/fancybox/jquery.mousewheel-3.0.4.pack.js"></script> 
    <!--common script for all pages--> 
    <script src="../../js/common-scripts.js"></script> 
    <script src="../../js/gritter.js" type="text/javascript"></script> 
    <script type="text/javascript" charset="utf-8"> 
          $(document).ready(function() { 
              $('#example2').dataTable( { 
                  "aaSorting": [[ 4, "desc" ]] 
              } ); 
			   
          } ); 
      </script> 
      <script> 
        <!-- for add model--> 
$(document).ready(function() { 
	$(".btn-info").fancybox({ 
		'width'	      : '100%', 
		'height'         : '100%', 
		'autoScale'      : 'true', 
		'transitionIn'   : 'none', 
		'transitionOut'  : 'none', 
		 
		 
	}); 
       
       
      </script> 
    </section> 
 </body> 
</html> 
