<?php  
	require_once("../layout/initialize.php"); 
	if(isset($_GET['id']) && is_numeric($_GET['id'])){ 
		$record_id = $_GET["id"]; 
		$record_info = PollQuestionsOptions::find_by_id($record_id); 
		//check id access 
		if(empty($record_info->id)){ 
			redirect_to("view.php");} 
	}else{ 
		redirect_to("view.php"); 
	} 
		require_once("../layout/header.php"); 
?> 
  <script type="text/javascript" src="../../js-crud/poll_questions_options.js"></script>  
  <!--header end-->  
  <!--sidebar start--> 
  <?php require_once("../layout/navigation.php");?> 
  <!--sidebar end-->  
  <!--main content start--> 
  <section id="main-content"> 
    <section class="wrapper site-min-height"> 
       <h4>Poll Options  Module</h4>  
      <div class="row"> 
        <aside class="col-lg-7"> 
          <section> 
            <div class="panel"> 
              <div class="panel-heading"> Edit  Poll Option</div> 
              <div class="panel-body"> <strong> 
                <form class="form-horizontal tasi-form" role="form" id="form_crud" action="data_model/update.php"> 
                  <input type="hidden" id="process_type" value="update"> 
                  <input type="hidden" id="record" value="<?php  echo $record_id; ?>"> 
                  <input type="hidden" id="poll_id" value="<?php  echo $record_info->poll_id; ?>"> 
                   
                   <div class="form-group"> 
                    <label  class="col-lg-2 ">Option:</label> 
                    <div class="col-lg-8"> 
                    <input type="text" class="form-control" id="option" placeholder=" " autocomplete="off" value="<?php echo $record_info->poll_option?>"> 
                    </div> 
                  </div> 
                   
                  <div class="form-group"> 
                    <div class="col-lg-offset-2 col-lg-10"> 
                      <button type="submit" class="btn btn-info" id="submit">Save</button> 
                      <button type="button" class=" btn btn-info " onClick="window.location.href = 'full_info.php?id='+<?php echo $record_id?>" > <i class="icon-info-sign"></i> View Full Info </button> 
                      <div id="loading_data"></div>                    </div> 
                  </div> 
                </form> 
              </div> 
            </div> 
          </section> 
         </aside> 
          
      </div> 
       
      <!-- page end-->  
    </section> 
  </section> 
  <!--main content end-->  
  <!--footer start-->  
   
  <?php require_once("../layout/footer.php");?>