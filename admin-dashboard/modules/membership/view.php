<?php  
	require_once("../layout/initialize.php"); 
	$records = Customers::find_all("id","DESC"); 
    
	require_once("../layout/header.php"); 
?> <!--header end--> 
<script src="../../js-crud/member_ship_delete.js"></script>
      <!--sidebar start--> 
       <?php require_once("../layout/navigation.php");?> 
      <!--sidebar end--> 
      <!--main content start--> 
      <section id="main-content"> 
     <section class="wrapper site-min-height"> 
      <h4>Membership Inquiry Module</h4> 
              <section class="panel"> 
                  <header class="panel-heading"> 
                      View Membership Inquires
                  </header> 
                  <div class="panel-body"> 
                      <div class="adv-table editable-table "> 
                         
                          <div class="space15"></div> 
                          <table class="table table-striped table-hover table-bordered" id="editable-sample"> 
                          <thead> 
                            <tr> 
                               <th>#</th> 
                              <th><i class=""></i> User name</th> 
                              <th><i class=""></i> User email</th>
                              <th><i class=""></i> User comment</th> 
                              <th>Action</th> 
                            </tr> 
                          </thead> 
                          <tbody id="myTable"> 
                              <?php  
							      $serialize = 1;  
							     foreach($records as $record){ 
								 echo "<tr id='row_{$record->id}'> 
								  <td>{$serialize}</td> 
                                  <td>{$record->customer_name}</td> 
								  <td>{$record->customer_email}</td> 
                                  <td>{$record->customer_comment}</td> 
								   <td>"; 
								   $opened_module_page_fullinfo = 'membership/full_info'; 
									//full info
								if(!in_array($opened_module_page_fullinfo, $user_allowed_page_array)){ 
								echo " <a href='' class='btn btn-primary btn-xs tooltips' data-placement='top' data-toggle='tooltip'  
										  data-original-title='Full info' disabled>&nbsp;<i class='icon-info'></i>&nbsp;</a>";	 
							   }else{ 
								 
									echo " <a href='full_info.php?id={$record->id}' class='btn btn-primary btn-xs tooltips' data-placement='top' data-toggle='tooltip'  
										  data-original-title='Full info' >&nbsp;<i class='icon-info'></i>&nbsp;</a>"; 
							   }
								   $opened_module_delete = 'membership/delete'; 
									if(!in_array($opened_module_delete, $user_allowed_page_array)){ 
										echo " <a href= '' class='btn btn-primary btn-xs tooltips' data-placement='top' data-toggle='tooltip'  
										data-original-title='Delete' disabled ><i class='icon-remove'></i></a>"; 
									}else{ 
										if($user_profile->global_delete == 'all_records' ){ 
											echo" <a href='#my{$record->id}' data-toggle='modal' class='btn btn-primary btn-xs tooltips'  
											data-placement='top' data-original-title='Delete'  >  <i class='icon-remove'></i></a>"; 
										}else{ 
											echo" <a href= '#myModal'  class='btn btn-primary btn-xs tooltips'  
											data-placement='top' data-toggle='tooltip' data-original-title='Delete'  disabled >  <i class='icon-remove'></i></a>"; 
										} 
									} 
									 	echo "</td>		 
				  </tr> 
				  <div class='modal fade' id='my{$record->id}' tabindex='-1' role='dialog' aria-labelledby='myModalLabel' aria-hidden='true'> 
                                  <div class='modal-dialog'> 
                                      <div class='modal-content'> 
                                          <div class='modal-header'> 
                                              <button type='button' class='close' data-dismiss='modal' aria-hidden='true'>&times;</button> 
                                              <h4 class='modal-title'>Delete</h4> 
                                          </div> 
                                          <div class='modal-body'> 
                                           <p> Are you sure you want delete  $record->customer_name ?</p> 
                                          </div> 
                                         <div class='modal-footer'>
										    <input type='hidden' id='task_type' value='delete'> 
                                              <button class='btn btn-warning confirm_delete' id='{$record->id}' type='button'  data-dismiss='modal' /> Confirm</button> 
											   <button data-dismiss='modal' class='btn btn-default' type='button'>cancel</button> 
                                          </div> 
                                      </div> 
                                  </div> 
                              </div>"; 
			  	 		$serialize++; 
 		            }?> 
              </tbody> 
            </table> 
                      </div> 
                  </div> 
              </section> 
              <!-- page end--> 
          </section> 
      </section> 
      <!--main content end--> 
      <!--footer start--> 
     <?php require_once("../layout/footer.php");?> 
