<?php
require_once('../../classes/FrontSession.php');
require_once('../../classes/Functions.php');
require_once('../../classes/MysqlDatabase.php');
require_once('../../classes/CustomerInfo.php');
require_once('../../classes/ForgetPassword.php');
require_once('../../localization/'.$_POST["lang"].'/validation_label.php');

header('Content-Type: application/json');

$new_pwd = md5($_POST["new_pwd"]);
$required_fields = array('new_pwd'=>$vnew_pass, 'conf_pwd'=>$vcpass);  
$check_required_fields = check_required_fields($required_fields);  
if(count($check_required_fields) != 0){  
	$comma_separated = implode("<br>", $check_required_fields); 
	$data  = array("status"=>"not_work", "message"=>$comma_separated); 
	echo json_encode($data); 		
}elseif(strlen($_POST["new_pwd"]) < 6){
	$data = array("status"=>"not_work","message"=>$vpass_length);
	echo json_encode($data); 		
}elseif($_POST["new_pwd"] != $_POST["conf_pwd"]){
	$data = array("status"=>"not_work","message"=>$vpass_missmatch);
	echo json_encode($data); 	
}else{
	//get user is by code 
	$code =  $_POST["code"];
	$get_info_by_code = ForgetPassword::find_by_custom_filed('code',$code);
	if($get_info_by_code){
		$user_info = CustomerInfo::find_by_id($get_info_by_code->user_id);
		$update = new CustomerInfo();
		$update->id = $user_info->id; 
		$update->first_name =$user_info->first_name; 
		$update->last_name = $user_info->last_name; 
		$update->email = $user_info->email; 
		$update->password = $new_pwd; 
		$update->mobile = $user_info->mobile; 
		$update->registration_date = $user_info->registration_date; 
		$update->birthday = $user_info->birthday; 
		$update->account_status = $user_info->account_status; 
		$edit = $update->update(); 
		if($edit){
			$find_code = ForgetPassword::find_by_custom_filed('code',$code);
			$find_code->delete();
			$data = array("status"=>"work","message"=>$vupdate_complete); 
			echo json_encode($data); 	
		}else{
			$data = array("status"=>"not_work","message"=>$verror_from_system);
			echo json_encode($data);
		}
	}
	
}
//close connection
if(isset($database)){
	$database->close_connection();
}
?>