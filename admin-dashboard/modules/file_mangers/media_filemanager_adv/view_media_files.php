<!DOCTYPE html> 
<html lang="en"> 
  <head> 
    <meta charset="utf-8"> 
    <meta name="viewport" content="width=device-width, initial-scale=1.0"> 
    <meta name="description" content=""> 
    <meta name="author" content="Mosaddek"> 
    <meta name="keyword" content="FlatLab, Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina"> 
    <!-- Bootstrap core CSS --> 
    <link href="../../../css/bootstrap.min.css" rel="stylesheet"> 
    <link href="../../../css/bootstrap-reset.css" rel="stylesheet"> 
    <!--external css--> 
    <link rel="stylesheet" type="text/css" href="../../../assets/gritter/css/jquery.gritter.css" /> 
    <link href="../../../assets/font-awesome/css/font-awesome.css" rel="stylesheet" /> 
     <link href="../../../assets/advanced-datatable/media/css/demo_page.css" rel="stylesheet" /> 
    <link href="../../../assets/advanced-datatable/media/css/demo_table.css" rel="stylesheet" /> 
    <link rel="stylesheet" type="text/css" href="../../../../js/fancybox/jquery.fancybox-1.3.4.css" media="screen" /> 
     <link href="../../../css/style.css" rel="stylesheet"> 
    <link href="../../../css/style-responsive.css" rel="stylesheet" /> 
   <?php  
   $directory_title= $_GET['title']; 
	$path = "../../../../media-library/"; 
	$docs = array(); 
	$path = "../../../../media-library/"; 
	//$records = array_diff(scandir($path.$directory_title), array('..', '.'));  
	foreach (glob($path.$directory_title."/*") as $path) { // lists all files in folder called "test" 
//foreach (glob("*.php") as $path) { // lists all files with .php extension in current folder 
    $docs[$path] = filectime($path); 
}  
	 
arsort($docs); 
	 
     
    
	?> 
  <script> 
function moveImage(val) { 
	 
	 
	parent.document.getElementById('imageVal').value=val; 
	parent.document.getElementById('imageShow').style.display="inline"; 
	parent.document.getElementById('imageSrc').src=val; 
	parent.jQuery.fancybox.close(); 
	//alert(val); 
	 
	} 
</script> 
    <!-- Custom styles for this template --> 
    <style> 
	  
	.panel-body ul li{ 
		padding-right:25px; 
		display:inline-block;	 
		 
	} 
	 
	.panel-body ul li a img{ 
		 
		padding:5px; 
		-webkit-transition:all .3s linear; 
		transition:all .3s linear; 
		} 
	 
	.panel-body ul li:hover a img { 
		border:1px solid #87b4df; 
		 
		 
		} 
	 
	</style> 
 </head> 
 <body style="background:#fff"> 
  <section class="panel"> 
   <header class="panel-heading"> View <?php echo $directory_title?> Files </header> 
   <br> 
  <a  id="upload" href="upload.php?title=<?php echo $directory_title;?>" class="btn   btn-danger"> <i class=" icon-upload-alt"></i>&nbsp; Upload New Image</a> 
    
    
<div class="panel-body"> 
<ul> 
<?php  
$ser = 1; 
foreach($docs as $record => $timestamp){ 
	$name = basename($record); 
	if($name != 'Thumbs.db'){ 
     if($name !='black_whit'){ 
	?> 
<li  ><a  href="<?php $directory_title."/".$record?>"> 
<img src='../../../../media-library/<?php echo  $directory_title?>/<?php echo  $name?>' onClick="moveImage(this.src)"  style="width:80px;height:80px"/></a> 
</li> 
<?php  
$ser++; 
	} 
 } 
}?> 
 </ul> 
 <button type="button" class="btn btn-info" onclick="history.back()">Back</button>      </div> 
     <!-- END JAVASCRIPTS --> 
      <script src="../../../js/jquery.js"></script> 
    <script src="../../../js/bootstrap.min.js"></script> 
    <script class="include" type="text/javascript" src="../../../js/jquery.dcjqaccordion.2.7.js"></script> 
    <script src="../../../js/jquery.scrollTo.min.js"></script> 
    <script src="../../../js/jquery.nicescroll.js" type="text/javascript"></script> 
    <script src="../../../js/respond.min.js" ></script> 
     <script type="text/javascript" src="../../../assets/gritter/js/jquery.gritter.js"></script> 
    <script type="text/javascript" language="javascript" src="../../../assets/advanced-datatable/media/js/jquery.dataTables.js"></script> 
    <script type="text/javascript" src="../../../js/fancybox/jquery.fancybox-1.3.4.pack.js"></script> 
      <script type="text/javascript" src="../../../js/fancybox/jquery.mousewheel-3.0.4.pack.js"></script> 
     <script> 
	<!-- for update model--> 
$(document).ready(function() { 
	$("#upload").fancybox({ 
		        'width'		: '80%', 
				'height'	   : '80%', 
				'autoScale'	: true, 
				'transitionIn' : 'none', 
				'transitionOut': 'none', 
				'type'		 : 'iframe' 
		 
		 
	}); 
}); 
</script> 
    <!--common script for all pages--> 
    <script src="../../../js/common-scripts.js"></script> 
    <script src="../../../js/gritter.js" type="text/javascript"></script> 
    
    </section> 
 </body> 
</html> 
