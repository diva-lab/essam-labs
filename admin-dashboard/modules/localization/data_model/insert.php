<?php 
require_once('../../../../classes/Session.php'); 
require_once('../../../../classes/Functions.php'); 
require_once('../../../../classes/MysqlDatabase.php'); 
require_once('../../../../classes/Localization.php'); 
//check log in  
if($session->is_logged() == false){ 
	redirect_to("../../index.php"); 
} 
header('Content-Type: application/json'); 
if(!empty($_POST["task"]) && $_POST["task"] == "insert"){ 
	// check if language is exist 
	$required_fields = array('name'=>"- Insert name",'label'=>'- Insert label' ); 
	$check_required_fields = check_required_fields($required_fields); 
    if(count($check_required_fields) == 0){ 
		  $path="../../../../localization/"; 
		  $label=$_POST["label"]; 
		  $path_file=$path.$label."/"; 
		  $check_title_availability = Localization::find_by_custom_filed('label',$_POST["label"]); 
		  if($check_title_availability=="" && !file_exists($path_file)){ 
				 if(strpbrk($label, "\\/?%*:|\"<>") === false){ 
					  $add = new Localization(); 
					  $add->name = $_POST["name"]; 
					  $add->label = $_POST["label"]; 
					  $add->sorting = $_POST['sorting'];
					  $add->status = $_POST['shadow'];
					  $insert = $add->insert(); 
					  // create languge directory and files 
					  mkdir( $path.$add->label, 0777); 
					  $file_maessges=fopen($path_file."messages.php","x+"); 
					  $file_labels=fopen($path_file."labels.php","x+"); 
					  if($insert){ 
						  $data  = array("status"=>"work"); 
						  echo json_encode($data); 
					  }else{ 
						  $data  = array("status"=>"error"); 
						  echo json_encode($data); 
					  } 
					}else{ 
					  $data  = array("status"=>"wrong"); 
					  echo json_encode($data); 
				   
					}	 
				 }else{ 
					 $data  = array("status"=>"exist"); 
					 echo json_encode($data); 
					 } 
			}else{ 
		//validation error 
		$comma_separated = implode("<br>", $check_required_fields); 
		$data  = array("status"=>"valid_error", "fileds"=>$comma_separated); 
		echo json_encode($data); 
	}			  
			   
} 
//close connection 
if(isset($database)){ 
	$database->close_connection(); 
} 
?>