<?php 
	require_once("../layout/initialize.php"); 
	require_once("../layout/header.php"); 
?> 
<script type="text/javascript" src="../../js-crud/menu_group.js"></script> 
<!--header end--> 
<!--sidebar start--> 
<?php require_once("../layout/navigation.php");?> 
<!--sidebar end-->  
<!--main content start--> 
<section id="main-content"> 
  <section class="wrapper site-min-height"> 
    <h4> Structure Menu Group  Module</h4> 
    <!-- page start--> 
    <div class="row"> 
      <aside class="col-lg-8"> 
        <section> 
          <div class="panel"> 
            <div class="panel-heading"> Add Menu Group</div> 
            <div class="panel-body"> 
              <form class="form-horizontal tasi-form" role="form" id="form_crud" action="data_model/insert.php"> 
                <input type="hidden" id="process_type" value="insert"> 
                <div class="form-group"> 
                  <label  class="col-lg-2 ">Title:</label> 
                  <div class="col-lg-8"> 
                    <input type="text" class="form-control" id="title" placeholder=" " autocomplete="off" onchange="add_char('title','alias');"> 
                  </div> 
                </div> 
                <div class="form-group"> 
                  <label  class="col-lg-2">Alias:</label> 
                  <div class="col-lg-8"> 
                    <input type="text" class="form-control" id="alias" placeholder=" " autocomplete="off"> 
                  </div> 
                </div> 
                <div class="form-group"> 
                  <label  class="col-lg-2 ">Description:</label> 
                  <div class="col-lg-8"> 
                    <textarea class="wysihtml5 form-control" id="description" placeholder=" " rows="10"></textarea> 
                  </div> 
                </div> 
                <div class="form-group"> 
                  <div class="col-lg-offset-2 col-lg-10"> 
                    <button type="submit" class="btn btn-info" id="submit">Save</button> 
                    <button type="reset" class="btn btn-default">Cancel</button> 
                    <div id="loading_data"></div> 
                  </div> 
                </div> 
              </form> 
            </div> 
          </div> 
        </section> 
      </aside> 
      <div class="col-lg-4"> 
        <section class="panel panel-primary"> 
          <header class="panel-heading">Included Images :</header> 
          <div class="panel-body"> 
            <form class="form-horizontal tasi-form" role="form" id="form_image"> 
              
                <div class="form-group"> 
                  <label  class="col-lg-3 ">Image Cover:</label> 
                  <div class="col-lg-7"> <a href="../media_filemanager_menu/view_media_directories.php?type=group" id="image_cover">Select Image</a> <br /> 
                    <br /> 
                    <input type="hidden" class="form-control" id="imageVal" placeholder=" " autocomplete="off"> 
                    <div style="display:none" id="imageShow"> <img src="" id="imageSrc" style="width:80px; height:80px;"></div> 
                  </div> 
                </div> 
              
            </form> 
          </div> 
        </section> 
      </div> 
    </div> 
    <!-- page end-->  
  </section> 
</section> 
<!--main content end-->  
<!--footer start--> 
<?php require_once("../layout/footer.php");?>