$(document).ready(function (){
//for insert and update page when button clicked get data and send to php 
$( "#save" ).click(function() {
	//get value of selected checkboxes
    event.preventDefault();
    var searchIDs = $("input:checkbox:checked").map(function(){
        return this.value;
    }).toArray();
	
	var data = {
		task: 'update',
		pages_id : searchIDs,
		profile_id: $('#profile_id').val() 	
	}

	$.ajax({
		type: 'POST',
		url: 'data_model/update.php',
		data: data,
		dataType:"JSON",
		beforeSend: function(){
		//show laoding 
		$('#loading_data').html('<img src="../../img/loading.gif"/>&nbsp Loading ....');
		},
		success: function(data){
			if(data.status == 'work'){
				 window.location.href = "../profiles/view.php";
			}else if(data.status == 'error_update_all'){
				$("#loading_data").html("Cant Updated all Pages");
			}else{
				$("#loading_data").html("Cant Updated Selected Pages");
			}
		}
	})
return false;
});  
});
 
 




