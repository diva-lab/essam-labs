
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="Mosaddek">
<!-- Bootstrap core CSS -->
<link href="../../../css/bootstrap.min.css" rel="stylesheet">
<link href="../../../css/bootstrap-reset.css" rel="stylesheet">
<!--external css-->
<link rel="stylesheet" type="text/css" href="../../../assets/gritter/css/jquery.gritter.css" />
<link href="../../../assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
<link href="../../../assets/advanced-datatable/media/css/demo_page.css" rel="stylesheet" />
<link href="../../../assets/advanced-datatable/media/css/demo_table.css" rel="stylesheet" />
<link rel="stylesheet" type="text/css" href="../../../../js/fancybox/jquery.fancybox-1.3.4.css" media="screen" />
<link href="../../../css/style.css" rel="stylesheet">
<link href="../../../css/style-responsive.css" rel="stylesheet" />

</head>
<body style="background:#fff">
<div class="row"> 
        <aside class="profile-info col-lg-8"> 
          <section> 
            <div class="panel"> 
              <div class="panel-heading"> Add Media Directory</div> 
              <div class="panel-body"> 
                <form class="form-horizontal tasi-form" role="form" id="form_crud" action="data_model/insert.php"> 
                <input type="hidden" id="process_type" value="insert"> 
                  
                  <div class="form-group"> 
                    <label  class="col-lg-2">Title:</label> 
                    <div class="col-lg-"> 
                      <input type="text" class="form-control" id="title" placeholder=" " autocomplete="off"> 
                    </div> 
                  </div> 
                 
                  <div class="form-group"> 
                    <div class="col-lg-offset-2 col-lg-10"> 
                      <button type="submit" class="btn btn-info" id="submit">Save</button> 
                     <a href="view_media_directories.php" class="btn  btn-default " >Back</a>  
                      <div id="loading_data"></div> 
                    </div> 
                  </div> 
                </form> 
              </div> 
            </div> 
          </section> 
        </aside> 
      </div>

  
  
  <!-- END JAVASCRIPTS --> 
  <script src="../../../js/jquery.js"></script> 
  <script type="text/javascript" src="../../../js-crud/add_directory.js"></script>
  <script src="../../../js/bootstrap.min.js"></script> 
  <script class="include" type="text/javascript" src="../../../js/jquery.dcjqaccordion.2.7.js"></script> 
  <script src="../../../js/jquery.scrollTo.min.js"></script> 
  <script src="../../../js/jquery.nicescroll.js" type="text/javascript"></script> 
  <script src="../../../js/respond.min.js" ></script> 
  <script type="text/javascript" src="../../../assets/gritter/js/jquery.gritter.js"></script> 
  <script type="text/javascript" language="javascript" src="../../../assets/advanced-datatable/media/js/jquery.dataTables.js"></script> 
  <script type="text/javascript" src="../../../js/fancybox/jquery.fancybox-1.3.4.pack.js"></script> 
  <script type="text/javascript" src="../../../js/fancybox/jquery.mousewheel-3.0.4.pack.js"></script> 
 <script> 
	<!-- for update model--> 
$(document).ready(function() { 
	$("#back").fancybox({ 
		        'width'		: '80%', 
				'height'	   : '80%', 
				'autoScale'	: true, 
				'transitionIn' : 'none', 
				'transitionOut': 'none', 
				'type'		 : 'iframe' 
		 
		 
	}); 
}); 
</script> 
  <!--common script for all pages--> 
  <script src="../../../js/common-scripts.js"></script> 
  <script src="../../../js/gritter.js" type="text/javascript"></script> 

</body>
</html>
