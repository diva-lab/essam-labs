<?php 
require_once 'CRUD.php'; 
class Profile extends CRUD{ 
   //calss attributes 
   public $id; 
   public $title; 
   public $inserted_by; 
   public $inserted_date; 
   public $last_update; 
   public $global_edit; 
   public $global_delete; 
   public $developer_mode; 
   public $profile_block; 
   public $post_publishing; 
   public $page_publishing; 
   public $event_publishing; 
    
   //relation table attribute 
   public $user_name; 
   public $first_name; 
   public $last_name; 
    
    //push attributes for relational tables 
   public function enable_relation(){ 
		array_push(static::$primary_fields , 'user_name', 'first_name', 'last_name'); 
   }    
   //define table name and fields 
	protected static $table_name = 'profile'; 
	protected static $primary_fields = array('id', 'title','inserted_by','inserted_date', 'last_update','global_edit','global_delete','developer_mode', 
	'profile_block','post_publishing','page_publishing','event_publishing'); 
	 
	public function profile_data($sort_filed = null, $order_by = null, $id = null){ 
		$sql = "SELECT profile.id AS id, profile.title AS title, profile.inserted_date AS inserted_date, profile.last_update AS last_update, 
				users.user_name AS user_name, users.first_name AS first_name, users.last_name AS last_name  ,profile.profile_block AS profile_block, 
				profile.developer_mode AS developer_mode, profile.global_edit AS global_edit, 
				profile.global_delete AS global_delete,profile.inserted_by AS inserted_by, 
				profile.post_publishing AS post_publishing,profile.page_publishing AS page_publishing,profile.event_publishing AS event_publishing 			 
				FROM profile, users  
				WHERE users.id = profile.inserted_by"; 
		if(!empty($id)){ 
			$sql .= "   AND  profile.id  = $id "; 
			$result_array = static::find_by_sql($sql); 
			return !empty($result_array)? array_shift($result_array) : false; 
		}else{ 
			if(!empty($sort_filed) && !empty($order_by)){ 
				$sql .= " ORDER BY ".$sort_filed." ".$order_by;  
			 } 
			return self::find_by_sql($sql);  		 
		} 
	} 
	 
} 
?>