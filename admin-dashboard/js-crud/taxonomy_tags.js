$(document).ready(function (){

$("#form_crud").submit(function (){
	 //disable button
	 $('#submit').attr("disabled", "true");
	 var type = 'POST';
	 var url = $('#form_crud').attr('action');
	 //send json data
	 var data = {	
			task: $('#process_type').val(),
			record: $('#record').val(),
			name: $('#name').val(), 
			content_id:$('#content_id').val(),
			shadow: $('input[name=shadow]:checked').val(), 
		 	description: tinymce.get('description').getContent(),
			alias:$('#alias').val()
		};
	$.ajax({
	 type: type,
	 url: url,
	 data: data,
	 dataType:"JSON",
	 beforeSend: function(){
		//show laoding 
		$('#loading_data').html('<img src="../../img/loading.gif"/>&nbsp Loading ....');
	 },
	 success: function(data){
 		if(data.status == 'work'){
			 if($('#process_type').val() == 'update'){
				 window.location.href = "full_info.php?id="+$('#record').val();
				//alert("2220");
			}else{
				$('#form_crud')[0].reset();
				$('#submit').removeAttr("disabled");
				$('#loading_data').html('Successful Insert Process');				 
			 }
		 }
		  else if(data.status == 'exist')
		 {
			 $('#form_crud')[0].reset();
			 $('#submit').removeAttr("disabled");
			 $('#loading_data').html(' The Taxonomy Tage Is Already Exist');
			 
		}else if(data.status == 'valid_error'){
			 $('#loading_data').html(data.fileds);
			 $('#loading_data').css('color', 'red');
			 $('#submit').removeAttr('disabled');	 
		}else{
			$('#loading_data').html('Error In Process');
		 }
	 }
})
 //do not go to any where
  return false;     
 });
 $('#add').click(function(){
	 window.location.href = "insert.php";
	 
}); 
 
    
})