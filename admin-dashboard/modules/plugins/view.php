<?php 
	require_once("../layout/initialize.php"); 
	$define_class = new Plugins(); 
	$records = $define_class->plugins_data("uploaded_date","DESC"); 
	require_once("../layout/header.php"); 
?> 
<!--header end--> 
<!--sidebar start--> 
<?php require_once("../layout/navigation.php");?> 
<!--sidebar end-->  
<!--main content start--> 
<section id="main-content"> 
  <section class="wrapper site-min-height"> 
    <h4>Plugin Module</h4> 
    <!-- page start--> 
    <section class="panel"> 
      <header class="panel-heading"> View Plguins </header> 
      <br> 
      <button type="button" class="btn btn-danger" style="margin-left:15px" onClick="window.location.href = 'insert.php'"> 
      <li class="icon-plus-sign"></li> 
      Upload New Plugin </button> 
      <br> 
      <div class="panel-body"> 
        <div class="adv-table editable-table ">  
          <!--<div class="clearfix"> 
                              <div class="btn-group"> 
                                  <button id="editable-sample_new" class="btn green"> 
                                      Add New <i class="icon-plus"></i> 
                                  </button> 
                              </div> 
                              <div class="btn-group pull-right"> 
                                  <button class="btn dropdown-toggle" data-toggle="dropdown">Tools <i class="icon-angle-down"></i> 
                                  </button> 
                                  <ul class="dropdown-menu pull-right"> 
                                      <li><a href="#">Print</a></li> 
                                      <li><a href="#">Save as PDF</a></li> 
                                      <li><a href="#">Export to Excel</a></li> 
                                  </ul> 
                              </div> 
                          </div>--> 
          <div class="space15"></div> 
          <table class="table table-striped table-hover table-bordered" id="editable-sample"> 
            <thead> 
              <tr> 
                <th>#</th> 
                <th>Name</th> 
                <th>Version</th> 
                <th>Author</th> 
                <th>Uploaded</th> 
                <th>Description</th> 
              </tr> 
            </thead> 
            <tbody> 
              <?php   
				  $serialize = 1; 
				  foreach($records as $record){ 
					  echo "<tr> 
					  <td>{$serialize}</td> 
					  <td>{$record->name}</td> 
					  <td>{$record->version}</td> 
					  <td>{$record->author}</td> 
					  <td>{$record->uploaded_by} 
					  <br>{$record->uploaded_date}</td> 
					  <td>{$record->description}</td> 
					</tr>"; 
					$serialize++; 
				  } 
				  ?> 
            </tbody> 
          </table> 
        </div> 
      </div> 
    </section> 
    <!-- page end-->  
  </section> 
</section> 
<!--main content end-->  
<!--footer start--> 
<?php require_once("../layout/footer.php");?> 
