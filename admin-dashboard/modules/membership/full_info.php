<?php  
	require_once("../layout/initialize.php"); 
	if(isset($_GET['id']) && is_numeric($_GET['id'])){ 
		$record_id = $_GET['id']; 
		$define_class = new Customers(); 
		 
		$record_info = $define_class->find_by_id($record_id); 
		//check id access 
		if(empty($record_info->id)){ 
			redirect_to("view.php");
		}
	}else{ 
		redirect_to("view.php");	 
	} 
	require_once("../layout/header.php"); 
?> 
<!--header end--> 
<!--sidebar start--> 
<?php require_once("../layout/navigation.php");?> 
<!--sidebar end-->  
<!--main content start--> 
<section id="main-content"> 
  <section class="wrapper site-min-height"> 
    <h4>Social Activity Module</h4> 
    <!-- page start--> 
    <div class="row"> 
      <aside class="col-lg-8"> 
        <section> 
          <div class="panel"> 
            <div class="panel-heading"> Contact us Info</div> 
            <div class="panel-body"> 
              <div class="panel-body"> 
                <form class="form-horizontal tasi-form" role="form" > 
                    <div class="form-group"> 
                          <label  class="col-lg-4"> User name</label> 
                          <div class="col-lg-8"> <?php echo $record_info->customer_name;?> </div> 
                        </div> 
                        <div class="form-group"> 
                          <label  class="col-lg-4">User email</label> 
                          <div class="col-lg-8"> <?php echo $record_info->customer_email;?> </div> 
                        </div> 
                        <div class="form-group"> 
                          <label  class="col-lg-4">User position</label> 
                          <div class="col-lg-8"> <?php echo $record_info->customer_position;?> </div> 
                        </div> 
                        <div class="form-group"> 
                        <label  class="col-lg-4">User company</label> 
                        <div class="col-lg-8"> <?php echo $record_info->customer_company ;?></div> 
                      </div>  

                      <div class="form-group"> 
                        <label  class="col-lg-4">User mobile</label> 
                        <div class="col-lg-8"> <?php echo $record_info->customer_mobile ;?></div> 
                      </div> 
                      <div class="form-group"> 
                        <label  class="col-lg-4">User fax</label> 
                        <div class="col-lg-8"> <?php echo $record_info->customer_fax ;?></div> 
                      </div> 
                      <div class="form-group"> 
                        <label  class="col-lg-4">User marital status</label> 
                        <div class="col-lg-8"> <?php echo $record_info->customer_marital_status ;?></div> 
                      </div> 
                      <div class="form-group"> 
                        <label  class="col-lg-4">User credit type</label> 
                        <div class="col-lg-8"> <?php echo $record_info->customer_credit_type ;?></div> 
                      </div> 
                      <div class="form-group"> 
                        <label  class="col-lg-4">User graduation year</label> 
                        <div class="col-lg-8"> <?php echo $record_info->customer_graduation_year ;?></div> 
                      </div> 
                      <div class="form-group"> 
                        <label  class="col-lg-4">User education year</label> 
                        <div class="col-lg-8"> <?php echo $record_info->customer_education_year ;?></div> 
                      </div> 
                      <div class="form-group"> 
                        <label  class="col-lg-4">User nationality</label> 
                        <div class="col-lg-8"> <?php echo $record_info->customer_nationality ;?></div> 
                      </div> 
                      <div class="form-group"> 
                        <label  class="col-lg-4">User residence</label> 
                        <div class="col-lg-8"> <?php echo $record_info->customer_residence ;?></div> 
                      </div> 
                       <div class="form-group"> 
                        <label  class="col-lg-4">User monthly income</label> 
                        <div class="col-lg-8"> <?php echo $record_info->customer_monthly_income ;?></div> 
                      </div>
                       <div class="form-group"> 
                        <label  class="col-lg-4">User traveled country</label> 
                        <div class="col-lg-8"> <?php echo $record_info->cutomer_traveled_country ;?></div> 
                      </div>
                       <div class="form-group"> 
                        <label  class="col-lg-4">User traveled hotel</label> 
                        <div class="col-lg-8"> <?php echo $record_info->cutomer_traveled_hotel ;?></div> 
                      </div>
                       <div class="form-group"> 
                        <label  class="col-lg-4">User traveled year</label> 
                        <div class="col-lg-8"> <?php echo $record_info->customer_traveled_year ;?></div> 
                      </div>
                      <div class="form-group"> 
                        <label  class="col-lg-4">User comment</label> 
                        <div class="col-lg-8"> <?php echo $record_info->customer_comment ;?></div> 
                      </div>

                
                  
                  <div class="form-group"> 
                  <div class="col-lg-offset-2 col-lg-10"> 
                    <button type="button" class="btn btn-info"   
                        onClick="window.location.href = 'view.php'" > <li class="icon-info"></li> Back to view </button> 
                     
                        </div> 
                  </div> 
                </form> 
              </div> 
            </div> 
          </div> 
        </section> 
      </aside> 
    </div> 
    <!-- page end-->  
  </section> 
</section> 
<!--main content end-->  
<!--footer start--> 
<?php require_once("../layout/footer.php");?> 
