<?php 

require_once 'CRUD.php';

	 

class CustomerInfo extends CRUD{ 

	//calss attributes 

	public $id; 

	public $user_name ; 

	public $first_name; 

	public $last_name; 

	public $password;

	public $email;

	public $gender; 

	public $city; 

	public $area;

	public $street;

	public $birth_date;

	public $registeration_date; 

	public $account_status; 

	public $activation_code;
	
    public $colleage_department;
	
	public $mobile;
	
	public $otherMobile;
	
	public $nationality;



	//push attributes for relational tables 

	public function enable_relation(){ 

		array_push(static::$primary_fields , 'customer_id' ); 

	}    

	//define table first_name and fields 

	protected static $table_name = 'customer_info'; 

	protected static $primary_fields = array('id','first_name','last_name','password','gender','city','area','street','email','birth_date','registeration_date','account_status','activation_code','user_name', 'colleage_department' , 'mobile' , 'otherMobile' , 'nationality'); 

	

	//get by customer personal_email and pass 

	public static function find_by_email_pass($email, $pass){ 

	   global $database; 

	   $email = $database->escape_values($email); 

	   $password = $database->escape_values($pass); 

	   $sql = "SELECT * FROM ".self::$table_name." WHERE email = '{$email}' AND password = '{$pass}' limit 1"; 

	   $result_set = self::find_by_sql($sql); 

	   return !empty($result_set)? array_shift($result_set) : false; 

	} 

	 //get customer full name

	public  function full_name(){ 

		if(isset($this->first_name) && isset($this->last_name)){ 

			return 	$this->first_name." ".$this->last_name; 

		}else{ 

			return false;	 

		} 

	} 

  //get customer data 	 

	public function customer_data($id = null, $sort_filed = null, $order_by = null,$black_list = null,$city = null, $area = null,$registration_date_from = null,$registration_date_to = null){ 

			$sql = "SELECT  Distinct  customer_info.id AS id, customer_info.first_name AS first_name,customer_info.last_name AS last_name, customer_info.email AS email , customer_info.password AS password, customer_info.registeration_date AS registeration_date, customer_info.city AS city , customer_info.area AS area , customer_info.street AS street , customer_info.account_status AS account_status,customer_info.birth_date AS birth_date , customer_info.activation_code AS activation_code , customer_info.user_name AS user_name  , customer_info.colleage_department AS colleage_department , customer_info.gender AS gender , 
			    customer_info.mobile AS mobile , customer_info.otherMobile AS otherMobile , customer_info.nationality AS nationality

				FROM customer_info
				WHERE 1 "; 

		if(!empty($id)){		 

			 $sql .= " AND  customer_info.id = $id "; 

			 $result_array = static::find_by_sql($sql); 

			 return !empty($result_array)? array_shift($result_array) : false; 

		}else{ 

	     	

			return self::find_by_sql($sql);   

		}				 

	} 

	 

	

	 

}?> 