<?php  
require_once('../../../classes/Session.php'); 
require_once('../../../classes/Users.php'); 
require_once('../../../classes/ProfileModulesAccess.php'); 
require_once('../../../classes/ProfilePagesAccess.php'); 
//get user profile by user session 
$user_id = $session->user_id; 
$user = users::find_by_id($user_id); 
$user_profile_id = $user->user_profile; 
//get user accessible modules 
$define_user_modules = new ProfileModulesAccess(); 
$define_user_modules->enable_relation(); 
$user_modules = $define_user_modules->user_profile_modules_data($user_profile_id);  
?> 
<aside> 
  <div id="sidebar"  class="nav-collapse ">  
    <!-- sidebar menu start--> 
    <ul class="sidebar-menu" id="nav-accordion"> 
      <li> <a class="active" href="../../modules/home/index.php"> <i class="icon-dashboard"></i> <span>Dashboard</span> </a> </li> 
      <?php  
	  		foreach($user_modules as $module): 
			//get pages  
			$define_user_pages = new ProfilePagesAccess(); 
			$define_user_pages->enable_relation(); 
			$user_pages = $define_user_pages->user_profile_pages_access($user_profile_id, $module->module_id);  
			//get open file directory (get info from url) 
			$currentFile = $_SERVER["PHP_SELF"]; 
			$parts = Explode('/', $currentFile); 
			$directory_name = $parts[count($parts) - 2];  //give directory 
			// 
			$module_sources = explode(",", $module->module_source); 
		?> 
      <li class="sub-menu"> <a class= "<?php if(in_array($directory_name, $module_sources)){ echo "active";}?>" href="javascript:;" >  
      <i class="<?php echo $module->module_icon?>"></i> <span><?php echo ucwords($module->module_title)?></span> </a> 
        <ul class="sub"> 
        <?php foreach($user_pages as $page):?> 
          <li><a  href="../../modules/<?php echo $page->source?>.php"><?php echo ucwords($page->page_title)?></a></li> 
          <?php endforeach;?> 
        </ul> 
      </li> 
      <?php endforeach;?> 
    </ul> 
    <!-- sidebar menu end-->  
  </div> 
</aside> 
