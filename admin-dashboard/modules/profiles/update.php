<?php  
	require_once("../layout/initialize.php"); 
	//get profile info 
	$profile_id = $_GET['id']; 
	$record_info = Profile::find_by_id($profile_id); 
	if($user_profile->global_edit != 'all_records' && $record_info->inserted_by != $session->user_id ){ 
		  redirect_to('view.php');	 
	 } 
	//get all modules 
	$modules = CMSModules::find_all_by_custom_filed('sid', 0, 'sorting', 'ASC'); 
	require_once("../layout/header.php"); 
?> 
  <script type="text/javascript" src="../../js-crud/crud_profile.js"></script>  
  <!--header end-->  
  <!--sidebar start--> 
  <?php require_once("../layout/navigation.php"); ?> 
  <!--sidebar end-->  
  <!--main content start--> 
  <section id="main-content"> 
    <section class="wrapper site-min-height"> 
      <h4>Profile Module</h4>         
       <!-- page start--> 
      <div class="row"> 
        <aside class="col-lg-8"> 
          <section> 
            <div class="panel"> 
              <div class="panel-heading"> Edit Profile</div> 
              <div class="panel-body"> 
                <form class="form-horizontal tasi-form" role="form" id="form_crud" action="data_model/update.php"> 
                  <input type="hidden" id="process_type" value="update"> 
                  <input type="hidden" id="record" value="<?php echo $profile_id ?>"> 
                  <div class="form-group"> 
                    <label  class="col-lg-2">Title:</label> 
                    <div class="col-lg-8"> 
                      <input type="text" class="form-control" id="title" placeholder=" " value="<?php echo $record_info->title?>" autocomplete="off"/> 
                    </div> 
                  </div> 
                  <div class="form-group"> 
                    <label  class="col-lg-2">Modules:</label> 
                    <div class="col-lg-4"> 
                      <select multiple="multiple" class="multi-select" id="my_multi_select1" name="my_multi_select1[]"> 
                       <?php  
					   	foreach($modules as $module){  
						//check if this profile have access for this module 
							$check_module = ProfileModulesAccess::check_module_availabilty($profile_id, $module->id, 'yes'); 
							if($check_module){ 
								echo "<option value={$module->id} selected>{$module->title}</option>"; 
							}else{ 
								echo "<option value={$module->id} >{$module->title}</option>"; 
							} 
						} 
					   ?> 
                 	 </select> 
                    </div> 
                  </div> 
                  <div class="form-group"> 
                    <div class="col-lg-offset-2 col-lg-10"> 
                      <button type="submit" class="btn btn-info" id="submit">Save</button> 
                      <button type="button" class=" btn btn-info "   
                    onClick="window.location.href = 'full_info.php?id='+<?php echo $profile_id?>" > <i class="icon-info-sign"></i> View Full Info </button> 
                       
                      <div id="loading_data"></div> 
                    </div> 
                  </div> 
                </form> 
              </div> 
            </div> 
          </section> 
        </aside> 
        <div class="col-lg-4"> 
          <section class="panel panel-primary"> 
           <header class="panel-heading tab-bg-dark-navy-blue"> 
      <ul class="nav nav-tabs"> 
      <li  class=" center-block active" style="width:175px"> 
          <a data-toggle="tab" href="#op" class="text-center"> 
              <i class=" icon-check"></i> 
             <strong> Rules Options</strong> 
          </a> 
      </li> 
      <li   style="width:175px"> 
          <a data-toggle="tab" href="#op1" class="text-center"> 
              <i class=" icon-check "></i> 
             <strong>  Module Options  </strong> 
          </a> 
      </li> 
       
  </ul> 
  </header> 
            <div class="panel-body"> 
                <form class="form-horizontal tasi-form" role="form" id="form_option"> 
                 <div class="tab-content"> 
                <div id="op" class="tab-pane active "> 
                <br/> 
                <div class="form-group"> 
                  <label class="col-lg-4 ">Global Edit:</label> 
                  <div class="col-lg-8"> 
                    <label class="checkbox-inline"> 
                      <input type="radio" name="global_edit" class="radio" value="all_records" 
                       <?php if($record_info->global_edit == 'all_records')  echo "checked" ?>> 
                      All records</label> 
                    <label class="checkbox-inline"> 
                      <input type="radio" name="global_edit" class="radio" value="awn_record"   
					  <?php if($record_info->global_edit == 'awn_record')  echo "checked" ?>> 
                      Own record</label> 
                  </div> 
                </div> 
                <div class="form-group"> 
                  <label class="col-lg-4 ">Global Delete:</label> 
                  <div class="col-lg-8"> 
                    <label class="checkbox-inline"> 
                      <input type="radio" name="global_delete" class="radio" value="all_records" 
                       <?php if($record_info->global_delete == 'all_records')  echo "checked" ?>> 
                      All records</label> 
                    <label class="checkbox-inline"> 
                      <input type="radio" name="global_delete" class="radio" value="awn_record"   
					  <?php if($record_info->global_delete == 'awn_record')  echo "checked" ?>> 
                      Own record</label> 
                  </div> 
                </div> 
                <div class="form-group"> 
                  <label class="col-lg-4 ">Developer Mode:</label> 
                  <div class="col-lg-8"> 
                    <label class="checkbox-inline"> 
                      <input type="radio" name="developer_mode" class="radio" value="yes"  
					  <?php if($record_info->developer_mode == 'yes')  echo "checked" ?>> 
                      yes</label> 
                    <label class="checkbox-inline"> 
                      <input type="radio" name="developer_mode" class="radio" value="no"  
					  <?php if($record_info->developer_mode == 'no')  echo "checked" ?>> 
                      No</label> 
                  </div> 
                </div> 
                <div class="form-group"> 
                  <label class="col-lg-4 ">Profile Block:</label> 
                  <div class="col-lg-8"> 
                    <label class="checkbox-inline"> 
                      <input type="radio" name="profile_turn_off" class="radio" value="yes"  
                       <?php if($record_info->profile_block == 'yes')  echo "checked" ?>> 
                      yes</label> 
                    <label class="checkbox-inline"> 
                      <input type="radio" name="profile_turn_off" class="radio" value="no"  
                       <?php if($record_info->profile_block == 'no')  echo "checked" ?>> 
                      No</label> 
                  </div> 
                </div> 
                </div> 
                <div id="op1" class="tab-pane "> 
                 <br /> 
                 <div class="form-group"> 
                  <label class="col-lg-4 ">Publish Post:</label> 
                  <div class="col-lg-8"> 
                    <label class="checkbox-inline"> 
                      <input type="radio" name="post_publishing" class="radio" value="yes"  
					  <?php if($record_info->post_publishing == 'yes')  echo "checked" ?>> 
                      yes</label> 
                    <label class="checkbox-inline"> 
                      <input type="radio" name="post_publishing" class="radio" value="no" 
					  <?php if($record_info->post_publishing == 'no')  echo "checked" ?> > 
                      No</label> 
                  </div> 
                </div> 
                <div class="form-group"> 
                  <label class="col-lg-4 ">Publish Page:</label> 
                  <div class="col-lg-8"> 
                    <label class="checkbox-inline"> 
                      <input type="radio" name="page_publishing" class="radio" value="yes"  
					  <?php if($record_info->page_publishing == 'yes')  echo "checked" ?>> 
                      yes</label> 
                    <label class="checkbox-inline"> 
                      <input type="radio" name="page_publishing" class="radio" value="no" 
                       <?php if($record_info->page_publishing == 'no')  echo "checked" ?> > 
                      No</label> 
                  </div> 
                </div> 
                <div class="form-group"> 
                  <label class="col-lg-4 ">Publish Event:</label> 
                  <div class="col-lg-8"> 
                    <label class="checkbox-inline"> 
                      <input type="radio" name="event_publishing" class="radio" value="yes"  
                       <?php if($record_info->event_publishing == 'yes')  echo "checked" ?>> 
                      yes</label> 
                    <label class="checkbox-inline"> 
                      <input type="radio" name="event_publishing" class="radio" value="no" 
                       <?php if($record_info->event_publishing == 'no')  echo "checked" ?> > 
                      No</label> 
                  </div> 
                </div> 
               <br /> 
               </div> 
                </div> 
                   
                </form> 
              </div> 
          </section> 
        </div> 
      </div> 
      <!-- page end-->  
    </section> 
  </section> 
  <!--main content end-->  
  <!--footer start--> 
  <?php require_once("../layout/footer.php"); ?>