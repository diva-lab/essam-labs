jQuery(document).ready(function($) {

	// process the form

	$('#delete_activity').on('click',function() {

		var lang =  $('#delete_activity').attr('data-lang')
		 // language : 
		// lang : $('#')
		
		if(lang == 'en'){
		var confrimation = confirm('Are you sure you want to delete this activity ?');
        }else{
        var confrimation = confirm('هل تريد بالفعل حذف هذا النشاط ؟');
        }


		if(confrimation){

		var formData = {

			lang: lang  , 

			activity_id :  $('#delete_activity').attr('data-activity-id') 

		

		};

		// process the form

		$.ajax({

			type 		: 'POST', // define the type of HTTP verb we want to use (POST for our form)

			url 		: 'main/data_model/delete_activity.php', // the url where we want to POST

			data 		: formData, // our data object

			success : function(data) {
				 
				 
				if(data.status == 'work'){

					$("#activity_delete").html("<div class='alert alert-block alert-success'>"+data.message+"</div >");
						window.setTimeout(function(){location.reload();},220);
					//$('#general_info')[0].reset();

				}else if (data.status == 'not_work'){
					$("#activity_delete").html("<div class='alert alert-block alert-danger'>"+data.message+"</div >");
					 
				}else{
					$("#activity_delete").html("<div class='alert alert-block alert-danger'>"+data.message+"</div >");
					}


			}

		});

	return false;


		}

	});	

});