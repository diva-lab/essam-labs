<?php 
require_once("MysqlDatabase.php"); 
require_once("TimeZone.php"); 
require_once("GeneralSettings.php"); 
//redirect to page 
function redirect_to($location = NULL) { 
  if ($location != NULL) { 
    header("Location: {$location}"); 
    exit; 
  } 
} 
//function return date now 
function date_now(){ 
	$record_id = 1; 
	$record_info = GeneralSettings::find_by_id($record_id); 
    $gmt_time = TimeZone::find_by_id($record_info->time_zone_id); 
	$now = gmdate("Y-m-d H:i:s"); 
	$hours_plus = $gmt_time->GMT; 
	$hours =  floor($hours_plus); 
    $timestamp =gmdate('Y-m-d H:i:s', strtotime(" $hours hours", strtotime($now))); 
     return $timestamp; 
} 
function arabic_date($date) { 
	$timeStamp = strtotime($date); 
	$day =  date('j', $timeStamp); 
	$day_of_week = date('D', $timeStamp); 
	$month = date('M', $timeStamp); 
	$year = date('Y', $timeStamp); 
	$ar_month =  $ar_month =  array("Jan"=>"يناير","Feb"=>"فبراير","Mar"=>"مارس","Apr"=>"ابريل","May"=>"مايو","Jun"=>"يونيو","Jul"=>"يوليو","Aug"=>"اغسطس", 
					"Sep"=>"سبتمبر","Oct"=>"اكتوبر","Nov"=>"نوفمبر","Dec"=>"ديسمبر"); 
	$ar_day = array("Sat"=>"السبت","Sun"=>"الأحد","Mon"=>"الإثنين","Tue"=>"الثلاثاء","Wed"=>"الأربعاء","Thu"=>"الخميس","Fri"=>"الجمعة"); 
	RETURN $ar_day[$day_of_week]." ". $day." ".$ar_month[$month]." ".$year; 
} 
function output_message($message="") { 
  if (!empty($message)) {  
    return "<p class=\"message\">{$message}</p>"; 
  } else { 
    return ""; 
  } 
}
function english_date($date) {
	$timeStamp = strtotime($date);
	$day =  date('j', $timeStamp);
	$day_of_week = date('D', $timeStamp);
	$month = date('M', $timeStamp);
	$year = date('Y', $timeStamp);
	$en_month =  $en_month =  array("Jan"=>"January","Feb"=>"February","Mar"=>"March","Apr"=>"April","May"=>"May","Jun"=>"June ","Jul"=>"July","Aug"=>"August","Sep"=>"September ","Oct"=>"October","Nov"=>"November ","Dec"=>"December ");
	$en_day = array("Sat"=>"Sat","Sun"=>"Sun","Mon"=>"Mon","Tue"=>"Tue","Wed"=>"Wed","Thu"=>"Thu","Fri"=>"Fri");
	RETURN   $en_month[$month]."  ".$day.", ".$year;
}
 
//return empty fileds 
function check_required_fields($required_array) { 
	$field_errors = array(); 
	foreach($required_array as $fieldname=>$message) { 
		if (!isset($_POST[$fieldname]) || empty($_POST[$fieldname])) { 
			$field_errors[] = $message;  
		} 
	} 
	return $field_errors; 
} 
//return file size 
function formatBytes($bytes, $precision = 2) {  
    $units = array('B', 'KB', 'MB', 'GB', 'TB');  
    $bytes = max($bytes, 0);  
    $pow = floor(($bytes ? log($bytes) : 0) / log(1024));  
    $pow = min($pow, count($units) - 1);  
    // Uncomment one of the following alternatives 
    // $bytes /= pow(1024, $pow); 
    // $bytes /= (1 << (10 * $pow));  
    return round($bytes, $precision) . ' ' . $units[$pow];  
} 
//return folder and file name without server 
function file_folder_src($root){ 
	if($root){ 
		$parts = explode('/',$root); 
		$folder = $parts[count($parts)-2];	 
		$file = $parts[count($parts)-1]; 
		return $folder."/".$file;		 
	}else{ 
		return null;		 
	} 
} 
//limt the summary 
function string_limit_words($string, $word_limit) { 
   $words = explode(' ', $string); 
   $string =  implode(' ', array_slice($words,0, $word_limit));
   return $string;
 }
 function get_image_src($image,$folder_thumb_name){
	 $parts = explode('/',$image); 
	 $folder = $parts[count($parts)-2];	 
	 $file = $parts[count($parts)-1]; 
	 $path = "media-library-thumb/$folder/$folder_thumb_name/$file";
	 return $path;
	 
}
function get_enum_values( $table, $field )
{
    $type = $this->db->query( "SHOW COLUMNS FROM {$table} WHERE Field = '{$field}'" )->row( 0 )->Type;
    preg_match("/^enum\(\'(.*)\'\)$/", $type, $matches);
    $enum = explode("','", $matches[1]);
    return $enum;
}
?>
