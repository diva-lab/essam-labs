<?php  
	require_once("../layout/initialize.php"); 
	$profile_id = $_GET['id']; 
	$modules = new ProfileModulesAccess(); 
	$modules->enable_relation(); 
	$records = $modules->profile_modules_data($profile_id, 'yes', 'module_sorting', 'ASC');	 
	require_once("../layout/header.php"); 
?> 
<script type="text/javascript" src="../../js-crud/crud_profile_pages.js"></script> 
  <!--header end-->  
  <!--sidebar start--> 
  <?php require_once("../layout/navigation.php"); ?> 
  <!--sidebar end-->  
  <!--main content start--> 
  <section id="main-content"> 
        <section class="wrapper site-min-height"> 
       <!-- page start--> 
      <div class="row"> 
        <div class="col-lg-12"> 
          <div class="border-head"> 
            <h3>Profile Module:</h3> 
          </div> 
        </div> 
        <form><input type="hidden" id="profile_id" value="<?php echo $profile_id ?>"></form> 
        <?php foreach($records as $record):?> 
        <div class="col-lg-4"> 
          <section class="panel"> 
            <header class="panel-heading"><?php echo ucwords($record->module_title); ?> </header> 
            <div class="panel-body"> 
              <form action="#" method="get" accept-charset="utf-8"> 
                <div class="checkboxes"> 
                  <?php 
				  $pages = new ProfilePagesAccess(); 
				  $pages->enable_relation(); 
				  $page_records = $pages->profile_pages_data($record->module_id, $profile_id, 'page_sorting', 'ASC'); 
				  if(count($page_records) > 0){ 
					foreach($page_records as $page) { 
						echo "<label><input type='checkbox' id='checkbox-{$page->Page_id}' value='{$page->Page_id}' "; 
						if($page->access == 'yes'){ 
							echo "checked";	 
						} 
						echo " />"; echo ucwords($page->page_title)."</label> ";  
					}				   
				  } 
				 ?> 
                </div> 
              </form> 
            </div> 
          </section> 
        </div> 
        <?php endforeach;?> 
      </div> 
      <div class="col-sm-6"> 
        <section > 
          	<button id="save" type="button" class="btn btn-shadow btn-default">Save Changes</button> 
            <button id="save" type="button" class="btn btn-shadow btn-default">Cancel</button> 
            <span id="loading_data"></span> 
        </section> 
      </div> 
    </section> 
  </section> 
  <!--main content end-->  
  <!--footer start--> 
  <?php require_once("../layout/footer.php"); ?>