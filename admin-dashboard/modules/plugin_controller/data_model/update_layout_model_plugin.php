<?php 
require_once('../../../../classes/Session.php'); 
require_once('../../../../classes/Functions.php'); 
require_once('../../../../classes/MysqlDatabase.php'); 
require_once('../../../../classes/ThemeLayoutModel.php'); 
require_once('../../../../classes/ThemeLayoutModelPlugin.php'); 
//update theme model  
$id = $_POST['record']; 
$update_model = ThemeLayoutModel::find_by_id($id); 
$theme = $update_model->theme_id; 
$update_model->name = $_POST["name"]; 
$update_model->type = $_POST["type"]; 
$update_model = $update_model->update(); 
if($update_model){ 
	 $model_plugins = ThemeLayoutModelPlugin::find_all_by_custom_filed('model_id', $id); 
	 $theme_layout_plugin_class = new  ThemeLayoutModelPlugin(); 
	 if(empty($model_plugins)){ 
			//insert model plugin  
			if(!empty($_POST["plugins_left"])){ 
				 
				$selected_plugins =   $_POST["plugins_left"]; 
				$sort = 1; 
				foreach($selected_plugins as $plugin) 
				{   $add_model_plugin = new ThemeLayoutModelPlugin(); 
					$add_model_plugin->model_id = $id; 
					$add_model_plugin->plugin_id = $plugin; 
					$add_model_plugin->sorting = $sort; 
					$add_model_plugin->position = 'left'; 
					$insert_model_plugin = $add_model_plugin->insert(); 
					 
					$sort++; 
			   } 
			  } 
			  if(!empty($_POST["plugins_right"])){ 
				$selected_plugins =   $_POST["plugins_right"]; 
				$sort = 1; 
				foreach($selected_plugins as $plugin) 
				{   $add_model_plugin = new ThemeLayoutModelPlugin(); 
					$add_model_plugin->model_id = $id; 
					$add_model_plugin->plugin_id = $plugin; 
					$add_model_plugin->sorting = $sort; 
					$add_model_plugin->position = 'right'; 
					$insert_model_plugin = $add_model_plugin->insert(); 
					 
					$sort++; 
			   } 
			  } 
	 }else{ 
		 foreach($model_plugins as $plugin){ 
			 //delete all model plugin 
			 $sql_delete_plugin = "DELETE FROM theme_layout_model_plugin WHERE model_id = '{$id}'"; 
				  $preform_delete_plugin = $database->query($sql_delete_plugin); 
				  //insert model plugin  
			if(!empty($_POST["plugins_left"])){ 
				$selected_plugins =   $_POST["plugins_left"]; 
				$sort = 1; 
				foreach($selected_plugins as $plugin) 
				{   $add_model_plugin = new ThemeLayoutModelPlugin(); 
					$add_model_plugin->model_id = $id; 
					$add_model_plugin->plugin_id = $plugin; 
					$add_model_plugin->sorting = $sort; 
					$add_model_plugin->position = 'left'; 
					$insert_model_plugin = $add_model_plugin->insert(); 
					 
					$sort++; 
			   } 
			  } 
			  if(!empty($_POST["plugins_right"])){ 
				$selected_plugins =   $_POST["plugins_right"]; 
				$sort = 1; 
				foreach($selected_plugins as $plugin) 
				{   $add_model_plugin = new ThemeLayoutModelPlugin(); 
					$add_model_plugin->model_id = $id; 
					$add_model_plugin->plugin_id = $plugin; 
					$add_model_plugin->sorting = $sort; 
					$add_model_plugin->position = 'right'; 
					$insert_model_plugin = $add_model_plugin->insert(); 
					 
					$sort++; 
			   } 
			  } 
				   
		 } 
		  
		  
		  
		  
		 } 
			   
	 redirect_to("../../themes/view_layouts.php?id=$theme");	  
}else{ 
	  redirect_to("../../themes/view_layouts.php?id=$theme"); 
	} 
//close connection 
if(isset($database)){ 
$database->close_connection();	 
} 
?>