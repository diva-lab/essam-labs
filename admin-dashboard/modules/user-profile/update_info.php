<?php 
	require_once("../layout/initialize.php"); 
	$id = $session->user_id; 
	$record_info = Users::find_by_id($id); 
	require_once("../layout/header.php"); 
?> 
  <script type="text/javascript" src="../../js-crud/crud_user_profile.js"></script>  
  <!--header end-->  
  <!--sidebar start--> 
  <?php require_once("../layout/navigation.php");?> 
  <!--sidebar end-->  
  <!--main content start--> 
  <section id="main-content"> 
    <section class="wrapper site-min-height"> 
      <h4>Uesr Profile</h4> 
      <div class="row"> 
        <aside class="profile-info col-lg-8"> 
          <section> 
            <div class="panel panel-primary"> 
              <div class="panel-heading"> Update Profile</div> 
              <div class="panel-body"> 
                <form class="form-horizontal tasi-form" role="form" id="form_crud" action="data_model/update_info.php"> 
                  <input type="hidden" id="process_type" value="update"> 
                  <div class="form-group"> 
                    <label  class="col-lg-2 ">User Name:</label> 
                    <div class="col-lg-8"> 
                      <input type="text" class="form-control" id="user_name" disabled value="<?php echo $record_info->user_name?>" autocomplete="off"> 
                    </div> 
                  </div> 
                  <div class="form-group"> 
                    <label  class="col-lg-2 ">First Name:</label> 
                    <div class="col-lg-8"> 
                      <input type="text" class="form-control" id="first_name" value="<?php echo $record_info->first_name?>" autocomplete="off"> 
                    </div> 
                  </div> 
                  <div class="form-group"> 
                    <label  class="col-lg-2 ">Last Name:</label> 
                    <div class="col-lg-8"> 
                      <input type="text" class="form-control" id="last_name" value="<?php echo $record_info->last_name?>" autocomplete="off"> 
                    </div> 
                  </div> 
                  <div class="form-group"> 
                    <label  class="col-lg-2 ">Email:</label> 
                    <div class="col-lg-8"> 
                      <input type="text" class="form-control" id="email" value="<?php echo $record_info->email?>" autocomplete="off"> 
                    </div> 
                  </div> 
                  <div class="form-group" style="display:none" id="pass"> 
                    <label  class="col-lg-2 ">Enter password to Complete</label> 
                    <div class="col-lg-8"> 
                      <input type="password" class="form-control" id="pwd" value="" autocomplete="off"> 
                      <br/> 
                      <button type="submit" class="btn btn-info" id="submit">Save</button> 
                      <button type="submit" class="btn btn-defult" id="cancel">Cancel</button> 
                    </div> 
                  </div> 
                  <div class="form-group" id="btn-g"> 
                    <div class="col-lg-offset-2 col-lg-8"> 
                      <button type="submit" class="btn btn-info" id="confirm">Save</button> 
                      <button type="submit" class="btn btn-defult" id="cancel">Cancel</button> 
                      <div id="loading_data"></div> 
                    </div> 
                  </div> 
                </form> 
              </div> 
            </div> 
          </section> 
        </aside> 
      </div> 
      <!-- page end-->  
    </section> 
  </section> 
  <!--main content end-->  
  <!--footer start--> 
   
  <?php require_once("../layout/footer.php");?>