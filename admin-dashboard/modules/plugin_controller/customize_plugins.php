<?php 
require_once("../../../classes/Plugins.php"); 
require_once("../../../classes/ThemeIndex.php"); 
require_once("../../../classes/Themes.php"); 
$id = $_GET["id"]; 
$theme_data = Themes::find_by_id($id); 
$define_class = new Plugins(); 
$records = $define_class->plugins_data("uploaded_date","ASC"); 
$them_part = ThemeIndex::find_all_by_custom_filed('theme_id',$id); 
?> 
<!DOCTYPE html> 
<html lang="en"> 
  <head> 
    <meta charset="utf-8"> 
    <head> 
    <meta charset="utf-8"> 
    <meta name="viewport" content="width=device-width, initial-scale=1.0"> 
    <meta name="description" content=""> 
    <meta name="author" content="Mosaddek"> 
    <meta name="keyword" content="FlatLab, Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina"> 
    <!-- Bootstrap core CSS --> 
    <link href="../../css/bootstrap.min.css" rel="stylesheet"> 
    <link href="../../css/bootstrap-reset.css" rel="stylesheet"> 
    <!--external css--> 
    <link rel="stylesheet" type="text/css" href="../../assets/gritter/css/jquery.gritter.css" /> 
    <link href="../../assets/font-awesome/css/font-awesome.css" rel="stylesheet" /> 
     <link href="../../assets/advanced-datatable/media/css/demo_page.css" rel="stylesheet" /> 
    <link href="../../assets/advanced-datatable/media/css/demo_table.css" rel="stylesheet" /> 
    <link rel="stylesheet" type="text/css" href="../../js/fancybox/jquery.fancybox-1.3.4.css" media="screen" /> 
     <link href="../../css/style.css" rel="stylesheet"> 
    <link href="../../css/style-responsive.css" rel="stylesheet" /> 
     
 </head> 
 <body style="background:#fff"> 
  <section class="panel"> 
  <div class="panel-body"> 
  <header class="panel-heading"> Customize Plugins </header> 
  <div class="row"> 
  <div class="col-lg-6"> 
  <br> 
  <form action="data_model/update_custmize_plugin.php" class="form-horizontal tasi-form" method="post"> 
  <input type="hidden" id="theme_id" value="<?php echo $id;?>" name="theme_id"/> 
  <input type="hidden" id="task" value="customize_plugin" name="task" /> 
   
   
  <?php 
   $cout = 0; 
     
   foreach($them_part as $part){?> 
  <div class="form-group"> 
      <label class="control-label col-lg-4"> Part <?php echo $part->plugin_place_number;?> </label> 
      <div class="col-lg-8"> 
          <select class="form-control" id="part<?php echo $part->plugin_place_number;?>" name="part<?php echo $part->plugin_place_number;?>" > 
            <option value="">Select plugin</option> 
             <?php 
              foreach($records as $record): 
			   
			  ?> 
               
          <option value="<?php echo $record->id?>" <?php if($record->id == $them_part[$cout]->plugin) {echo 'selected';}?> > <?php echo $record->name?></option> 
             <?php 
			 
			  endforeach;?> 
          </select> 
          <?php $cout++;?> 
      </div> 
   </div> 
   <?php   }?> 
   <div class="form-group"> 
    <div class="col-lg-offset-2 col-lg-10"> 
      <button type="submit" class="btn btn-info" id="submit">Save Changes </button> 
      
      
    </div> 
  </div> 
  </form> 
  </div> 
  
  <div class="col-lg-6"> 
  <br> 
  <div  style="width:500px !important; "> 
   
   
 <img src="../../../<?php echo $theme_data->name;?>/layout_plugins_design/index.jpg"   width="300" height="500"  /> 
  
  </div> 
 </div> 
       </div>  
                  
        
      </div> 
  <script src="../../js/jquery.js"></script> 
    <script src="../../js/bootstrap.min.js"></script> 
    <script class="include" type="text/javascript" src="../../js/jquery.dcjqaccordion.2.7.js"></script> 
    <script src="../../js/jquery.scrollTo.min.js"></script> 
    <script src="../../js/jquery.nicescroll.js" type="text/javascript"></script> 
    <script src="../../js/respond.min.js" ></script> 
     <script type="text/javascript" src="../../assets/gritter/js/jquery.gritter.js"></script> 
    <script type="text/javascript" language="javascript" src="../../assets/advanced-datatable/media/js/jquery.dataTables.js"></script> 
    <script type="text/javascript" src="../../js/fancybox/jquery.fancybox-1.3.4.pack.js"></script> 
      <script type="text/javascript" src="../../js/fancybox/jquery.mousewheel-3.0.4.pack.js"></script> 
    <!--common script for all pages--> 
    <script src="../../js/common-scripts.js"></script> 
    <script src="../../js/gritter.js" type="text/javascript"></script> 
     
     
  <!--script for this page--> 
  <script src="../../js/form-component.js"></script> 
</section> 
 </body> 
</html> 
