$(document).ready(function (){
$("#form_crud").submit(function (){
	
	 //disable button
	 $('#submit').attr("disabled", "true");
	 var type = 'POST';
	 var url = $('#form_crud').attr('action');
	 //send json data
	 var data = {	
			task: $('#process_type').val(),
			record: $('#record').val(),
			title :$('#title').val(),
			header_title :$('#header_title').val(),
			postion: $('#postion').val(),
			status :$('input[name=status]:checked').val(),
			plugin_value: $('#plugin_value').val(),
			plugin: $('#plugin').val()
			
		};
	$.ajax({
	 type: type,
	 url: url,
	 data: data,
	 beforeSend: function(){
		//show laoding 
		$('#loading_data').html('<img src="../../img/loading.gif"/>&nbsp Loading ....');
	 },
	 success: function(data){
		//alert(data);
		if(data.status == 'work'){
			 window.location.href = "view.php";
		 }else if(data.status == 'valid_error'){
			 $('#loading_data').html(data.fileds);
			 $('#loading_data').css('color', 'red');
			 $('#submit').removeAttr('disabled');
		 }else{
			$('#loading_data').html('Error In Process');
		 }
	 }
})
 //do not go to any where
  return false;     
 })    
})