<?php
require_once('../../classes/FrontSession.php');
require_once('../../classes/MysqlDatabase.php');
require_once('../../classes/CustomerInfo.php');
require_once('../../localization/'.$_POST["lang"].'/validation_label.php');
header('Content-Type: application/json');
if(!empty($_POST["email"]) && !empty($_POST["pass"])){
	$email = $_POST["email"];
	$pass = $_POST["pass"];
	$password = md5($pass);
	$find_user = CustomerInfo::find_by_email_pass($email, $password);
	//send message
	if($find_user){
		if($find_user->account_status == "blocked" ){
			$data = array("status"=>"not_work","message"=>$vlogin_user_blocked);
			echo json_encode($data);					
		}elseif($find_user->account_status == "not_verified"){
			$data = array("status"=>"not_work","message"=>$vlogin_user_not_verified);
			echo json_encode($data);				
		}else{
			$front_session->Login($find_user);
			$data  = array("status"=>"work","message"=>"redirect");
			echo json_encode($data);			
		}
	}else{
		$data  = array("status"=>"not_work","message"=>$vlogin_user_not_exist);
		echo json_encode($data);
	}		
}else{
	$data  = array("status"=>"not_work","message"=>$vlogin_valid_error);
	echo json_encode($data);
}
//close connection
if(isset($database)){
	$database->close_connection();
}
?>	