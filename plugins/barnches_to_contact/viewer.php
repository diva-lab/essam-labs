 <section class="col-md-12 vision clearfix"><!--start of vision-->

<?php
$define_page_categories = new NodesSelectedTaxonomies();
$define_page_categories->enable_relation();
$page_categories = $define_page_categories->return_node_taxonomy($get_page_content->id,'page','category','one',$lang_info->id);
$List_posts_for_blog = $define_node->front_node_data(null,"post",null,null,$lang_info->id,null,null,null,$page_categories->id, null,null,null,"many",null,null);

?>

          <?php
          echo "<div class='tab-content col-md-4'>" ;
            $loop = 0 ;
            foreach($List_posts_for_blog as $post){

              echo " <div class=' contact-us tab-pane " ;
              if($loop == 0 ){
                echo " active " ;
              }
              echo " ' id='$post->id'>" ;
             echo " <h2 class='c-red'>$post->title<span class='c-grey'></span></h2>";
             $post_summary = $post->summary ;

                          echo $post_summary ;

                          echo "<div class=' contact-inner'>";
                            echo "<ul class=' social-icons new-social'>";
                              // start implementation of social menu links  and listing all of them
                                 $social_menu = $define_menu_link->menu_submenu_front_data('sorting','ASC','social_menu',0,$lang_info->id);
                                    if(count($social_menu)){
                                    foreach($social_menu as $link){
                                      $path_link_data = $define_node->get_node_content($link->path,$lang_info->id);
                                        $path = "";
                                        if($link->path_type == "page"){
                                          $path = "content.php";
                                        }else if($link->path_type == "post"){
                                          $path = "post_details.php";
                                        }else if($link->path_type == "event"){
                                          $path = "event_details.php";
                                        }
                                        if($link->path_type == "external"){
                                          echo "<li><a href='$link->external_path' target='__blank' class='btn fa $link->icon'></a></li>";

                                        }else{
                                           // echo "<li><a href='$path?alias=$path_link_data->alias&lang=$lang'>$link->title</a></li>";
                                        }
                                     }
                                }

                            echo "</ul>";

                          echo "</div> " ;




               echo "</div>" ;
               $loop ++ ;

            }
              echo "</div>";
          ?>






          <div class="col-md-4 clearfix message"><!--start of message-->

            <form id="contact_us_form" action="main/data_model/contact_us.php" method="POST">
            <input type="hidden" name="lang" id='lang' value="<?php echo $lang ;  ?>">
              <h2 class="c-red">Quick message<span class="c-grey"></span></h2>

              <input type="text" class="form-control quick-input search-bar" id="name" placeholder="<?php echo $name  ;  ?>">

              <input type="text" class="form-control search-bar quick-input" id="email" placeholder="<?php echo $email ;  ?>">
              <textarea class="form-control quick-input" rows="9" placeholder="<?php echo $message ;  ?>" id="message"></textarea>
              <button class="btn text-center red-bg search-bar send-btn" id="send_message" type="submit"><?php echo $submit ;  ?></button>
            </form>
          </div><!--end of message-->

          <div class="col-md-4 our-branches"><!--start of our-branches-->
            <ul class="col-md-12">
          <?php
             $get_main_cat = $define_taxonomy_class->get_taxonomy_content($page_categories->id , $lang_info->id);

          ?>
            <h2 class="text-center red-bg"><a href=""><?php echo $get_main_cat->name; ?></a></h2>
              <?php
                $index = 0 ;
                foreach($List_posts_for_blog as $tab_info){

                    echo "<li class='c-black bg-grey " ;
                    if ($index == 0){
                      echo " active " ;
                    }
                    echo " '><a href='#$tab_info->id' data-toggle='tab'>$tab_info->title</a></li>" ;
                    $index++;
                   }

              ?>



            </ul>
          </div><!--end of our-branches-->
          <div class="col-md-8 map"><!--start of map-->
            <?php foreach ($List_posts_for_blog as $post) {
              echo $post->body ; 
            } ?>
            <!-- <iframe class="map-inner" src="https://www.google.com/maps/embed?pb=!1m10!1m8!1m3!1d6913.470074831786!2d30.9184254!3d29.958299099999994!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sar!2seg!4v1475075351815" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe> -->
          </div><!--end of map-->




          </section><!--end of vision-->
