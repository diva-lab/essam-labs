<?php 
require_once('../../../../classes/Session.php'); 
require_once('../../../../classes/Functions.php'); 
require_once('../../../../classes/MysqlDatabase.php'); 
require_once('../../../../classes/Customers.php'); 
//check log in  
if($session->is_logged() == false){ 
	redirect_to("../../index.php"); 
} 
if(!empty($_GET["task"]) && $_GET["task"] == "delete"){ 
		//send json
	header('Content-Type: application/json');
	//get data 
	$id = $_GET['id']; 
	//find record	 
	$find_customer = new Customers(); 
	$customer_to_delete = $find_customer->customer_data($id); 
	//if there is record perform delete 
	//if there is no record go back to view 
	if($customer_to_delete){ 
		$delete = $customer_to_delete->delete(); 
		if($delete){ 
				 $data = array("status"=>"work");
				echo json_encode($data); 
		}else{ 
               $data = array("status"=>"failed");
				echo json_encode($data);		}	 
		//if there is no record go back to view 
	}else{ 
		redirect_to("../view.php");	 
	}  
}else{ 
	//if task wasnot delete go back to view 
	redirect_to("../view.php");	 
} 
//close connection 
if(isset($database)){ 
	$database->close_connection(); 
} 
?>