<?php 
require_once 'CRUD.php'; 
class NodesContent extends CRUD{ 
   //calss attributes 
   public $id; 
   public $node_id;  
   public $lang_id; 
   public $title; 
   public $alias; 
   public $summary; 
   public $body; 
   public $meta_keys; 
   public $meta_description; 
   
   
   
   //push attributes for relational tables 
   public function enable_relation(){ 
		array_push(static::$primary_fields , ''); 
   }    
	//define table name and fields 
	protected static $table_name = 'nodes_content'; 
	protected static $primary_fields = array('id', 'node_id','lang_id','title','alias','summary','body','meta_keys','meta_description'); 
	
	 
	 
} 
?>