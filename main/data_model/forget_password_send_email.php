<?php
require_once('../../classes/Session.php');
require_once('../../classes/Functions.php');
require_once('../../classes/MysqlDatabase.php');
require_once('../../classes/CustomerInfo.php');
require_once('../../classes/ForgetPassword.php');
require_once("../../classes/GeneralSettings.php");
require_once('../../localization/'.$_POST["lang"].'/validation_label.php');

header('Content-Type: text/html; charset=utf-8');
          //phpMailer code
		      date_default_timezone_set('Etc/UTC');
          require_once ("PHPMailer/PHPMailerAutoload.php");

header('Content-Type: application/json');
$define_general_setting =  new GeneralSettings();
$define_general_setting->enable_relation();
$general_setting_info = $define_general_setting->general_settings_data();


if(empty($_POST["email"])){
	$data = array("status"=>"not_work","message"=>$vemail);
	echo json_encode($data);
}else{
	//check email exist
	$user_info = CustomerInfo::find_by_custom_filed('email', $_POST["email"]);
	if(empty($user_info)){
		$data = array("status"=>"not_work","message"=>$vemail_not_exist);
		echo json_encode($data);
	}else{
		//delete all user forget password
		$sql_delete_old_forget_pass = "DELETE FROM forget_password WHERE user_id = '{$user_info->id}'";
		$preform_delete_old_forget_pass = $database->query($sql_delete_old_forget_pass);
		//insert new one
		$lang = $_POST['lang'];
		$rand_code = date("ymdhis");
		$add = new ForgetPassword();
		$add->user_id = $user_info->id;
		$add->code = $rand_code;
		$insert = $add->insert();
		if($insert){
			//send email
			$from = $general_setting_info->email;
			$to = $_POST["email"];
			$user_name  = $user_info->first_name ;

			if($_POST["lang"]=="ar") {


				$subject = " - تغيير كلمة المرور";


				}
				else  {

				$subject = "New Motores - Forget Password";

					}


			$mailheaders = "From:  . strip_tags($from) . \r\n";
            $mailheaders .= "Reply-to: $from \n";
            $mailheaders .= "Content-Type: text/html; charset=utf-8\r\n"."Content-Transfer-Encoding: base64";
	     
            $message ="<a href='$general_setting_info->site_url/forget_passsword.php?code=$rand_code&lang=$lang'>Click Here To change Your password</a>";
            //	$send = mail($to, $subject,$message, $mailheaders);




//Create a new PHPMailer instance
$mail = new PHPMailer;

//Tell PHPMailer to use SMTP
$mail->isMail();
//Enable SMTP debugging
// 0 = off (for production use)
// 1 = client messages
// 2 = client and server messages
$mail->SMTPDebug = 0;
//Ask for HTML-friendly debug output
$mail->Debugoutput = 'html';
$mail->CharSet = 'UTF-8';  
//Set the hostname of the mail server
$mail->Host = "smtp.gmail.com";
//Set the SMTP port number - likely to be 25, 465 or 587
$mail->Port = 587;
//Set the encryption system to use - ssl (deprecated) or tls
$mail->SMTPSecure = 'tls';
//Whether to use SMTP authentication
$mail->SMTPAuth = true;

//Username to use for SMTP authentication - use full email address for gmail
$mail->Username = "wajbety@wajbety.com";

//Password to use for SMTP authentication
$mail->Password = "wajbety20162016";
//Set who the message is to be sent from
$mail->setFrom( $from ,  $subject);
//Set an alternative reply-to address
$mail->addReplyTo( $from , $subject);
//Set who the message is to be sent to
$mail->addAddress( $to , $user_name);


//Set the subject line
$mail->Subject = $subject;
//Read an HTML message body from an external file, convert referenced images to embedded,
//convert HTML into a basic plain-text alternative body
$mail->msgHTML(file_get_contents('contents.html'), dirname(__FILE__));
//Replace the plain text body with one created manually
$mail->AltBody = 'This is a plain-text message body';

$mail->Body  =  $message ;
//Attach an image file

$mail->send();




			$data  = array("status"=>"work","message"=>$vforget_pass_sent);
			echo json_encode($data);
		}else{
			$data = array("status"=>"not_work","message"=>$verror_from_system);
			echo json_encode($data);
		}

	}
}
//close connection
if(isset($database)){
	$database->close_connection();
}
?>