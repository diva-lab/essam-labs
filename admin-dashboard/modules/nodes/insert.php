<?php  
require_once("../layout/initialize.php"); 
$languages = Localization::find_all(); 
//get all category 
$define_class= new Taxonomies(); 
$define_class->enable_relation();	 
require_once("../layout/header.php");	 
include("../../assets/texteditor4/head.php");  
?>
<script type="text/javascript" src="../../js-crud/gallery.js"></script>  
<script type="text/javascript" src="../../js-crud/nodes.js"></script> 
<script type="text/javascript" src="../../js-crud/auto_complete.js"></script> 
<!--header end--> 
<!--sidebar start--> 
<?php require_once("../layout/navigation.php");?> 
<!--sidebar end-->  
<!--main content start--> 
<section id="main-content"> 
<section class="wrapper site-min-height"> 
<h4>Nodes Module</h4> 
<div class="row"> 
  <aside class="col-lg-8"> 
	<section> 
	  <div class="panel"> 
		<div class="panel-heading"> Add Node</div> 
		<div class="panel-body"> 
		  <form class="form-horizontal tasi-form" role="form" id="form_crud" action="data_model/insert.php"  > 
			<input type="hidden" id="process_type" value="insert"> 
			<section class="panel "> 
			  <header class="panel-heading tab-bg-dark-navy-blue"> 
				<ul class="nav nav-tabs"> 
				  <li class=" center-block active" > <a data-toggle="tab" href="#main_option" class="text-center"><strong> Main Info</strong></a></li> 
				  <li> <a data-toggle="tab" href="#model_images" class="text-center"><strong>Gallery &  Images</strong> </a> </li> 
				  <li> <a data-toggle="tab" href="#events_details" class="text-center"><strong>Nodes Details </strong> </a> </li> 
				  <li> <a data-toggle="tab" href="#taxonomies" class="text-center"><strong> Taxonomies</strong> </a> </li> 
				</ul> 
			  </header> 
			  <div class="panel-body"> 
				<div class="tab-content"> 
				  <div id="main_option" class="tab-pane active "> 
					 <section class="panel col-lg-9"> 
                        <header class="panel-heading tab-bg-dark-navy-blue "> 
                          <ul class="nav nav-tabs"> 
                            <?php 
                            //create tabs for all available languages  
                            $languages = Localization::find_all('id','desc'); 
                            $serial_tabs = 1; 
                            foreach($languages as $language){ 
                                $lang_tab_header = ucfirst($language->name); 
                                echo "<li class='";if($serial_tabs == 1){ echo " active ";}  echo"'> <a data-toggle='tab' href='#$language->name'> 
								<strong>$lang_tab_header</strong></a></li>"; 
                                $serial_tabs++; 
                            } 
                          ?> 
                          </ul> 
                        </header> 
                        <div class="panel-body"> 
                          <div class="tab-content"> 
                            <?php 
                            $serial_tabs_content = 1; 
                            foreach($languages as $language){ 
								echo " 
								<div id='$language->name' class='tab-pane"; if($serial_tabs_content == 1){ echo " active ";} echo"'> 
										<div class='form-group'> 
										<label  class='col-lg-2'>Title:</label> 
										<div class='col-lg-9'> 
										  <input type='text' class='form-control main_content' id='title_$language->label'  autocomplete='off'  
										  onchange=\"add_char('title_$language->label','alias_$language->label')\"> 
										</div> 
									  </div> 
									  <div class='form-group'> 
										<label class='col-lg-2'>Alias:</label> 
										<div class='col-lg-9'> 
										  <input type='text' class='form-control main_content' id='alias_$language->label'> 
										</div> 
									  </div> 
									  <div class='form-group'> 
										<label  class='col-lg-2'>Summary:</label> 
										<div class='col-lg-9'> 
										  <textarea class=' form-control main_content' id='summary_$language->label'></textarea> 
										</div> 
									  </div> 
									  <div class='form-group'> 
										<label class='col-lg-2'>Body:</label> 
										<div class='col-md-9'> 
										  <textarea class='form-control main_content' id='full_content_$language->label'></textarea> 
										</div> 
									  </div> 
									  <div class='form-group'> 
										<label  class='col-lg-2'>Meta Keys:</label> 
										<div class='col-lg-9'> 
										  <input type='text' class='form-control main_content' id='meta_keys_$language->label' autocomplete='off'> 
										</div> 
									  </div> 
									  <div class='form-group'> 
										<label  class='col-lg-2'>Meta Description:</label> 
										<div class='col-lg-9'> 
										  <input type='text' class='form-control main_content' id='meta_description_$language->label'> 
										</div> 
									  </div> 
								</div>"; 
                                $serial_tabs_content++; 
                            } 
                          ?> 
                          </div> 
                        </div> 
				</section>   
				  </div> 
				  <div id="model_images" class="tab-pane  "> 
                     <div class="form-group"> 
                        <label  class="col-lg-2">Cover Image:</label> 
                        <div class="col-lg-9"> <a href="../file_mangers/media_filemanager/view_media_directories.php" id="image_cover">Select Image</a> 
                          <input type="hidden" class="form-control" id="imageVal" placeholder=" " autocomplete="off" value=""> 
                          <div style="display:none" id="imageShow"> <img src="" id="imageSrc" style="width:100px; height:200px;"><a href='#' class=' btn btn-danger btn-xs tooltips DeleteCover glyphicon glyphicon-remove' style="width: 25px;height: 19px;margin-top: -197px;"></a></div> 
                        </div> 
                      </div> 
                      <div class="form-group"> 
                        <label  class="col-lg-2">Slider Image:</label> 
                        <div class="col-lg-9"> <a href="../file_mangers/media_filemanager_slider/view_media_directories.php" id="slider_cover">Select Image</a>                             <span style="font-size:12px">(Width:940XHeightt:390)</span> 
                          <input type="hidden" class="form-control" id="imageVal_slider" value=""> 
                          <div  id="imageSlider" style="display:none"> <img src="" id="imageSrcSlider" style="width:200px; height:100px"><a href='#' class=' btn btn-danger btn-xs tooltips DeleteSlider glyphicon glyphicon-remove' 
   style="width: 25px;height: 19px;margin-top: -55px;margin-left: 4px;"></a></div> 
                        </div> 
                      </div>
                      <div class="form-group">
                         <label class="col-lg-2">Gallery Images:</label>
                         <div class="adv-table editable-table col-lg-10 ">
                            <table class="table table-striped table-hover table-bordered image_gallery_tbl">
                              <thead>
                                <tr>
                                  <th class="name">Image</th>
                                  <th class="sort">Caption</th>
                                  <th class="sort" colspan="3">Sort</th>
                                </tr>
                              </thead>
                              <tbody>
                                <tr class="selected_image_gallery" id="1">
                                    <td><input type="hidden" class="image_gallery_name" id="imageVal1">
                                    <div style="display:none" id="imageShow1"> <img src="" id="imageSrc1" style="width:80px; height:80px;"></div></td>
                                    <td><input type='text' class='form-control image_gallery_caption'  autocomplete='off'></td>
                                    <td><input type='text' class='form-control image_gallery_sort'  autocomplete='off'></td>
                                    <td width="30%">
                                     <a href='../file_mangers/filemanager_product_gallery/view_media_directories.php?selected_gallery_row=1'
                                     class='btn btn-primary btn-xs tooltips image_cover' data-placement='top' data-toggle='tooltip' data-original-title='Add new value'>
                                     <i class='icon-picture'></i></a>
                                    <a href='' class='btn btn-primary btn-xs tooltips add_tr' data-placement='top' data-toggle='tooltip'
                                    data-original-title='Add new value'><i class=' icon-plus-sign-alt'></i></a>
                                    </td>
                                </tr>
                              </tbody>
                             </table>
                         </div>
                      </div>
                      </div>
                 <div id="events_details" class="tab-pane "> 
                 <div class="form-group "> 
					  <label  class="col-lg-2">Type:</label> 
					  <div class="col-lg-8"> 
						<select class="form-control" id="type" >
                         <option value="">Select Type</option>
                        <?php 
						$types = array("post","page","event");
						foreach($types as $type){
							echo "<option value='$type'>$type</option>";
						}
						
						?>
                        </select> 
					  </div> 
					</div>
					<div class="form-group events_details hide"> 
					  <label  class="col-lg-2">Place:</label> 
					  <div class="col-lg-8"> 
						<input type="text" class="form-control" id="place" placeholder=" " autocomplete="off"> 
					  </div> 
					</div> 
					<div class="form-group events_details hide"> 
					  <label class="col-lg-2">Start Date</label> 
					  <div class="col-lg-8"> 
						<input type="text"  class="form-control"  value="" id="start_date"/> 
					  </div> 
					</div> 
					<div class="form-group  events_details hide"> 
					  <label class="col-lg-2">End Date</label> 
					  <div class="col-lg-8"> 
						<input type="text"  class="form-control"  value="" id="end_date"/> 
					  </div> 
					</div> 
				  </div>
				  <div id="taxonomies" class="tab-pane "> 
                      <div class="form-group" > 
                          <label class="col-lg-2"> Categories:</label> 
                          <div class="col-lg-8"> 
                            <ul class="selected_category"> 
                            </ul> 
                            <br> 
                            <a id="show_inserted_data" href="../utilities/categories.php" class="btn btn-default btn-info">Select Category</a>
                          </div>
                        </div>
				  </div> 
				</div> 
			  </div> 
			</section> 
			<div class="form-group"> 
			  <div class="col-lg-offset-2 col-lg-4"> 
				<button type="submit" class="btn btn-info" id="submit">Save</button> 
				<button type="reset" class="btn btn-default">Reset</button>
				<div id="loading_data"></div> 
			  </div> 
			</div> 
		  </form> 
		</div> 
	  </div> 
	</section> 
  </aside> 
  <div class="col-lg-4"> 
        <section class="panel "> 
          <header class="panel-heading tab-bg-dark-navy-blue"> 
            <ul class="nav nav-tabs"> 
              <li class=" center-block active" style="width:170px"> <a data-toggle="tab" href="#op" class="text-center"> <i class=" icon-check"></i> <strong> Publish Option</strong></a></li> 
              <li style="width:170px"> <a data-toggle="tab" href="#op1" class="text-center"> <i class=" icon-calendar "></i> <strong> Publish Date </strong> </a> </li> 
            </ul> 
          </header> 
          <div class="panel-body"> 
            <form class="form-horizontal tasi-form" role="form" id="form_option"> 
              <div class="tab-content"> 
                <div id="op" class="tab-pane active "> <br /> 
                
                  <div class="form-group"> 
                    <label class="col-lg-6">Status:</label> 
                    <div class="col-lg-6"> 
                      <label class="checkbox-inline"> 
                        <input type="radio" name="shadow" class="radio" value="draft"> 
                        Draft</label> 
                      <label class="checkbox-inline"> 
                        <input type="radio" name="shadow" class="radio" value="publish" checked> 
                        Publish</label> 
                    </div> 
                  </div> 
                 
                  <div class="form-group"> 
                    <label class="col-lg-6 ">Enable Summary:</label> 
                    <div class="col-lg-6"> 
                      <label class="checkbox-inline"> 
                        <input type="radio" name="enable_summary" class="radio" value="yes"> 
                        Yas</label> 
                      <label class="checkbox-inline"> 
                        <input type="radio" name="enable_summary" class="radio" value="no" checked> 
                        No</label> 
                    </div> 
                  </div> 
                  <div class="form-group"> 
                    <label  class="col-lg-6">Enable Comments:</label> 
                    <div class="col-lg-6"> 
                      <label class="checkbox-inline"> 
                        <input type="radio" name="comments" class="radio" value="yes"> 
                        Yas</label> 
                      <label class="checkbox-inline"> 
                        <input type="radio"  name="comments" class="radio" value="no" checked> 
                        No</label> 
                    </div> 
                  </div> 
                  
                  <div class="form-group"> 
                    <label class="col-lg-6 ">Front Page:</label> 
                    <div class="col-lg-6"> 
                      <label class="checkbox-inline"> 
                        <input type="radio" name="show_in_front" class="radio" value="yes" /> 
                        Yes</label> 
                      <label class="checkbox-inline"> 
                        <input type="radio" name="show_in_front" class="radio" value="no" checked/> 
                        No</label> 
                    </div> 
                  </div> 
                  <div class="form-group"> 
                    <label class="col-lg-6 ">Slide Show:</label> 
                    <div class="col-lg-6"> 
                      <label class="checkbox-inline"> 
                        <input type="radio" name="show_in_slide" class="radio" value="yes" /> 
                        Yes</label> 
                      <label class="checkbox-inline"> 
                        <input type="radio" name="show_in_slide" class="radio" value="no" checked/> 
                        No</label> 
                    </div> 
                  </div> 
                  <div class="form-group"> 
                    <label  class="col-lg-6">Side Bar :</label> 
                    <div class="col-lg-6"> 
                      <label class="checkbox-inline"> 
                        <input type="radio" name="side_bar" class="radio" value="yes"> 
                        Yas</label> 
                      <label class="checkbox-inline"> 
                        <input type="radio"  name="side_bar" class="radio" value="no" checked> 
                        No</label> 
                    </div> 
                  </div>
                </div> 
                <div id="op1" class="tab-pane "> <br /> 
                  <div class="form-group"> 
                    <label class="col-lg-4">Start Publish:</label> 
                    <div class="col-lg-8"> 
                      <input type="text"  class="form-control"  value="" id="start_time"/> 
                    </div> 
                  </div> 
                  <div class="form-group"> 
                    <label class="col-lg-4">End Publish:</label> 
                    <div class="col-lg-8"> 
                      <input type="text"  class="form-control"  value="" id="end_time"/> 
                    </div> 
                  </div> 
                </div> 
              </div> 
            </form> 
          </div> 
        </section> 
      <section class="panel panel-primary"> 
          <header class="panel-heading">Layout Model: </header> 
          <div class="panel-body"> 
            <form class="form-horizontal tasi-form" role="form" id="form_category"> 
              <div class="form-group"> 
                  <label class="col-lg-4"> Select Model:</label> 
                  <div class="col-lg-8"> 
                    <select class="form-control" id="model"> 
                      <option value=""> Select Model </option> 
                    
                    </select> 
                 </div> 
               </div> 
          </form> 
          </div> 
        </section> 
      </div>
</div> 
<!-- page end-->  
</section> 
</section> 
<!--main content end-->  
<!--footer start--> 
<?php require_once("../layout/footer.php");?> 
