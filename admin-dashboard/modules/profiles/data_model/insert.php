<?php 
require_once('../../../../classes/Session.php'); 
require_once('../../../../classes/Functions.php'); 
require_once('../../../../classes/MysqlDatabase.php'); 
require_once('../../../../classes/Profile.php'); 
require_once('../../../../classes/CMSModules.php'); 
require_once('../../../../classes/ProfileModulesAccess.php'); 
require_once('../../../../classes/ProfilePagesAccess.php'); 
//check log in  
if($session->is_logged() == false){ 
	redirect_to("../../index.php"); 
} 
//get all selected modules 
//foreach modules from cmsmodule tbl 
//for module:when module that in loop equale to selected module set access to be yes 
//for page: when module that in loop equale to selected module set access to be yes 
//send json data 
 header('Content-Type: application/json'); 
if(!empty($_POST["task"]) && $_POST["task"] == "insert"){ 
	//check rqquierd fields 
    $required_fields = array('title'=>"- Insert title",'modules'=>'- Insert modules'); 
	$check_required_fields = check_required_fields($required_fields); 
	if(count($check_required_fields) == 0){ 
		$add = new Profile(); 
		$add->title = $_POST["title"]; 
		$add->inserted_by = $session->user_id; 
		$add->global_edit = $_POST["global_edit"]; 
		$add->profile_block = $_POST["profile_turn_off"]; 
		$add->developer_mode = $_POST["developer_mode"]; 
		$add->page_publishing = $_POST["page_publishing"]; 
		$add->global_delete = $_POST["global_delete"]; 
		$add->post_publishing = $_POST["post_publishing"]; 
		$add->event_publishing = $_POST["event_publishing"]; 
		$add->inserted_date = date_now(); 
		$insert = $add->insert(); 
		$inserted_profile_id = $database->inserted_id(); 
		 
		if($insert){ 
			$selected_modules = $_POST['modules']; 
			$modules = CMSModules::find_all_by_custom_filed('sid', 0, 'sorting', 'ASC'); 
			foreach($modules as $module){ 
				//insert all modules 
				$insert_module = new ProfileModulesAccess(); 
				$insert_module->profile_id = $inserted_profile_id; 
				$insert_module->module_id = $module->id; 
				$insert_module->inserted_by = $session->user_id; 
				if(in_array($module->id, $selected_modules)){ 
					$insert_module->access = 'yes'; 
				}else{ 
					$insert_module->access = 'no'; 
				} 
				$insert_module->inserted_date = date_now(); 
				$insert_module->insert(); 
				//destroy class after each loop 
				unset($insert_modules); 
			} 
			//push pages for this profile 
			foreach($modules as $module){ 
				$module_pages = CMSModules::find_all_by_custom_filed('sid', $module->id, 'sorting', 'ASC'); 
				foreach($module_pages as $page){ 
				//insert all pages under each module 
				 $insert_page = new ProfilePagesAccess(); 
				 $insert_page->profile_id = $inserted_profile_id; 
				 $insert_page->module_id = $module->id; 
				 $insert_page->Page_id = $page->id; 
				if(in_array($module->id, $selected_modules)){ 
					$insert_page->access = 'yes'; 
				}else{ 
					$insert_page->access = 'no'; 
				} 
				 $insert_page->inserted_by = $session->user_id; 
				 $insert_page->inserted_date = date_now(); 
				 $insert_page->insert(); 
				 unset($insert_page); 
				} 
			} 
			$data  = array("status"=>"work", "id"=>$inserted_profile_id ); 
			echo json_encode($data); 
		}else{ 
			$data  = array("status"=>"error"); 
			echo json_encode($data); 
		} 
	}else{ 
		  //validation error 
		  $comma_separated = implode("<br>", $check_required_fields); 
		  $data  = array("status"=>"valid_error", "fileds"=>$comma_separated); 
		  echo json_encode($data); 
	  }		 
} 
//close connection 
if(isset($database)){ 
	$database->close_connection(); 
} 
?>