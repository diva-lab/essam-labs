-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Oct 25, 2016 at 05:40 PM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 7.0.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `essam_labs`
--

-- --------------------------------------------------------

--
-- Table structure for table `advertisements`
--

CREATE TABLE `advertisements` (
  `id` int(11) NOT NULL,
  `image_cover` varchar(500) NOT NULL,
  `path` int(11) NOT NULL,
  `status` enum('active','disable') NOT NULL,
  `type` enum('post','page','event','external') NOT NULL,
  `external` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `advertisement_content`
--

CREATE TABLE `advertisement_content` (
  `id` int(11) NOT NULL,
  `lang_id` int(11) NOT NULL,
  `adv_id` int(11) NOT NULL,
  `title` varchar(500) NOT NULL,
  `content` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `attributes`
--

CREATE TABLE `attributes` (
  `id` int(11) NOT NULL,
  `type` varchar(500) CHARACTER SET latin1 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf32;

--
-- Dumping data for table `attributes`
--

INSERT INTO `attributes` (`id`, `type`) VALUES
(1, 'text'),
(2, 'email'),
(3, 'url'),
(4, 'textarea'),
(5, 'radio'),
(6, 'checkbox'),
(7, 'select'),
(8, 'number'),
(9, 'password'),
(1, 'text'),
(2, 'email'),
(3, 'url'),
(4, 'textarea'),
(5, 'radio'),
(6, 'checkbox'),
(7, 'select'),
(8, 'number'),
(9, 'password'),
(1, 'text'),
(2, 'email'),
(3, 'url'),
(4, 'textarea'),
(5, 'radio'),
(6, 'checkbox'),
(7, 'select'),
(8, 'number'),
(9, 'password'),
(1, 'text'),
(2, 'email'),
(3, 'url'),
(4, 'textarea'),
(5, 'radio'),
(6, 'checkbox'),
(7, 'select'),
(8, 'number'),
(9, 'password'),
(1, 'text'),
(2, 'email'),
(3, 'url'),
(4, 'textarea'),
(5, 'radio'),
(6, 'checkbox'),
(7, 'select'),
(8, 'number'),
(9, 'password'),
(1, 'text'),
(2, 'email'),
(3, 'url'),
(4, 'textarea'),
(5, 'radio'),
(6, 'checkbox'),
(7, 'select'),
(8, 'number'),
(9, 'password'),
(1, 'text'),
(2, 'email'),
(3, 'url'),
(4, 'textarea'),
(5, 'radio'),
(6, 'checkbox'),
(7, 'select'),
(8, 'number'),
(9, 'password'),
(1, 'text'),
(2, 'email'),
(3, 'url'),
(4, 'textarea'),
(5, 'radio'),
(6, 'checkbox'),
(7, 'select'),
(8, 'number'),
(9, 'password');

-- --------------------------------------------------------

--
-- Table structure for table `cms_module_access`
--

CREATE TABLE `cms_module_access` (
  `id` int(11) NOT NULL,
  `sid` int(11) NOT NULL,
  `title` varchar(50) NOT NULL,
  `icon` varchar(30) NOT NULL,
  `sorting` int(11) NOT NULL,
  `type` enum('module','page') NOT NULL,
  `shadow` enum('yes','no') NOT NULL,
  `file_source` text NOT NULL,
  `inserted_by` int(11) NOT NULL,
  `inserted_date` datetime NOT NULL,
  `update_by` int(11) NOT NULL,
  `last_update` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cms_module_access`
--

INSERT INTO `cms_module_access` (`id`, `sid`, `title`, `icon`, `sorting`, `type`, `shadow`, `file_source`, `inserted_by`, `inserted_date`, `update_by`, `last_update`) VALUES
(5, 0, 'Media Library', 'icon-inbox', 2, 'module', '', 'media_library_directories', 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(6, 0, 'Users', 'icon-user', 1, 'module', 'no', 'users', 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(7, 0, 'Menus Group', ' icon-link', 3, 'module', '', 'menu_group,menu_link', 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(10, 0, 'Nodes', 'icon-file-text-alt', 4, 'module', 'no', 'nodes', 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(11, 0, 'Taxonomies', 'icon-tags', 8, 'module', 'no', 'taxonomies', 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(12, 0, 'Setting', 'icon-cogs', 10, 'module', 'no', 'profiles,profile_pages,localization,cms_modules,general_settings,ecom_customers_groups,ecom_taxes,ecom_payment_methods,home_layout', 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(13, 5, 'Media Directories', '', 1, 'page', 'no', 'media_library_directories/view', 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(16, 6, 'show all', '', 1, 'page', 'no', 'users/view', 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(17, 6, 'add new', '', 2, 'page', 'no', 'users/insert', 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(22, 10, 'Posts', '', 1, 'page', 'no', 'nodes/view_posts', 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(23, 10, 'add new', '', 4, 'page', 'no', 'nodes/insert', 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(25, 11, 'tags', '', 6, 'page', 'no', 'taxonomies/view_tags', 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(26, 12, 'profiles', '', 2, 'page', 'no', 'profiles/view', 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(27, 12, 'Localization', '', 8, 'page', 'no', 'localization/view', 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(28, 12, 'CPanel Menus Structure', '', 7, 'page', 'no', 'cms_modules/view', 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(29, 7, 'show all ', '', 1, 'page', 'no', 'menu_group/view', 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(30, 7, 'add new ', '', 2, 'page', 'no', 'menu_group/insert', 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(31, 11, 'update', '', 11, 'page', 'no', 'taxonomies/update', 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(33, 0, 'Social Activity', ' icon-comments-alt', 7, 'module', 'no', 'social_suggestion_topics,social_comments,social_email_subscription,poll_questions_options,poll_questions,form_attributes,forms,advertisements', 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(34, 33, 'Comments ', '', 1, 'page', 'no', 'social_comments/view', 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(35, 33, 'Polls ', '', 5, 'page', 'no', 'poll_questions/view', 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(36, 33, 'Poll Options  View', '', 10, 'page', 'yes', 'poll_questions_options/view', 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(37, 33, 'Email Subscription ', '', 15, 'page', 'no', 'social_email_subscription/view', 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(38, 33, 'suggestion topics ', '', 16, 'page', 'no', 'social_suggestion_topics/view', 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(39, 12, 'General Settings', '', 1, 'page', 'no', 'general_settings/update', 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(40, 275, 'Plugins', '', 1, 'page', 'no', 'plugins/view', 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(42, 275, 'Themes & Layouts', '', 2, 'page', 'no', 'themes/view', 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(43, 12, 'Profile Insert', '', 0, 'page', 'yes', 'profiles/insert', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(44, 12, 'Profile Update', '', 2, 'page', 'yes', 'profiles/update', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(45, 12, 'Profile Delete', '', 2, 'page', 'yes', 'profiles/delete', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(47, 12, 'CPanel Menus Structure Insert', '', 3, 'page', 'yes', 'cms_modules/insert', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(48, 12, 'CPanel Menus Structure Update', '', 3, 'page', 'yes', 'cms_modules/update', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(50, 12, 'Localization  Insert ', '', 8, 'page', 'yes', 'localization/insert', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(52, 12, 'Localization update', '', 8, 'page', 'yes', 'localization/update', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(53, 12, 'Localization Delete', '', 8, 'page', 'yes', 'localization/delete', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(54, 12, 'CPanel Menus Structure Delete', '', 8, 'page', 'yes', 'cms_modules/delete', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(56, 11, 'Categories', '', 1, 'page', 'no', 'taxonomies/view_categories', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(64, 10, 'update', '', 3, 'page', 'yes', 'nodes/update', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(65, 10, 'delete', '', 5, 'page', 'yes', 'nodes/delete', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(70, 6, 'update', '', 3, 'page', 'yes', 'users/update', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(71, 6, 'delete', '', 5, 'page', 'yes', 'users/delete', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(72, 7, 'group update ', '', 3, 'page', 'yes', 'menu_group/update', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(73, 7, 'group delete ', '', 5, 'page', 'yes', 'menu_group/delete', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(74, 33, 'Comments update ', '', 2, 'page', 'yes', 'social_comments/update', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(77, 33, ' Poll Insert', '', 6, 'page', 'yes', 'poll_questions/insert', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(78, 33, 'Poll Update ', '', 7, 'page', 'yes', 'poll_questions/update', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(79, 33, 'Poll full info', '', 8, 'page', 'yes', 'poll_questions/full_info', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(80, 33, ' Poll delete', '', 9, 'page', 'yes', 'poll_questions/delete', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(81, 33, 'Poll Options Insert', '', 11, 'page', 'yes', 'poll_questions_options/insert', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(83, 33, 'Poll Options update', '', 12, 'page', 'yes', 'poll_questions_options/update', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(84, 33, 'Poll Option full info', '', 13, 'page', 'yes', 'poll_questions_options/full_info', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(86, 33, 'Poll Options delete', '', 14, 'page', 'yes', 'poll_questions_options/delete', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(87, 33, 'Suggestion topics delete ', '', 17, 'page', 'yes', ' social_suggestion_topics /delete', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(91, 6, 'full info', '', 4, 'page', 'yes', 'users/full_info', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(92, 7, 'group full info ', '', 4, 'page', 'yes', 'menu_group/full_info', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(93, 10, 'full info', '', 4, 'page', 'yes', 'nodes/full_info', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(95, 11, 'Add New ', '', 7, 'page', 'no', 'taxonomies/insert', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(96, 11, 'Full info', '', 14, 'page', 'yes', 'taxonomies/full_info', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(97, 12, 'profile access updates', '', 2, 'page', 'yes', 'profile_pages/update', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(98, 7, 'menu show all', '', 6, 'page', 'yes', 'menu_link/view', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(99, 7, 'menu add new', '', 7, 'page', 'yes', 'menu_link/insert', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(101, 7, 'menu update', '', 7, 'page', 'yes', 'menu_link/update', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(102, 7, 'menu delete', '', 9, 'page', 'yes', 'menu_link/delete', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(103, 7, 'menu full info', '', 8, 'page', 'yes', 'menu_link/full_info', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(107, 12, 'localization label&message', '', 17, 'page', 'yes', 'localization/insert_content', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(108, 12, 'Profile Full Info', '', 2, 'page', 'yes', 'profiles/full_info', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(109, 33, 'suggestion topics full info', '', 16, 'page', 'yes', ' social_suggestion_topics /full_info', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(110, 33, 'comments full info', '', 3, 'page', 'yes', 'social_comments/full_info', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(111, 33, 'Email Subscription  delete', '', 15, 'page', 'yes', 'social_email_subscription/delete', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(112, 33, 'Comments delete', '', 4, 'page', 'yes', 'social_comments/delete', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(113, 11, 'Authors', '', 5, 'page', 'no', 'taxonomies/view_authors', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(114, 11, 'delete', '', 10, 'page', 'yes', 'taxonomies/delete', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(116, 12, 'insert theme', '', 7, 'page', 'yes', 'themes/insert', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(117, 12, 'view layouts', '', 7, 'page', 'yes', 'themes/view_layouts', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(118, 12, 'plugins insert', '', 6, 'page', 'yes', 'plugins/insert', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(119, 5, 'media library Insert', '', 2, 'page', 'yes', 'media_library_directories/insert', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(120, 5, 'media library edit', '', 3, 'page', 'yes', 'media_library_directories/update', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(121, 5, 'media File Insert', '', 4, 'page', 'yes', 'media_library_files/insert', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(122, 12, 'customize layout plugin', '', 14, 'page', 'yes', 'themes/customize_layout_plugin', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(127, 10, 'Pages', '', 3, 'page', 'no', 'nodes/view_pages', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(129, 7, 'Insert menu link content', '', 11, 'page', 'yes', 'menu_link/insert_content', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(131, 10, ' Plugin Option', '', 7, 'page', 'yes', 'nodes/plugin_option', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(132, 33, 'Forms', '', 2, 'page', 'no', 'forms/view', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(133, 33, 'form insert ', '', 21, 'page', 'yes', 'forms/insert', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(134, 33, 'forms update', '', 22, 'page', 'yes', 'forms/update', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(135, 33, 'forms full info', '', 23, 'page', 'yes', 'forms/full_info', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(136, 33, 'forms delete', '', 24, 'page', 'yes', 'forms/delete', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(137, 33, 'form attributes view', '', 25, 'page', 'yes', 'form_attributes/view', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(138, 33, 'form attributes Insert', '', 26, 'page', 'yes', 'form_attributes/insert', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(139, 33, 'form attributes  update', '', 27, 'page', 'yes', 'form_attributes/update', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(140, 33, 'form attributes  full info', '', 28, 'page', 'yes', 'form_attributes/full_info', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(141, 33, 'form attributes delete', '', 29, 'page', 'yes', 'form_attributes/delete', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(142, 33, 'advertisements ', '', 30, 'page', 'no', 'advertisements/view', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(143, 33, 'advertisements insert', '', 31, 'page', 'yes', 'advertisements/insert', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(144, 33, 'advertisements update', '', 32, 'page', 'yes', 'advertisements/update', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(145, 33, 'advertisements full info', '', 33, 'page', 'yes', 'advertisements/full_info', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(146, 33, 'advertisements delete', '', 34, 'page', 'yes', 'advertisements/delete', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(149, 33, 'form content', '', 34, 'page', 'yes', 'forms/insert_content', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(150, 33, 'advertisements content', '', 36, 'page', 'yes', 'advertisements/insert_content', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(151, 33, 'view form table', '', 38, 'page', 'yes', 'form_attributes/view_table', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(152, 33, 'form view table', '', 4, 'page', 'yes', 'form_attributes/view_table', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(153, 33, 'form inserted data', '', 5, 'page', 'yes', 'form_attributes/inserted_data', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(154, 5, 'view files', '', 2, 'page', 'yes', 'media_library_files/view', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(229, 12, 'taxes full info', '', 2, 'page', 'yes', 'ecom_taxes/full_info', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(235, 12, 'payment method insert', '', 3, 'page', 'yes', 'ecom_payment_methods/insert', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(237, 12, 'payment methods update', '', 3, 'page', 'yes', 'ecom_payment_methods/update', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(238, 12, ' payment methods info', '', 3, 'page', 'yes', 'ecom_payment_methods/full_info', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(254, 12, 'Home layout ', '', 40, 'page', 'no', 'home_layout/view', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(255, 12, 'Home layout Insert', '', 41, 'page', 'yes', 'home_layout/insert', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(256, 12, 'home layout update', '', 41, 'page', 'yes', 'home_layout/update', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(257, 12, 'home layout full_info', '', 41, 'page', 'yes', 'index_layout/full_info', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(258, 12, 'home layout delete', '', 41, 'page', 'yes', 'home_layout/delete', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(267, 33, 'Contact US', '', 39, 'page', 'no', 'contact_us/view', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(268, 33, 'Contact us delete', '', 39, 'page', 'yes', 'contact_us/delete', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(275, 0, 'utilities ', ' icon-wrench', 11, 'module', 'no', 'plugins,themes,cities,options,ecom_order_statuses', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(282, 10, 'Events', '', 3, 'page', 'no', 'nodes/view_events', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(287, 33, 'Contact us full info', '', 40, 'page', 'yes', 'contact_us/full_info', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `contact_us`
--

CREATE TABLE `contact_us` (
  `id` int(11) NOT NULL,
  `user_name` varchar(256) NOT NULL,
  `email` varchar(265) CHARACTER SET utf16 COLLATE utf16_esperanto_ci NOT NULL,
  `body` longtext NOT NULL,
  `inserted_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `contact_us`
--

INSERT INTO `contact_us` (`id`, `user_name`, `email`, `body`, `inserted_date`) VALUES
(1, '', '', '', '2016-10-25 17:35:12'),
(2, '', '', '', '2016-10-25 15:36:45'),
(3, '', '', '', '2016-10-25 15:37:03');

-- --------------------------------------------------------

--
-- Table structure for table `customer_info`
--

CREATE TABLE `customer_info` (
  `id` int(11) NOT NULL,
  `first_name` varchar(250) NOT NULL,
  `last_name` varchar(250) NOT NULL,
  `fbid` varchar(250) NOT NULL,
  `password` varchar(250) NOT NULL,
  `email` varchar(250) NOT NULL,
  `mobile` varchar(30) NOT NULL,
  `company` varchar(250) NOT NULL,
  `birthday` date DEFAULT '0000-00-00',
  `registration_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `account_status` enum('verified','not_verified','blocked') NOT NULL,
  `activate_code` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `customer_selections`
--

CREATE TABLE `customer_selections` (
  `id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `events_details`
--

CREATE TABLE `events_details` (
  `id` int(11) NOT NULL,
  `event_id` int(11) NOT NULL,
  `place` varchar(2580) NOT NULL,
  `start_date` datetime NOT NULL,
  `end_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `events_details`
--

INSERT INTO `events_details` (`id`, `event_id`, `place`, `start_date`, `end_date`) VALUES
(1, 1, 'uytuty', '2016-06-02 11:58:00', '2016-06-02 11:58:00'),
(2, 2, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 3, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 4, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, 5, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(6, 6, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(7, 7, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(8, 8, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(9, 9, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(10, 10, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(11, 11, '', '2016-06-07 05:17:00', '2016-06-18 05:17:00'),
(12, 12, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(13, 13, '', '2016-06-07 11:44:00', '2016-06-07 11:44:00'),
(14, 14, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(15, 15, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(16, 16, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(17, 1, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(18, 2, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(19, 3, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(20, 4, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(21, 5, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(22, 6, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(23, 7, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(24, 8, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(25, 9, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(26, 10, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(27, 11, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(28, 12, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(29, 13, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(30, 14, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(31, 15, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(32, 16, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(33, 17, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(34, 18, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(35, 19, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(36, 20, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(37, 21, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(38, 22, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(39, 23, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(40, 24, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(41, 25, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(42, 26, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(43, 27, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(44, 28, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(45, 29, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(46, 30, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(47, 31, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(48, 32, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(49, 33, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(50, 34, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(51, 35, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(52, 36, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(53, 37, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(54, 38, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(55, 39, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(56, 40, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(57, 41, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(58, 42, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(59, 43, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(60, 44, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(61, 1, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(62, 2, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(63, 3, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(64, 4, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(65, 1, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(66, 2, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(67, 3, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(68, 4, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(69, 5, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(70, 6, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(71, 7, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(72, 8, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(73, 9, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(74, 10, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(75, 11, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(76, 12, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(77, 13, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(78, 14, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(79, 15, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(80, 16, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(81, 17, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(82, 18, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(83, 19, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(84, 20, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(85, 21, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(86, 22, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(87, 23, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(88, 24, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(89, 25, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(90, 26, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(91, 27, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(92, 28, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(93, 29, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(94, 30, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(95, 31, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(96, 32, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(97, 33, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(98, 34, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(99, 35, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(100, 36, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(101, 37, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(102, 38, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(103, 39, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(104, 40, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `forget_password`
--

CREATE TABLE `forget_password` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `code` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `forms`
--

CREATE TABLE `forms` (
  `id` int(11) NOT NULL,
  `name` varchar(250) NOT NULL,
  `label` varchar(250) NOT NULL,
  `enable` enum('yes','no') NOT NULL,
  `email_to` varchar(500) NOT NULL,
  `inserted_date` datetime NOT NULL,
  `inserted_by` int(11) NOT NULL,
  `last_update` datetime NOT NULL,
  `update_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `forms`
--

INSERT INTO `forms` (`id`, `name`, `label`, `enable`, `email_to`, `inserted_date`, `inserted_by`, `last_update`, `update_by`) VALUES
(1, 'dfgdfg', 'tret', 'no', 'ttrt', '2015-05-24 16:09:05', 1, '0000-00-00 00:00:00', 0);

-- --------------------------------------------------------

--
-- Table structure for table `form_attributes`
--

CREATE TABLE `form_attributes` (
  `id` int(11) NOT NULL,
  `attribute_label` varchar(250) NOT NULL,
  `sorting` int(11) NOT NULL,
  `required` enum('yes','no') NOT NULL,
  `form_id` int(11) NOT NULL,
  `attribute_id` int(11) NOT NULL,
  `attribute_values` text NOT NULL,
  `inserted_date` datetime NOT NULL,
  `inserted_by` int(11) NOT NULL,
  `last_update` date NOT NULL,
  `update_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `form_attributes`
--

INSERT INTO `form_attributes` (`id`, `attribute_label`, `sorting`, `required`, `form_id`, `attribute_id`, `attribute_values`, `inserted_date`, `inserted_by`, `last_update`, `update_by`) VALUES
(1, 'Name', 1, 'no', 1, 1, '', '2015-05-24 16:10:14', 1, '0000-00-00', 0);

-- --------------------------------------------------------

--
-- Table structure for table `form_inserted_data`
--

CREATE TABLE `form_inserted_data` (
  `id` int(11) NOT NULL,
  `form_id` int(11) NOT NULL,
  `inserted_date` datetime NOT NULL,
  `label` varchar(25) NOT NULL,
  `value` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `general_setting`
--

CREATE TABLE `general_setting` (
  `id` int(11) NOT NULL,
  `title` varchar(256) NOT NULL,
  `site_url` varchar(500) NOT NULL,
  `meta_key` text NOT NULL,
  `email` varchar(256) NOT NULL,
  `time_zone_id` int(11) NOT NULL,
  `front_lang_id` int(11) NOT NULL,
  `translate_lang_id` int(11) NOT NULL,
  `enable_website` enum('yes','no') NOT NULL,
  `offline_messages` longtext NOT NULL,
  `description` longtext NOT NULL,
  `google_analitic` longtext CHARACTER SET utf16 NOT NULL,
  `main_order_statues` int(11) NOT NULL,
  `enable_store` enum('yes','no') NOT NULL,
  `update_by` int(11) NOT NULL,
  `last_update` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `general_setting`
--

INSERT INTO `general_setting` (`id`, `title`, `site_url`, `meta_key`, `email`, `time_zone_id`, `front_lang_id`, `translate_lang_id`, `enable_website`, `offline_messages`, `description`, `google_analitic`, `main_order_statues`, `enable_store`, `update_by`, `last_update`) VALUES
(1, 'Welcome To Essam Baligh Labs|أهلا بكم فى معامل الدكتور عصام بليغ', 'localhost/hilton', 'website design in Egypt, web development in egypt,Mobile solutions, web applications, web based applications, web marketing, web services, seo, search engine optimization||', 'emadtab97@gmail.com', 35, 1, 1, 'yes', '7567', 'Diva company provides the latest technologies in web design and web development in Egypt & ME providing responsive and good user experience interface||', '767', 8, '', 1, '2016-10-04 14:44:37');

-- --------------------------------------------------------

--
-- Table structure for table `home_page_layout`
--

CREATE TABLE `home_page_layout` (
  `id` int(11) NOT NULL,
  `title` varchar(250) NOT NULL,
  `status` enum('publish','draft') NOT NULL,
  `position` int(11) NOT NULL,
  `plugin` int(11) NOT NULL,
  `header_title` varchar(500) NOT NULL,
  `plugin_value` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `home_page_layout`
--

INSERT INTO `home_page_layout` (`id`, `title`, `status`, `position`, `plugin`, `header_title`, `plugin_value`) VALUES
(1, 'position one ', 'publish', 1, 38, '', ''),
(2, 'postion2', 'publish', 3, 39, '', ''),
(3, '3', 'publish', 2, 40, '', ''),
(4, '7', 'publish', 9, 41, 'Latest News||Ø§Ø®Ø± Ø§Ù„Ø§Ø®Ø¨Ø§Ø±', '2'),
(5, '9', 'publish', 7, 42, 'Important links||Ø±ÙˆØ§Ø¨Ø· Ù…Ù‡Ù…Ø©', 'main_menu'),
(6, '8', 'publish', 8, 43, 'Upcoming Event ||Ø§Ø®Ø± Ø§Ù„Ø§Ø­Ø¯Ø§Ø«', '12');

-- --------------------------------------------------------

--
-- Table structure for table `localization`
--

CREATE TABLE `localization` (
  `id` int(11) NOT NULL,
  `name` varchar(25) NOT NULL,
  `sorting` int(11) NOT NULL,
  `status` enum('active','disable') NOT NULL,
  `label` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `localization`
--

INSERT INTO `localization` (`id`, `name`, `sorting`, `status`, `label`) VALUES
(1, 'English', 1, 'active', 'en'),
(2, 'Arabic', 2, 'active', 'ar');

-- --------------------------------------------------------

--
-- Table structure for table `membership_inquery`
--

CREATE TABLE `membership_inquery` (
  `id` int(11) NOT NULL,
  `customer_name` varchar(150) NOT NULL,
  `customer_email` varchar(150) NOT NULL,
  `customer_position` varchar(150) NOT NULL,
  `customer_company` varchar(150) NOT NULL,
  `customer_mobile` varchar(150) NOT NULL,
  `customer_fax` varchar(150) NOT NULL,
  `customer_marital_status` varchar(150) NOT NULL,
  `customer_credit_type` varchar(150) NOT NULL,
  `customer_graduation_year` varchar(150) NOT NULL,
  `customer_education_year` varchar(150) NOT NULL,
  `customer_nationality` varchar(150) NOT NULL,
  `customer_residence` varchar(150) NOT NULL,
  `customer_monthly_income` varchar(150) NOT NULL,
  `cutomer_traveled_country` varchar(150) NOT NULL,
  `cutomer_traveled_hotel` varchar(150) NOT NULL,
  `customer_traveled_year` varchar(150) NOT NULL,
  `customer_comment` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `membership_inquery`
--

INSERT INTO `membership_inquery` (`id`, `customer_name`, `customer_email`, `customer_position`, `customer_company`, `customer_mobile`, `customer_fax`, `customer_marital_status`, `customer_credit_type`, `customer_graduation_year`, `customer_education_year`, `customer_nationality`, `customer_residence`, `customer_monthly_income`, `cutomer_traveled_country`, `cutomer_traveled_hotel`, `customer_traveled_year`, `customer_comment`) VALUES
(1, 'emad rashad', 'emadtab97@gmail.com', 'Backend dev', 'Diva', '010206903031', '++2336', 'maried', 'visa', '2012', '2015', 'Egyptian', 'egypt', '!!!!!', 'Usa', 'Hilton', '2012', 'It was very nice experience'),
(2, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(3, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(6, 'صثص', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(7, 'fdfd', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(8, 'ffg', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(9, 'sdssd', 'dsdsds', 'sdsd', 'sdsd', 'sdsds', 'dsdsd', 'single', 'credit one', '2016', '2013', 'germany', 'sdsdsd', '3000$', 'norway', 'Hotel one', '2016', 'sdsdsdsdsdsds');

-- --------------------------------------------------------

--
-- Table structure for table `nodes`
--

CREATE TABLE `nodes` (
  `id` int(11) NOT NULL,
  `status` enum('draft','publish') NOT NULL,
  `enable_summary` enum('yes','no') NOT NULL,
  `inserted_date` datetime NOT NULL,
  `inserted_by` int(11) NOT NULL,
  `last_update` datetime NOT NULL,
  `update_by` int(11) NOT NULL,
  `start_publishing` datetime NOT NULL,
  `end_publishing` datetime NOT NULL,
  `enable_comments` enum('yes','no') NOT NULL,
  `front_page` enum('yes','no') NOT NULL,
  `slide_show` enum('yes','no') NOT NULL,
  `side_bar` enum('yes','no') NOT NULL,
  `cover_image` varchar(250) NOT NULL,
  `slider_cover` varchar(250) NOT NULL,
  `node_type` enum('post','page','event') NOT NULL,
  `place` varchar(256) NOT NULL,
  `start_date` datetime NOT NULL,
  `end_date` datetime NOT NULL,
  `model` int(11) NOT NULL,
  `shortcut_link` varchar(35) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `nodes`
--

INSERT INTO `nodes` (`id`, `status`, `enable_summary`, `inserted_date`, `inserted_by`, `last_update`, `update_by`, `start_publishing`, `end_publishing`, `enable_comments`, `front_page`, `slide_show`, `side_bar`, `cover_image`, `slider_cover`, `node_type`, `place`, `start_date`, `end_date`, `model`, `shortcut_link`) VALUES
(1, 'publish', 'no', '2016-10-04 15:25:42', 1, '2016-10-05 14:25:43', 1, '2016-10-04 15:25:42', '0000-00-00 00:00:00', 'no', 'no', 'no', 'no', '', '', 'page', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 3, '1475587542'),
(2, 'publish', 'no', '2016-10-04 15:26:03', 1, '2016-10-05 15:28:15', 1, '2016-10-04 15:26:03', '0000-00-00 00:00:00', 'no', 'no', 'no', 'no', '', '', 'page', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 16, '1475587563'),
(3, 'publish', 'no', '2016-10-04 15:26:34', 1, '2016-10-25 15:21:26', 1, '2016-10-04 15:26:34', '0000-00-00 00:00:00', 'no', 'no', 'no', 'no', 'images/v-img.jpg  ', '', 'page', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 3, '1475587594'),
(4, 'publish', 'no', '2016-10-04 15:31:15', 1, '2016-10-13 11:47:09', 1, '2016-10-04 15:31:15', '0000-00-00 00:00:00', 'no', 'no', 'no', 'no', '', '', 'page', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 21, '1475587875'),
(5, 'publish', 'no', '2016-10-04 16:00:11', 1, '0000-00-00 00:00:00', 0, '2016-10-04 16:00:11', '0000-00-00 00:00:00', 'no', 'no', 'no', 'no', '', '', 'page', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 3, '1475589611'),
(6, 'publish', 'no', '2016-10-04 16:27:50', 1, '2016-10-11 15:05:18', 1, '2016-10-04 16:27:50', '0000-00-00 00:00:00', 'no', 'yes', 'yes', 'no', 'Services%20Categories%20Images/servi.jpg', 'slider1.jpg', 'post', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, '1475591270'),
(7, 'publish', 'no', '2016-10-04 16:44:57', 1, '2016-10-04 16:50:08', 1, '2016-10-04 16:44:57', '0000-00-00 00:00:00', 'no', 'yes', 'yes', 'no', '', 'slider1.jpg', 'post', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, '1475592297'),
(8, 'publish', 'no', '2016-10-04 17:02:46', 1, '2016-10-05 10:25:37', 1, '2016-10-04 17:02:46', '0000-00-00 00:00:00', 'no', 'no', 'no', 'no', '', '', 'post', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, '1475593366'),
(9, 'publish', 'no', '2016-10-05 11:20:37', 1, '2016-10-05 11:22:53', 1, '2016-10-05 11:20:37', '0000-00-00 00:00:00', 'no', 'no', 'no', 'no', '', '', 'post', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, '1475659237'),
(10, 'publish', 'no', '2016-10-05 11:27:44', 1, '2016-10-05 11:46:51', 1, '2016-10-05 11:27:44', '0000-00-00 00:00:00', 'no', 'no', 'no', 'no', 'images/bg1.jpg ', '', 'post', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, '1475659664'),
(11, 'publish', 'no', '2016-10-05 12:24:15', 1, '2016-10-05 12:24:22', 1, '2016-10-05 12:24:15', '0000-00-00 00:00:00', 'no', 'no', 'no', 'no', 'images/post.jpg', '', 'post', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, '1475663055'),
(13, 'publish', 'no', '2016-10-05 17:52:15', 1, '2016-10-16 10:04:27', 1, '2016-10-05 17:52:15', '0000-00-00 00:00:00', 'no', 'no', 'no', 'no', 'images/doctor1.jpg ', '', 'post', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, '1475682735'),
(14, 'publish', 'no', '2016-10-05 18:05:28', 1, '2016-10-16 10:04:55', 1, '2016-10-05 18:05:28', '0000-00-00 00:00:00', 'no', 'no', 'no', 'no', 'images/v-img.jpg ', '', 'post', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, '1475683528'),
(16, 'publish', 'no', '2016-10-10 10:11:06', 1, '2016-10-10 10:42:15', 1, '2016-10-10 10:11:06', '0000-00-00 00:00:00', 'no', 'no', 'no', 'no', '', '', 'page', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 18, '1476087066'),
(17, 'publish', 'no', '2016-10-10 10:50:55', 1, '2016-10-16 13:33:05', 1, '2016-10-10 10:50:55', '0000-00-00 00:00:00', 'no', 'no', 'no', 'no', 'images/doctor1.jpg  ', '', 'post', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, '1476089455'),
(18, 'publish', 'no', '2016-10-10 12:22:48', 1, '2016-10-16 16:08:00', 1, '2016-10-10 12:22:48', '0000-00-00 00:00:00', 'no', 'no', 'no', 'no', 'images/post.jpg               ', '', 'post', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 23, '1476094968'),
(19, 'publish', 'no', '2016-10-10 13:27:02', 1, '2016-10-10 13:59:23', 1, '2016-10-10 13:27:02', '0000-00-00 00:00:00', 'no', 'no', 'no', 'no', '', '', 'page', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 19, '1476098822'),
(20, 'publish', 'no', '2016-10-10 13:45:25', 1, '2016-10-10 16:00:01', 1, '2016-10-10 13:45:25', '0000-00-00 00:00:00', 'no', 'no', 'no', 'no', 'images/doctor1.jpg  ', '', 'post', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, '1476099925'),
(21, 'publish', 'no', '2016-10-10 15:22:32', 1, '2016-10-10 16:00:25', 1, '2016-10-10 15:22:32', '0000-00-00 00:00:00', 'no', 'no', 'no', 'no', 'images/post.jpg', '', 'post', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, '1476105752'),
(22, 'publish', 'no', '2016-10-11 13:59:31', 1, '0000-00-00 00:00:00', 0, '2016-10-11 13:59:31', '0000-00-00 00:00:00', 'no', 'no', 'no', 'no', '', '', 'page', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 20, '1476187171'),
(24, 'publish', 'no', '2016-10-11 14:11:38', 1, '2016-10-11 14:22:34', 1, '2016-10-11 14:11:38', '0000-00-00 00:00:00', 'no', 'no', 'no', 'no', 'Doctors/doctor1.jpg ', '', 'post', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, '1476187898'),
(25, 'publish', 'no', '2016-10-11 14:12:25', 1, '2016-10-11 14:22:15', 1, '2016-10-11 14:12:25', '0000-00-00 00:00:00', 'no', 'no', 'no', 'no', 'Doctors/doctor1.jpg ', '', 'post', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, '1476187945'),
(26, 'publish', 'no', '2016-10-11 14:35:29', 1, '2016-10-11 14:35:35', 1, '2016-10-11 14:35:29', '0000-00-00 00:00:00', 'no', 'no', 'no', 'no', 'Doctors/doctor1.jpg', '', 'post', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, '1476189329'),
(29, 'publish', 'no', '2016-10-13 11:41:56', 1, '2016-10-16 13:32:33', 1, '2016-10-13 11:41:56', '0000-00-00 00:00:00', 'no', 'no', 'no', 'no', '', '', 'post', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, '1476351716'),
(30, 'publish', 'no', '2016-10-13 11:42:22', 1, '2016-10-13 13:42:26', 1, '2016-10-13 11:42:22', '0000-00-00 00:00:00', 'no', 'no', 'no', 'no', '', '', 'post', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, '1476351742'),
(31, 'publish', 'no', '2016-10-13 11:42:49', 1, '2016-10-25 15:24:02', 1, '2016-10-13 11:42:49', '0000-00-00 00:00:00', 'no', 'no', 'no', 'no', '', '', 'post', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, '1476351769'),
(32, 'publish', 'no', '2016-10-16 10:00:03', 1, '2016-10-16 11:53:28', 1, '2016-10-16 10:00:03', '0000-00-00 00:00:00', 'no', 'no', 'no', 'no', '', '', 'page', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 17, '1476604803'),
(33, 'publish', 'no', '2016-10-16 10:56:59', 1, '2016-10-16 11:53:15', 1, '2016-10-16 10:56:59', '0000-00-00 00:00:00', 'no', 'no', 'no', 'no', '', '', 'page', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 17, '1476608219'),
(34, 'publish', 'no', '2016-10-16 11:01:09', 1, '2016-10-16 11:08:42', 1, '2016-10-16 11:01:09', '0000-00-00 00:00:00', 'no', 'no', 'no', 'no', '', '', 'page', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 17, '1476608469'),
(35, 'publish', 'no', '2016-10-16 11:51:11', 1, '2016-10-16 15:45:35', 1, '2016-10-16 11:51:11', '0000-00-00 00:00:00', 'no', 'no', 'no', 'no', 'images/doctor1.jpg                       ', '', 'post', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 23, '1476611471'),
(36, 'publish', 'no', '2016-10-16 12:01:37', 1, '2016-10-16 16:07:45', 1, '2016-10-16 12:01:37', '0000-00-00 00:00:00', 'no', 'no', 'no', 'no', 'images/v-img.jpg  ', '', 'post', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 23, '1476612097'),
(37, 'publish', 'no', '2016-10-16 12:20:09', 1, '2016-10-16 16:07:35', 1, '2016-10-16 12:20:09', '0000-00-00 00:00:00', 'no', 'no', 'no', 'no', 'images/post.jpg  ', '', 'post', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 23, '1476613209'),
(38, 'publish', 'no', '2016-10-16 12:58:09', 1, '2016-10-16 13:24:47', 1, '2016-10-16 12:58:09', '0000-00-00 00:00:00', 'no', 'no', 'no', 'no', '', '', 'page', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 22, '1476615489'),
(39, 'publish', 'no', '2016-10-16 13:35:31', 1, '2016-10-16 14:28:49', 1, '2016-10-16 13:35:31', '0000-00-00 00:00:00', 'no', 'no', 'no', 'no', 'images/v-img.jpg ', '', 'post', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, '1476617731'),
(40, 'publish', 'no', '2016-10-16 13:37:06', 1, '2016-10-16 14:29:02', 1, '2016-10-16 13:37:06', '0000-00-00 00:00:00', 'no', 'no', 'no', 'no', 'images/doctor1.jpg  ', '', 'post', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 23, '1476617826');

-- --------------------------------------------------------

--
-- Table structure for table `nodes_content`
--

CREATE TABLE `nodes_content` (
  `id` int(11) NOT NULL,
  `node_id` int(11) NOT NULL,
  `lang_id` int(11) NOT NULL,
  `title` varchar(250) NOT NULL,
  `alias` varchar(250) NOT NULL,
  `summary` text NOT NULL,
  `body` longtext NOT NULL,
  `meta_keys` varchar(250) NOT NULL,
  `meta_description` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `nodes_content`
--

INSERT INTO `nodes_content` (`id`, `node_id`, `lang_id`, `title`, `alias`, `summary`, `body`, `meta_keys`, `meta_description`) VALUES
(1, 1, 2, 'من نحن', 'من_نحن', '', '<section class="col-md-12 vision"><!--start of vision-->\n<h2 class="c-red"><span class="c-grey">نصائح المعمل //</span></h2>\n<p class="c-black">مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار ر مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار ر ر ر مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبارر مجرد بيانات اختبار مجرد بيانات اختبار مجرد ب.</p>\n<div class="col-md-12 vision-img"><!--vision-img--> <img src="media-library/images/v-img.jpg" alt="" /></div>\n<!--end vision-img-->\n<div class="col-md-12 wrapping4 clearfix"><!--wrapping4-->\n<h2 class="c-red text-left"><span class="c-grey">الهدف //</span></h2>\n<div class="col-md-5"><img src="media-library/images/post.jpg" alt="" /></div>\n<div class="col-md-7">\n<p class="c-black">مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار ر مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار ر ر ر مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبارر مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار  مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار</p>\n<p class="c-black">مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبارمجرد بيانات اختبارمجرد بيانات اختبار مجرد بيانات اختبارمجرد بيانات اختبار مجرد بيانات اختبارمجرد بيانات اختبارمجرد بيانات اختبار  </p>\n<p class="c-black">مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبارمجرد بيانات اختبارمجرد بيانات اختبار مجرد بيانات اختبارمجرد بيانات اختبار مجرد بيانات اختبارمجرد بيانات اختبارمجرد بيانات اختبار  </p>\n</div>\n</div>\n<!--end wrapping4-->\n<div class="col-md-12 wrapping4 clearfix"><!--wrapping4-->\n<h2 class="c-red text-left"><span class="c-grey">الهدف //</span></h2>\n<div class="col-md-7">\n<p class="c-black">مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار ر مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار ر ر ر مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبارر مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار  مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار</p>\n<p class="c-black">مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبارمجرد بيانات اختبارمجرد بيانات اختبار مجرد بيانات اختبارمجرد بيانات اختبار مجرد بيانات اختبارمجرد بيانات اختبارمجرد بيانات اختبار  </p>\n</div>\n<div class="col-md-5"><img src="media-library/images/post.jpg" alt="" /></div>\n</div>\n<!--end wrapping4--></section>\n<p><!--end of vision--></p>', '', ''),
(2, 1, 1, 'About Us', 'about_us', '', '<h2 class="c-red">Lab Advice <span class="c-grey">//</span></h2>\n<p class="c-black">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem IpsumLorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s stand.</p>\n<div class="col-md-12 vision-img"><img src="media-library/images/v-img.jpg" alt="" /></div>\n<div class="col-md-12 wrapping4 clearfix">\n<h2 class="c-red text-left">Target<span class="c-grey">//</span></h2>\n<div class="col-md-5"><img src="media-library/images/post.jpg" alt="" /></div>\n<div class="col-md-7">\n<p class="c-black">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem IpsumLorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s stand</p>\n<p class="c-black">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of</p>\n</div>\n</div>\n<div class="col-md-12 wrapping4 clearfix">\n<h2 class="c-red text-left">Target<span class="c-grey">//</span></h2>\n<div class="col-md-7">\n<p class="c-black">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem IpsumLorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s stand</p>\n<p class="c-black">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with thef</p>\n</div>\n<div class="col-md-5"><img src="media-library/images/post.jpg" alt="" /></div>\n</div>', '', ''),
(3, 2, 2, 'الخدمات', 'الخدمات', '', '', '', ''),
(4, 2, 1, 'Services', 'services', '', '', '', ''),
(5, 3, 2, 'الاسئله الشائعه', 'الاسئله_الشائعه', '', '<p>مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار </p>', '', ''),
(6, 3, 1, 'FAQ', 'faq', '', '<p>Test Info for now Test Info for now Test Info for now Test Info for now Test Info for now Test Info for now Test Info for now Test Info for now Test Info for now Test Info for now Test Info for now Test Info for now Test Info for now Test Info for now Test Info for now Test Info for now Test Info for now Test Info for now Test Info for now Test Info for now Test Info for now Test Info for now Test Info for now Test Info for now Test Info for now Test Info for now Test Info for now Test Info for now Test Info for now Test Info for now Test Info for n</p>', '', ''),
(7, 4, 2, 'اتصل بنا', 'اتصل_بنا', '', '', '', ''),
(8, 4, 1, 'Contact Us', 'contact_us', '', '', '', ''),
(9, 5, 2, 'صفحه البحث', 'صفحه_البحث', '', '', '', ''),
(10, 5, 1, 'Search', 'search', '', '', '', ''),
(11, 6, 2, 'سليدر 1', 'سليدر_1', '', '<p>اختبار اختبار اختبار اختبار اختبار اختبار اختبار اختبار اختبار اختبار اختبار اختبار </p>', '', ''),
(12, 6, 1, 'First Slide', 'first_slide', '<p>Dummmmmmmmmmmmmy Text here </p>', '<p>Test Test Test Test Test Test Test Test Test Test Test Test Test Test Test </p>', '', ''),
(13, 7, 2, 'سليد 2', 'سليد_2', '', '', '', ''),
(14, 7, 1, 'Slider 2', 'slider_2', '<p>hello from this language to another one we hope It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus</p>', '', '', ''),
(15, 8, 2, 'خدمات الاطباء', 'خدمات_الاطباء', '<div>\n<h2 class="text-left">Doctors services</h2>\n<p class="fa fa-angle-double-right"> </p>\n<ul class="col-md-5 service-shape list-inline left-shape ">\n<ul class="col-md-5 service-shape list-inline left-shape ">\n<li>Lorem Ipsum is simply dummy text of the printing and typesetting</li>\n</ul>\n</ul>\n<p class="fa fa-angle-double-right"> </p>\n<ul class="col-md-5 service-shape list-inline left-shape ">\n<ul class="col-md-5 service-shape list-inline left-shape ">\n<li>Lorem Ipsum is simply dummy text of</li>\n</ul>\n</ul>\n<p class="fa fa-angle-double-right"> </p>\n<ul class="col-md-5 service-shape list-inline left-shape ">\n<ul class="col-md-5 service-shape list-inline left-shape ">\n<li>Lorem Ipsum is simply dummy text of the printing and typesetting</li>\n</ul>\n</ul>\n<p class="fa fa-angle-double-right"> </p>\n<ul class="col-md-5 service-shape list-inline left-shape ">\n<li>Lorem Ipsum is simply dummy text of theprint</li>\n</ul>\n<h2 class="text-left">Patient Services</h2>\n<p class="fa fa-angle-double-right"> </p>\n<ul class="col-md-5 service-shape list-inline right-shape">\n<ul class="col-md-5 service-shape list-inline right-shape">\n<li>Lorem Ipsum is simply dummy text of the printing and typesetting</li>\n</ul>\n</ul>\n<p class="fa fa-angle-double-right"> </p>\n<ul class="col-md-5 service-shape list-inline right-shape">\n<ul class="col-md-5 service-shape list-inline right-shape">\n<li>Lorem Ipsum is simply dummy text of</li>\n</ul>\n</ul>\n<p class="fa fa-angle-double-right"> </p>\n<ul class="col-md-5 service-shape list-inline right-shape">\n<ul class="col-md-5 service-shape list-inline right-shape">\n<li>Lorem Ipsum is simply dummy text of the printing and typesetting</li>\n</ul>\n</ul>\n<p class="fa fa-angle-double-right"> </p>\n<ul class="col-md-5 service-shape list-inline right-shape">\n<li>Lorem Ipsum is simply dummy text of theprint</li>\n</ul>\n</div>', '', '', ''),
(16, 8, 1, 'Doctors And Patients Services', 'doctors_and_patients_services', '', '', '', ''),
(17, 9, 2, 'المعمل واخر الاخبار', 'المعمل_واخر_الاخبار', '<h2 class="c-red"><span class="c-grey">نصائح المعمل //</span></h2>\n<ul class="col-md-6">\n<li class="c-black">Lorem Ipsum is simply dummy text of the printing</li>\n<li class="c-black">Lorem Ipsum is simply dummy text of the printing</li>\n<li class="c-black">Lorem Ipsum is simply dummy text of the printing</li>\n<li class="c-black">Lorem Ipsum is simply dummy text of the printing</li>\n</ul>\n<h2 class="c-red"><span class="c-grey">اخر الاخبار //</span></h2>\n<ul class="col-md-6">\n<li class="c-black">Lorem Ipsum is simply dummy text of the printing</li>\n<li class="c-black">Lorem Ipsum is simply dummy text of the printing</li>\n<li class="c-black">Lorem Ipsum is simply dummy text of the printing</li>\n<li class="c-black">Lorem Ipsum is simply dummy text of the printing</li>\n</ul>', '', '', ''),
(18, 9, 1, 'Lab And Latest News', 'lab_and_latest_news', '<h2 class="c-red">Lab Advice <span class="c-grey">//</span></h2>\n<ul class="col-md-6">\n<li class="c-black">Lorem Ipsum is simply dummy text of the printing</li>\n<li class="c-black">Lorem Ipsum is simply dummy text of the printing</li>\n<li class="c-black">Lorem Ipsum is simply dummy text of the printing</li>\n<li class="c-black">Lorem Ipsum is simply dummy text of the printing</li>\n</ul>\n<h2 class="c-red">Latest news <span class="c-grey">//</span></h2>\n<ul class="col-md-6">\n<li class="c-black">Lorem Ipsum is simply dummy text of the printing</li>\n<li class="c-black">Lorem Ipsum is simply dummy text of the printing</li>\n<li class="c-black">Lorem Ipsum is simply dummy text of the printing</li>\n<li class="c-black">Lorem Ipsum is simply dummy text of the printing</li>\n</ul>', '', '', ''),
(19, 10, 2, 'اخبار مع الصوره', 'اخبار_مع_الصوره', '<p>الخبر الرئيسى</p>', '<p>يواجه غالبية الطلاب الجامعيين في أميركا وبريطانيا مشكلة تغطية نفقاتهم التعليمية، خصوصاً مع ارتفاع فاتورة التعليم في هذه البلدان، الأمر الذي يدفعهم إلى الاقتراض لتغطية تلك النفقات، لكن طالبة بريطانية استطاعت التغلب على المشكلة، بعدما باعت ملابسها القديمة على شبكة الانترنت، وحققت مبيعات بمبلغ 30 ألف استرليني.</p>', '', ''),
(20, 10, 1, 'Main News with Image', 'main_news_with_image', '<p>What is Lorem Ipsum?</p>', '<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but.</p>', '', ''),
(21, 11, 2, 'الاعلان الرئيسى', 'الاعلان_الرئيسى', '', '', '', ''),
(22, 11, 1, 'Main Advertisment', 'main_advertisment', '', '', '', ''),
(25, 13, 2, 'بوست رقم 1', 'بوست_رقم_1', '', '', '', ''),
(26, 13, 1, 'Test Post for First Sub Category', 'test_post_for_first_sub_category', '<p>this is atest for posts services </p>', '<p>this is atest for posts services  this is atest for posts services  this is atest for posts services  this is atest for posts services </p>', '', ''),
(27, 14, 2, 'تست 2', 'تست_2', '', '', '', ''),
(28, 14, 1, 'Test 2', 'test_2', '<p>dfdff</p>', '<p>dfdfdfdfdfdfdfdfdfd</p>', '', ''),
(31, 16, 2, 'الاخبار', 'الاخبار', '', '', '', ''),
(32, 16, 1, 'News', 'news', '', '', '', ''),
(33, 17, 2, 'خبر اختبار', 'خبر_اختبار', '<p>مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار </p>', '', '', ''),
(34, 17, 1, 'Test News', 'test_news', '<p>Test News Dummy Info Test News Dummy Info Test News Dummy Info Test News Dummy Info Test News Dummy Info Test News Dummy Info Test News Dummy Info Test News Dummy Info Test News Dummy Info Test News Dummy Info Test News Dummy Info Test News Dummy Info Test News Dummy Info Test News Dummy Info Test News Dummy Info Test News Dummy Info Test News Dummy Info Test News Dummy Info Test News Dummy Info Test News Dummy Info Test News Dummy Info Test News Dummy Info </p>', '', '', ''),
(35, 18, 2, 'خبر ااخر', 'خبر_ااخر', '<p>اختبار خبر جديد اختبار خبر جديد اختبار خبر جديد اختبار خبر جديد اختبار خبر جديد اختبار خبر جديد اختبار خبر جديد اختبار خبر جديد اختبار خبر جديد اختبار خبر جديد اختبار خبر جديد اختبار خبر جديد اختبار خبر جديد اختبار خبر جديد اختبار خبر جديد اختبار خبر جديد اختبار خبر جديد اختبار خبر جديد اختبار خبر جديد اختبار خبر جديد اختبار خبر جديد اختبار خبر جديد </p>', '<p>اختبار خبر جديد اختبار خبر جديد اختبار خبر جديد اختبار خبر جديد اختبار خبر جديد اختبار خبر جديد اختبار خبر جديد اختبار خبر جديد اختبار خبر جديد اختبار خبر جديد اختبار خبر جديد اختبار خبر جديد اختبار خبر جديد اختبار خبر جديد اختبار خبر جديد اختبار خبر جديد اختبار خبر جديد اختبار خبر جديد اختبار خبر جديد اختبار خبر جديد اختبار خبر جديد اختبار خبر جديد </p>', '', ''),
(36, 18, 1, 'Another News', 'another_news', '<p>this is test this is test this is test this is test this is test this is test this is test this is test this is test this is test this is test this is test this is test this is test this is test this is test this is test </p>', '<p>this is test this is test this is test this is test this is test this is test this is test this is test this is test this is test this is test this is test this is test this is test this is test this is test this is test</p>', '', ''),
(37, 19, 2, 'تفاصيل الخبر', 'تفاصيل_الخبر', '', '', '', ''),
(38, 19, 1, 'News Details', 'news_details', '', '', '', ''),
(39, 20, 2, 'خبر تصنيف واحد', 'خبر_تصنيف_واحد', '', '', '', ''),
(40, 20, 1, 'News cat one test', 'news_cat_one_test', '', '', '', ''),
(41, 21, 2, 'تست خبر', 'تست_خبر', '', '', '', ''),
(42, 21, 1, 'News cat 2', 'news_cat_2', '', '', '', ''),
(43, 22, 2, 'الاطباء', 'الاطباء', '', '', '', ''),
(44, 22, 1, 'Doctors', 'doctors', '', '', '', ''),
(47, 24, 2, 'دكتور 2', 'دكتور_2', '<p>مسالك بوليه</p>', '', '', ''),
(48, 24, 1, 'Doctor 2', 'doctor_2', '<p>this is Test Job</p>', '', '', ''),
(49, 25, 2, 'دكتور 3', 'دكتور_3', '<p>باطنه</p>', '', '', ''),
(50, 25, 1, 'Doctor 3', 'doctor_3', '<p>Test Job</p>', '', '', ''),
(51, 26, 2, 'دكتور 4', 'دكتور_4', '<p>قلب ورئه</p>', '', '', ''),
(52, 26, 1, 'Doctor 4', 'doctor_4', '<p>HEART</p>', '', '', ''),
(57, 29, 2, 'الفرع الاول', 'الفرع_الاول', '<div class="  contact-inner">\n<p class="text-center c-black c-inline"><span class="fa fa-phone c-red"> 02 010 023 2365</span></p>\n<p class="text-center c-black new-ph"><span class="fa fa-phone c-red"> 02 010 023 2365</span></p>\n</div>\n<div class=" contact-inner">\n<p class="text-center c-black c-inline"><span class="fa fa-phone c-red"> 02 010 023 2365</span></p>\n<p class="text-center c-black new-ph"><span class="fa fa-phone c-red"> 02 010 023 2365</span></p>\n</div>\n<div class=" contact-inner">\n<p class="text-center c-black c-inline"><span class="fa fa-envelope company@info.com c-red"> company@info.com</span></p>\n</div>', '', '', ''),
(58, 29, 1, 'Branch one', 'branch_one', '<div class="  contact-inner">\n<p class="text-center c-black c-inline"><span class="fa fa-phone c-red"> 02 010 023 2365</span></p>\n<p class="text-center c-black new-ph"><span class="fa fa-phone c-red"> 02 010 023 2365</span></p>\n</div>\n<div class=" contact-inner">\n<p class="text-center c-black c-inline"><span class="fa fa-phone c-red"> 02 010 023 2365</span></p>\n<p class="text-center c-black new-ph"><span class="fa fa-phone c-red"> 02 010 023 2365</span></p>\n</div>\n<div class=" contact-inner">\n<p class="text-center c-black c-inline"><span class="fa fa-envelope company@info.com c-red"> company@info.com</span></p>\n</div>', '', '', ''),
(59, 30, 2, 'الفرع الثانى', 'الفرع_الثانى', '<div class="  contact-inner">\n<p class="text-center c-black c-inline"><span class="fa fa-phone c-red"> 02 010 023 2365</span></p>\n<p class="text-center c-black new-ph"><span class="fa fa-phone c-red"> 02 010 023 2365</span></p>\n</div>\n<div class=" contact-inner">\n<p class="text-center c-black c-inline"><span class="fa fa-phone c-red"> 02 010 023 2365</span></p>\n<p class="text-center c-black new-ph"><span class="fa fa-phone c-red"> 02 010 023 2365</span></p>\n</div>\n<div class=" contact-inner">\n<p class="text-center c-black c-inline"><span class="fa fa-envelope company@info.com c-red"> company@info.com</span></p>\n</div>', '', '', ''),
(60, 30, 1, 'Branch Two', 'branch_two', '<div class="  contact-inner">\n<p class="text-center c-black c-inline"><span class="fa fa-phone c-red"> 02 010 023 2365</span></p>\n<p class="text-center c-black new-ph"><span class="fa fa-phone c-red"> 02 010 023 2365</span></p>\n</div>\n<div class=" contact-inner">\n<p class="text-center c-black c-inline"><span class="fa fa-phone c-red"> 02 010 023 2365</span></p>\n<p class="text-center c-black new-ph"><span class="fa fa-phone c-red"> 02 010 023 2365</span></p>\n</div>\n<div class=" contact-inner">\n<p class="text-center c-black c-inline"><span class="fa fa-envelope company@info.com c-red"> company@info.com</span></p>\n</div>', '', '', ''),
(61, 31, 2, 'الفرع الثالث', 'الفرع_الثالث', '<div class="  contact-inner">\n<p class="text-center c-black c-inline"><span class="fa fa-phone c-red"> 02 111 023 2365</span></p>\n<p class="text-center c-black new-ph"><span class="fa fa-phone c-red"> 02 010 023 2365</span></p>\n</div>\n<div class=" contact-inner">\n<p class="text-center c-black c-inline"><span class="fa fa-phone c-red"> 02 010 023 2365</span></p>\n<p class="text-center c-black new-ph"><span class="fa fa-phone c-red"> 02 010 023 2365</span></p>\n</div>\n<div class=" contact-inner">\n<p class="text-center c-black c-inline"><span class="fa fa-envelope company@info.com c-red"> company@info.com</span></p>\n</div>', '', '', ''),
(62, 31, 1, 'Branch Three', 'branch_three', '<div class="  contact-inner">\n<p class="text-center c-black c-inline"><span class="fa fa-phone c-red"> 02 111 023 2365</span></p>\n<p class="text-center c-black new-ph"><span class="fa fa-phone c-red"> 02 010 023 2365</span></p>\n</div>\n<div class=" contact-inner">\n<p class="text-center c-black c-inline"><span class="fa fa-phone c-red"> 02 010 023 2365</span></p>\n<p class="text-center c-black new-ph"><span class="fa fa-phone c-red"> 02 010 023 2365</span></p>\n</div>\n<div class=" contact-inner">\n<p class="text-center c-black c-inline"><span class="fa fa-envelope company@info.com c-red"> company@info.com</span></p>\n</div>', '', '', ''),
(63, 32, 2, 'امراض الصدر', 'امراض_الصدر', '', '', '', ''),
(64, 32, 1, 'Breast Diseases', 'breast_diseases', '', '', '', ''),
(65, 33, 2, 'امراض القلب', 'امراض_القلب', '', '', '', ''),
(66, 33, 1, 'Heart diseases', 'heart_diseases', '', '', '', ''),
(67, 34, 2, 'امراض القدم', 'امراض_القدم', '', '', '', ''),
(68, 34, 1, 'foot Diseases', 'foot_diseases', '', '', '', ''),
(69, 35, 2, 'مقال اختبار', 'مقال_اختبار', '<p>مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار</p>', '<div class="col-md-8">\n<div class="clearfix wrapping5">\n<div class="col-md-12 servi-img"><img src="media-library/posts_images/" alt="" /></div>\n<div class="col-md-12 serv-text-wrapp">\n<h3 class="c-red">We offer you an all-new desalination</h3>\n<p class="c-black">The act states parties. Is the point of the Great Russian reins, but changes and Russian relationship. And not defending equivalent necessary. From both a relationship of nations acted to Klt the disposal of the parties either. Is the point of the Great Russian reins, but changes and Russian relationship. And not defending equivalent necessary. From both a relationship of nations acted to Klt the disposal of the parties either. Is the point of the Great Russian reins, but changes and Russian relationship. And not defending equivalent necessary. From both a relationship of nations acted each</p>\n<p class="c-black">The act states parties. Is the point of the Great Russian reins, but changes and Russian relationship. And not defending equivalent necessary. From both a relationship of nations acted to Klt the disposal of the parties either. Is the point of the Great Russian reins, but changes and Russian relationship. And not be defending.</p>\n<p class="c-black">The act states parties. Is the point of the Great Russian reins, but changes and Russian relationship. And not defending equivalent necessary. From both a relationship of nations acted each</p>\n</div>\n</div>\n</div>\n<div class="col-md-12 serv-text-wrapp">\n<h3 class="c-red">We offer you an all-new desalination</h3>\n<p class="c-black">The act states parties. Is the point of the Great Russian reins, but changes and Russian relationship. And not defending equivalent necessary. From both a relationship of nations acted to Klt the disposal of the parties either. Is the point of the Great Russian reins, but changes and Russian relationship. And not defending equivalent necessary. From both a relationship of nations acted to Klt the disposal of the parties either. Is the point of the Great Russian reins, but changes and Russian relationship. And not defending equivalent necessary. From both a relationship of nations acted each</p>\n<p class="c-black">The act states parties. Is the point of the Great Russian reins, but changes and Russian relationship. And not defending equivalent necessary. From both a relationship of nations acted to Klt the disposal of the parties either. Is the point of the Great Russian reins, but changes and Russian relationship. And not be defending</p>\n<p class="c-black">The act states parties. Is the point of the Great Russian reins, but changes and Russian relationship. And not defending equivalent necessary. From both a relationship of nations acted each</p>\n</div>', '', ''),
(70, 35, 1, 'Post Test', 'post_test', '<p>Test dummy data Test dummy data Test dummy data Test dummy data Test dummy data Test dummy data Test dummy data Test dummy data Test dummy data Test dummy data Test dummy data Test dummy data Test dummy data Test dummy data</p>', '<div class="col-md-8">\n<div class="clearfix wrapping5">\n<div class="col-md-12 servi-img"><img src="media-library/posts_images/" alt="" /></div>\n<div class="col-md-12 serv-text-wrapp">\n<h3 class="c-red">We offer you an all-new desalination</h3>\n<p class="c-black">The act states parties. Is the point of the Great Russian reins, but changes and Russian relationship. And not defending equivalent necessary. From both a relationship of nations acted to Klt the disposal of the parties either. Is the point of the Great Russian reins, but changes and Russian relationship. And not defending equivalent necessary. From both a relationship of nations acted to Klt the disposal of the parties either. Is the point of the Great Russian reins, but changes and Russian relationship. And not defending equivalent necessary. From both a relationship of nations acted each</p>\n<p class="c-black">The act states parties. Is the point of the Great Russian reins, but changes and Russian relationship. And not defending equivalent necessary. From both a relationship of nations acted to Klt the disposal of the parties either. Is the point of the Great Russian reins, but changes and Russian relationship. And not be defending.</p>\n<p class="c-black">The act states parties. Is the point of the Great Russian reins, but changes and Russian relationship. And not defending equivalent necessary. From both a relationship of nations acted each</p>\n</div>\n</div>\n</div>\n<div class="col-md-12 serv-text-wrapp">\n<h3 class="c-red">We offer you an all-new desalination</h3>\n<p class="c-black">The act states parties. Is the point of the Great Russian reins, but changes and Russian relationship. And not defending equivalent necessary. From both a relationship of nations acted to Klt the disposal of the parties either. Is the point of the Great Russian reins, but changes and Russian relationship. And not defending equivalent necessary. From both a relationship of nations acted to Klt the disposal of the parties either. Is the point of the Great Russian reins, but changes and Russian relationship. And not defending equivalent necessary. From both a relationship of nations acted each</p>\n<p class="c-black">The act states parties. Is the point of the Great Russian reins, but changes and Russian relationship. And not defending equivalent necessary. From both a relationship of nations acted to Klt the disposal of the parties either. Is the point of the Great Russian reins, but changes and Russian relationship. And not be defending</p>\n<p class="c-black">The act states parties. Is the point of the Great Russian reins, but changes and Russian relationship. And not defending equivalent necessary. From both a relationship of nations acted each</p>\n</div>', '', ''),
(71, 36, 2, 'مقال اختبار 2', 'مقال_اختبار_2', '<p>مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار مجرد بيانات اختبار </p>', '', '', ''),
(72, 36, 1, 'Post test 2', 'post_test_2', '<p>test dummy data test dummy data test dummy data test dummy data test dummy data test dummy data test dummy data test dummy data </p>', '', '', ''),
(73, 37, 2, 'بوست اختبار', 'بوست_اختبار', '', '', '', ''),
(74, 37, 1, 'Post new test', 'post_new_test', '', '', '', ''),
(75, 38, 2, 'اختبار التصنيف الفرعى', 'اختبار_التصنيف_الفرعى', '', '', '', ''),
(76, 38, 1, 'test sub category', 'test_sub_category', '', '', '', ''),
(77, 39, 2, 'بوست اختبار تصنيف فرعى فرعى', 'بوست_اختبار_تصنيف_فرعى_فرعى', '<p>بيانات اختبار بيانات اختبار بيانات اختبار بيانات اختبار بيانات اختبار بيانات اختبار بيانات اختبار بيانات اختبار بيانات اختبار بيانات اختبار بيانات اختبار بيانات اختبار بيانات اختبار بيانات اختبار بيانات اختبار </p>', '', '', ''),
(78, 39, 1, 'Post for sub sub category', 'post_for_sub_sub_category', '<p>test dummy test dummy test dummy test dummy test dummy test dummy test dummy test dummy test dummy test dummy test dummy test dummy </p>', '', '', ''),
(79, 40, 2, 'بوست تانى', 'بوست_تانى', '<p>اختبار اختبار اختبار اختبار اختبار اختبار اختبار اختبار اختبار اختبار اختبار اختبار اختبار اختبار اختبار اختبار اختبار اختبار اختبار اختبار اختبار اختبار اختبار اختبار اختبار اختبار اختبار اختبار اختبار اختبار اختبار اختبار </p>', '', '', ''),
(80, 40, 1, 'Post 2 sub sub', 'post_2_sub_sub', '<p>test dummy test dummy test dummy test dummy test dummy test dummy test dummy test dummy test dummy test dummy test dummy </p>', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `nodes_image_gallery`
--

CREATE TABLE `nodes_image_gallery` (
  `id` int(11) NOT NULL,
  `type` enum('node','propertie') NOT NULL,
  `related_id` int(11) NOT NULL,
  `image` varchar(50) NOT NULL,
  `sort` varchar(5) NOT NULL,
  `caption` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `nodes_image_gallery`
--

INSERT INTO `nodes_image_gallery` (`id`, `type`, `related_id`, `image`, `sort`, `caption`) VALUES
(4, 'node', 16, 'News%20images%20folder/carousel-n.jpg', '', ''),
(5, 'node', 16, 'News%20images%20folder/test.jpg', '', ''),
(6, 'node', 16, 'News%20images%20folder/test_2.jpg', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `nodes_plugins_values`
--

CREATE TABLE `nodes_plugins_values` (
  `id` int(11) NOT NULL,
  `type` enum('post','page','event') NOT NULL,
  `title` varchar(250) NOT NULL,
  `node_id` int(11) NOT NULL,
  `plugin_id` int(11) NOT NULL,
  `lang_id` int(11) NOT NULL,
  `content` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `nodes_plugins_values`
--

INSERT INTO `nodes_plugins_values` (`id`, `type`, `title`, `node_id`, `plugin_id`, `lang_id`, `content`) VALUES
(1, 'post', 'test', 10, 44, 1, '5'),
(2, 'post', 'test', 10, 44, 2, '4'),
(3, 'post', 'Latest News', 10, 46, 1, '2'),
(4, 'post', 'Ø£Ø­Ø¯Ø« Ø§Ù„Ø£Ø®Ø¨Ø§Ø±', 10, 46, 2, '2'),
(5, 'post', '', 10, 45, 1, ''),
(6, 'post', '', 10, 45, 2, ''),
(7, 'post', 'test', 2, 44, 1, '1'),
(8, 'post', '', 2, 44, 2, '0'),
(9, 'post', 'rtyret', 2, 46, 1, '2'),
(10, 'post', 'tete', 2, 46, 2, '2'),
(11, 'post', '', 2, 45, 1, ''),
(12, 'post', '', 2, 45, 2, ''),
(13, 'post', 'Upcoming Events', 2, 47, 1, '12'),
(14, 'post', 'Ø§Ù„Ø£Ø­Ø¯Ø§Ø« Ø§Ù„Ù‚Ø§Ø¯Ù…Ø©', 2, 47, 2, '12'),
(15, 'event', 'Social Media', 11, 44, 1, '5'),
(16, 'event', '', 11, 44, 2, '0'),
(17, 'event', 'Upcoming Events', 11, 47, 1, '12'),
(18, 'event', '', 11, 47, 2, '0'),
(19, 'page', 'upcoming events ', 13, 47, 1, '12'),
(20, 'page', '', 13, 47, 2, '0'),
(21, 'page', 'Latest News', 13, 46, 1, '2'),
(22, 'page', '', 13, 46, 2, '0'),
(23, 'page', 'Important lINKS ', 13, 44, 1, '1'),
(24, 'page', '', 13, 44, 2, '0'),
(25, 'post', '', 10, 47, 1, ''),
(26, 'post', '', 10, 47, 2, '');

-- --------------------------------------------------------

--
-- Table structure for table `nodes_selected_taxonomies`
--

CREATE TABLE `nodes_selected_taxonomies` (
  `id` int(11) NOT NULL,
  `taxonomy_id` int(11) NOT NULL,
  `node_id` int(11) NOT NULL,
  `taxonomy_type` enum('tag','category','author','country') NOT NULL,
  `node_type` enum('post','page','event','faq','product') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `nodes_selected_taxonomies`
--

INSERT INTO `nodes_selected_taxonomies` (`id`, `taxonomy_id`, `node_id`, `taxonomy_type`, `node_type`) VALUES
(6, 4, 12, 'category', 'post'),
(13, 4, 27, 'category', 'page'),
(14, 4, 18, 'category', 'post'),
(25, 6, 33, 'category', 'post'),
(27, 6, 34, 'category', 'post'),
(36, 6, 36, 'category', 'post'),
(43, 7, 38, 'category', 'post'),
(48, 10, 44, 'category', 'post'),
(53, 6, 43, 'category', 'post'),
(55, 1, 2, 'category', 'page'),
(62, 7, 16, 'category', 'page'),
(63, 7, 17, 'category', 'post'),
(65, 7, 19, 'category', 'page'),
(69, 7, 21, 'category', 'post'),
(70, 9, 21, 'category', 'post'),
(71, 7, 20, 'category', 'post'),
(72, 8, 17, 'category', 'post'),
(73, 9, 20, 'category', 'post'),
(77, 10, 22, 'category', 'page'),
(79, 10, 24, 'category', 'post'),
(80, 10, 25, 'category', 'post'),
(81, 10, 26, 'category', 'post'),
(88, 15, 31, 'category', 'post'),
(89, 15, 30, 'category', 'post'),
(90, 15, 29, 'category', 'post'),
(91, 15, 4, 'category', 'page'),
(92, 2, 32, 'category', 'page'),
(95, 4, 33, 'category', 'page'),
(96, 6, 34, 'category', 'page'),
(97, 1, 34, 'category', 'page'),
(101, 4, 35, 'category', 'post'),
(102, 6, 37, 'category', 'post'),
(103, 5, 38, 'category', 'page'),
(104, 5, 39, 'category', 'post'),
(105, 5, 40, 'category', 'post');

-- --------------------------------------------------------

--
-- Table structure for table `plugins`
--

CREATE TABLE `plugins` (
  `id` int(11) NOT NULL,
  `name` varchar(150) NOT NULL,
  `description` text NOT NULL,
  `version` varchar(50) NOT NULL,
  `author` varchar(150) NOT NULL,
  `uploaded_date` datetime NOT NULL,
  `uploaded_by` int(11) NOT NULL,
  `source` varchar(150) NOT NULL,
  `enable_options` enum('yes','no') NOT NULL,
  `enable_translate` enum('yes','no') NOT NULL,
  `enable_event` enum('no','yes') NOT NULL,
  `enable_post` enum('no','yes') NOT NULL,
  `enable_page` enum('no','yes') NOT NULL,
  `enable_index` enum('no','yes') NOT NULL,
  `position` enum('left','right','no_position') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `plugins`
--

INSERT INTO `plugins` (`id`, `name`, `description`, `version`, `author`, `uploaded_date`, `uploaded_by`, `source`, `enable_options`, `enable_translate`, `enable_event`, `enable_post`, `enable_page`, `enable_index`, `position`) VALUES
(1, 'Show Page details', 'this plug to Show Page details', '1.0', 'diva', '2015-07-29 20:21:57', 1, 'show_page_details', '', 'yes', 'no', 'no', 'yes', 'no', 'left'),
(3, 'Show contact us page details', 'this plug to Show contact us Page details', '1.0', 'diva', '2015-07-31 13:16:26', 1, 'contact_us_page', '', 'yes', 'no', 'no', 'yes', 'no', 'left'),
(7, 'Show post details', 'this plug to list to Show post details', '1.0', 'diva', '2015-08-12 15:24:19', 1, 'show_post_details', '', 'yes', 'no', 'yes', 'no', 'no', 'left'),
(34, 'Show event details', 'this plug to list to Show event details', '1.0', 'diva', '2016-01-24 17:07:36', 1, 'show_event_details', '', 'yes', 'yes', 'no', 'no', 'no', 'left'),
(35, 'posts list for page', 'posts_list_for_page', '1.0', 'diva', '2016-05-18 00:00:00', 1, 'posts_list_for_page', 'no', 'no', 'no', 'no', 'yes', 'no', 'left'),
(36, 'list events in brands page', 'this plug to list all brands in page', '1.0', 'diva', '2016-05-19 13:50:56', 1, 'page_list_brands', '', 'yes', 'no', 'no', 'yes', 'no', 'left'),
(38, 'Home page latest database', 'this plug to list all database in home page', '1.0', 'diva', '2016-06-05 13:07:27', 1, 'home_page_latest_data_base', '', 'yes', 'no', 'no', 'no', 'yes', 'left'),
(39, 'Home page latest database resourses', 'this plug to list all database  resourses in home page', '1.0', 'diva', '2016-06-05 13:07:33', 1, 'home_page_latest_data_base_resourses', '', 'yes', 'no', 'no', 'no', 'yes', 'left'),
(40, 'Home page latest Courses', 'this plug to list all Courses in home page', '1.0', 'diva', '2016-06-05 13:07:41', 1, 'home_page_upcoming_courses', '', 'yes', 'no', 'no', 'no', 'yes', 'left'),
(41, 'Sidebare latest news', 'this plug to latest news in side bar', '1.0', 'diva', '2016-06-05 13:07:55', 1, 'side_bar_latest_news', '', 'yes', 'no', 'no', 'no', 'yes', 'left'),
(42, 'Sidebare menu links', 'this plug to sidebar menu links in sidebar', '1.0', 'diva', '2016-06-05 13:08:02', 1, 'side_bar_menu_links', '', 'yes', 'no', 'no', 'no', 'yes', 'left'),
(43, 'sidebar  upcoming events', 'this plug to list all sidebar  upcoming events in side bar', '1.0', 'diva', '2016-06-05 13:08:11', 1, 'side_bar_upcoming_events', '', 'yes', 'no', 'no', 'no', 'yes', 'left'),
(44, 'sidebar menu link in internal page', 'this plugin to view menu link in sidebar bin internal page', '1.0', 'diva', '2016-06-07 09:17:43', 1, 'sidebar_menu_link_internal', 'yes', 'yes', 'yes', 'yes', 'yes', 'no', 'right'),
(45, 'sidebar text editor', 'this plug-in used to add free text in sidebar', '1.0', 'diva', '2016-06-07 09:50:35', 1, 'sidebar_texteditor', 'yes', 'yes', 'yes', 'yes', 'yes', 'no', 'right'),
(46, 'sidebar menu link in post by category page', 'this plugin to view menu link in sidebar bin internal post by category page', '1.0', 'diva', '2016-06-07 09:58:56', 1, 'side_bar_latest_posts_internal', 'yes', 'yes', 'yes', 'yes', 'yes', 'no', 'right'),
(47, 'sidebar Upcoming events', 'this plugin to view Upcoming events', '1.0', 'diva', '2016-06-07 11:00:00', 1, 'side_bar_upcoming_events_internal', 'yes', 'yes', 'yes', 'yes', 'yes', 'no', 'right'),
(48, 'list view events', 'this plug to list to view all events', '1.0', 'diva', '2016-06-07 12:12:33', 1, 'events_list_for_page', '', 'yes', 'no', 'no', 'yes', 'no', 'left'),
(49, 'parsing', 'this plug is to parse xml ', '1.0', '1', '2016-08-29 00:00:00', 1, 'parsing', '', '', '', '', 'yes', '', 'left'),
(50, 'membership', 'this plugin to show membership inquiry form', '1.0', 'diva', '2016-09-18 00:00:00', 1, 'membership', 'no', 'yes', 'no', 'no', 'yes', 'no', 'left'),
(51, 'offers plugin', 'this plug to show all posts for offers categories \r\n', '1.0', 'diva', '2016-09-19 00:00:00', 1, 'offers', 'no', 'yes', 'no', 'no', 'yes', 'no', 'left'),
(52, 'survey plugin', 'this plug to show survey  form  ', '1.0', 'diva', '2016-09-20 00:00:00', 1, 'survey', 'no', 'yes', 'no', 'no', 'yes', 'no', 'left'),
(53, 'testimonials plugin', 'this plug to list all testimonials under this page', '1.0', 'diva', '2016-09-20 00:00:00', 1, 'testimonials', 'no', 'yes', 'no', 'no', 'yes', 'no', 'left'),
(54, 'about sharm plugin', 'this plugin to show about plugin with gallery', '1.0', 'diva', '2016-09-20 00:00:00', 1, 'about', 'no', 'yes', 'no', 'no', 'yes', 'no', 'left'),
(55, 'page_gallery', 'this plug to insert photo gallery inside page ', '1.0', 'diva', '2016-09-20 00:00:00', 1, 'page_gallery', 'no', 'yes', 'no', 'no', 'yes', 'no', 'left'),
(56, 'about_part_2', 'test_part 2 of about ', '1.0', 'diva', '2016-09-20 00:00:00', 1, 'about_part_2', 'no', 'yes', 'no', 'no', 'yes', 'no', 'left'),
(57, 'spa_gallery', 'this plugin to insert gallery with short code inside spa page  ', '1.0', 'diva', '2016-09-20 00:00:00', 1, 'spa_gallery', 'no', 'yes', 'no', 'no', 'yes', 'no', 'left'),
(58, 'testimonials_for_spa', 'list three testimonials', '1.0', 'diva', '2016-09-20 00:00:00', 1, 'tetimonials_for_spa', 'no', 'yes', 'no', 'no', 'yes', 'no', 'left'),
(59, 'rooms plugin', 'to get posts for rooms inside rooms page', '1.0', 'diva', '2016-09-21 00:00:00', 1, 'rooms', 'no', 'yes', 'no', 'no', 'yes', 'no', 'left'),
(60, 'gallery_page plugin', 'to list all galleries inside this page and filtrate by category ', '1.0', 'diva', '2016-09-21 00:00:00', 1, 'gallery_page', 'no', 'yes', 'no', 'no', 'yes', 'no', 'left'),
(61, 'Services Categories', 'Show all services as categories ', '1.0', 'diva', '2016-10-05 00:00:00', 1, 'services', 'no', 'no', 'no', 'no', 'yes', 'no', 'left'),
(62, 'Services Categorized in all', 'this is the page that holds all services sidebar and all posted listed under chosen category  ', '1.0', 'diva', '2016-10-05 00:00:00', 1, 'services_categorized', 'no', 'no', 'no', 'no', 'yes', 'no', 'left'),
(63, 'news plugin', 'this plugin to list all news posts and news slider  ', '1.0', 'diva', '2016-10-10 00:00:00', 1, 'news_plugin', 'no', 'no', 'no', 'no', 'yes', 'no', 'left'),
(64, 'news_details_plugin_with_sidebar', 'this to list post details with a special way and showing related posts sidebar holding all posts related to sub category', '1.0', 'diva', '2016-10-10 00:00:00', 1, 'news_details_plugin', 'no', 'no', 'no', 'no', 'yes', 'no', 'left'),
(65, 'Doctors Plugin', 'this plug is to list all doctors posts under Docotrs page', '1.0', 'diva', '2016-10-11 00:00:00', 1, 'doctors_plugin', 'no', 'no', 'no', 'no', 'yes', 'no', 'left'),
(66, 'Branches in Contact', 'this plug is to plug all branches inside contact us  page  . \r\n', '1.0', 'diva', '2016-10-13 00:00:00', 1, 'barnches_to_contact', 'no', 'no', 'no', 'no', 'yes', 'no', 'left'),
(67, 'Sub sub categories plugin', 'this plugin to show all posts under specific sub sub category ', '1.0', 'diva', '2016-10-16 00:00:00', 1, 'services_subcats_categorized', 'no', 'no', 'no', 'no', 'yes', 'no', 'left'),
(68, 'Services Details plugin', 'this plug is to list a details of a specific services with a sidebar ', '1.0', 'diva', '2016-10-16 00:00:00', 1, 'services_details_plugin', 'no', 'no', 'no', 'yes', 'no', 'no', 'left');

-- --------------------------------------------------------

--
-- Table structure for table `poll_questions`
--

CREATE TABLE `poll_questions` (
  `id` int(11) NOT NULL,
  `poll` varchar(250) NOT NULL,
  `status` enum('publish','draft') NOT NULL,
  `inserted_by` int(11) NOT NULL,
  `inserted_date` datetime NOT NULL,
  `update_by` int(11) NOT NULL,
  `last_update` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `poll_questions_options`
--

CREATE TABLE `poll_questions_options` (
  `id` int(11) NOT NULL,
  `poll_id` int(11) NOT NULL,
  `poll_option` varchar(250) NOT NULL,
  `option_counter` int(11) NOT NULL,
  `inserted_by` int(11) NOT NULL,
  `inserted_date` datetime NOT NULL,
  `update_by` int(11) NOT NULL,
  `last_update` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `profile`
--

CREATE TABLE `profile` (
  `id` int(11) NOT NULL,
  `title` varchar(25) NOT NULL,
  `description` varchar(150) NOT NULL,
  `global_edit` enum('all_records','awn_record') NOT NULL,
  `global_delete` enum('all_records','awn_record') NOT NULL,
  `developer_mode` enum('yes','no') NOT NULL,
  `profile_block` enum('yes','no') NOT NULL,
  `post_publishing` enum('yes','no') NOT NULL,
  `page_publishing` enum('yes','no') NOT NULL,
  `event_publishing` enum('yes','no') NOT NULL,
  `inserted_by` int(11) NOT NULL,
  `inserted_date` datetime NOT NULL,
  `last_update` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `profile`
--

INSERT INTO `profile` (`id`, `title`, `description`, `global_edit`, `global_delete`, `developer_mode`, `profile_block`, `post_publishing`, `page_publishing`, `event_publishing`, `inserted_by`, `inserted_date`, `last_update`) VALUES
(1, 'access all', '', 'all_records', 'all_records', 'yes', 'no', 'yes', 'yes', 'yes', 1, '2014-05-04 10:42:08', '2016-09-26 12:03:18'),
(9, 'new admin', '', 'all_records', 'awn_record', 'no', 'yes', 'yes', 'yes', 'yes', 1, '2016-02-03 18:40:32', '2016-08-23 17:41:32'),
(10, 'admin', '', 'all_records', 'all_records', 'no', 'no', 'yes', 'yes', 'yes', 1, '2016-02-24 08:34:23', '2016-08-23 16:57:42'),
(11, 'Super Admin', '', 'awn_record', 'awn_record', 'no', 'no', 'no', 'no', 'no', 1, '2016-02-29 11:40:27', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `profile_modules_access`
--

CREATE TABLE `profile_modules_access` (
  `id` int(11) NOT NULL,
  `module_id` int(11) NOT NULL,
  `access` enum('yes','no') NOT NULL,
  `profile_id` int(11) NOT NULL,
  `inserted_by` int(11) NOT NULL,
  `inserted_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `profile_modules_access`
--

INSERT INTO `profile_modules_access` (`id`, `module_id`, `access`, `profile_id`, `inserted_by`, `inserted_date`) VALUES
(1, 5, 'yes', 1, 1, '2014-05-04 10:42:08'),
(2, 6, 'yes', 1, 1, '2014-05-04 10:42:08'),
(3, 7, 'yes', 1, 1, '2014-05-04 10:42:09'),
(6, 10, 'yes', 1, 1, '2014-05-04 10:42:09'),
(7, 11, 'yes', 1, 1, '2014-05-04 10:42:09'),
(8, 12, 'yes', 1, 1, '2014-05-04 10:42:09'),
(9, 33, 'yes', 1, 1, '2014-05-10 08:51:36'),
(33, 275, 'yes', 1, 1, '2015-07-01 05:16:22'),
(107, 6, 'yes', 9, 1, '2016-02-03 18:40:32'),
(108, 5, 'yes', 9, 1, '2016-02-03 18:40:32'),
(109, 7, 'yes', 9, 1, '2016-02-03 18:40:32'),
(112, 10, 'yes', 9, 1, '2016-02-03 18:40:32'),
(113, 33, 'no', 9, 1, '2016-02-03 18:40:32'),
(114, 11, 'no', 9, 1, '2016-02-03 18:40:32'),
(115, 12, 'no', 9, 1, '2016-02-03 18:40:32'),
(116, 275, 'no', 9, 1, '2016-02-03 18:40:32'),
(117, 6, 'yes', 10, 1, '2016-02-24 08:34:23'),
(118, 5, 'yes', 10, 1, '2016-02-24 08:34:23'),
(119, 7, 'yes', 10, 1, '2016-02-24 08:34:23'),
(122, 10, 'yes', 10, 1, '2016-02-24 08:34:23'),
(123, 33, 'no', 10, 1, '2016-02-24 08:34:23'),
(124, 11, 'yes', 10, 1, '2016-02-24 08:34:23'),
(125, 12, 'no', 10, 1, '2016-02-24 08:34:23'),
(126, 275, 'no', 10, 1, '2016-02-24 08:34:23'),
(127, 6, 'yes', 11, 1, '2016-02-29 11:40:27'),
(128, 5, 'yes', 11, 1, '2016-02-29 11:40:27'),
(129, 7, 'yes', 11, 1, '2016-02-29 11:40:27'),
(132, 10, 'yes', 11, 1, '2016-02-29 11:40:27'),
(133, 33, 'yes', 11, 1, '2016-02-29 11:40:27'),
(134, 11, 'yes', 11, 1, '2016-02-29 11:40:27'),
(135, 12, 'yes', 11, 1, '2016-02-29 11:40:27'),
(136, 275, 'yes', 11, 1, '2016-02-29 11:40:27');

-- --------------------------------------------------------

--
-- Table structure for table `profile_pages_access`
--

CREATE TABLE `profile_pages_access` (
  `id` int(11) NOT NULL,
  `profile_id` int(11) NOT NULL,
  `module_id` int(11) NOT NULL,
  `page_id` int(11) NOT NULL,
  `access` enum('yes','no') NOT NULL,
  `inserted_by` int(11) NOT NULL,
  `inserted_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `profile_pages_access`
--

INSERT INTO `profile_pages_access` (`id`, `profile_id`, `module_id`, `page_id`, `access`, `inserted_by`, `inserted_date`) VALUES
(1, 1, 5, 13, 'yes', 1, '2014-05-04 10:42:09'),
(3, 1, 6, 16, 'yes', 1, '2014-05-04 10:42:09'),
(4, 1, 6, 17, 'yes', 1, '2014-05-04 10:42:09'),
(5, 1, 7, 29, 'yes', 1, '2014-05-04 10:42:09'),
(6, 1, 7, 30, 'yes', 1, '2014-05-04 10:42:09'),
(11, 1, 10, 22, 'yes', 1, '2014-05-04 10:42:09'),
(12, 1, 10, 23, 'yes', 1, '2014-05-04 10:42:09'),
(14, 1, 11, 25, 'yes', 1, '2014-05-04 10:42:09'),
(15, 1, 12, 26, 'yes', 1, '2014-05-04 10:42:09'),
(16, 1, 12, 27, 'yes', 1, '2014-05-04 10:42:09'),
(17, 1, 12, 28, 'yes', 1, '2014-05-04 10:42:09'),
(18, 1, 11, 31, 'yes', 1, '2014-05-04 10:42:09'),
(20, 1, 33, 34, 'yes', 1, '2014-05-10 08:56:40'),
(21, 1, 33, 35, 'yes', 1, '2014-05-10 08:57:56'),
(22, 1, 33, 36, 'yes', 1, '2014-05-10 08:58:46'),
(23, 1, 33, 37, 'yes', 1, '2014-05-10 02:54:51'),
(24, 1, 33, 38, 'no', 1, '2014-05-11 08:38:53'),
(49, 1, 12, 39, 'yes', 1, '2014-05-31 09:45:31'),
(51, 1, 275, 40, 'yes', 1, '2014-06-23 07:44:38'),
(53, 1, 275, 42, 'yes', 1, '2014-06-23 08:04:35'),
(237, 1, 12, 43, 'yes', 1, '2014-07-19 15:06:57'),
(238, 1, 12, 44, 'yes', 1, '2014-07-19 15:24:29'),
(239, 1, 12, 45, 'yes', 1, '2014-07-19 15:27:29'),
(241, 1, 12, 47, 'yes', 1, '2014-07-19 15:43:14'),
(242, 1, 12, 48, 'yes', 1, '2014-07-19 15:46:43'),
(244, 1, 12, 50, 'yes', 1, '2014-07-19 15:55:50'),
(246, 1, 12, 52, 'yes', 1, '2014-07-19 15:59:55'),
(247, 1, 12, 53, 'yes', 1, '2014-07-19 16:03:18'),
(248, 1, 12, 54, 'yes', 1, '2014-07-19 16:07:07'),
(250, 1, 11, 56, 'yes', 1, '2014-07-19 16:12:43'),
(258, 1, 10, 64, 'yes', 1, '2014-07-19 16:24:05'),
(259, 1, 10, 65, 'yes', 1, '2014-07-19 16:25:03'),
(264, 1, 6, 70, 'no', 1, '2014-07-19 16:31:33'),
(265, 1, 6, 71, 'yes', 1, '2014-07-19 16:32:05'),
(266, 1, 7, 72, 'yes', 1, '2014-07-19 16:33:37'),
(267, 1, 7, 73, 'yes', 1, '2014-07-19 16:33:54'),
(268, 1, 33, 74, 'yes', 1, '2014-07-19 16:36:05'),
(271, 1, 33, 77, 'yes', 1, '2014-07-19 16:47:13'),
(272, 1, 33, 78, 'yes', 1, '2014-07-19 16:47:49'),
(273, 1, 33, 79, 'yes', 1, '2014-07-19 16:48:28'),
(274, 1, 33, 80, 'yes', 1, '2014-07-19 16:51:47'),
(275, 1, 33, 81, 'yes', 1, '2014-07-19 16:53:27'),
(277, 1, 33, 83, 'yes', 1, '2014-07-19 16:56:50'),
(278, 1, 33, 84, 'yes', 1, '2014-07-19 16:58:58'),
(280, 1, 33, 86, 'yes', 1, '2014-07-19 17:09:19'),
(281, 1, 33, 87, 'no', 1, '2014-07-19 17:12:40'),
(285, 1, 6, 91, 'yes', 1, '2014-07-19 17:22:38'),
(286, 1, 7, 92, 'yes', 1, '2014-07-19 17:23:39'),
(287, 1, 10, 93, 'yes', 1, '2014-07-19 17:24:46'),
(289, 1, 11, 95, 'yes', 1, '2014-07-19 17:28:12'),
(290, 1, 11, 96, 'yes', 1, '2014-07-19 17:29:24'),
(291, 1, 12, 97, 'yes', 1, '2014-07-20 13:23:07'),
(292, 1, 7, 98, 'yes', 1, '2014-07-20 14:07:32'),
(293, 1, 7, 99, 'yes', 1, '2014-07-20 14:08:28'),
(295, 1, 7, 101, 'yes', 1, '2014-07-20 14:12:07'),
(296, 1, 7, 102, 'yes', 1, '2014-07-20 14:13:13'),
(297, 1, 7, 103, 'yes', 1, '2014-07-20 14:18:52'),
(301, 1, 12, 107, 'yes', 1, '2014-07-23 14:43:37'),
(302, 1, 12, 108, 'yes', 1, '2014-07-24 02:03:06'),
(303, 1, 33, 109, 'no', 1, '2014-07-27 19:28:05'),
(304, 1, 33, 110, 'yes', 1, '2014-07-27 19:43:43'),
(305, 1, 33, 111, 'yes', 1, '2014-07-27 20:19:35'),
(306, 1, 33, 112, 'yes', 1, '2014-07-31 10:45:13'),
(307, 1, 11, 113, 'yes', 1, '2014-07-31 10:47:42'),
(308, 1, 11, 114, 'yes', 1, '2014-07-31 10:49:27'),
(310, 1, 12, 116, 'no', 1, '2014-08-02 14:27:33'),
(311, 1, 12, 117, 'yes', 1, '2014-08-02 14:28:00'),
(312, 1, 12, 118, 'yes', 1, '2014-08-02 16:53:09'),
(313, 1, 5, 119, 'yes', 1, '2014-08-07 07:25:56'),
(314, 1, 5, 120, 'yes', 1, '2014-08-07 07:52:13'),
(315, 1, 5, 121, 'yes', 1, '2014-08-07 10:25:41'),
(316, 1, 12, 122, 'yes', 1, '2014-08-10 07:55:26'),
(321, 1, 10, 127, 'yes', 1, '2014-08-17 10:31:22'),
(323, 1, 7, 129, 'yes', 1, '2014-08-18 11:46:56'),
(325, 1, 10, 131, 'yes', 1, '2014-08-23 16:33:24'),
(326, 1, 33, 132, 'yes', 1, '2014-09-17 11:04:43'),
(327, 1, 33, 133, 'yes', 1, '2014-09-17 11:06:57'),
(328, 1, 33, 134, 'yes', 1, '2014-09-17 11:07:19'),
(329, 1, 33, 135, 'yes', 1, '2014-09-17 11:07:50'),
(330, 1, 33, 136, 'yes', 1, '2014-09-17 11:08:18'),
(331, 1, 33, 137, 'yes', 1, '2014-09-17 11:10:05'),
(332, 1, 33, 138, 'yes', 1, '2014-09-17 11:10:25'),
(333, 1, 33, 139, 'yes', 1, '2014-09-17 11:10:55'),
(334, 1, 33, 140, 'yes', 1, '2014-09-17 11:11:19'),
(335, 1, 33, 141, 'yes', 1, '2014-09-17 11:11:46'),
(336, 1, 33, 142, 'yes', 1, '2014-09-17 11:46:09'),
(337, 1, 33, 143, 'yes', 1, '2014-09-17 11:46:49'),
(338, 1, 33, 144, 'yes', 1, '2014-09-17 11:47:16'),
(339, 1, 33, 145, 'yes', 1, '2014-09-17 11:47:41'),
(340, 1, 33, 146, 'yes', 1, '2014-09-17 11:47:59'),
(341, 1, 33, 149, 'yes', 1, '2014-09-20 02:09:15'),
(342, 1, 33, 150, 'yes', 1, '2014-09-20 02:10:19'),
(343, 1, 33, 152, 'yes', 1, '2014-09-20 11:12:36'),
(344, 1, 33, 153, 'yes', 1, '2014-09-20 17:12:38'),
(345, 1, 5, 154, 'yes', 1, '2014-09-21 00:01:05'),
(540, 1, 12, 229, 'yes', 1, '2015-03-30 08:00:31'),
(546, 1, 12, 235, 'yes', 1, '2015-03-31 14:02:26'),
(548, 1, 12, 237, 'yes', 1, '2015-03-31 14:04:06'),
(549, 1, 12, 238, 'yes', 1, '2015-03-31 14:06:46'),
(565, 1, 12, 254, 'yes', 1, '2015-04-06 18:30:02'),
(566, 1, 12, 255, 'yes', 1, '2015-04-06 18:30:32'),
(567, 1, 12, 256, 'yes', 1, '2015-04-06 18:30:57'),
(568, 1, 12, 257, 'yes', 1, '2015-04-06 18:31:36'),
(569, 1, 12, 258, 'yes', 1, '2015-04-06 18:32:04'),
(784, 1, 33, 267, 'yes', 1, '2015-05-24 16:14:17'),
(786, 1, 33, 268, 'yes', 1, '2015-05-24 16:14:45'),
(2401, 9, 6, 16, 'yes', 1, '2016-02-03 18:40:32'),
(2402, 9, 6, 17, 'yes', 1, '2016-02-03 18:40:32'),
(2403, 9, 6, 70, 'yes', 1, '2016-02-03 18:40:32'),
(2404, 9, 6, 91, 'yes', 1, '2016-02-03 18:40:32'),
(2405, 9, 6, 71, 'yes', 1, '2016-02-03 18:40:32'),
(2406, 9, 5, 13, 'yes', 1, '2016-02-03 18:40:32'),
(2407, 9, 5, 119, 'yes', 1, '2016-02-03 18:40:32'),
(2408, 9, 5, 154, 'yes', 1, '2016-02-03 18:40:32'),
(2409, 9, 5, 120, 'yes', 1, '2016-02-03 18:40:32'),
(2410, 9, 5, 121, 'yes', 1, '2016-02-03 18:40:32'),
(2411, 9, 7, 29, 'yes', 1, '2016-02-03 18:40:32'),
(2412, 9, 7, 30, 'yes', 1, '2016-02-03 18:40:32'),
(2413, 9, 7, 72, 'yes', 1, '2016-02-03 18:40:32'),
(2414, 9, 7, 92, 'yes', 1, '2016-02-03 18:40:32'),
(2415, 9, 7, 73, 'yes', 1, '2016-02-03 18:40:32'),
(2416, 9, 7, 98, 'yes', 1, '2016-02-03 18:40:32'),
(2417, 9, 7, 99, 'yes', 1, '2016-02-03 18:40:32'),
(2418, 9, 7, 101, 'yes', 1, '2016-02-03 18:40:32'),
(2419, 9, 7, 103, 'yes', 1, '2016-02-03 18:40:32'),
(2420, 9, 7, 102, 'yes', 1, '2016-02-03 18:40:32'),
(2421, 9, 7, 129, 'yes', 1, '2016-02-03 18:40:32'),
(2436, 9, 10, 22, 'yes', 1, '2016-02-03 18:40:32'),
(2437, 9, 10, 23, 'yes', 1, '2016-02-03 18:40:32'),
(2438, 9, 10, 64, 'yes', 1, '2016-02-03 18:40:32'),
(2439, 9, 10, 127, 'yes', 1, '2016-02-03 18:40:32'),
(2440, 9, 10, 93, 'yes', 1, '2016-02-03 18:40:32'),
(2441, 9, 10, 65, 'yes', 1, '2016-02-03 18:40:32'),
(2442, 9, 10, 131, 'yes', 1, '2016-02-03 18:40:32'),
(2443, 9, 33, 34, 'no', 1, '2016-02-03 18:40:32'),
(2444, 9, 33, 74, 'no', 1, '2016-02-03 18:40:32'),
(2445, 9, 33, 132, 'no', 1, '2016-02-03 18:40:32'),
(2446, 9, 33, 110, 'no', 1, '2016-02-03 18:40:32'),
(2447, 9, 33, 112, 'no', 1, '2016-02-03 18:40:32'),
(2448, 9, 33, 152, 'no', 1, '2016-02-03 18:40:32'),
(2449, 9, 33, 35, 'no', 1, '2016-02-03 18:40:32'),
(2450, 9, 33, 153, 'no', 1, '2016-02-03 18:40:32'),
(2451, 9, 33, 77, 'no', 1, '2016-02-03 18:40:32'),
(2452, 9, 33, 78, 'no', 1, '2016-02-03 18:40:32'),
(2453, 9, 33, 79, 'no', 1, '2016-02-03 18:40:32'),
(2454, 9, 33, 80, 'no', 1, '2016-02-03 18:40:32'),
(2455, 9, 33, 36, 'no', 1, '2016-02-03 18:40:32'),
(2456, 9, 33, 81, 'no', 1, '2016-02-03 18:40:32'),
(2457, 9, 33, 83, 'no', 1, '2016-02-03 18:40:32'),
(2458, 9, 33, 84, 'no', 1, '2016-02-03 18:40:32'),
(2459, 9, 33, 86, 'no', 1, '2016-02-03 18:40:32'),
(2460, 9, 33, 37, 'no', 1, '2016-02-03 18:40:32'),
(2461, 9, 33, 111, 'no', 1, '2016-02-03 18:40:32'),
(2462, 9, 33, 38, 'no', 1, '2016-02-03 18:40:32'),
(2463, 9, 33, 109, 'no', 1, '2016-02-03 18:40:32'),
(2464, 9, 33, 87, 'no', 1, '2016-02-03 18:40:32'),
(2465, 9, 33, 133, 'no', 1, '2016-02-03 18:40:32'),
(2466, 9, 33, 134, 'no', 1, '2016-02-03 18:40:32'),
(2467, 9, 33, 135, 'no', 1, '2016-02-03 18:40:32'),
(2468, 9, 33, 136, 'no', 1, '2016-02-03 18:40:32'),
(2469, 9, 33, 137, 'no', 1, '2016-02-03 18:40:32'),
(2470, 9, 33, 138, 'no', 1, '2016-02-03 18:40:32'),
(2471, 9, 33, 139, 'no', 1, '2016-02-03 18:40:32'),
(2472, 9, 33, 140, 'no', 1, '2016-02-03 18:40:32'),
(2473, 9, 33, 141, 'no', 1, '2016-02-03 18:40:32'),
(2474, 9, 33, 142, 'no', 1, '2016-02-03 18:40:32'),
(2475, 9, 33, 143, 'no', 1, '2016-02-03 18:40:32'),
(2476, 9, 33, 144, 'no', 1, '2016-02-03 18:40:32'),
(2477, 9, 33, 145, 'no', 1, '2016-02-03 18:40:32'),
(2478, 9, 33, 146, 'no', 1, '2016-02-03 18:40:32'),
(2479, 9, 33, 149, 'no', 1, '2016-02-03 18:40:32'),
(2480, 9, 33, 150, 'no', 1, '2016-02-03 18:40:32'),
(2481, 9, 33, 151, 'no', 1, '2016-02-03 18:40:32'),
(2482, 9, 33, 267, 'no', 1, '2016-02-03 18:40:32'),
(2483, 9, 33, 268, 'no', 1, '2016-02-03 18:40:32'),
(2486, 9, 11, 56, 'no', 1, '2016-02-03 18:40:32'),
(2488, 9, 11, 113, 'no', 1, '2016-02-03 18:40:32'),
(2489, 9, 11, 25, 'no', 1, '2016-02-03 18:40:32'),
(2492, 9, 11, 95, 'no', 1, '2016-02-03 18:40:32'),
(2493, 9, 11, 114, 'no', 1, '2016-02-03 18:40:32'),
(2494, 9, 11, 31, 'no', 1, '2016-02-03 18:40:32'),
(2497, 9, 11, 96, 'no', 1, '2016-02-03 18:40:32'),
(2503, 9, 12, 43, 'no', 1, '2016-02-03 18:40:32'),
(2504, 9, 12, 39, 'no', 1, '2016-02-03 18:40:32'),
(2505, 9, 12, 26, 'no', 1, '2016-02-03 18:40:32'),
(2506, 9, 12, 44, 'no', 1, '2016-02-03 18:40:32'),
(2507, 9, 12, 45, 'no', 1, '2016-02-03 18:40:32'),
(2508, 9, 12, 97, 'no', 1, '2016-02-03 18:40:32'),
(2509, 9, 12, 108, 'no', 1, '2016-02-03 18:40:32'),
(2513, 9, 12, 229, 'no', 1, '2016-02-03 18:40:32'),
(2514, 9, 12, 47, 'no', 1, '2016-02-03 18:40:32'),
(2515, 9, 12, 48, 'no', 1, '2016-02-03 18:40:32'),
(2517, 9, 12, 235, 'no', 1, '2016-02-03 18:40:32'),
(2518, 9, 12, 237, 'no', 1, '2016-02-03 18:40:32'),
(2519, 9, 12, 238, 'no', 1, '2016-02-03 18:40:32'),
(2520, 9, 12, 118, 'no', 1, '2016-02-03 18:40:32'),
(2521, 9, 12, 28, 'no', 1, '2016-02-03 18:40:32'),
(2522, 9, 12, 116, 'no', 1, '2016-02-03 18:40:32'),
(2523, 9, 12, 117, 'no', 1, '2016-02-03 18:40:32'),
(2524, 9, 12, 27, 'no', 1, '2016-02-03 18:40:32'),
(2525, 9, 12, 50, 'no', 1, '2016-02-03 18:40:32'),
(2526, 9, 12, 52, 'no', 1, '2016-02-03 18:40:32'),
(2527, 9, 12, 53, 'no', 1, '2016-02-03 18:40:32'),
(2528, 9, 12, 54, 'no', 1, '2016-02-03 18:40:32'),
(2529, 9, 12, 122, 'no', 1, '2016-02-03 18:40:32'),
(2530, 9, 12, 107, 'no', 1, '2016-02-03 18:40:32'),
(2533, 9, 12, 254, 'no', 1, '2016-02-03 18:40:32'),
(2534, 9, 12, 256, 'no', 1, '2016-02-03 18:40:32'),
(2535, 9, 12, 257, 'no', 1, '2016-02-03 18:40:32'),
(2536, 9, 12, 258, 'no', 1, '2016-02-03 18:40:32'),
(2537, 9, 12, 255, 'no', 1, '2016-02-03 18:40:32'),
(2538, 9, 275, 40, 'no', 1, '2016-02-03 18:40:32'),
(2539, 9, 275, 42, 'no', 1, '2016-02-03 18:40:32'),
(2540, 10, 6, 16, 'yes', 1, '2016-02-24 08:34:23'),
(2541, 10, 6, 17, 'yes', 1, '2016-02-24 08:34:23'),
(2542, 10, 6, 70, 'yes', 1, '2016-02-24 08:34:23'),
(2543, 10, 6, 91, 'yes', 1, '2016-02-24 08:34:23'),
(2544, 10, 6, 71, 'yes', 1, '2016-02-24 08:34:23'),
(2545, 10, 5, 13, 'yes', 1, '2016-02-24 08:34:23'),
(2546, 10, 5, 119, 'yes', 1, '2016-02-24 08:34:23'),
(2547, 10, 5, 154, 'yes', 1, '2016-02-24 08:34:23'),
(2548, 10, 5, 120, 'yes', 1, '2016-02-24 08:34:23'),
(2549, 10, 5, 121, 'yes', 1, '2016-02-24 08:34:23'),
(2550, 10, 7, 29, 'yes', 1, '2016-02-24 08:34:23'),
(2551, 10, 7, 30, 'yes', 1, '2016-02-24 08:34:23'),
(2552, 10, 7, 72, 'yes', 1, '2016-02-24 08:34:23'),
(2553, 10, 7, 92, 'yes', 1, '2016-02-24 08:34:23'),
(2554, 10, 7, 73, 'yes', 1, '2016-02-24 08:34:23'),
(2555, 10, 7, 98, 'yes', 1, '2016-02-24 08:34:23'),
(2556, 10, 7, 99, 'yes', 1, '2016-02-24 08:34:23'),
(2557, 10, 7, 101, 'yes', 1, '2016-02-24 08:34:23'),
(2558, 10, 7, 103, 'yes', 1, '2016-02-24 08:34:23'),
(2559, 10, 7, 102, 'yes', 1, '2016-02-24 08:34:23'),
(2560, 10, 7, 129, 'yes', 1, '2016-02-24 08:34:23'),
(2575, 10, 10, 22, 'yes', 1, '2016-02-24 08:34:23'),
(2576, 10, 10, 23, 'yes', 1, '2016-02-24 08:34:23'),
(2577, 10, 10, 64, 'yes', 1, '2016-02-24 08:34:23'),
(2578, 10, 10, 127, 'yes', 1, '2016-02-24 08:34:23'),
(2579, 10, 10, 93, 'yes', 1, '2016-02-24 08:34:23'),
(2580, 10, 10, 65, 'yes', 1, '2016-02-24 08:34:23'),
(2581, 10, 10, 131, 'yes', 1, '2016-02-24 08:34:23'),
(2582, 10, 33, 34, 'no', 1, '2016-02-24 08:34:23'),
(2583, 10, 33, 74, 'no', 1, '2016-02-24 08:34:23'),
(2584, 10, 33, 132, 'no', 1, '2016-02-24 08:34:23'),
(2585, 10, 33, 110, 'no', 1, '2016-02-24 08:34:23'),
(2586, 10, 33, 112, 'no', 1, '2016-02-24 08:34:23'),
(2587, 10, 33, 152, 'no', 1, '2016-02-24 08:34:23'),
(2588, 10, 33, 35, 'no', 1, '2016-02-24 08:34:23'),
(2589, 10, 33, 153, 'no', 1, '2016-02-24 08:34:23'),
(2590, 10, 33, 77, 'no', 1, '2016-02-24 08:34:23'),
(2591, 10, 33, 78, 'no', 1, '2016-02-24 08:34:23'),
(2592, 10, 33, 79, 'no', 1, '2016-02-24 08:34:23'),
(2593, 10, 33, 80, 'no', 1, '2016-02-24 08:34:23'),
(2594, 10, 33, 36, 'no', 1, '2016-02-24 08:34:23'),
(2595, 10, 33, 81, 'no', 1, '2016-02-24 08:34:23'),
(2596, 10, 33, 83, 'no', 1, '2016-02-24 08:34:23'),
(2597, 10, 33, 84, 'no', 1, '2016-02-24 08:34:23'),
(2598, 10, 33, 86, 'no', 1, '2016-02-24 08:34:23'),
(2599, 10, 33, 37, 'no', 1, '2016-02-24 08:34:23'),
(2600, 10, 33, 111, 'no', 1, '2016-02-24 08:34:23'),
(2601, 10, 33, 38, 'no', 1, '2016-02-24 08:34:23'),
(2602, 10, 33, 109, 'no', 1, '2016-02-24 08:34:23'),
(2603, 10, 33, 87, 'no', 1, '2016-02-24 08:34:23'),
(2604, 10, 33, 133, 'no', 1, '2016-02-24 08:34:23'),
(2605, 10, 33, 134, 'no', 1, '2016-02-24 08:34:23'),
(2606, 10, 33, 135, 'no', 1, '2016-02-24 08:34:23'),
(2607, 10, 33, 136, 'no', 1, '2016-02-24 08:34:23'),
(2608, 10, 33, 137, 'no', 1, '2016-02-24 08:34:23'),
(2609, 10, 33, 138, 'no', 1, '2016-02-24 08:34:23'),
(2610, 10, 33, 139, 'no', 1, '2016-02-24 08:34:23'),
(2611, 10, 33, 140, 'no', 1, '2016-02-24 08:34:23'),
(2612, 10, 33, 141, 'no', 1, '2016-02-24 08:34:23'),
(2613, 10, 33, 142, 'no', 1, '2016-02-24 08:34:23'),
(2614, 10, 33, 143, 'no', 1, '2016-02-24 08:34:23'),
(2615, 10, 33, 144, 'no', 1, '2016-02-24 08:34:23'),
(2616, 10, 33, 145, 'no', 1, '2016-02-24 08:34:23'),
(2617, 10, 33, 146, 'no', 1, '2016-02-24 08:34:23'),
(2618, 10, 33, 149, 'no', 1, '2016-02-24 08:34:23'),
(2619, 10, 33, 150, 'no', 1, '2016-02-24 08:34:23'),
(2620, 10, 33, 151, 'no', 1, '2016-02-24 08:34:23'),
(2621, 10, 33, 267, 'no', 1, '2016-02-24 08:34:23'),
(2622, 10, 33, 268, 'no', 1, '2016-02-24 08:34:23'),
(2625, 10, 11, 56, 'yes', 1, '2016-02-24 08:34:23'),
(2627, 10, 11, 113, 'yes', 1, '2016-02-24 08:34:23'),
(2628, 10, 11, 25, 'yes', 1, '2016-02-24 08:34:23'),
(2631, 10, 11, 95, 'yes', 1, '2016-02-24 08:34:23'),
(2632, 10, 11, 114, 'yes', 1, '2016-02-24 08:34:23'),
(2633, 10, 11, 31, 'yes', 1, '2016-02-24 08:34:23'),
(2636, 10, 11, 96, 'yes', 1, '2016-02-24 08:34:23'),
(2642, 10, 12, 43, 'no', 1, '2016-02-24 08:34:23'),
(2643, 10, 12, 39, 'no', 1, '2016-02-24 08:34:23'),
(2644, 10, 12, 26, 'no', 1, '2016-02-24 08:34:23'),
(2645, 10, 12, 44, 'no', 1, '2016-02-24 08:34:23'),
(2646, 10, 12, 45, 'no', 1, '2016-02-24 08:34:23'),
(2647, 10, 12, 97, 'no', 1, '2016-02-24 08:34:23'),
(2648, 10, 12, 108, 'no', 1, '2016-02-24 08:34:23'),
(2652, 10, 12, 229, 'no', 1, '2016-02-24 08:34:23'),
(2653, 10, 12, 47, 'no', 1, '2016-02-24 08:34:23'),
(2654, 10, 12, 48, 'no', 1, '2016-02-24 08:34:23'),
(2656, 10, 12, 235, 'no', 1, '2016-02-24 08:34:23'),
(2657, 10, 12, 237, 'no', 1, '2016-02-24 08:34:23'),
(2658, 10, 12, 238, 'no', 1, '2016-02-24 08:34:23'),
(2659, 10, 12, 118, 'no', 1, '2016-02-24 08:34:23'),
(2660, 10, 12, 28, 'no', 1, '2016-02-24 08:34:23'),
(2661, 10, 12, 116, 'no', 1, '2016-02-24 08:34:23'),
(2662, 10, 12, 117, 'no', 1, '2016-02-24 08:34:23'),
(2663, 10, 12, 27, 'no', 1, '2016-02-24 08:34:23'),
(2664, 10, 12, 50, 'no', 1, '2016-02-24 08:34:23'),
(2665, 10, 12, 52, 'no', 1, '2016-02-24 08:34:23'),
(2666, 10, 12, 53, 'no', 1, '2016-02-24 08:34:23'),
(2667, 10, 12, 54, 'no', 1, '2016-02-24 08:34:23'),
(2668, 10, 12, 122, 'no', 1, '2016-02-24 08:34:23'),
(2669, 10, 12, 107, 'no', 1, '2016-02-24 08:34:23'),
(2672, 10, 12, 254, 'no', 1, '2016-02-24 08:34:23'),
(2673, 10, 12, 256, 'no', 1, '2016-02-24 08:34:23'),
(2674, 10, 12, 257, 'no', 1, '2016-02-24 08:34:23'),
(2675, 10, 12, 258, 'no', 1, '2016-02-24 08:34:23'),
(2676, 10, 12, 255, 'no', 1, '2016-02-24 08:34:23'),
(2677, 10, 275, 40, 'no', 1, '2016-02-24 08:34:23'),
(2678, 10, 275, 42, 'no', 1, '2016-02-24 08:34:23'),
(2679, 11, 6, 16, 'yes', 1, '2016-02-29 11:40:27'),
(2680, 11, 6, 17, 'yes', 1, '2016-02-29 11:40:27'),
(2681, 11, 6, 70, 'yes', 1, '2016-02-29 11:40:27'),
(2682, 11, 6, 91, 'yes', 1, '2016-02-29 11:40:27'),
(2683, 11, 6, 71, 'yes', 1, '2016-02-29 11:40:27'),
(2684, 11, 5, 13, 'yes', 1, '2016-02-29 11:40:27'),
(2685, 11, 5, 119, 'yes', 1, '2016-02-29 11:40:27'),
(2686, 11, 5, 154, 'yes', 1, '2016-02-29 11:40:27'),
(2687, 11, 5, 120, 'yes', 1, '2016-02-29 11:40:27'),
(2688, 11, 5, 121, 'yes', 1, '2016-02-29 11:40:27'),
(2689, 11, 7, 29, 'yes', 1, '2016-02-29 11:40:27'),
(2690, 11, 7, 30, 'yes', 1, '2016-02-29 11:40:27'),
(2691, 11, 7, 72, 'yes', 1, '2016-02-29 11:40:27'),
(2692, 11, 7, 92, 'yes', 1, '2016-02-29 11:40:27'),
(2693, 11, 7, 73, 'yes', 1, '2016-02-29 11:40:27'),
(2694, 11, 7, 98, 'yes', 1, '2016-02-29 11:40:27'),
(2695, 11, 7, 99, 'yes', 1, '2016-02-29 11:40:27'),
(2696, 11, 7, 101, 'yes', 1, '2016-02-29 11:40:27'),
(2697, 11, 7, 103, 'yes', 1, '2016-02-29 11:40:27'),
(2698, 11, 7, 102, 'yes', 1, '2016-02-29 11:40:27'),
(2699, 11, 7, 129, 'yes', 1, '2016-02-29 11:40:27'),
(2714, 11, 10, 22, 'yes', 1, '2016-02-29 11:40:27'),
(2715, 11, 10, 23, 'yes', 1, '2016-02-29 11:40:27'),
(2716, 11, 10, 64, 'yes', 1, '2016-02-29 11:40:27'),
(2717, 11, 10, 127, 'yes', 1, '2016-02-29 11:40:27'),
(2718, 11, 10, 93, 'yes', 1, '2016-02-29 11:40:27'),
(2719, 11, 10, 65, 'yes', 1, '2016-02-29 11:40:27'),
(2720, 11, 10, 131, 'yes', 1, '2016-02-29 11:40:27'),
(2721, 11, 33, 34, 'yes', 1, '2016-02-29 11:40:27'),
(2722, 11, 33, 74, 'yes', 1, '2016-02-29 11:40:27'),
(2723, 11, 33, 132, 'yes', 1, '2016-02-29 11:40:27'),
(2724, 11, 33, 110, 'yes', 1, '2016-02-29 11:40:27'),
(2725, 11, 33, 112, 'yes', 1, '2016-02-29 11:40:27'),
(2726, 11, 33, 152, 'yes', 1, '2016-02-29 11:40:27'),
(2727, 11, 33, 35, 'yes', 1, '2016-02-29 11:40:27'),
(2728, 11, 33, 153, 'yes', 1, '2016-02-29 11:40:27'),
(2729, 11, 33, 77, 'yes', 1, '2016-02-29 11:40:27'),
(2730, 11, 33, 78, 'yes', 1, '2016-02-29 11:40:27'),
(2731, 11, 33, 79, 'yes', 1, '2016-02-29 11:40:27'),
(2732, 11, 33, 80, 'yes', 1, '2016-02-29 11:40:27'),
(2733, 11, 33, 36, 'yes', 1, '2016-02-29 11:40:27'),
(2734, 11, 33, 81, 'yes', 1, '2016-02-29 11:40:27'),
(2735, 11, 33, 83, 'yes', 1, '2016-02-29 11:40:27'),
(2736, 11, 33, 84, 'yes', 1, '2016-02-29 11:40:27'),
(2737, 11, 33, 86, 'yes', 1, '2016-02-29 11:40:27'),
(2738, 11, 33, 37, 'yes', 1, '2016-02-29 11:40:27'),
(2739, 11, 33, 111, 'yes', 1, '2016-02-29 11:40:27'),
(2740, 11, 33, 38, 'yes', 1, '2016-02-29 11:40:27'),
(2741, 11, 33, 109, 'yes', 1, '2016-02-29 11:40:27'),
(2742, 11, 33, 87, 'yes', 1, '2016-02-29 11:40:27'),
(2743, 11, 33, 133, 'yes', 1, '2016-02-29 11:40:27'),
(2744, 11, 33, 134, 'yes', 1, '2016-02-29 11:40:27'),
(2745, 11, 33, 135, 'yes', 1, '2016-02-29 11:40:27'),
(2746, 11, 33, 136, 'yes', 1, '2016-02-29 11:40:27'),
(2747, 11, 33, 137, 'yes', 1, '2016-02-29 11:40:27'),
(2748, 11, 33, 138, 'yes', 1, '2016-02-29 11:40:27'),
(2749, 11, 33, 139, 'yes', 1, '2016-02-29 11:40:27'),
(2750, 11, 33, 140, 'yes', 1, '2016-02-29 11:40:27'),
(2751, 11, 33, 141, 'yes', 1, '2016-02-29 11:40:27'),
(2752, 11, 33, 142, 'yes', 1, '2016-02-29 11:40:27'),
(2753, 11, 33, 143, 'yes', 1, '2016-02-29 11:40:27'),
(2754, 11, 33, 144, 'yes', 1, '2016-02-29 11:40:27'),
(2755, 11, 33, 145, 'yes', 1, '2016-02-29 11:40:27'),
(2756, 11, 33, 146, 'yes', 1, '2016-02-29 11:40:27'),
(2757, 11, 33, 149, 'yes', 1, '2016-02-29 11:40:27'),
(2758, 11, 33, 150, 'yes', 1, '2016-02-29 11:40:27'),
(2759, 11, 33, 151, 'yes', 1, '2016-02-29 11:40:27'),
(2760, 11, 33, 267, 'yes', 1, '2016-02-29 11:40:27'),
(2761, 11, 33, 268, 'yes', 1, '2016-02-29 11:40:27'),
(2764, 11, 11, 56, 'yes', 1, '2016-02-29 11:40:27'),
(2766, 11, 11, 113, 'yes', 1, '2016-02-29 11:40:27'),
(2767, 11, 11, 25, 'yes', 1, '2016-02-29 11:40:27'),
(2770, 11, 11, 95, 'yes', 1, '2016-02-29 11:40:27'),
(2771, 11, 11, 114, 'yes', 1, '2016-02-29 11:40:27'),
(2772, 11, 11, 31, 'yes', 1, '2016-02-29 11:40:27'),
(2775, 11, 11, 96, 'yes', 1, '2016-02-29 11:40:27'),
(2781, 11, 12, 43, 'yes', 1, '2016-02-29 11:40:27'),
(2782, 11, 12, 39, 'yes', 1, '2016-02-29 11:40:27'),
(2783, 11, 12, 26, 'yes', 1, '2016-02-29 11:40:27'),
(2784, 11, 12, 44, 'yes', 1, '2016-02-29 11:40:27'),
(2785, 11, 12, 45, 'yes', 1, '2016-02-29 11:40:27'),
(2786, 11, 12, 97, 'yes', 1, '2016-02-29 11:40:27'),
(2787, 11, 12, 108, 'yes', 1, '2016-02-29 11:40:27'),
(2791, 11, 12, 229, 'yes', 1, '2016-02-29 11:40:27'),
(2792, 11, 12, 47, 'yes', 1, '2016-02-29 11:40:27'),
(2793, 11, 12, 48, 'yes', 1, '2016-02-29 11:40:27'),
(2795, 11, 12, 235, 'yes', 1, '2016-02-29 11:40:27'),
(2796, 11, 12, 237, 'yes', 1, '2016-02-29 11:40:27'),
(2797, 11, 12, 238, 'yes', 1, '2016-02-29 11:40:27'),
(2798, 11, 12, 118, 'yes', 1, '2016-02-29 11:40:27'),
(2799, 11, 12, 28, 'yes', 1, '2016-02-29 11:40:27'),
(2800, 11, 12, 116, 'yes', 1, '2016-02-29 11:40:27'),
(2801, 11, 12, 117, 'yes', 1, '2016-02-29 11:40:27'),
(2802, 11, 12, 27, 'yes', 1, '2016-02-29 11:40:27'),
(2803, 11, 12, 50, 'yes', 1, '2016-02-29 11:40:27'),
(2804, 11, 12, 52, 'yes', 1, '2016-02-29 11:40:27'),
(2805, 11, 12, 53, 'yes', 1, '2016-02-29 11:40:27'),
(2806, 11, 12, 54, 'yes', 1, '2016-02-29 11:40:27'),
(2807, 11, 12, 122, 'yes', 1, '2016-02-29 11:40:27'),
(2808, 11, 12, 107, 'yes', 1, '2016-02-29 11:40:27'),
(2811, 11, 12, 254, 'yes', 1, '2016-02-29 11:40:27'),
(2812, 11, 12, 256, 'yes', 1, '2016-02-29 11:40:27'),
(2813, 11, 12, 257, 'yes', 1, '2016-02-29 11:40:27'),
(2814, 11, 12, 258, 'yes', 1, '2016-02-29 11:40:27'),
(2815, 11, 12, 255, 'yes', 1, '2016-02-29 11:40:27'),
(2816, 11, 275, 40, 'yes', 1, '2016-02-29 11:40:27'),
(2817, 11, 275, 42, 'yes', 1, '2016-02-29 11:40:27'),
(2838, 1, 10, 282, 'yes', 1, '2016-05-31 13:38:52'),
(2839, 9, 10, 282, 'no', 1, '2016-05-31 13:38:52'),
(2840, 10, 10, 282, 'no', 1, '2016-05-31 13:38:52'),
(2841, 11, 10, 282, 'no', 1, '2016-05-31 13:38:52'),
(2854, 1, 33, 287, 'yes', 1, '2016-09-26 13:41:26'),
(2855, 9, 33, 287, 'no', 1, '2016-09-26 13:41:26'),
(2856, 10, 33, 287, 'no', 1, '2016-09-26 13:41:26'),
(2857, 11, 33, 287, 'no', 1, '2016-09-26 13:41:26');

-- --------------------------------------------------------

--
-- Table structure for table `social_comments`
--

CREATE TABLE `social_comments` (
  `id` int(11) NOT NULL,
  `node_id` int(11) NOT NULL,
  `user_name` varchar(256) NOT NULL,
  `title` varchar(256) NOT NULL,
  `status` enum('draft','publish') NOT NULL,
  `email` varchar(256) NOT NULL,
  `body` longtext NOT NULL,
  `inserted_date` datetime NOT NULL,
  `update_by` int(11) NOT NULL,
  `last_update` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `social_email_subscription`
--

CREATE TABLE `social_email_subscription` (
  `id` int(11) NOT NULL,
  `email` varchar(265) NOT NULL,
  `inserted_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `structure_menu_group`
--

CREATE TABLE `structure_menu_group` (
  `id` int(11) NOT NULL,
  `title` varchar(50) NOT NULL,
  `alias` varchar(50) NOT NULL,
  `image` varchar(256) NOT NULL,
  `description` text NOT NULL,
  `inserted_by` int(11) NOT NULL,
  `inserted_date` datetime NOT NULL,
  `update_by` int(11) NOT NULL,
  `last_update` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `structure_menu_group`
--

INSERT INTO `structure_menu_group` (`id`, `title`, `alias`, `image`, `description`, `inserted_by`, `inserted_date`, `update_by`, `last_update`) VALUES
(1, 'Main Menu', 'main_menu', '', '', 1, '2016-06-05 15:45:29', 0, '0000-00-00 00:00:00'),
(2, 'Footer menu one', 'footer_menu_one', '', '', 1, '2016-06-05 15:46:00', 0, '0000-00-00 00:00:00'),
(3, 'Footer menu two', 'footer_menu_two', '', '', 1, '2016-06-05 15:46:14', 0, '0000-00-00 00:00:00'),
(4, 'Footer menu three', 'footer_menu_three', '', '', 1, '2016-06-05 15:46:26', 0, '0000-00-00 00:00:00'),
(5, 'social menu', 'social_menu', '', '', 1, '2016-06-05 16:11:37', 0, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `structure_menu_link`
--

CREATE TABLE `structure_menu_link` (
  `id` int(11) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `group_id` int(11) NOT NULL,
  `sorting` int(11) DEFAULT NULL,
  `path_type` enum('post','external','page','event','category') NOT NULL,
  `path` int(11) DEFAULT NULL,
  `external_path` varchar(250) NOT NULL,
  `status` enum('draft','publish') NOT NULL,
  `icon` varchar(250) NOT NULL,
  `image` varchar(250) NOT NULL,
  `drop_down` enum('yes','no') NOT NULL,
  `drop_down_style` enum('op1','op2','op3') NOT NULL,
  `inserted_by` int(11) NOT NULL,
  `inserted_date` datetime NOT NULL,
  `update_by` int(11) NOT NULL,
  `last_update` datetime NOT NULL,
  `description` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `structure_menu_link`
--

INSERT INTO `structure_menu_link` (`id`, `parent_id`, `group_id`, `sorting`, `path_type`, `path`, `external_path`, `status`, `icon`, `image`, `drop_down`, `drop_down_style`, `inserted_by`, `inserted_date`, `update_by`, `last_update`, `description`) VALUES
(1, 0, 5, 1, 'external', 0, 'https://www.facebook.com/', 'publish', 'fa-facebook', 'media-library/', '', '', 1, '2016-10-04 14:57:35', 1, '2016-10-04 15:14:08', ''),
(2, 0, 5, 2, 'external', 0, 'https://twitter.com/', 'publish', 'fa-twitter', 'media-library/', '', '', 1, '2016-10-04 15:01:52', 1, '2016-10-04 15:11:00', ''),
(3, 0, 5, 3, 'external', 0, 'https://www.linkedin.com', 'publish', 'fa-linkedin', 'media-library/', '', '', 1, '2016-10-04 15:02:56', 1, '2016-10-04 15:11:26', ''),
(4, 0, 5, 4, 'external', 0, 'https://www.youtube.com', 'publish', 'fa-youtube', '../', '', '', 1, '2016-10-04 15:03:30', 1, '2016-10-04 15:11:42', ''),
(5, 0, 1, 1, 'page', 1, '', 'publish', '', '', '', '', 1, '2016-10-04 15:31:45', 0, '0000-00-00 00:00:00', ''),
(6, 0, 1, 2, 'page', 2, '', 'publish', '', '', '', '', 1, '2016-10-04 15:32:07', 0, '0000-00-00 00:00:00', ''),
(7, 0, 1, 3, 'page', 3, '', 'publish', '', '', '', '', 1, '2016-10-04 15:32:48', 0, '0000-00-00 00:00:00', ''),
(8, 0, 1, 4, 'page', 4, '', 'publish', '', '', '', '', 1, '2016-10-04 15:33:21', 0, '0000-00-00 00:00:00', ''),
(9, 0, 1, 5, 'page', 16, '', 'publish', '', '', '', '', 1, '2016-10-12 09:20:04', 0, '0000-00-00 00:00:00', ''),
(10, 0, 1, 6, 'page', 22, '', 'publish', '', '', '', '', 1, '2016-10-12 09:20:29', 0, '0000-00-00 00:00:00', ''),
(11, 0, 2, 1, 'external', 0, 'https://www.facebook.com', 'publish', '', '../', '', '', 1, '2016-10-13 09:35:49', 1, '2016-10-13 09:36:19', ''),
(12, 0, 3, 1, 'external', 0, 'https://twitter.com/', 'publish', '', '', '', '', 1, '2016-10-13 09:44:51', 0, '0000-00-00 00:00:00', ''),
(13, 0, 4, 1, 'external', 0, 'https://www.instagram.com', 'publish', '', '', '', '', 1, '2016-10-13 09:50:54', 0, '0000-00-00 00:00:00', '');

-- --------------------------------------------------------

--
-- Table structure for table `structure_menu_link_content`
--

CREATE TABLE `structure_menu_link_content` (
  `id` int(11) NOT NULL,
  `link_id` int(11) NOT NULL,
  `title` varchar(250) NOT NULL,
  `lang_id` int(11) NOT NULL,
  `description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `structure_menu_link_content`
--

INSERT INTO `structure_menu_link_content` (`id`, `link_id`, `title`, `lang_id`, `description`) VALUES
(1, 1, 'فيس بوك', 2, ''),
(2, 1, 'Facebook', 1, ''),
(3, 2, 'تويتر', 2, ''),
(4, 2, 'Twitter', 1, ''),
(5, 3, 'لينكد ان', 2, ''),
(6, 3, 'Linkedin', 1, ''),
(7, 4, 'يوتيوب', 2, ''),
(8, 4, 'Youtube', 1, ''),
(9, 5, 'من نحن', 2, ''),
(10, 5, 'About Us', 1, ''),
(11, 6, 'الخدمات', 2, ''),
(12, 6, 'Services', 1, ''),
(13, 7, 'الاسئله الشائعه', 2, ''),
(14, 7, 'FAQ', 1, ''),
(15, 8, 'اتصل بنا', 2, ''),
(16, 8, 'Contact Us', 1, ''),
(17, 9, 'الاخبار', 2, ''),
(18, 9, 'News', 1, ''),
(19, 10, 'الاطباء', 2, ''),
(20, 10, 'Doctors', 1, ''),
(21, 11, 'فيس بوك', 2, ''),
(22, 11, 'Facebook', 1, ''),
(23, 12, 'تويتر', 2, ''),
(24, 12, 'Twitter', 1, ''),
(25, 13, 'انستغرام', 2, ''),
(26, 13, 'Instagram', 1, '');

-- --------------------------------------------------------

--
-- Table structure for table `taxonomies`
--

CREATE TABLE `taxonomies` (
  `id` int(11) NOT NULL,
  `sort` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `taxonomy_type` enum('tag','category','author','country') NOT NULL,
  `sorting` int(11) NOT NULL,
  `status` enum('draft','publish') NOT NULL,
  `cover` varchar(250) NOT NULL,
  `main_menu` enum('no','yes') NOT NULL,
  `show_image` enum('no','yes') NOT NULL,
  `inserted_by` int(11) NOT NULL,
  `inserted_date` datetime NOT NULL,
  `update_by` int(11) NOT NULL,
  `last_update` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `taxonomies`
--

INSERT INTO `taxonomies` (`id`, `sort`, `parent_id`, `taxonomy_type`, `sorting`, `status`, `cover`, `main_menu`, `show_image`, `inserted_by`, `inserted_date`, `update_by`, `last_update`) VALUES
(1, 0, 0, 'category', 1, 'publish', '', 'no', 'no', 1, '2016-10-05 15:15:49', 0, '0000-00-00 00:00:00'),
(2, 0, 1, 'category', 1, 'publish', 'Services%20Categories%20Images/servi.jpg', 'no', 'no', 1, '2016-10-05 15:21:42', 0, '0000-00-00 00:00:00'),
(3, 0, 0, '', 0, 'publish', 'Services%20Categories%20Images/lab.jpg', 'no', 'no', 1, '2016-10-05 15:26:15', 0, '0000-00-00 00:00:00'),
(4, 0, 1, 'category', 2, 'publish', 'Services%20Categories%20Images/lab.jpg', 'no', 'no', 1, '2016-10-05 15:27:04', 0, '0000-00-00 00:00:00'),
(5, 0, 4, 'category', 1, 'publish', '', 'no', 'no', 1, '2016-10-05 16:48:00', 0, '0000-00-00 00:00:00'),
(6, 0, 1, 'category', 3, 'publish', 'Services%20Categories%20Images/lab.jpg', 'no', 'no', 1, '2016-10-10 08:23:28', 1, '2016-10-10 08:24:06'),
(7, 0, 0, 'category', 4, 'publish', '', 'no', 'no', 1, '2016-10-10 09:19:21', 0, '0000-00-00 00:00:00'),
(8, 0, 7, 'category', 1, 'publish', '', 'no', 'no', 1, '2016-10-10 13:32:13', 0, '0000-00-00 00:00:00'),
(9, 0, 7, 'category', 2, 'publish', '', 'no', 'no', 1, '2016-10-10 13:32:52', 0, '0000-00-00 00:00:00'),
(10, 0, 0, 'category', 6, 'publish', '', 'no', 'no', 1, '2016-10-11 13:58:51', 0, '0000-00-00 00:00:00'),
(11, 0, 0, 'category', 8, 'publish', '', 'no', 'no', 1, '2016-10-13 10:48:57', 0, '0000-00-00 00:00:00'),
(12, 0, 11, 'category', 1, 'publish', '', 'no', 'no', 1, '2016-10-13 11:18:42', 0, '0000-00-00 00:00:00'),
(13, 0, 11, 'category', 2, 'publish', '', 'no', 'no', 1, '2016-10-13 11:19:06', 0, '0000-00-00 00:00:00'),
(14, 0, 11, 'category', 3, 'publish', '', 'no', 'no', 1, '2016-10-13 11:19:29', 0, '0000-00-00 00:00:00'),
(15, 0, 0, 'category', 9, 'publish', 'media-library/', 'no', 'no', 1, '2016-10-13 11:44:45', 1, '2016-10-13 11:47:31');

-- --------------------------------------------------------

--
-- Table structure for table `taxonomies_content`
--

CREATE TABLE `taxonomies_content` (
  `id` int(11) NOT NULL,
  `taxonomy_id` int(11) NOT NULL,
  `name` varchar(256) NOT NULL,
  `alias` varchar(256) NOT NULL,
  `description` longtext CHARACTER SET utf16 NOT NULL,
  `lang_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `taxonomies_content`
--

INSERT INTO `taxonomies_content` (`id`, `taxonomy_id`, `name`, `alias`, `description`, `lang_id`) VALUES
(1, 1, 'الخدمات', 'الخدمات', '', 2),
(2, 1, 'Services', 'services', '', 1),
(3, 2, 'امراض الصدر', 'امراض_الصدر', '', 2),
(4, 2, 'Breast Diseases', 'breast_diseases', '', 1),
(5, 3, 'امراض القلب', 'امراض_القلب', '', 2),
(6, 3, 'Heart Disease', 'heart_disease', '', 1),
(7, 4, 'امراض القلب', 'امراض_القلب', '', 2),
(8, 4, 'Heart disease', 'heart_disease', '', 1),
(9, 5, 'تست سب كات', 'تست_سب_كات', '', 2),
(10, 5, 'test sub cat', 'test_sub_cat', '', 1),
(11, 6, 'امراض القدم', 'امراض_القدم', '', 2),
(12, 6, 'Foot Diseases', 'foot_diseases', '', 1),
(13, 7, 'الاخبار', 'الاخبار', '', 2),
(14, 7, 'News', 'news', '', 1),
(15, 8, 'تصنيف خبر واحد', 'تصنيف_خبر_واحد', '', 2),
(16, 8, 'News cat 1', 'news_cat_1', '', 1),
(17, 9, 'تصنيف خبر 2', 'تصنيف_خبر_2', '', 2),
(18, 9, 'News Cat 2', 'news_cat_2', '', 1),
(19, 10, 'الاطباء', 'الاطباء', '', 2),
(20, 10, 'Doctors', 'doctors', '', 1),
(21, 11, 'الفروع', 'الفروع', '', 2),
(22, 11, 'Branches', 'branches', '', 1),
(23, 12, 'الفرع الاول', 'الفرع_الاول', '', 2),
(24, 12, 'Branch one', 'branch_one', '', 1),
(25, 13, 'الفرع الثانى', 'الفرع_الثانى', '', 2),
(26, 13, 'Branch two', 'branch_two', '', 1),
(27, 14, 'الفرع الثالث', 'الفرع_الثالث', '', 2),
(28, 14, 'Branch Three', 'branch_three', '', 1),
(29, 15, 'كل الفروع', 'كل_الفروع', '', 2),
(30, 15, 'All Branches', 'all_branches', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `themes`
--

CREATE TABLE `themes` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `version` char(5) NOT NULL,
  `author` varchar(50) NOT NULL,
  `source` varchar(150) NOT NULL,
  `uploaded_date` datetime NOT NULL,
  `uploaded_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `themes`
--

INSERT INTO `themes` (`id`, `name`, `version`, `author`, `source`, `uploaded_date`, `uploaded_by`) VALUES
(2, 'main', '1.0', 'diva', 'main', '2014-08-02 14:33:30', 1);

-- --------------------------------------------------------

--
-- Table structure for table `themes_layouts`
--

CREATE TABLE `themes_layouts` (
  `id` int(11) NOT NULL,
  `theme_id` int(11) NOT NULL,
  `version` char(5) NOT NULL,
  `author` varchar(50) NOT NULL,
  `layout_name` varchar(100) NOT NULL,
  `layout_cells` int(11) NOT NULL,
  `source` varchar(100) NOT NULL,
  `uploaded_date` datetime NOT NULL,
  `uploaded_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `themes_layouts`
--

INSERT INTO `themes_layouts` (`id`, `theme_id`, `version`, `author`, `layout_name`, `layout_cells`, `source`, `uploaded_date`, `uploaded_by`) VALUES
(4, 2, '1.0', 'diva', 'internal-page-2side', 2, 'main', '2014-08-02 14:33:30', 1);

-- --------------------------------------------------------

--
-- Table structure for table `theme_layout_model`
--

CREATE TABLE `theme_layout_model` (
  `id` int(11) NOT NULL,
  `name` varchar(250) NOT NULL,
  `theme_id` int(11) NOT NULL,
  `layout_id` int(11) NOT NULL,
  `type` enum('page','post','event') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `theme_layout_model`
--

INSERT INTO `theme_layout_model` (`id`, `name`, `theme_id`, `layout_id`, `type`) VALUES
(1, 'post model', 2, 4, 'post'),
(2, 'event model', 2, 4, 'event'),
(3, 'page model', 2, 4, 'page'),
(4, '', 2, 4, 'page'),
(5, 'events page', 2, 4, 'page'),
(6, 'parsing', 2, 4, 'page'),
(7, 'contact us page', 2, 4, 'page'),
(8, 'membership_model', 2, 4, 'page'),
(9, 'offers_model', 2, 4, 'page'),
(10, 'survey', 2, 4, 'page'),
(11, 'survey_model', 2, 4, 'page'),
(12, 'testimopnial_model', 2, 4, 'page'),
(13, 'about_page_model', 2, 4, 'page'),
(14, 'rooms_model', 2, 4, 'page'),
(15, 'gallery_model_page', 2, 4, 'page'),
(16, 'List All Categories', 2, 4, 'page'),
(17, 'services_categorized_in_all', 2, 4, 'page'),
(18, 'News Model', 2, 4, 'page'),
(19, 'news_details_model', 2, 4, 'page'),
(20, 'Doctors_model', 2, 4, 'page'),
(21, 'Branches_contact_model', 2, 4, 'page'),
(22, 'Sub Sub Categories Model', 2, 4, 'page'),
(23, 'Services Details Model', 2, 4, 'post');

-- --------------------------------------------------------

--
-- Table structure for table `theme_layout_model_plugin`
--

CREATE TABLE `theme_layout_model_plugin` (
  `id` int(11) NOT NULL,
  `model_id` int(11) NOT NULL,
  `plugin_id` int(11) NOT NULL,
  `sorting` int(11) NOT NULL,
  `position` enum('left','right') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `theme_layout_model_plugin`
--

INSERT INTO `theme_layout_model_plugin` (`id`, `model_id`, `plugin_id`, `sorting`, `position`) VALUES
(1, 3, 1, 1, 'left'),
(32, 2, 34, 1, 'left'),
(33, 2, 44, 1, 'right'),
(34, 2, 47, 2, 'right'),
(35, 4, 35, 1, 'left'),
(36, 4, 47, 1, 'right'),
(37, 4, 46, 2, 'right'),
(38, 4, 44, 3, 'right'),
(39, 5, 48, 1, 'left'),
(40, 5, 44, 1, 'right'),
(41, 5, 45, 2, 'right'),
(42, 6, 49, 1, 'left'),
(63, 1, 7, 1, 'left'),
(64, 1, 44, 1, 'right'),
(65, 1, 45, 2, 'right'),
(66, 1, 46, 3, 'right'),
(67, 1, 47, 4, 'right'),
(68, 7, 3, 1, 'left'),
(69, 8, 50, 1, 'left'),
(70, 9, 51, 1, 'left'),
(71, 10, 52, 1, 'left'),
(72, 11, 52, 1, 'left'),
(73, 12, 53, 1, 'left'),
(74, 13, 54, 1, 'left'),
(75, 14, 59, 1, 'left'),
(76, 15, 60, 1, 'left'),
(77, 16, 61, 1, 'left'),
(78, 17, 62, 1, 'left'),
(79, 18, 63, 1, 'left'),
(80, 19, 64, 1, 'left'),
(81, 20, 65, 1, 'left'),
(82, 21, 66, 1, 'left'),
(83, 22, 67, 1, 'left'),
(85, 23, 68, 1, 'left');

-- --------------------------------------------------------

--
-- Table structure for table `time_zones`
--

CREATE TABLE `time_zones` (
  `id` int(11) NOT NULL,
  `GMT` varchar(5) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `name` varchar(120) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `time_zones`
--

INSERT INTO `time_zones` (`id`, `GMT`, `name`) VALUES
(1, '-12.0', '(GMT-12:00)-International Date Line West'),
(2, '-11.0', '(GMT-11:00)-Midway Island, Samoa'),
(3, '-10.0', '(GMT-10:00)-Hawaii'),
(4, '-9.0', '(GMT-09:00)-Alaska'),
(5, '-8.0', '(GMT-08:00)-Pacific Time (US & Canada); Tijuana'),
(6, '-7.0', '(GMT-07:00)-Arizona'),
(7, '-7.0', '(GMT-07:00)-Chihuahua, La Paz, Mazatlan'),
(8, '-7.0', '(GMT-07:00)-Mountain Time (US & Canada)'),
(9, '-6.0', '(GMT-06:00)-Central America'),
(10, '-6.0', '(GMT-06:00)-Central Time (US & Canada)'),
(11, '-6.0', '(GMT-06:00)-Guadalajara, Mexico City, Monterrey'),
(12, '-6.0', '(GMT-06:00)-Saskatchewan'),
(13, '-5.0', '(GMT-05:00)-Bogota, Lima, Quito'),
(14, '-5.0', '(GMT-05:00)-Eastern Time (US & Canada)'),
(15, '-5.0', '(GMT-05:00)-Indiana (East)'),
(16, '-4.0', '(GMT-04:00)-Atlantic Time (Canada)'),
(17, '-4.0', '(GMT-04:00)-Caracas, La Paz'),
(18, '-4.0', '(GMT-04:00)-Santiago'),
(19, '-3.5', '(GMT-03:30)-Newfoundland'),
(20, '-3.0', '(GMT-03:00)-Brasilia'),
(21, '-3.0', '(GMT-03:00)-Buenos Aires, Georgetown'),
(22, '-3.0', '(GMT-03:00)-Greenland'),
(23, '-2.0', '(GMT-02:00)-Mid-Atlantic'),
(24, '-1.0', '(GMT-01:00)-Azores'),
(25, '-1.0', '(GMT-01:00)-Cape Verde Is.'),
(26, '0.0', '(GMT)-Casablanca, Monrovia'),
(27, '0.0', '(GMT)-Greenwich Mean Time: Dublin, Edinburgh, Lisbon, London'),
(28, '1.0', '(GMT+01:00)-Amsterdam, Berlin, Bern, Rome, Stockholm, Vienna'),
(29, '1.0', '(GMT+01:00)-Belgrade, Bratislava, Budapest, Ljubljana, Prague'),
(30, '1.0', '(GMT+01:00)-Brussels, Copenhagen, Madrid, Paris'),
(31, '1.0', '(GMT+01:00)-Sarajevo, Skopje, Warsaw, Zagreb'),
(32, '1.0', '(GMT+01:00)-West Central Africa'),
(33, '2.0', '(GMT+02:00)-Athens, Beirut, Istanbul, Minsk'),
(34, '2.0', '(GMT+02:00)-Bucharest'),
(35, '2.0', '(GMT+02:00)-Cairo'),
(36, '2.0', '(GMT+02:00)-Harare, Pretoria'),
(37, '2.0', '(GMT+02:00)-Helsinki, Kyiv, Riga, Sofia, Tallinn, Vilnius'),
(38, '2.0', '(GMT+02:00)-Jerusalem'),
(39, '3.0', '(GMT+03:00)-Baghdad'),
(40, '3.0', '(GMT+03:00)-Kuwait, Riyadh'),
(41, '3.0', '(GMT+03:00)-Moscow, St. Petersburg, Volgograd'),
(42, '3.0', '(GMT+03:00)-Nairobi'),
(43, '3.5', '(GMT+03:30)-Tehran'),
(44, '4.0', '(GMT+04:00)-Abu Dhabi, Muscat'),
(45, '4.0', '(GMT+04:00)-Baku, Tbilisi, Yerevan'),
(46, '4.5', '(GMT+04:30)-Kabul'),
(47, '5.0', '(GMT+05:00)-Ekaterinburg'),
(48, '5.0', '(GMT+05:00)-Islamabad, Karachi, Tashkent'),
(49, '5.5', '(GMT+05:30)-Chennai, Kolkata, Mumbai, New Delhi'),
(50, '5.75', '(GMT+05:45)-Kathmandu'),
(51, '6.0', '(GMT+06:00)-Almaty, Novosibirsk'),
(52, '6.0', '(GMT+06:00)-Astana, Dhaka'),
(53, '6.0', '(GMT+06:00)-Sri Jayawardenepura'),
(54, '6.5', '(GMT+06:30)-Rangoon'),
(55, '7.0', '(GMT+07:00)-Bangkok, Hanoi, Jakarta'),
(56, '7.0', '(GMT+07:00)-Krasnoyarsk'),
(57, '8.0', '(GMT+08:00)-Beijing, Chongqing, Hong Kong, Urumqi'),
(58, '8.0', '(GMT+08:00)-Irkutsk, Ulaan Bataar'),
(59, '8.0', '(GMT+08:00)-Kuala Lumpur, Singapore'),
(60, '8.0', '(GMT+08:00)-Perth'),
(61, '8.0', '(GMT+08:00)-Taipei'),
(62, '9.0', '(GMT+09:00)-Osaka, Sapporo, Tokyo'),
(63, '9.0', '(GMT+09:00)-Seoul'),
(64, '9.0', '(GMT+09:00)-Vakutsk'),
(65, '9.5', '(GMT+09:30)-Adelaide'),
(66, '9.5', '(GMT+09:30)-Darwin'),
(67, '10.0', '(GMT+10:00)-Brisbane'),
(68, '10.0', '(GMT+10:00)-Canberra, Melbourne, Sydney'),
(69, '10.0', '(GMT+10:00)-Guam, Port Moresby'),
(70, '10.0', '(GMT+10:00)-Hobart'),
(71, '10.0', '(GMT+10:00)-Vladivostok'),
(72, '11.0', '(GMT+11:00)-Magadan, Solomon Is., New Caledonia'),
(73, '12.0', '(GMT+12:00)-Auckland, Wellington'),
(74, '12.0', '(GMT+12:00)-Fiji, Kamchatka, Marshall Is.'),
(75, '-12.0', '(GMT-12:00)-International Date Line West'),
(76, '-11.0', '(GMT-11:00)-Midway Island, Samoa'),
(77, '-10.0', '(GMT-10:00)-Hawaii'),
(78, '-9.0', '(GMT-09:00)-Alaska'),
(79, '-8.0', '(GMT-08:00)-Pacific Time (US & Canada); Tijuana'),
(80, '-7.0', '(GMT-07:00)-Arizona'),
(81, '-7.0', '(GMT-07:00)-Chihuahua, La Paz, Mazatlan'),
(82, '-7.0', '(GMT-07:00)-Mountain Time (US & Canada)'),
(83, '-6.0', '(GMT-06:00)-Central America'),
(84, '-6.0', '(GMT-06:00)-Central Time (US & Canada)'),
(85, '-6.0', '(GMT-06:00)-Guadalajara, Mexico City, Monterrey'),
(86, '-6.0', '(GMT-06:00)-Saskatchewan'),
(87, '-5.0', '(GMT-05:00)-Bogota, Lima, Quito'),
(88, '-5.0', '(GMT-05:00)-Eastern Time (US & Canada)'),
(89, '-5.0', '(GMT-05:00)-Indiana (East)'),
(90, '-4.0', '(GMT-04:00)-Atlantic Time (Canada)'),
(91, '-4.0', '(GMT-04:00)-Caracas, La Paz'),
(92, '-4.0', '(GMT-04:00)-Santiago'),
(93, '-3.5', '(GMT-03:30)-Newfoundland'),
(94, '-3.0', '(GMT-03:00)-Brasilia'),
(95, '-3.0', '(GMT-03:00)-Buenos Aires, Georgetown'),
(96, '-3.0', '(GMT-03:00)-Greenland'),
(97, '-2.0', '(GMT-02:00)-Mid-Atlantic'),
(98, '-1.0', '(GMT-01:00)-Azores'),
(99, '-1.0', '(GMT-01:00)-Cape Verde Is.'),
(100, '0.0', '(GMT)-Casablanca, Monrovia'),
(101, '0.0', '(GMT)-Greenwich Mean Time: Dublin, Edinburgh, Lisbon, London'),
(102, '1.0', '(GMT+01:00)-Amsterdam, Berlin, Bern, Rome, Stockholm, Vienna'),
(103, '1.0', '(GMT+01:00)-Belgrade, Bratislava, Budapest, Ljubljana, Prague'),
(104, '1.0', '(GMT+01:00)-Brussels, Copenhagen, Madrid, Paris'),
(105, '1.0', '(GMT+01:00)-Sarajevo, Skopje, Warsaw, Zagreb'),
(106, '1.0', '(GMT+01:00)-West Central Africa'),
(107, '2.0', '(GMT+02:00)-Athens, Beirut, Istanbul, Minsk'),
(108, '2.0', '(GMT+02:00)-Bucharest'),
(109, '2.0', '(GMT+02:00)-Cairo'),
(110, '2.0', '(GMT+02:00)-Harare, Pretoria'),
(111, '2.0', '(GMT+02:00)-Helsinki, Kyiv, Riga, Sofia, Tallinn, Vilnius'),
(112, '2.0', '(GMT+02:00)-Jerusalem'),
(113, '3.0', '(GMT+03:00)-Baghdad'),
(114, '3.0', '(GMT+03:00)-Kuwait, Riyadh'),
(115, '3.0', '(GMT+03:00)-Moscow, St. Petersburg, Volgograd'),
(116, '3.0', '(GMT+03:00)-Nairobi'),
(117, '3.5', '(GMT+03:30)-Tehran'),
(118, '4.0', '(GMT+04:00)-Abu Dhabi, Muscat'),
(119, '4.0', '(GMT+04:00)-Baku, Tbilisi, Yerevan'),
(120, '4.5', '(GMT+04:30)-Kabul'),
(121, '5.0', '(GMT+05:00)-Ekaterinburg'),
(122, '5.0', '(GMT+05:00)-Islamabad, Karachi, Tashkent'),
(123, '5.5', '(GMT+05:30)-Chennai, Kolkata, Mumbai, New Delhi'),
(124, '5.75', '(GMT+05:45)-Kathmandu'),
(125, '6.0', '(GMT+06:00)-Almaty, Novosibirsk'),
(126, '6.0', '(GMT+06:00)-Astana, Dhaka'),
(127, '6.0', '(GMT+06:00)-Sri Jayawardenepura'),
(128, '6.5', '(GMT+06:30)-Rangoon'),
(129, '7.0', '(GMT+07:00)-Bangkok, Hanoi, Jakarta'),
(130, '7.0', '(GMT+07:00)-Krasnoyarsk'),
(131, '8.0', '(GMT+08:00)-Beijing, Chongqing, Hong Kong, Urumqi'),
(132, '8.0', '(GMT+08:00)-Irkutsk, Ulaan Bataar'),
(133, '8.0', '(GMT+08:00)-Kuala Lumpur, Singapore'),
(134, '8.0', '(GMT+08:00)-Perth'),
(135, '8.0', '(GMT+08:00)-Taipei'),
(136, '9.0', '(GMT+09:00)-Osaka, Sapporo, Tokyo'),
(137, '9.0', '(GMT+09:00)-Seoul'),
(138, '9.0', '(GMT+09:00)-Vakutsk'),
(139, '9.5', '(GMT+09:30)-Adelaide'),
(140, '9.5', '(GMT+09:30)-Darwin'),
(141, '10.0', '(GMT+10:00)-Brisbane'),
(142, '10.0', '(GMT+10:00)-Canberra, Melbourne, Sydney'),
(143, '10.0', '(GMT+10:00)-Guam, Port Moresby'),
(144, '10.0', '(GMT+10:00)-Hobart'),
(145, '10.0', '(GMT+10:00)-Vladivostok'),
(146, '11.0', '(GMT+11:00)-Magadan, Solomon Is., New Caledonia'),
(147, '12.0', '(GMT+12:00)-Auckland, Wellington'),
(148, '12.0', '(GMT+12:00)-Fiji, Kamchatka, Marshall Is.'),
(149, '13.0', '(GMT+13:00)-Nuku''alofa ');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `user_name` varchar(50) NOT NULL,
  `first_name` varchar(25) NOT NULL,
  `last_name` varchar(25) NOT NULL,
  `user_level` enum('admin','restaurant_owner','branch_manager','meal_handler','pilot','operation') NOT NULL,
  `restaurant_id` int(11) NOT NULL,
  `branch_id` int(11) NOT NULL,
  `email` varchar(100) NOT NULL,
  `user_profile` int(11) NOT NULL,
  `password` varchar(150) NOT NULL,
  `inserted_by` int(11) NOT NULL,
  `inserted_date` datetime NOT NULL,
  `last_update` datetime NOT NULL,
  `update_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `user_name`, `first_name`, `last_name`, `user_level`, `restaurant_id`, `branch_id`, `email`, `user_profile`, `password`, `inserted_by`, `inserted_date`, `last_update`, `update_by`) VALUES
(1, 'waled.rayan80', 'waleed', 'rayan', 'admin', 0, 0, 'waled@gmail.com', 1, 'fe703d258c7ef5f50b71e06565a65aa07194907f', 4, '2014-04-14 10:45:49', '2015-10-27 15:55:29', 1),
(6, 'AlaaAssem', 'Alaa', 'Assem', 'admin', 0, 0, 'aassem@mls-egypt.org', 1, '889b4f5d5e81327037322672384ec4a719206516', 1, '2016-02-29 11:50:57', '0000-00-00 00:00:00', 0),
(7, 'heba.hussien', 'heba', 'hussien', 'admin', 0, 0, 'hebahussien83@gmail.com', 1, '2bb2125955689088cffd9a68ff483c98a6f694db', 1, '2016-03-01 09:09:03', '0000-00-00 00:00:00', 0),
(8, 'hello friends', 'let''s do it ', 'for you just do it ', 'admin', 0, 0, 'root', 10, '83353d597cbad458989f2b1a5c1fa1f9f665c858', 1, '2016-08-24 08:02:41', '0000-00-00 00:00:00', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `advertisements`
--
ALTER TABLE `advertisements`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `advertisement_content`
--
ALTER TABLE `advertisement_content`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `attributes`
--
ALTER TABLE `attributes`
  ADD KEY `id` (`id`);

--
-- Indexes for table `cms_module_access`
--
ALTER TABLE `cms_module_access`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contact_us`
--
ALTER TABLE `contact_us`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customer_info`
--
ALTER TABLE `customer_info`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customer_selections`
--
ALTER TABLE `customer_selections`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `events_details`
--
ALTER TABLE `events_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `forget_password`
--
ALTER TABLE `forget_password`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `forms`
--
ALTER TABLE `forms`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `form_attributes`
--
ALTER TABLE `form_attributes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `form_inserted_data`
--
ALTER TABLE `form_inserted_data`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `general_setting`
--
ALTER TABLE `general_setting`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `home_page_layout`
--
ALTER TABLE `home_page_layout`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `localization`
--
ALTER TABLE `localization`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `membership_inquery`
--
ALTER TABLE `membership_inquery`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `nodes`
--
ALTER TABLE `nodes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `nodes_content`
--
ALTER TABLE `nodes_content`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `nodes_image_gallery`
--
ALTER TABLE `nodes_image_gallery`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `nodes_plugins_values`
--
ALTER TABLE `nodes_plugins_values`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `nodes_selected_taxonomies`
--
ALTER TABLE `nodes_selected_taxonomies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `plugins`
--
ALTER TABLE `plugins`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `poll_questions`
--
ALTER TABLE `poll_questions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `poll_questions_options`
--
ALTER TABLE `poll_questions_options`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `profile`
--
ALTER TABLE `profile`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `profile_modules_access`
--
ALTER TABLE `profile_modules_access`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `profile_pages_access`
--
ALTER TABLE `profile_pages_access`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `social_comments`
--
ALTER TABLE `social_comments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `social_email_subscription`
--
ALTER TABLE `social_email_subscription`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `structure_menu_group`
--
ALTER TABLE `structure_menu_group`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `structure_menu_link`
--
ALTER TABLE `structure_menu_link`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `structure_menu_link_content`
--
ALTER TABLE `structure_menu_link_content`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `taxonomies`
--
ALTER TABLE `taxonomies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `taxonomies_content`
--
ALTER TABLE `taxonomies_content`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `themes`
--
ALTER TABLE `themes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `themes_layouts`
--
ALTER TABLE `themes_layouts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `theme_layout_model`
--
ALTER TABLE `theme_layout_model`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `theme_layout_model_plugin`
--
ALTER TABLE `theme_layout_model_plugin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `time_zones`
--
ALTER TABLE `time_zones`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `advertisements`
--
ALTER TABLE `advertisements`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `advertisement_content`
--
ALTER TABLE `advertisement_content`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `attributes`
--
ALTER TABLE `attributes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `cms_module_access`
--
ALTER TABLE `cms_module_access`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=288;
--
-- AUTO_INCREMENT for table `contact_us`
--
ALTER TABLE `contact_us`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `customer_info`
--
ALTER TABLE `customer_info`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `customer_selections`
--
ALTER TABLE `customer_selections`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `events_details`
--
ALTER TABLE `events_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=105;
--
-- AUTO_INCREMENT for table `forget_password`
--
ALTER TABLE `forget_password`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `forms`
--
ALTER TABLE `forms`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `form_attributes`
--
ALTER TABLE `form_attributes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `form_inserted_data`
--
ALTER TABLE `form_inserted_data`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `home_page_layout`
--
ALTER TABLE `home_page_layout`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `localization`
--
ALTER TABLE `localization`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `membership_inquery`
--
ALTER TABLE `membership_inquery`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `nodes`
--
ALTER TABLE `nodes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;
--
-- AUTO_INCREMENT for table `nodes_content`
--
ALTER TABLE `nodes_content`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=81;
--
-- AUTO_INCREMENT for table `nodes_image_gallery`
--
ALTER TABLE `nodes_image_gallery`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `nodes_plugins_values`
--
ALTER TABLE `nodes_plugins_values`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;
--
-- AUTO_INCREMENT for table `nodes_selected_taxonomies`
--
ALTER TABLE `nodes_selected_taxonomies`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=106;
--
-- AUTO_INCREMENT for table `plugins`
--
ALTER TABLE `plugins`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=69;
--
-- AUTO_INCREMENT for table `poll_questions`
--
ALTER TABLE `poll_questions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `poll_questions_options`
--
ALTER TABLE `poll_questions_options`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `profile`
--
ALTER TABLE `profile`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `profile_modules_access`
--
ALTER TABLE `profile_modules_access`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=137;
--
-- AUTO_INCREMENT for table `profile_pages_access`
--
ALTER TABLE `profile_pages_access`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2858;
--
-- AUTO_INCREMENT for table `social_comments`
--
ALTER TABLE `social_comments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `social_email_subscription`
--
ALTER TABLE `social_email_subscription`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `structure_menu_group`
--
ALTER TABLE `structure_menu_group`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `structure_menu_link`
--
ALTER TABLE `structure_menu_link`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `structure_menu_link_content`
--
ALTER TABLE `structure_menu_link_content`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;
--
-- AUTO_INCREMENT for table `taxonomies`
--
ALTER TABLE `taxonomies`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `taxonomies_content`
--
ALTER TABLE `taxonomies_content`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;
--
-- AUTO_INCREMENT for table `themes`
--
ALTER TABLE `themes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `themes_layouts`
--
ALTER TABLE `themes_layouts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `theme_layout_model`
--
ALTER TABLE `theme_layout_model`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;
--
-- AUTO_INCREMENT for table `theme_layout_model_plugin`
--
ALTER TABLE `theme_layout_model_plugin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=86;
--
-- AUTO_INCREMENT for table `time_zones`
--
ALTER TABLE `time_zones`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=150;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
