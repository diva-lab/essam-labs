<?php  
 	require_once("../layout/initialize.php"); 
	$define_class = new Themes(); 
	$records = $define_class->themes_data("uploaded_date","DESC"); 
	require_once("../layout/header.php"); 
?> 
  <!--header end-->  
  <!--sidebar start--> 
  <?php require_once("../layout/navigation.php");?> 
  <!--sidebar end-->  
  <!--main content start--> 
  <section id="main-content"> 
    <section class="wrapper site-min-height"> 
      <h4>Theme Module</h4> 
      <!-- page start--> 
      <section class="panel"> 
        <header class="panel-heading"> View Themes </header> 
        <br> 
          <button type="button" class="btn btn-danger" style="margin-left:15px" onClick="window.location.href = 'insert.php'"> 
               <li class="icon-plus-sign"></li>  Upload New Theme </button> 
        <br> 
        <div class="panel-body"> 
          <div class="adv-table editable-table ">  
            <!--<div class="clearfix"> 
                              <div class="btn-group"> 
                                  <button id="editable-sample_new" class="btn green"> 
                                      Add New <i class="icon-plus"></i> 
                                  </button> 
                              </div> 
                              <div class="btn-group pull-right"> 
                                  <button class="btn dropdown-toggle" data-toggle="dropdown">Tools <i class="icon-angle-down"></i> 
                                  </button> 
                                  <ul class="dropdown-menu pull-right"> 
                                      <li><a href="#">Print</a></li> 
                                      <li><a href="#">Save as PDF</a></li> 
                                      <li><a href="#">Export to Excel</a></li> 
                                  </ul> 
                              </div> 
                          </div>--> 
            <div class="space15"></div> 
                          <table class="table table-striped table-advance table-hover" style="width:550px;"> 
              <thead> 
                <tr> 
                  <th>Template</th> 
                  <th>Information</th> 
                </tr> 
              </thead> 
              <tbody> 
                <?php   
				  foreach($records as $record){ 
					  echo "<tr> 
					  <td><img src='../../../{$record->source}/layout_templates/index.png' width='250' height='500'></td> 
					  <td> 
					  <span style='font-weight:bold'>Name:</span> {$record->name} 
					  <br><span style='font-weight:bold'>Version:</span> {$record->version} 
					  <br><span style='font-weight:bold'>Author:</span> {$record->author} 
					  <br><span style='font-weight:bold'>Uploaded By:</span> {$record->uploaded_by} 
					  <br><span style='font-weight:bold'>Uploaded In:</span> {$record->uploaded_date} 
					  <br><br><br><a href='view_layouts.php?id=$record->id' style='font-weight:bold'>Show Related Layouts</a> 
					  <br><br><a id='custimize_plugin{$record->id}' 
					   href='../plugin_controller/customize_plugins.php?id=$record->id' style='font-weight:bold'>Set Index Plugins</a> 
					  </td> 
					</tr>";?> 
                     <script> 
  <!-- for filtrition--> 
$("#custimize_plugin<?php echo $record->id;?>").fancybox({ 
				'width'				: '75%', 
				'height'			: '75%', 
				'autoScale'			: false, 
				'transitionIn'		: 'none', 
				'transitionOut'		: 'none', 
				 
			 }); 
</script> 
					 
				<?php  } ?> 
                   
              </tbody> 
            </table> 
          </div> 
        </div> 
      </section> 
      <!-- page end-->  
    </section> 
  </section> 
  <!--main content end-->  
  <!--footer start--> 
  <?php require_once("../layout/footer.php");?> 
  