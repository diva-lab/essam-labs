jQuery(document).ready(function($) {
	// process the form
	$('#user_feed').submit(function() {
		$('#submit').attr("disabled", "true");
		var formData = {
			lang: $('#lang').val(), 
			title: $('#title').val(), 
			fbody: $('#body').val(),
			summary:  $('#summary').val(),
			
		};
		// process the form
		$.ajax({
			type 		: 'POST', // define the type of HTTP verb we want to use (POST for our form)
			url 		: 'main/data_model/user_feed.php', // the url where we want to POST
			data 		: formData, // our data object
			success : function(data) {
				if(data.status == 'work'){
					var file_data = new FormData($('#user_feed')[0]);
					$.ajax({
					  type: 'POST',
					  url: "main/data_model/update_cover.php?id="+data.id+"&lang="+$('#lang').val(),
					  data: file_data,
					  cache: false,
					  contentType: false,
					  processData: false,
					  success: function(upload){
				       $("#validation_message_feed").html("<div class='alert alert-block alert-success'>"+upload.message+"</div >");
					   $('#user_feed')[0].reset();
					  }
					});
			
			
					
				}else if (data.status == 'not_work'){
					$("#validation_message_feed").html("<div class='alert alert-block alert-danger'>"+upload.message+"</div >");
					$('#submit').removeAttr('disabled'); 
				
				}else{
					$("#validation_message_feed").html("<div class='alert alert-block alert-danger'>"+upload.message+"</div >");
				}
				
				
			}
		});
	return false;
	});
	
	
		
});