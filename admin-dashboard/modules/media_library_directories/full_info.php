<?php  
	require_once("../layout/initialize.php"); 
	$record_id = $_GET['id']; 
	$define_class = new MediaLibraryDirectories;	 
    $record_info =$define_class->directory_data(null,null,$record_id); 
	require_once("../layout/header.php"); 
?> 
 <!--header end-->  
  <!--sidebar start--> 
  <?php require_once("../layout/navigation.php");?> 
  <!--sidebar end-->  
  <!--main content start--> 
  <section id="main-content"> 
    <section class="wrapper site-min-height"> 
      <h4>Media Library Module</h4> 
      <!-- page start--> 
      <div class="row"> 
        <aside class="col-lg-8"> 
          <section> 
            <div class="panel"> 
              <div class="panel-heading"> Media Library Directory Info</div> 
              <div class="panel-body"> 
                <form class="form-horizontal tasi-form" role="form" id="form_crud" action="data_model/insert.php"> 
                <input type="hidden" id="process_type" value="insert"> 
                  <div class="form-group"> 
                    <label  class="col-lg-2">Name:</label> 
                    <div class="col-lg-8"> 
                    <?php echo $record_info->title?> 
                    </div> 
                    </div> 
                  <div class="form-group"> 
                    <label  class="col-lg-2">Description:</label> 
                    <div class="col-lg-8"> 
                    <?php echo html_entity_decode($record_info->description)?> 
                    </div> 
                  </div> 
                  <div class="form-group"> 
                  <div class="col-lg-offset-2 col-lg-10"> 
                    <button type="button" class="btn btn-info"   
                        onClick="window.location.href = 'update.php?id='+<?php echo $record_id?>" > <li class="icon-edit-sign"></li> Update </button></div> 
                  </div> 
                </form> 
              </div> 
            </div> 
          </section> 
        </aside> 
<div class="col-lg-3"> 
          <section class="panel panel-primary"> 
            <header class="panel-heading"> Entry Information: </header> 
            <div class="panel-body"> 
              <div id="list_info"> 
                <ul> 
                  <li><span style="color:#428bca">> Created By:</span> <?php echo $record_info->inserted_by?></li> 
                  <li><span style="color:#428bca">> Created Date:</span> <?php echo $record_info->created_date?></li> 
                    <li><span style="color:#428bca">> Last Update By:</span> 
                    <?php if($record_info->update_by!=""){ 
					  echo $record_info->update_by; 
				      }else{ 
						  echo "--";} 
					  ?> 
                  </li> 
                  <li><span style="color:#428bca">> Last Update Date:</span> 
                    <?php if($record_info->last_update!="0000-00-00 00:00:00"){ 
					  echo $record_info->last_update; 
				      }else{ 
						  echo "--";} 
					  ?> 
                  </li> 
                </ul> 
              </div> 
            </div> 
          </section> 
        </div>         
      </div> 
      <!-- page end-->  
    </section> 
  </section> 
  <!--main content end-->  
  <!--footer start--> 
   
  <?php require_once("../layout/footer.php");?>