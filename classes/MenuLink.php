<?php 
require_once 'CRUD.php'; 
class MenuLink extends CRUD{ 
   //calss attributes 
   public $id; 
   public $group_id; 
   public $parent_id; 
   public $status; 
   public $image; 
   public $drop_down; 
   public $drop_down_style; 
   public $path_type; 
   public $path; 
   public $external_path; 
   public $inserted_by; 
   public $inserted_date; 
   public $update_by; 
   public $last_update; 
   public $sorting; 
   public $icon; 
   public $user_allowed_page_array; 
   //relation table attribute 
   public $title; 
   public $lang_id; 
   public $description; 
   //push attributes for relational tables 
   public function enable_relation(){ 
		array_push(static::$primary_fields,'title','lang_id','description'); 
   }    
   //define table name and fields 
	protected static $table_name = 'structure_menu_link'; 
	protected static $primary_fields = array('id','parent_id','group_id','sorting','drop_down','drop_down_style','path_type','path','external_path','status','icon', 
	'inserted_by','inserted_date','update_by','last_update','image'); 
	 
	//front pages classes 
	// get Menu and Sub menu Links data for front end only   
	public static function menu_submenu_front_data($sort_filed = null, $order_by = null, $menu_group_alias = null, $parent_id = null, $lang){ 
		$sql = "SELECT structure_menu_link.id AS id, structure_menu_link.parent_id AS parent_id, structure_menu_link.group_id AS group_id,  
				structure_menu_link.icon AS icon, structure_menu_link.path_type AS path_type, structure_menu_link_content.title AS title, 
				structure_menu_link.path AS path,structure_menu_link_content.description AS description,structure_menu_link.image AS image, 
				structure_menu_link.external_path AS external_path,structure_menu_link.drop_down_style AS drop_down_style,structure_menu_link.drop_down 
				AS drop_down 
				FROM structure_menu_link 
				LEFT JOIN nodes ON structure_menu_link.path= nodes.id 
				LEFT JOIN structure_menu_link_content ON structure_menu_link.id = structure_menu_link_content.link_id 
				LEFT JOIN structure_menu_group ON structure_menu_group.id = structure_menu_link.group_id 
				WHERE structure_menu_group.alias = '{$menu_group_alias}'  
				AND parent_id = '{$parent_id}' AND structure_menu_link.status = 'publish' 
				AND structure_menu_link_content.lang_id = $lang"; 
			if(!empty($sort_filed) && !empty($order_by)){ 
				$sql .= " ORDER BY ".$sort_filed." ".$order_by;  
			 } 
			return self::find_by_sql($sql);   
	}	 
	// get the menu link in select option; 
   public function getMenu($parent_id=0, $selected=0, $m_id,$lang,$tab = "" ,$space = ""){   
    
   $record_info = self::find_by_sql("SELECT structure_menu_link.id AS id,  
	            structure_menu_link.parent_id AS parent_id, structure_menu_link.group_id AS group_id,  
		        structure_menu_link_content.title AS title,structure_menu_link.inserted_date AS inserted_date,  
				structure_menu_link.last_update AS last_update, structure_menu_link.path_type AS path_type, 
				structure_menu_link.path AS path,structure_menu_link_content.lang_id as lang_id, 
				structure_menu_link.status AS status,structure_menu_link.icon AS icon,structure_menu_link.sorting AS sorting 
			    FROM `structure_menu_link_content`,`structure_menu_link` 
				LEFT JOIN nodes ON structure_menu_link.path= nodes.id 
				WHERE group_id= {$m_id} 
				AND  `structure_menu_link`.id =`structure_menu_link_content`.link_id 
				AND parent_id = {$parent_id} 
				AND lang_id = '$lang'  
				Order By sorting ASC 
				 "); 
	   foreach($record_info as $record){  
	   if ($record->parent_id > 0){ 
			 $tab .= "-" ; 
			// $space ="&nbsp;&nbsp;"; 
		 }else{ 
			 $tab=""; 
			 $space=""; 
		   } 
		    
	   echo "<option value='" . $record->id ."'"; 
		if($record->id==$selected) {echo "selected"; } 
	        echo " >" .$space.$tab .$record->title . "</option>"; 
	    $this->getMenu($record->id,$selected,$m_id,$lang,$tab,$space); 
		if($record->parent_id  == 0 || $parent_id ==  $record->parent_id ){ 
			$tab =  ""; 
            $space = ""; 
		}else{ 
			$tab  =  substr($tab, -1);  
			$space = ""; 
		} 
	 } 
	   
	  
	  
	 } 
	 // view menu link into ul after inserte 
	 public function view_inserted_menu_links($parent_id=0, $m_id,$lang = null){   
       static $tab; 
       static $space; 
	   $record_info = self::find_by_sql("SELECT structure_menu_link.id AS id, structure_menu_link.parent_id AS parent_id,  
	            structure_menu_link.group_id AS group_id,  
		        structure_menu_link_content.title AS title,structure_menu_link.inserted_date AS inserted_date,  
				structure_menu_link.last_update AS last_update, structure_menu_link.path_type AS path_type, 
				structure_menu_link.sorting AS sorting, 
				structure_menu_link.path AS path, 
				structure_menu_link_content.lang_id as lang_id,structure_menu_link.status AS status,structure_menu_link.icon AS icon 
				FROM `structure_menu_link_content`,`structure_menu_link` 
				LEFT JOIN nodes ON structure_menu_link.path = nodes.id 
				WHERE group_id= {$m_id} 
				AND  `structure_menu_link`.id =`structure_menu_link_content`.link_id 
				AND parent_id={$parent_id} 
				AND lang_id = $lang  
			   ORDER BY sorting ASC"); 
	   echo "<ul class='nav prod-cat module_page_view_list'>"; 
	   $i = 1; 
	   $x =0; 
	   foreach($record_info as $record){  
	    if ($record->parent_id > 0){ 
			 $tab.= ">"; 
			 $space = "&nbsp;&nbsp;"; 
			 $x++; 
			 
		 }else{ 
			 $tab = ""; 
			 $space = ""; 
		   } 
	   echo "<li><a>$space $tab"; 
	   if ($record->parent_id >0){ 
	     echo " $x)"; 
		}else{ 
			echo " $i)"; 
		} 
	    echo " $record->title </a></li>" ; 
	   $this->view_inserted_menu_links($record->id,$m_id,$lang); 
	   $tab = ""; 
	  $space = ""; 
	   $i++; 
	  } 
	  echo "</ul>"; 
	 } 
	  
	// view all  menu link in table   
	public function view_menu_links($parent_id=0,$m_id=1,$lang,$globel_edit,$globel_delete,$user_data){    
	     static $tab; 
		 static $space; 
		 $record_info = self::find_by_sql("SELECT structure_menu_link.id AS id, structure_menu_link.parent_id AS parent_id,  
		        structure_menu_link.group_id AS group_id,  
		        structure_menu_link_content.title AS title,structure_menu_link.inserted_date AS inserted_date,  
				structure_menu_link.last_update AS last_update, structure_menu_link.path_type AS path_type, 
				structure_menu_link.path AS path,  
				structure_menu_link_content.lang_id as lang_id,structure_menu_link.status AS status,structure_menu_link.icon AS icon 
				FROM `structure_menu_link_content`,`structure_menu_link` 
				LEFT JOIN nodes ON structure_menu_link.path= nodes.id 
				WHERE group_id= {$m_id} 
				AND  `structure_menu_link`.id =`structure_menu_link_content`.link_id 
				AND parent_id={$parent_id} 
				AND lang_id = $lang 
				Order BY structure_menu_link.sorting ASC " 
				 
				); 
		  foreach($record_info as $record){ 
		  if($record->group_id==$m_id){ 
			if($record->parent_id!=0){  
				$tab.= ">"; 
			    $space="&nbsp;&nbsp;"; 
			 }else{  
			  $tab=""; 
			  $space=""; 
			 } 
			 echo "<tr> 
			 <td><a href='full_info.php?id={$record->id}'> $space $tab{$record->title}</a></td> 
			 <td>{$record->inserted_date}</td> 
			 <td>"; 
				$opened_module_page_fullinfo = 'menu_link/full_info'; 
				$opened_module_page_update = 'menu_link/update'; 
				$opened_module_page_delete = 'menu_link/delete';	 
				//full info 
				if(!in_array($opened_module_page_fullinfo, $this->user_allowed_page_array)){ 
					echo " <a href='full_info.php?id={$record->id}' class='btn btn-primary btn-xs tooltips' data-placement='top' data-toggle='tooltip' 
					data-original-title='Full info' disabled>&nbsp;<i class='icon-info'></i>&nbsp;</a>";	 
				}else{ 
					echo " <a href='full_info.php?id={$record->id}' class='btn btn-primary btn-xs tooltips' data-placement='top' data-toggle='tooltip'  
					data-original-title='Full info' >&nbsp;<i class='icon-info'></i>&nbsp;</a>";	 
				} 
				//update 
				if(!in_array($opened_module_page_update, $this->user_allowed_page_array)){ 
					 echo " <a href='update.php?id={$record->id}' class='btn btn-primary btn-xs tooltips' data-placement='top' data-toggle='tooltip'  
					   data-original-title='Edit' disabled >  <i class='icon-edit'></i></a>"; 
				}else{ 
			        if($globel_edit == 'all_records' || $record->inserted_by == $user_data){ 
						echo " <a href='update.php?id={$record->id}' class='btn btn-primary btn-xs tooltips' data-placement='top' data-toggle='tooltip  
						data-original-title='Edit' ><i class='icon-edit'></i></a>"; 
					}else { 
					   echo "<a href='update.php?id={$record->id}' class='btn btn-primary btn-xs tooltips' data-placement='top' data-toggle='tooltip'  
					   data-original-title='Edit' disabled >  <i class='icon-edit'></i></a>"; 
					}	 
				}	 
				//delete	 
				if(!in_array($opened_module_page_delete, $this->user_allowed_page_array)){ 
					 echo " <a href= '#myModal'  class='btn btn-primary btn-xs tooltips' data-placement='top' data-toggle='tooltip' data-original-title='Delete'  
					 disabled ><i class='icon-remove'></i></a>"; 
				}else{ 
				    if($globel_delete == 'all_records' || $record->inserted_by == $user_data){ 
						 echo " <a href='#my{$record->id}' data-toggle='modal' class='btn btn-primary btn-xs tooltips' data-placement='top'  
						 data-original-title='Delete'> <i class='icon-remove'></i></a>"; 
					}else{ 
						 echo " <a href= '#myModal'  class='btn btn-primary btn-xs tooltips' data-placement='top' data-toggle='tooltip' data-original-title='Delete' 
						  disabled > <i class='icon-remove'></i></a>"; 
					}		 
				}				 
							echo   
				 "</td> 
              </tr> 
			  <div class='modal fade' id='my{$record->id}' tabindex='-1' role='dialog' aria-labelledby='myModalLabel' aria-hidden='true'> 
                                  <div class='modal-dialog'> 
                                      <div class='modal-content'> 
                                          <div class='modal-header'> 
                                              <button type='button' class='close' data-dismiss='modal' aria-hidden='true'>&times;</button> 
                                              <h4 class='modal-title'>Delete</h4> 
                                          </div> 
                                          <div class='modal-body'> 
                                           <p> Are you sure you want delete  $record->title??</p> 
                                          </div> 
                                          <div class='modal-footer'> 
                                              <button class='btn btn-warning' type='button' onClick=\"window.location.href =  
											  'data_model/delete.php?task=delete&id={$record->id}&lang={$lang}'\"/> Confirm</button> 
                                              <button data-dismiss='modal' class='btn btn-default' type='button'>Close</button> 
                                          </div> 
                                      </div> 
                                  </div> 
                              </div>"; 
		  			 $this->view_menu_links($record->id,$m_id,$lang,$globel_edit,$globel_delete,$user_data); 
		   			 $tab=""; 
					 $space=""; 
		  } 
	    } 
	} 
	 
	 
} 
?>