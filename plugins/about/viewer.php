 <section class="col-md-12 give-padding-tb"><!--start of internal content-->
        <div class="container">
          <h2 class="col-md-12 give-title-slogan give-display-inline">Sharm El Shiekh</h2>
          <div class="row clearfix give-mb-lg">
            <div class="col-md-6 about-text">
              <p class="col-md-12 give-text-c">Ideally situated on the southern tip of the Sinai Peninsula, Sharm El Sheikh is considered as one of the world’s renowned vacation destinations.

With its unique marine life, Sharm El Sheikh has given the Red Sea an international reputation as one of the world’s most extraordinary and exquisite diving sites. Sharm El Sheikh is known for its magical desert mountains, which offer adventure and excitement on jeep safaris, horse and camel rides.

As for the nightlife, Sharm El Sheikh offers a wide range of venues, discos, pubs, and casinos. A wide array of restaurants offering a variety of cuisines including Oriental, Lebanese, Indian, Chinese, French and Italian are available, health clubs, golf courses, and shops offering local and foreign products.

Sharm El Sheikh combines the thrill of water sports and the simplicity of the sun, sea and sand with the joy of night activities.

Enjoying a warm and sunny climate all year round, Sharm El Sheikh is only a 50-minute flight from the Egyptian capital; Cairo, and many European charter operators now offer direct flights to Sharm El Sheikh from several European and Arabs capitals.</p>
            </div>
           
          </div>

          <h2 class="col-md-12 give-title-slogan give-display-inline">HSDVC Location</h2>
          <p class="col-md-12 give-text-c">
            Hilton Sharm Dreams Vacation Club commands one of the finest positions in central Sharm El Sheikh, in the heart of Naama Bay and 7.5 miles from Sharm el-Sheikh International Airport.
 
Providing its owners and guests 750 meters of the best stretch of Naama Bay; a truly unique experience in the center of the most popular beachfront in Sharm El Sheikh.
 Excellent facilities on the beach such as football, volleyball courts, water sports activities, onsite diving center and offshore activities to keep the most active guest satisfied.
 
Gourmet restaurants on the beach for the perfect romantic dinner and bars serving a full complement of beverages and snacks to enjoy an unforgettable seaside experience.
 
Board the Caribi Bar 'ship' to listen to live music or play billiards. Dine on a delicious buffet on the terrace of Le Jardin, try Italian dishes at Casa Sharm or Mexican food at Tex-Mex.
 
Enjoy authentic Lebanese food in Al Arze overlooking the main swimming pool and the mountains of Sinai.
          </p>

           <div class="col-md-12 internal-map">
              <iframe src="https://www.google.com/maps/embed?pb=!1m10!1m8!1m3!1d13827.245523305372!2d30.91304335!3d29.956103600000006!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2seg!4v1465214332398" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
              
            </div>

        </div>
      </section><!--end of internal content-->