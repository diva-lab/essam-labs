<?php 
require_once("../layout/initialize.php"); 
require_once("../layout/header.php"); 
?> 
<script type="text/javascript" src="../../js-crud/crud_localization.js"></script> 
  <!--header end-->  
  <!--sidebar start--> 
  <?php require_once("../layout/navigation.php");?> 
  <!--sidebar end-->  
  <!--main content start--> 
  <section id="main-content"> 
      <section class="wrapper site-min-height"> 
      <h4> Localization Module</h4> 
       <div class="row"> 
          <aside class="col-lg-8"> 
            <section> 
              <div class="panel "> 
                <div class="panel-heading"> Add Languages</div> 
                <div class="panel-body"> 
              <form class="form-horizontal tasi-form" role="form" id="form_crud" action="data_model/insert.php">
                  <input type="hidden" id="process_type" value="insert"> 
                  <div class="form-group"> 
                  <label  class="col-lg-2">Name:</label> 
                  <div class="col-lg-8"> 
                    <input type="text" class="form-control" id="name" placeholder=" " autocomplete="off"/> 
                  </div> 
                </div> 
                  <div class="form-group"> 
                  <label  class="col-lg-2">Label:</label> 
                  <div class="col-lg-8"> 
                    <input type="text" class="form-control" id="label" placeholder=" " autocomplete="off"/> 
                  </div> 
                </div> 
                  <div class="form-group"> 
                  <label  class="col-lg-2">Sorting:</label> 
                  	<div class="col-lg-8"> 
                    	<input type="text" class="form-control" id="sorting" placeholder=" " autocomplete="off" style="width:50px;"/> 
                  	</div> 
                </div> 
                <div class="form-group">
                  <div class="col-lg-offset-2 col-lg-10">
                    <button type="submit" class="btn btn-info" id="submit">Save</button>
                    <button type="reset" class="btn btn-default">Cancel</button>
                    <div id="loading_data"></div>
                  </div>
                </div>
                
                  
               </form> 
              </div> 
             </div> 
             
           </section> 
        </aside>
        <div class="col-lg-4"> 
          <section class="panel panel-primary"> 
            <header class="panel-heading"> Publish Options: </header> 
            <div class="panel-body"> 
                <form class="form-horizontal tasi-form" role="form"> 
                 <div class="form-group"> 
              <label class="col-lg-5">Status:</label> 
              <div class="col-lg-6"> 
                <label class="checkbox-inline"> 
                  <input type="radio" name="shadow" class="radio" value="disable"> 
                  disable</label> 
                <label class="checkbox-inline"> 
                  <input type="radio" name="shadow" class="radio" value="active" checked> 
                  active</label> 
              </div> 
            </div> 
               </form> 
              </div> 
          </section> 
        </div> 
       </div> 
       <!-- page end-->  
    </section> 
  </section> 
  <!--main content end-->  
  <!--footer start--> 
  
  <?php require_once("../layout/footer.php");?>