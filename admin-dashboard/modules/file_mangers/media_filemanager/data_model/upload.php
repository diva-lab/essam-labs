<?php 
require_once('../../../../../classes/Session.php'); 
require_once('../../../../../classes/Functions.php'); 
require_once('../../../../../classes/UploadFile.php'); 
require_once("../../../../../classes/resize_class.php"); 
//get file id and return file name 
$directory_title = $_GET['title']; 
$path = "../../../../../media-library/"; 
//if file exist get name and begin upload 
if(file_exists($path.$directory_title)){ 
	//file 
	$file =  $_FILES['file']; 
	//root 
	$root = "../../../../../media-library/{$directory_title}/"; 
	$root_thumb = "../../../../../media-library-thumb/{$directory_title}/"; 
	//initialize class, insert rrot , allow exte. size 
	$upload_file = new UploadFile($root , array('jpg','JPG','png','PNG','jpeg','JPEG','gif','GIF','svg'), 5000000); 
	//uploading file 
	$upload_file->attach_file($file); 
	//if uploading succeeded 
	if($upload_file->succeeded == "yes"){ 
	  //resize images 
		//file name 
		$file_name = $upload_file->new_file_name; 
		$file_extension = $upload_file->file_extension; 
		$typs =array('jpg','JPG','png','PNG','jpeg','JPEG','gif','GIF');	 
		//resize	 
		// *** 1) Initialise / load image 
		if(in_array($file_extension,$typs)){	 
			 
			//large 
			// *** 1) Initialise / load image 
			$resizeObj_medium = new resize($root.$file_name); 
			// *** 2) Resize image (options: exact, portrait, landscape, auto, crop) 
			$resizeObj_medium->resizeImage(900,524, 'exact'); 
			// *** 3) Save image 
			$resizeObj_medium->saveImage($root_thumb.'large/'.$file_name, 100); 
			//medium 
			// *** 1) Initialise / load image 
			$resizeObj_small = new resize($root.$file_name); 
			// *** 2) Resize image (options: exact, portrait, landscape, auto, crop) 
			$resizeObj_small->resizeImage(228,297, 'exact'); 
			// *** 3) Save image 
			$resizeObj_small->saveImage($root_thumb.'medium/'.$file_name, 100); 
			//small 
			// *** 1) Initialise / load image 
			$resizeObj_thumb = new resize($root.$file_name); 
			// *** 2) Resize image (options: exact, portrait, landscape, auto, crop) 
			$resizeObj_thumb->resizeImage(131 ,87, 'exact'); 
			// *** 3) Save image 
			$resizeObj_thumb->saveImage($root_thumb.'small/'.$file_name, 100); 
			
			}
			 
		} 
	}