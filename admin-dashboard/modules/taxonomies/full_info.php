<?php	 
require_once("../layout/initialize.php"); 
if(isset($_GET['id']) && is_numeric($_GET['id'])){ 
	$record_id = $_GET['id']; 
	$define_class = new Taxonomies();
	$define_class->enable_relation(); 
	$record_info = $define_class->find_by_id($record_id); 
	//check id access 
	if(empty($record_info->id)){ 
		redirect_to("view.php");	 
	}
}else{ 
	redirect_to("view.php");	 
} 
require_once("../layout/header.php");	 
?> 
<!--header end--> 
<!--sidebar start--> 
<?php require_once("../layout/navigation.php");?> 
<!--sidebar end-->  
<!--main content start--> 
<section id="main-content"> 
  <section class="wrapper site-min-height"> 
    <h4>Taxonomy Categories Module</h4> 
    <div class="row"> 
      <aside class="col-lg-8"> 
        <section> 
          <div class="panel"> 
            <div class="panel-heading"> Taxonomy Category Info</div> 
            <div class="panel-body"> 
              <form class="form-horizontal tasi-form" role="form" id="form_crud"> 
                <input type="hidden" id="process_type" value="insert"> 
                <section class="panel"> 
                  <header class="panel-heading tab-bg-dark-navy-blue"> 
                    <ul class="nav nav-tabs"> 
                      <li class=" center-block active" > <a data-toggle="tab" href="#main_info" class="text-center"><strong>Main Info</strong></a></li>
                       
                      <li class=" center-block"> <a data-toggle="tab" href="#sub_cat_option" class="text-center"><strong>Sub-Cat Option</strong></a></li> 
                    </ul> 
                  </header> 
                  <div class="panel-body"> 
                    <div class="tab-content"> 
                      <div id="main_info" class="tab-pane active "> 
                        <section class="panel col-lg-9"> 
                          <header class="panel-heading tab-bg-dark-navy-blue "> 
                            <ul class="nav nav-tabs"> 
                              <?php 
                            //create tabs for all available languages  
                            $languages = Localization::find_all('id','desc'); 
                            $serial_tabs = 1; 
                            foreach($languages as $language){ 
                                $lang_tab_header = ucfirst($language->name); 
                                echo "<li class='";if($serial_tabs == 1){ echo " active ";}  echo"'> <a data-toggle='tab' href='#$language->name'> 
                                <strong>$lang_tab_header</strong></a></li>"; 
                                $serial_tabs++; 
                            } 
                          ?> 
                            </ul> 
                          </header> 
                          <div class="panel-body"> 
                            <div class="tab-content"> 
                              <?php 
                            $serial_tabs_content = 1; 
                            foreach($languages as $language){ 
								//get data by language 
								$main_content = $define_class->get_taxonomy_content($record_id, $language->id); 
								echo "<div id='$language->name' class='tab-pane"; if($serial_tabs_content == 1){ echo " active ";} echo"'>"; 
								if($main_content){ 
									echo " 
										<div class='form-group'> 
										<label  class='col-lg-2'>Name:</label> 
										<div class='col-lg-9'>$main_content->name</div> 
									  </div> 
									  <div class='form-group'> 
										<label class='col-lg-2'>Alias:</label> 
										<div class='col-lg-9'>$main_content->alias</div> 
									  </div> 
									  <div class='form-group'> 
										<label class='col-lg-2'>Description:</label> 
										<div class='col-md-9'>$main_content->description</div> 
									  </div>"; 
                            	} 
								echo "</div>"; 
								$serial_tabs_content++; 
							} 
                          ?> 
                            </div> 
                          </div> 
                        </section> 
                      </div> 
                      <div id="sub_cat_option" class="tab-pane "> 
                      <div class='form-group '  > 
                          <label class='col-lg-2'>Type:</label> 
                          <div class='col-lg-2'> 
                          <?php  echo $record_info->taxonomy_type?> 
                          </div> 
                        </div>
                        <div class="form-group <?php if($record_info->taxonomy_type =="tag" || $record_info->taxonomy_type =="author"){echo "hide"; } ?>"> 
                          <label  class="col-lg-2 ">Parent:</label> 
                          <div class="col-lg-8" > 
                            <?php  
                            if($record_info->parent_id == 0){ 
                                echo "Root"; 
                            }else{ 
                                  $perant =  $define_class->get_taxonomy_content($record_info->parent_id,$general_setting_info->translate_lang_id); 
                                  echo $perant->name; 
                            } ?> 
                          </div> 
                        </div> 
                         <div class='form-group <?php if($record_info->taxonomy_type =="tag" || $record_info->taxonomy_type =="author"){echo "hide"; } ?>'  > 
                          <label class='col-lg-2'>Sorting:</label> 
                          <div class='col-lg-2'> 
                          <?php  echo $record_info->sorting?> 
                          </div> 
                        </div>
                      </div> 
                    </div> 
                  </div> 
                </section> 
                <div class="form-group"> 
                <div class="col-lg-offset-2 col-lg-10"> 
                  <button type="button" class="btn btn-info" onClick="window.location.href = 'update.php?id=<?php echo $record_id?>'" ><i class="icon-edit-sign"></i>Update </button> 
                </div> 
              </div> 
              </form> 
            </div> 
          </div> 
        </section> 
      </aside> 
      <div class="col-lg-4"> 
        <section class="panel panel-primary"> 
          <header class="panel-heading tab-bg-dark-navy-blue"> 
            <ul class="nav nav-tabs"> 
              <li class=" center-block active" ><a data-toggle="tab" href="#op1" class="text-center"> <i class=" icon-check"> </i> <strong> Entry Info</strong> </a> </li> 
              <li class=" center-block " ><a data-toggle="tab" href="#op2" class="text-center"> <i class=" icon-check"> </i> <strong> Publish Option</strong> </a> </li> 
            </ul> 
          </header> 
          <div class="panel-body"> 
            <div id="list_info"> 
              <ul> 
                <div class="tab-content"> 
                  <div id="op1" class="tab-pane active "> 
                    <li><span style="color:#428bca">> Created By:</span> <?php echo $record_info->inserted_by?></li> 
                    <li><span style="color:#428bca">> Created Date:</span> <?php echo $record_info->inserted_date?></li> 
                    <li><span style="color:#428bca">> Last Update By:</span> 
                      <?php if($record_info->update_by){ 
					             echo $record_info->update_by; 
							  }else{ 
								  echo "--";} 
					 	?> 
                    </li> 
                    <li><span style="color:#428bca">> Last Update Date:</span> 
                      <?php if($record_info->last_update!=""){ 
					  echo $record_info->last_update; 
				      }else{ 
						  echo "--"; 
					 } 
					  ?> 
                    </li> 
                  </div> 
                  <div id="op2" class="tab-pane  "> 
                    <li><span style="color:#428bca">> Status:</span> <?php echo $record_info->status?></li> 
                     <li><span style="color:#428bca">> Show in Main Menu:</span> <?php echo $record_info->main_menu?></li> 
                     <li><span style="color:#428bca">> Show Image:</span> <?php echo $record_info->show_image?></li> 
                      
                  </div> 
                </div> 
              </ul> 
            </div> 
          </div> 
        </section> 
        <section class="panel panel-primary"> 
          <header class="panel-heading">Included Images</header> 
          <div class="panel-body"> 
            <div class="form-group"> 
              <label  class="col-lg-6">Image Cover</label> 
              <div class="col-lg-6  <?php if(!$record_info->cover) echo "hide"; ?>" > <img src="../../../media-library/<?php echo $record_info->cover?>"  style="width:100px; height:100px;"> </div> 
            </div> 
          </div> 
        </section> 
      </div> 
    </div> 
    <!-- page end-->  
  </section> 
</section> 
<!--main content end-->  
<!--footer start--> 
<?php require_once("../layout/footer.php");?>