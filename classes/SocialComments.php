<?php 
require_once 'CRUD.php'; 
class SocialComments extends CRUD{ 
 //calss attributes 
   public $id; 
   public $node_id; 
   public $user_name; 
   public $title; 
   public $status; 
   public $email; 
   public $body; 
   public $inserted_date; 
   public $update_by; 
   public $last_update; 
  //relation table attribute 
   public $node_title; 
   public $node_type; 
  //push attributes for relational tables 
   public function enable_relation(){ 
    array_push(static::$primary_fields , 'node_title','node_type'); 
  }    
   //define table name and fields 
	protected static $table_name = 'social_comments'; 
    protected static $primary_fields = array('id', 'node_id','user_name', 'title', 'status','email' ,'body','inserted_date','update_by', 'last_update'); 
   // get event data 
    public function comment_data($sort_filed = null, $order_by = null, $id = null,$lang = null){ 
       $sql = "SELECT  DISTINCT social_comments.id AS id, nodes_content.title AS node_title ,social_comments.title AS title, 
	           social_comments.user_name AS user_name ,social_comments.email AS email,nodes.node_type AS node_type, 
               social_comments.status AS status,social_comments.body AS body, social_comments.inserted_date AS inserted_date, 
               user2.user_name AS update_by,social_comments.last_update AS last_update 
               FROM social_comments 
               LEFT JOIN users AS user2 ON social_comments.update_by = user2.id 
               LEFT JOIN nodes_content ON social_comments.node_id = nodes_content.node_id 
			   LEFT JOIN nodes ON social_comments.node_id = nodes.id 
			   WHERE 1 "; 
			   if(!empty($lang )){ 
				   $sql.= "  AND  nodes_content.lang_id = {$lang}"; 
			    } 
			   if(!empty($id)){		 
					$sql .= " AND social_comments.id = $id "; 
					$result_array = static::find_by_sql($sql); 
			 return !empty($result_array)? array_shift($result_array) : false; 
	   
			  }else{		 
				if(!empty($sort_filed) && !empty($order_by)){ 
				   $sql .= " ORDER BY ".$sort_filed." ".$order_by; } 
					 return self::find_by_sql($sql);  }				 
	  } 
	//return post tags  
	public function return_post_social_comments($node_id){ 
         $sql = "SELECT social_comments.user_name AS  user_name , social_comments.body AS body,  
		    social_comments.title AS title, social_comments.inserted_date AS inserted_date FROM  social_comments, nodes 
          WHERE social_comments.node_id = '$node_id' AND nodes.id = social_comments.node_id AND social_comments.status ='publish'  "; 
          $result_array = static::find_by_sql($sql); 
          return self::find_by_sql($sql);   
	}	 
} 
?>
