<?php 
require_once 'CRUD.php'; 
class ProfileModulesAccess extends CRUD{ 
	//calss attributes 
	public $id; 
	public $profile_id; 
	public $module_id; 
	public $access; 
	public $inserted_by; 
	public $inserted_date; 
	//relation table attribute 
	public $module_title; 
	public $module_source; 
	public $module_sorting; 
	public $module_icon; 
	 
	//push attributes for relational tables 
	public function enable_relation(){ 
		array_push(static::$primary_fields, 'module_title', 'module_source', 'module_sorting', 'module_icon'); 
	}    
	 
	//define table name and fields 
	protected static $table_name = 'profile_modules_access'; 
	protected static $primary_fields = array('id', 'profile_id', 'module_id', 'access', 'inserted_by','inserted_date'); 
	 
	//check module availabilty 
	public static function check_module_availabilty($profile, $module, $access = null){ 
		$sql = "SELECT * FROM profile_modules_access WHERE profile_id = '{$profile}' AND module_id = '{$module}' "; 
		if(!empty($access)){ 
			$sql .= "AND access = '{$access}'"	; 
		} 
		$result_set = self::find_by_sql($sql); 
		return !empty($result_set)? array_shift($result_set) : false; 
	} 
	 
	//get module data 
	public function profile_modules_data($profile_id, $access = null, $sort_filed = null, $order_by = null){ 
		$sql = "SELECT cms_module_access.id AS module_id, cms_module_access.title AS module_title, cms_module_access.sorting AS module_sorting, 
		 		profile_modules_access.access as access 
				FROM cms_module_access, profile_modules_access 
				WHERE cms_module_access.id = profile_modules_access.module_id AND profile_modules_access.profile_id = $profile_id ";	 
		if(!empty($access)){ 
			$sql .= "   AND profile_modules_access.access = '{$access}'"; 
		} 
		if(!empty($sort_filed) && !empty($order_by)){ 
		   $sql .= "  ORDER BY ".$sort_filed."  ".$order_by; 
		} 
		return static::find_by_sql($sql);  		 
	}	 
	 
	//get modules for login user  
	public function user_profile_modules_data($profile_id){ 
		$sql = "SELECT  cms_module_access.icon AS module_icon, cms_module_access.id AS module_id, cms_module_access.title AS module_title, 
		 		cms_module_access.file_source AS module_source, profile_modules_access.profile_id AS profile_id 
				FROM profile_modules_access, cms_module_access 
				WHERE profile_modules_access.profile_id = '{$profile_id}' AND profile_modules_access.module_id = cms_module_access.id 
				AND cms_module_access.type = 'module' AND profile_modules_access.access = 'yes' ORDER BY cms_module_access.sorting ASC";	 
		return self::find_by_sql($sql);  		 
	}	 
} 
?>