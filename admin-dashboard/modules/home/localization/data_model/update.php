<?php 
require_once('../../../../classes/Functions.php'); 
require_once('../../../../classes/MysqlDatabase.php'); 
require_once('../../../../classes/Localization.php'); 
require_once('../../../../classes/Session.php'); 
header('Content-Type: application/json'); 
if(!empty($_POST["task"]) && $_POST["task"] == "update"){ 
	$required_fields = array('name'=>"- Insert name",'label'=>'- Insert label' ); 
	$check_required_fields = check_required_fields($required_fields); 
    if(count($check_required_fields) == 0){ 
	// Check if the directory is exist 
		  $path="../../../../localization/"; 
		  $label=$_POST["label"]; 
		  $path_file=$path.$label."/"; 
		  $check_title_availability = Localization::find_by_custom_filed('label',$_POST["label"]); 
		  //get data 
		  $id = $_POST['record']; 
		  $record_info = Localization::find_by_id($id); 
		   if($check_title_availability != "" && file_exists($path_file) && $check_title_availability->id != $id){ 
			  $data  = array("status"=>"exist"); 
			  echo json_encode($data);} 
		  else{ 
		   //update 
		   if(strpbrk($label, "\\/?%*:|\"<>") === false) 
			 { 	 
				  $edit = new Localization(); 
				  $edit->id = $id; 
				  $edit->name = $_POST["name"]; 
				  $edit->label = $_POST["label"]; 
				  $update = $edit->update(); 
				  $old_name = $path.$record_info->label; 
				  $new_name = $path.$edit->label; 
				  rename($old_name,$new_name); 
				  if($update){ 
					  $data  = array("status"=>"work"); 
					  echo json_encode($data); 
				  }else{ 
					  $data  = array("status"=>"error"); 
					  echo json_encode($data); 
				  } 
			 }else{ 
				  $data  = array("status"=>"wrong"); 
				  echo json_encode($data); 
				  } 
			} 
	}else{ 
		//validation error 
		$comma_separated = implode("<br>", $check_required_fields); 
		$data  = array("status"=>"valid_error", "fileds"=>$comma_separated); 
		echo json_encode($data); 
	}			     
} 
//close connection 
if(isset($database)){ 
	$database->close_connection(); 
} 
?>