<?php 
require_once 'CRUD.php'; 
class ThemesLayouts extends CRUD{ 
   //calss attributes 
   public $id; 
   public $theme_id; 
   public $version; 
   public $layout_name; 
   public $layout_cells; 
   public $source; 
   public $author; 
   public $uploaded_date; 
   public $uploaded_by;   
   //relation table attribute 
    //push attributes for relational tables 
   public function enable_relation(){ 
		array_push(static::$primary_fields , 'user_name'); 
   }    
   //define table name and fields 
	protected static $table_name = 'themes_layouts'; 
	protected static $primary_fields = array('id', 'theme_id', 'version', 'layout_name', 'layout_cells', 'source', 'author', 'uploaded_date', 'uploaded_by'); 
	 
	public function theme_layouts_data($sort_filed = null, $order_by = null, $id){ 
		$sql = "SELECT themes_layouts.id AS id, themes_layouts.version AS version, themes_layouts.theme_id AS theme_id, themes_layouts.layout_name AS layout_name, 
				themes_layouts.layout_cells AS layout_cells, themes_layouts.author AS author, themes_layouts.source AS source, users.user_name AS uploaded_by, 
				themes_layouts.uploaded_date AS uploaded_date 
				FROM themes_layouts 
				LEFT JOIN users ON themes_layouts.uploaded_by = users.id"; 
		if(!empty($id)){ 
			$sql .= " WHERE themes_layouts.theme_id = ".$id;  
		} 
		if(!empty($sort_filed) && !empty($order_by)){ 
			$sql .= " ORDER BY ".$sort_filed." ".$order_by;  
		 } 
		return self::find_by_sql($sql);  			 
	} 
} 
?>
