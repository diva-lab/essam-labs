<?php 
require_once('../../../../classes/Session.php'); 
require_once('../../../../classes/Functions.php'); 
require_once('../../../../classes/MysqlDatabase.php'); 
require_once('../../../../classes/Users.php'); 
if(!empty($_POST["task"]) && $_POST["task"] == "update"){ 
	header('Content-Type: application/json');			 
	$user_id = $session->user_id; 
	$current_password =  sha1(md5($_POST["current_password"])); 
	$new_password =  sha1(md5($_POST["new_password"])); 
	//edit user password 
	$edit = Users::find_by_id($user_id); 
	//if current password match with password from DB update else do not update 
	if($edit->password == $current_password){ 
		if($current_password == $new_password){ 
			$data  = array("status"=>"password_notnew"); 
			echo json_encode($data);				 
		}else{ 
			$edit->password = $new_password; 
			$update = $edit->update(); 
			if($update){ 
				$data  = array("status"=>"work"); 
				echo json_encode($data); 
			}else{ 
				$data  = array("status"=>"error"); 
				echo json_encode($data); 
			} 
		} 
	}else{ 
		$data  = array("status"=>"password_wrong"); 
		echo json_encode($data);		 
	} 
} 
//close connection 
if(isset($database)){ 
	$database->close_connection(); 
} 
?>