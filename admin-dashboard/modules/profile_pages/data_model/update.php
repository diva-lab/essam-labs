<?php 
require_once('../../../../classes/Session.php'); 
require_once('../../../../classes/Functions.php'); 
require_once('../../../../classes/MysqlDatabase.php'); 
require_once('../../../../classes/ProfilePagesAccess.php'); 
//check log in  
if($session->is_logged() == false){ 
	redirect_to("../../index.php"); 
} 
//update all pages access to be no 
//update selected pages access to be yes 
if(!empty($_POST["task"]) && $_POST["task"] == "update"){ 
	//change access to all pages under this profile to be no 
	$profile_id = $_POST["profile_id"]; 
	$profile_id = $database->escape_values($profile_id); 
	$sql_update_all_pages = "UPDATE profile_pages_access SET access = 'no' WHERE profile_id = '{$profile_id}'"; 
	$update_all_pages_query = $database->query($sql_update_all_pages); 
	 header('Content-Type: application/json'); 
	if($update_all_pages_query){ 
		//update access of selected pages 
		$pages_id = $_POST["pages_id"]; 
		$implode_pages = $database->escape_values(implode(',', $pages_id)); 
		$sql_update_pages_access = "UPDATE profile_pages_access SET access = 'yes' WHERE profile_id = '{$profile_id}' AND Page_id IN ($implode_pages)"; 
		$update_all_pages_access_query = $database->query($sql_update_pages_access); 
		if($update_all_pages_access_query){ 
			$data  = array("status"=>"work"); 
			echo json_encode($data); 
		}else{ 
			$data  = array("status"=>"error_update_selected"); 
			echo json_encode($data);			 
		} 
	}else{ 
		$data  = array("status"=>"error_update_all"); 
		echo json_encode($data);		 
	} 
} 
//close connection 
if(isset($database)){ 
	$database->close_connection(); 
} 
?>