
<!-- Place inside the <head> of your HTML -->
<script type="text/javascript" src="modules/plugins/texteditor4/tinymce/tinymce/js/tinymce/tinymce.min.js"></script>
<script type="text/javascript">
tinymce.init({
selector: "textarea",
theme: "modern",
width: 680,
height: 300,
subfolder:"",
plugins: [
"advlist autolink link image lists charmap print preview hr anchor pagebreak",
"searchreplace wordcount visualblocks visualchars code insertdatetime media nonbreaking",
"table contextmenu directionality emoticons paste textcolor filemanager template  fullscreen save autoresize"
],



templates: [ 
        {title: 'Artilce template 1', description: 'Article template 1', url: 'modules/plugins/texteditor4/templates/template1.html'}, 
        {title: 'ِArticle Template 2', description: 'Article Template 2', url: 'modules/plugins/texteditor4/templates/template2.html'} 
    ],

autoresize_min_height:400,
autoresize_max_height:800,
image_advtab: true,
directionality : 'rtl',

toolbar: " undo redo |styleselect | fontselect | fontsizeselect  bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | styleselect forecolor backcolor | link unlink anchor | image media | removeformat | print code template preview save fullscreen  ",

          fontsize_formats: "8pt 9pt 10pt 11pt 12pt 26pt 36pt",

      




}); 


</script>
