jQuery(document).ready(function($) {
	// process the form
	$('#fsubmit').click(function(){
		//$('#submit').attr("disabled", "true");
		var formData = {
			lang:$('#lang').val(),
			email:$('#email').val()
		};
		// process the form
		$.ajax({
			type 		: 'POST', // define the type of HTTP verb we want to use (POST for our form)
			url 		: 'main/data_model/forget_password_send_email.php', // the url where we want to POST
			data 		: formData, // our data object
			beforeSend: function(){
			//show laoding
			$('#validation_message').html('<img src="main/images/loading.gif"/>');
			},
			success : function(data) {

         
			
				if(data.status == "work"){
					$("#validation_message_sf").html("");
					$("#validation_message_sf").html("<div class='alert alert-block alert-success'>"+data.message+"</div >");
					$('.primary-form')[0].reset();
				}else{
					$("#validation_message").html("");
					$("#validation_message").html("<div class='alert alert-block alert-danger'>"+data.message+"</div >");
				}
			
			}
		});
	return false;
	});
});
