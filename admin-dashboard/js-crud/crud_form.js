$(document).ready(function (){
$("#form_crud").submit(function (){
	 //disable button
	 $('#submit').attr("disabled", "true");
	 var type = 'POST';
	 var url = $('#form_crud').attr('action');
	 //send json data
	 var data = {	
			task: $('#process_type').val(),
			record: $('#record').val(),
			name: $('#name').val(),
			email: $('#email').val(), 
			label: $('#label').val(),
			enable: $('input[name=enable]:checked').val(), 
		    
		};
	$.ajax({
	 type: type,
	 url: url,
	 data: data,
	 dataType:"JSON",
	 beforeSend: function(){
		//show laoding 
		$('#loading_data').html('<img src="../../img/loading.gif"/>&nbsp Loading ....');
	 },
	 success: function(data){
 		if(data.status == 'work'){
			 if($('#process_type').val() == 'update'){
				 window.location.href = "full_info.php?id="+$('#record').val();
			 }else{
				 window.location.href = "../form_attributes/insert.php?form_id="+data.id;				 
			 }
			 
		}else if(data.status == 'valid_error'){
			 $('#loading_data').html(data.fileds);
			 $('#loading_data').css('color', 'red');
			 $('#submit').removeAttr('disabled');
	   }else if(data.status == 'wrong'){
			 $('#form_crud')[0].reset();
			 $('#submit').removeAttr("disabled");
			 $('#loading_data').html(' The Title Not valid');
			 	 
		 }else{
			$('#loading_data').html('Error In Process');
		 }
	 }
})
 //do not go to any where
  return false;     
 }) 
 $('#add').click(function(){
	 window.location.href = "insert.php";
	 
});
    
})