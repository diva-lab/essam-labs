<?php 
require_once('../../../../classes/Session.php'); 
require_once('../../../../classes/Functions.php'); 
require_once('../../../../classes/MysqlDatabase.php'); 
require_once('../../../../classes/MenuLink.php'); 
require_once('../../../../classes/Users.php'); 
require_once('../../../../classes/Profile.php'); 
//check  session user  log in 
if($session->is_logged() == false){ 
	redirect_to("../../../index.php"); 
} 
// get user profile   
$user_data = Users::find_by_id($session->user_id); 
// get user profile data 
$user_profile  = Profile::Find_by_id($user_data->user_profile); 
// check if the user profile block 
if($user_profile->profile_block == "yes"){ 
   redirect_to("../../../index.php");	 
} 
if(!empty($_GET["task"]) && $_GET["task"] == "delete"){ 
	//send json
	header('Content-Type: application/json');
	//get data 
	$id = $_GET['id']; 
	$lang=$_GET['lang']; 
	//find record	 
	$find_menu_link = MenuLink::find_by_id($id); 
	//if there is record perform delete 
	//if there is no record go back to view 
	//check global  delete authorization 
	if($user_profile->global_delete == 'all_records' || $find_menu_link->inserted_by == $session->user_id){ 
		if($find_menu_link){ 
			$delete = $find_menu_link->delete(); 
			if($delete){ 
				//delete a menu  link content 
				$sql_delete_sub_menu_link = "DELETE FROM structure_menu_link_content  WHERE link_id ={$id} "; 
				$preform_delete_sub_menu_link = $database->query($sql_delete_sub_menu_link); 
				//delete all sub menu links 
				//select  al sub menu 
				$menu_sub_link_ids = array();  
				$menu_sub_links = MenuLink::find_all_by_custom_filed('parent_id',$id); 
				foreach($menu_sub_links as $menu_sub_link){ 
				 array_push($menu_sub_link_ids,$menu_sub_link->id);	 
				} 
				array_push($menu_sub_link_ids,$id); 
				$sub_liks=implode(",", $menu_sub_link_ids); 
				//delete all menu sub link 
				$sql_delete_sub_menu_link = "DELETE FROM structure_menu_link  WHERE id in ({$sub_liks})"; 
				$preform_delete_sub_menu_link = $database->query($sql_delete_sub_menu_link); 
				//delete all menu sub link content 
				$sql_delete_sub_menu_link = "DELETE FROM structure_menu_link_content  WHERE link_id in ({$sub_liks})"; 
				$preform_delete_sub_menu_link = $database->query($sql_delete_sub_menu_link); 
				 
				 $data = array("status"=>"work");
				 echo json_encode($data);
				redirect_to("../view.php?menu_id=$find_menu_link->menu_id&lang={$lang}"); 
			}else{ 
			    $data = array("status"=>"failed");
				echo json_encode($data);
				redirect_to("../view.php?menu_id=$find_menu_link->menu_id&lang={$lang}"); 
			}	 
			//if there is no record go back to view 
		}else{ 
			redirect_to("../view.php?menu_id=$find_menu_link->menu_id&lang={$lang}");	 
		}  
	}else{ 
		//if task wasnot delete go back to view 
		redirect_to("../view.php?menu_id=$find_menu_link->menu_id&lang={$lang}");	 
	} 
} 
//close connection 
if(isset($database)){ 
	$database->close_connection(); 
} 
?>