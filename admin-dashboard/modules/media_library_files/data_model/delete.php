<?php 
require_once('../../../../classes/Functions.php'); 
require_once('../../../../classes/MysqlDatabase.php'); 
require_once('../../../../classes/Session.php'); 
require_once('../../../../classes/Users.php'); 
require_once('../../../../classes/Profile.php'); 
//check log in  
if($session->is_logged() == false){ 
	redirect_to("../../index.php"); 
} 
// user log in profile details to chech authority 
// get user profile   
$user_data = Users::find_by_id($session->user_id); 
// get user profile data 
$user_profile  = Profile::Find_by_id($user_data->user_profile); 
if($user_profile->global_delete == 'all_records'){ 
	if(!empty($_GET["task"]) && $_GET["task"] == "delete"){ 
	  $file_name = $_GET['title']; 
	  $dir_name  = $_GET['dir']; 
	  // file and dir path 
	  $path = "../../../../media-library/".$dir_name."/"; 
	  $path_thumb = "../../../../media-library-thumb/".$dir_name."/"; 
	 //delete file 
		  if(file_exists($path.$file_name)){ 
			  $delete = unlink($path.$file_name); 
			  if($delete){ 
				  //delete thumb  
				 unlink($path_thumb."/small/".$file_name); 
				 unlink($path_thumb."/medium/".$file_name); 
				 unlink($path_thumb."/large/".$file_name); 
				 
				 redirect_to("../view.php?title=$dir_name"); 
			  }else{ 
				  redirect_to("../view.php?title=$dir_name"); 
			  } 
			}else{ 
				  redirect_to("../view.php?title=$dir_name");  
			   } 
		  }else{ 
			   redirect_to("../view.php?title=$dir_name");  
			   } 
}else{ 
	redirect_to("../view.php?title=$dir_name"); 
	 
	} 
?> 
