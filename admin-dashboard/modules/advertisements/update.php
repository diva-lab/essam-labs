<?php
	require_once("../layout/initialize.php");
	if(isset($_GET['id']) && is_numeric($_GET['id'])){
		$record_id = $_GET["id"];
		$record_info = Advertisements::find_by_id($record_id);
		
		//check id access
		if(empty($record_info->id)){
			redirect_to("view.php");
		}
		//check globel edit authority
		if($user_profile->globel_edit != 'all_records' && $record_info->inserted_by != $user_data->id ){
		  redirect_to('view.php');	
	    } 
	}else{
		redirect_to("view.php");
	}
	require_once("../layout/header.php");
?>
<script type="text/javascript" src="../../js-crud/adv.js"></script>
<!--header end-->
<!--sidebar start-->
<?php require_once("../layout/navigation.php");?>
<!--sidebar end--> 
<!--main content start-->
<section id="main-content">
  <section class="wrapper site-min-height">
    <h4> Advertisements  Module</h4>
    <div class="row">
      <aside class="col-lg-8">
        <section>
          <div class="panel">
            <div class="panel-heading"> Edit Advertisement </div>
            <div class="panel-body"> 
               <form class="form-horizontal tasi-form" role="form" id="form_crud" action="data_model/update.php">
                <input type="hidden" id="process_type" value="update">
                <input type="hidden" id="record" value="<?php  echo $record_id; ?>">
                <section class="panel"> 
                  <header class="panel-heading tab-bg-dark-navy-blue"> 
                    <ul class="nav nav-tabs"> 
                      <li class=" center-block active" > <a data-toggle="tab" href="#main_info" class="text-center"><strong>Main Info</strong></a></li> 
                      <li class=" center-block"> <a data-toggle="tab" href="#adv_option" class="text-center"><strong> Advertisement  Option</strong></a></li> 
                    </ul> 
                  </header> 
                  <div class="panel-body"> 
                    <div class="tab-content"> 
                      <div id="main_info" class="tab-pane active "> 
                        <section class="panel col-lg-9"> 
                          <header class="panel-heading tab-bg-dark-navy-blue "> 
                            <ul class="nav nav-tabs"> 
                              <?php 
                            //create tabs for all available languages  
                            $languages = Localization::find_all('id','desc'); 
                            $serial_tabs = 1; 
                            foreach($languages as $language){ 
                                $lang_tab_header = ucfirst($language->name); 
                                echo "<li class='";if($serial_tabs == 1){ echo " active ";}  echo"'> <a data-toggle='tab' href='#$language->name'> 
                                <strong>$lang_tab_header</strong></a></li>"; 
                                $serial_tabs++; 
                            } 
                          ?> 
                            </ul> 
                          </header> 
                          <div class="panel-body"> 
                            <div class="tab-content"> 
                              <?php 
                            $serial_tabs_content = 1; 
                            foreach($languages as $language): 
								//get data by language 
								$main_content = AdvertisementContent::get_adv_content($record_id, $language->id); 
								echo "<div id='$language->name' class='tab-pane"; if($serial_tabs_content == 1){ echo " active ";} echo"'>"; 
								?> 
                                 <input class='main_content' type='hidden' id='<?php echo "content_id_$language->label"; ?>' 
                          		value='<?php if(!empty($main_content )){echo $main_content->id;}else{ echo "0";} ?>'>	 
                                <div class='form-group'> 
                                  <label  class='col-lg-2'>Title:</label> 
                                  <div class='col-lg-9'> 
                                    <input type='text' class='form-control main_content' id='<?php echo "title_$language->label";?>' autocomplete='off'  
                                       value='<?php if(!empty($main_content ))echo $main_content->title; ?>' > 
                                  </div> 
                                </div> 
                                <div class='form-group'> 
                                <label  class='col-lg-2'>Content:</label> 
                                <div class='col-lg-9'> 
                                  <textarea class=' form-control main_content' id='<?php echo "description_$language->label";?>'> 
                                        <?php if(!empty($main_content ))echo $main_content->content; ?></textarea> 
                                </div> 
                              </div> 
                      </div> 
						  <?php   
                            $serial_tabs_content++; 
                                endforeach; 
                        ?> 
                            </div> 
                          </div> 
                        </section> 
                      </div> 
                      <div id="adv_option" class="tab-pane "> 
                        
                        <div class="form-group"> 
                          <label  class="col-lg-2 "> Path Type:</label> 
                          <div class="col-lg-8"> 
                            <select class="form-control" id="path_type"> 
                              <option value=""> Select Type </option> 
                              <option value="page">Page</option> 
                              <option value="post">Post</option> 
                              <option value="event">Event</option> 
                              <option value="external">External</option> 
                              <option value="category">Category</option> 
                            </select> 
                          </div> 
                        </div> 
                        <div class="form-group"  > 
                          <label  class="col-lg-2 "> Path:</label> 
                          <div class="col-lg-8" id="select_div"> 
                            <input type="text" class="form-control hide " id="external_path" placeholder=" "/> 
                            <select class="form-control" id="path"> 
                              <option value=""> Select Node </option> 
                            </select> 
                          </div> 
                          <div id="node_loading_data"></div> 
                        </div> 
                      </div> 
                    </div> 
                  </div> 
                </section> 
                <div class="form-group"> 
                  <div class="col-lg-offset-2 col-lg-10"> 
                    <button type="submit" class="btn btn-info" id="submit">Save</button> 
                    <button type="submit" class="btn btn-default">Cancel</button> 
                    <div id="loading_data"></div> 
                  </div> 
                </div> 
              </form>
            </div>
          </div>
        </section>
      </aside>
      <div class="col-lg-4"> 
        <section class="panel panel-primary"> 
          <header class="panel-heading"> Publish Options: </header> 
          <div class="panel-body"> 
            <form class="form-horizontal tasi-form" role="form" id="form_option"> 
              <div class="form-group"> 
                <label class="col-lg-5">Publish:</label> 
                <div class="col-lg-6"> 
                  <label class="checkbox-inline"> 
                    <input type="radio" name="publish" class="radio" value="yes" <?php if($record_info->publish == 'yes') echo  "checked"?>> 
                    Yes</label> 
                  <label class="checkbox-inline"> 
                    <input type="radio" name="publish" class="radio" value="no" <?php if($record_info->publish == 'yes') echo  "checked"?>> 
                    No</label> 
                </div> 
              </div> 
             </form> 
          </div> 
        </section> 
      </div>
    </div>
    
    <!-- page end--> 
  </section>
</section>
<!--main content end--> 
<!--footer start-->

<?php require_once("../layout/footer.php");?>
<script src="../../js-crud/load_node_for_adv.js"></script>