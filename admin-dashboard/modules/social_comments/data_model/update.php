<?php 
require_once('../../../../classes/Functions.php'); 
require_once('../../../../classes/MysqlDatabase.php'); 
require_once('../../../../classes/SocialComments.php'); 
require_once('../../../../classes/Session.php'); 
//check log in  
if($session->is_logged() == false){ 
	redirect_to("../../index.php"); 
} 
if(!empty($_POST["task"]) && $_POST["task"] == "update"){ 
	//get data 
	$id = $_POST['record']; 
	//update	 
	$edit = SocialComments::find_by_id($id); 
	$edit->title = $_POST["title"]; 
	$edit->status = $_POST["shadow"]; 
	$edit->body = $_POST["comment_body"]; 
	$edit->update_by=$session->user_id; 
	$edit->last_update = date_now(); 
	$update = $edit->update(); 
	// update category selected; 
	header('Content-Type: application/json'); 
	if($update){ 
		$data  = array("status"=>"work"); 
		echo json_encode($data); 
		 
		}else{ 
		  $data  = array("status"=>"error"); 
		  echo json_encode($data); 
	  } 
	   
	   
} 
//close connection 
if(isset($database)){ 
	$database->close_connection(); 
} 
?>