<?php 
require_once('../../../../classes/Session.php'); 
require_once('../../../../classes/Functions.php'); 
require_once('../../../../classes/MysqlDatabase.php'); 
require_once('../../../../classes/CMSModules.php'); 
require_once('../../../../classes/Users.php'); 
require_once('../../../../classes/Profile.php'); 
//check  session user  log in 
if($session->is_logged() == false){ 
	redirect_to("../../../index.php"); 
} 
// get user profile   
$user_data = Users::find_by_id($session->user_id); 
// get user profile data 
$user_profile  = Profile::Find_by_id($user_data->user_profile); 
// check if the user profile block 
if($user_profile->profile_block == "yes"){ 
   redirect_to("../../../index.php");	 
} 
if(!empty($_GET["task"]) && $_GET["task"] == "delete"){ 
	//get data 
	$id = $_GET['id']; 
	//find record	 
	$find_module = CMSModules::find_by_id($id); 
	$type=$find_module->type; 
	//if there is record perform delete 
	//if there is no record go back to view 
	if($find_module){ 
	$delete = $find_module->delete(); 
		if($delete){ 
			if($type=="module"){ 
			//delete all realted module access and pages  
			//delete  pages  
			$sql_delete_module_pages = "DELETE FROM cms_module_access  WHERE sid = '{$id}'"; 
			$preform_delete_module_pages = $database->query($sql_delete_module_pages); 
			//delete  profile  module access 
			$sql_delete_profile_module = "DELETE FROM profile_modules_access  WHERE module_id = '{$id}'"; 
			$preform_delete_profile_module = $database->query($sql_delete_profile_module); 
			//delete   profile  pages access 
			$sql_delete_profile_pages = "DELETE FROM profile_pages_access WHERE module_id = '{$id}'"; 
			$preform_delete_profile_pages = $database->query($sql_delete_profile_pages); 
			 
		  }else{ 
			  //delete   profile  pages access 
			 $sql_delete_profile_pages = "DELETE FROM profile_pages_access WHERE page_id = '{$id}'"; 
			 $preform_delete_profile_pages = $database->query($sql_delete_profile_pages); 
			   
			  } 
			  redirect_to("../view.php"); 
		}else{ 
			redirect_to("../view.php"); 
		} 
		 
	    
	   
		 
		//if there is no record go back to view 
	}else{ 
		redirect_to("../view.php");	 
	}  
  
}else{ 
	//if task wasnot delete go back to view 
	redirect_to("../view.php");	 
} 
//close connection 
if(isset($database)){ 
	$database->close_connection(); 
} 
?>