<?php
require_once 'CRUD.php';
class PollQuestionsOptions extends CRUD{
   //calss attributes
   public $id;
   public $poll_id;
   public $poll_option;
   public $option_counter;
   public $inserted_by;
   public $inserted_date;
   public $update_by;
   public $last_update;
   //relation attributes
   public $poll;
  //push attributes for relational tables
   public function enable_relation(){
		array_push(static::$primary_fields ,'poll' );
   }   
   //define table name and fields
	protected static $table_name = 'poll_questions_options';
	protected static $primary_fields = array('id','poll_id', 'poll_option','option_counter','inserted_by', 'inserted_date','update_by','last_update');
	// get vote answers data
	public function options_data($sort_filed = null, $order_by = null, $id = null, $menu_group_id = null){
		$sql = "SELECT poll_questions_options.id AS id,poll_questions_options.poll_option AS poll_option , users.user_name AS inserted_by,
		        poll_questions_options.inserted_date AS inserted_date,poll_questions.poll AS poll,
				poll_questions_options.option_counter AS poll_option_counter,users2.user_name AS update_by,
				poll_questions_options.last_update AS last_update,poll_questions.id AS poll_id
				FROM poll_questions_options
				LEFT JOIN users ON poll_questions_options.inserted_by = users.id
			    LEFT JOIN users AS users2 ON poll_questions_options.update_by = users2.id
				LEFT JOIN poll_questions  ON poll_questions_options.poll_id = poll_questions.id";
		if(!empty($id)){		
			 $sql .= " WHERE poll_questions_options.id = $id ";
			 $result_array = static::find_by_sql($sql);
			 return !empty($result_array)? array_shift($result_array) : false;
		}else{	
			if(!empty($sort_filed) && !empty($order_by)){
				$sql .= " ORDER BY ".$sort_filed." ".$order_by; 
			 }
			return self::find_by_sql($sql);  
		}				
	}
	
	
	
}
?>