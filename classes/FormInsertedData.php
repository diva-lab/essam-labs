<?php
require_once 'CRUD.php';
class FormInsertedData extends CRUD{
   //calss attributes
   public $id;
   public $form_id;
   public $inserted_date;
   public $label;
   public $value;
   //relation table attribute
   //push attributes for relational tables
   public function enable_relation(){
		array_push(static::$primary_fields,'');
   }   
   //define table name and fields
	protected static $table_name = 'form_inserted_data';
	protected static $primary_fields = array('id','form_id','inserted_date','label','value');
	
	public function form_inserted_data($form_id){
		$sql = "SELECT form_inserted_data.label AS label, form_inserted_data.value AS value,
		        form_inserted_data.inserted_date AS inserted_date,form_inserted_data.form_id AS form_id 
				FROM form_inserted_data
				WHERE form_inserted_data.form_id = {$form_id}  
				ORDER BY form_inserted_data.inserted_date DESC ";	
		        return self::find_by_sql($sql);  			
		
	}			
}
?>