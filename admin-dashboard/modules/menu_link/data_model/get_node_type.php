<?php 
require_once('../../../../classes/Session.php'); 
require_once('../../../../classes/Functions.php'); 
require_once('../../../../classes/MysqlDatabase.php'); 
require_once('../../../../classes/Nodes.php'); 
require_once('../../../../classes/Taxonomies.php'); 
$path_type = $_POST['path_type']; 
//get main lnaguage info 
$define_general_setting = new GeneralSettings(); 
$define_general_setting->enable_relation(); 
$general_setting_info = $define_general_setting->general_settings_data(); 
if($path_type == 'category'){ 
	$define_cat_class = new Taxonomies(); 
	$define_cat_class->enable_relation(); 
	$records = $define_cat_class->taxonomies_data('category',null,null,'inserted_date','DESC',null,null,$general_setting_info->translate_lang_id,'yes'); 
	 $json_array = array(); 
  foreach($records as $record){ 
	  $json_array[] =  array("id"=>$record->id, "title"=>$record->name); 
  } 
}else{ 
  //get nodes  
  $define_class = new Nodes;	 
  $define_class->enable_relation(); 
  $records = $define_class->node_data($path_type,null,null,null,null,null,$general_setting_info->translate_lang_id,null,"title","ASC"); 
  $json_array = array(); 
  foreach($records as $record){ 
	  $json_array[] =  array("id"=>$record->id, "title"=>$record->title); 
  } 
} 
echo json_encode($json_array); 
//close connection 
if(isset($database)){ 
	$database->close_connection(); 
} 
?>