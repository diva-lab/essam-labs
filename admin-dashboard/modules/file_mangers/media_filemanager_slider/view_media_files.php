<?php 
require_once("../../../../classes/Pagination.php"); 
$page = isset($_GET['page']) ? $_GET['page'] : 1 ;
$perpage = 14;
$directory_title= $_GET['title']; 
$docs = array(); 
$path = "../../../../media-library/"; 
$dir = $path.$directory_title;
$dir_glob = $dir . '/*';
$dir_iterator = new ArrayObject(glob($dir_glob));
$dir_iterator = $dir_iterator->getIterator();
$all_files = new LimitIterator($dir_iterator);
$total_files = 0;
foreach($all_files as $record){ 
$total_files++;
}
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="Mosaddek">
<!-- Bootstrap core CSS -->
<link href="../../../css/bootstrap.min.css" rel="stylesheet">
<link href="../../../css/bootstrap-reset.css" rel="stylesheet">
<!--external css-->
<link rel="stylesheet" type="text/css" href="../../../assets/gritter/css/jquery.gritter.css" />
<link href="../../../assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
<link href="../../../assets/advanced-datatable/media/css/demo_page.css" rel="stylesheet" />
<link href="../../../assets/advanced-datatable/media/css/demo_table.css" rel="stylesheet" />
<link rel="stylesheet" type="text/css" href="../../../../js/fancybox/jquery.fancybox-1.3.4.css" media="screen" />
<link href="../../../css/style.css" rel="stylesheet">
<link href="../../../css/style-responsive.css" rel="stylesheet" />
 <script> 
	 
function moveImage(val) { 
	parent.document.getElementById('imageVal_slider').value=val; 
	parent.document.getElementById('imageSlider').style.display="inline"; 
	parent.document.getElementById('imageSrcSlider').src=val; 
	parent.jQuery.fancybox.close(); 
	
} 
	 
</script> 
<!-- Custom styles for this template -->
<style>
.files li {
	padding-right: 25px;
	display: inline-block;
}
.files li a img {
	padding: 5px;
	-webkit-transition: all .3s linear;
	transition: all .3s linear;
}
.files li:hover a img {
	border: 1px solid #87b4df;
}
</style>
</head>
<body style="background:#fff">
<section class="panel">
<header class="panel-heading"> View <?php echo $directory_title?> Files </header> 
  <div class="row">
   <div class="panel-body">
   
    <div class="col-lg-6">
     <form  class="pull-right" > 
        <div class="form-group " > 
            <div class="col-lg-3"> 
              <input type="text" class="form-control" id="search_input" placeholder=" Search " autocomplete="off" style="width:250px" /> 
          </div> 
        </div>
        </form>
       </div>
        <div class="col-lg-6">
        <a  id="upload" href="upload.php?title=<?php echo $directory_title;?>" class="btn   btn-danger"> <i class=" icon-upload-alt"></i>&nbsp; Upload New Image     </a>
     </div>    
   </div>
   </div>
  <div class="row">
    <div class="col-lg-12" >
      <div class="panel-body">
        <?php 
		  if($total_files != 0){
			$pagination = new Pagination($page,$perpage,$total_files);
			 $offset = $pagination->offset();
			 $paginated = new LimitIterator($dir_iterator, $offset,$perpage);
			 echo " <ul class='files'>";
			 $ser = 1; 
			  foreach($paginated as $record){ 
			  $path_parts = pathinfo($record);
			  $name = $path_parts['filename'];
			  echo "<li><a href='{$record}'><img src='{$record}' onClick='moveImage(this.src)'   style='width:100px;height:100px' />
			  <br><span style='text-align:center;'>".substr($name, 0,10)."</span></a></li>";
			    $ser++; 
			  } 
              ?>
        </ul>
        <ul class="pagination pagination-sm pull-right">
          <?php
            if($pagination->total_pages()>1){
              if($pagination->has_previous_page()){
                  echo "<li> <a href='view_media_files.php?title={$directory_title}&page=";
                  echo $pagination->previous_page();
                  echo "'>&laquo; prev</a></li>";
              }
              for($i=1;$i<=$pagination->total_pages();$i++){
                  echo"<li ";
                  if($i == $page){
                      echo " class = 'active'";

                      }
                  echo "><a href=view_media_files.php?title={$directory_title}&page={$i}'";
                  
                  echo " >{$i}</a></li>";
                  }
              if($pagination->has_next_page()){
                  echo "<li> <a href='view_media_files.php?title={$directory_title}&page=";
                  echo $pagination->next_page();
                  echo "'>&raquo; next</a></li>";
              }  
                
            
            }
          ?>
        </ul>
        <?php }?>
        <button type="button" class="btn btn-info" onclick="history.back()">Back</button>
      </div>
    </div>
  </div>
  <!-- END JAVASCRIPTS --> 
  <script src="../../../js/jquery.js"></script> 
  <script src="../../../js/bootstrap.min.js"></script> 
  <script class="include" type="text/javascript" src="../../../js/jquery.dcjqaccordion.2.7.js"></script> 
  <script src="../../../js/jquery.scrollTo.min.js"></script> 
  <script src="../../../js/jquery.nicescroll.js" type="text/javascript"></script> 
  <script src="../../../js/respond.min.js" ></script> 
  <script type="text/javascript" src="../../../assets/gritter/js/jquery.gritter.js"></script> 
  <script type="text/javascript" language="javascript" src="../../../assets/advanced-datatable/media/js/jquery.dataTables.js"></script> 
  <script type="text/javascript" src="../../../js/fancybox/jquery.fancybox-1.3.4.pack.js"></script> 
  <script type="text/javascript" src="../../../js/fancybox/jquery.mousewheel-3.0.4.pack.js"></script> 
  <script> 
	<!-- for update model--> 
$(document).ready(function() { 
	$("#upload").fancybox({ 
		        'width'		: '80%', 
				'height'	   : '80%', 
				'autoScale'	: true, 
				'transitionIn' : 'none', 
				'transitionOut': 'none', 
				'type'		 : 'iframe' 
		 
		 
	}); 
}); 
</script> 
  <!--common script for all pages--> 
  <script src="../../../js/common-scripts.js"></script> 
  <script src="../../../js/gritter.js" type="text/javascript"></script> 
</section>
</body>
</html>
