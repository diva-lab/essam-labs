$(document).ready(function (){
$("#form_crud").submit(function (){
	 //disable button
	 $('#submit').attr("disabled", "true");
	 var type = 'POST';
	 var url = $('#form_crud').attr('action');
	 //send json data
	 var data = {	
			task: $('#process_type').val(),
			record: $('#record').val(),
			first_name: $('#first_name').val(), 
			last_name: $('#last_name').val(),
			birthday: $('#birthday').val(), 
			personal_email:$('#personal_email').val(),
			company_email:$('#company_email').val(),
			phone_one:  $('#phone_one').val(),
			phone_two:  $('#phone_two').val(),
			password: $('#pwd').val(),
			gender:$('input[name=gender]:checked').val(),
			status:$('input[name=status]:checked').val()
			
		};
	$.ajax({
	 type: type,
	 url: url,
	 data: data,
	 beforeSend: function(){
		//show laoding 
		$('#loading_data').html('<img src="../../img/loading.gif"/>&nbsp Loading ....');
	 },
	 success: function(data){
		if(data.status == 'work'){
			 if($('#process_type').val() == 'update'){
				 if(data.status == 'email_exist'){
					 $('#submit').removeAttr("disabled");
					 $('#pwd').val('');
					 $('#loading_data').html('Email already exist');
				 }else{
					 window.location.href = "full_info.php?id="+$('#record').val();
				 }
			 }else{
				$('#form_crud')[0].reset();
				$('#form_option')[0].reset();
				$('#submit').removeAttr("disabled");
				$('#loading_data').html('<h3>Successful Insert Process</h3>');				 
			 }
		 }else if(data.status == 'email_exist'){
			 $('#submit').removeAttr("disabled");
			 $('#pwd').val('');
			 $('#loading_data').html('<h3>Email already exist</h3>');
		 }else if(data.status == 'valid_error'){
			 $('#loading_data').html(data.fileds);
			 $('#loading_data').css('color', 'red');
			 $('#submit').removeAttr('disabled');
		 }else{
			 $('#submit').removeAttr("disabled");
			 $('#pwd').val('');
			$('#loading_data').html('<h3>Error In Process</h3>');
		 }
	 }
})
 //do not go to any where
  return false;     
 })    
})