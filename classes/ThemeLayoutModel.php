<?php 
require_once 'CRUD.php'; 
class ThemeLayoutModel extends CRUD{ 
   //calss attributes 
   public $id; 
   public $name; 
   public $theme_id; 
   public $layout_id; 
   public $type ; 
    
   //relation table attribute 
   public $category_name; 
   //push attributes for relational tables 
   public function enable_relation(){ 
		array_push(static::$primary_fields , ''); 
   }    
    
   //define table name and fields 
	protected static $table_name = 'theme_layout_model'; 
	protected static $primary_fields = array('id', 'name', 'theme_id','layout_id','type'); 
   //get the  all ids of categories in specific module 
    public static  function get_nodes_models($layout_id,$type){
		$sql = "SELECT * FROM  theme_layout_model WHERE layout_id = '$layout_id' AND type = '$type'";
		return self::find_by_sql($sql);
	}   
} 
?>
