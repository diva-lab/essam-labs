<?php 
require_once('../../../../classes/Functions.php'); 
require_once('../../../../classes/Session.php'); 
require_once("../../../../classes/CMSModules.php"); 
$modules = CMSModules::find_all_by_custom_filed('sid', 0, 'sorting', 'ASC'); 
?> 
<?php foreach ($modules as $module): ?> 
   <li> 
	  <a href="../cms_modules/update.php?id=<?php echo $module->id?>" class="active"><i class=" icon-angle-right"></i> <?php echo $module->title;?></a> 
	  <ul class="nav"> 
		<?php  
			 $pages = CMSModules::find_all_by_custom_filed('sid', $module->id, 'sorting', 'ASC'); 
			 $page_serial = 1; 
			foreach ($pages as $page): 
		?> 
		  <li><a href="../cms_modules/update.php?id=<?php echo $page->id?>"><?php echo $page_serial."-".$page->title;?></a></li> 
	   <?php 
	   	 $page_serial++; 
	    endforeach; 
	?> 
	  </ul> 
  </li> 
<?php  
	endforeach; 
//close connection 
if(isset($database)){ 
	$database->close_connection(); 
} 
?>