<?php 
require_once('../../../../classes/Session.php'); 
require_once('../../../../classes/Functions.php'); 
require_once('../../../../classes/MysqlDatabase.php'); 
//gson data 
header('Content-Type: application/json'); 
if(!empty($_POST["task"]) && $_POST["task"] == "update"){ 
	//dir paths 
	$path = "../../../../media-library/"; 
	$path_thumb = "../../../../media-library-thumb/"; 
	$dir_new_title = $_POST["title"]; 
	$dir_title = $_POST["record"]; 
	//check if new title folder exist 
	if(file_exists($path.$dir_new_title)){ 
        $data  = array("status"=>"exist"); 
		echo json_encode($data); 
	}else{ 
	//update 
	    //check if dir new title is valid 
		  if(strpbrk($dir_new_title, "\\/?%*:|\"<>") === false){	 
				$old_name = $path.$dir_title; 
				$old_name_thumb = $path_thumb.$dir_title; 
				$new_name = $path.$dir_new_title; 
				$new_name_thumb = $path_thumb.$dir_new_title; 
				//updat 
				//rename dir 
				$update = rename($old_name,$new_name); 
				 
				 
				if($update){ 
					$update_thumb = rename($old_name_thumb,$new_name_thumb); 
					$data = array("status"=>"work"); 
					echo json_encode($data); 
				}else{ 
					$data = array("status"=>"error"); 
					echo json_encode($data); 
				} 
				//if dir title is wrong 
		  }else{ 
			   $data = array("status"=>"wrong"); 
					echo json_encode($data); 
			   } 
   } 
} 
//close connection 
?>