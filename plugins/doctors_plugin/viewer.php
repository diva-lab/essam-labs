  <section class="col-md-12 new-carousel"><!--new-carousel-->
  <?php 

  $define_page_categories = new NodesSelectedTaxonomies();
  $define_page_categories->enable_relation();
  $page_categories = $define_page_categories->return_node_taxonomy($get_page_content->id,'page','category','one',$lang_info->id);
 
  if(count($page_categories) > 0){
 
     
    $page_all_post = $define_node->front_node_data(null,"post",null,null,$lang_info->id,null,null,null,$page_categories->id, null,null,null,"many");
    //total posts
    $total_posts = count($page_all_post);
    //get page id if exist
    $page_number = !empty($_GET['page']) ? (int)$_GET['page'] : 1;
   //number of posts in one page
    $per_page =3 ;
    $pagination = new Pagination($page_number , $per_page, $total_posts);
    //calculate the offset
    $offset = $pagination->offset();
    $count = $offset+1;
    $List_posts_for_blog = $define_node->front_node_data(null,"post",null,null,$lang_info->id,null,null,null,$page_categories->id, null,null,null,"many",$per_page,$offset);
    $count_show_result = count($List_posts_for_blog);


  ?>
            <div class="wrapping5 clearfix"><!--clearfix-->


  <?php 

      foreach ($List_posts_for_blog as $post) {
        $title = strip_tags($post->title) ; 
        $clean_summary = strip_tags($post->summary) ; 
        $limited_summary = string_limit_words($clean_summary , 10) ; 
     


              echo "<div class='col-md-4 pagi-wrapp'> 
              <div class='col-md-12 bg-black doctor-inner'>
                <img src='media-library/$post->cover_image' alt=''>
                <h2 class='text-center doctor-name'>$title</h2>
                <div class='col-md-12 text-wrapping new-text-wrapping2 doctor-job'> 
                  <p class='text-center job-name'>$limited_summary</p>
                </div> 
              </div>
            </div> ";

      }

    }


  ?>
 
 
              <div class='row clearfix'><!--pagination-->
             
                 <?php 
                      echo "<nav aria-label='...' class='pagination-items'>";
                  if($pagination->total_pages() > 1){
                  echo "<ul class='pagination'>";
                  if($pagination->has_previous_page()){
                    $previous_page = $pagination->previous_page();
                    
                    echo "<li>
                        <a href='content.php?lang=$lang&alias=$node_alias&page=$previous_page' aria-label='Previous'>
                        <span class='fa fa-caret-left'></span> 
                      
                        </a>
                      </li>";   
                  } 
                  for($i=1; $i <= $pagination->total_pages(); $i++) {
                      if($i == $page_number){
                      $active = "  active ";
                      }else{
                      $active = " ";
                      }
                  echo "<li class='$active'><a href='content.php?lang=$lang&alias=$node_alias&page={$i}'>$i</a></li>";


                  } 

                  // check next page
                  if($pagination->has_next_page()) { 
                  $next_page = $pagination->next_page();
                  echo "<li><a href='content.php?lang=$lang&alias=$node_alias&page=$next_page' aria-label='Next'>
                         <span class='fa fa-caret-right'></span></a></li>";
                  }
                  echo "</ul>";   

                  } 
                  echo "</nav>"; 
                ?>
            
        </div>



          </div><!--clearfix-->
          </section><!--end of new-carousel-->