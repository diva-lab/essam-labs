<?php 
	require_once("../layout/initialize.php"); 
	if(isset($_GET['id']) && is_numeric($_GET['id'])){ 
	$record_id = $_GET['id']; 
	$define_class = new PollQuestionsOptions(); 
	$record_info =$define_class->options_data(null,null,$record_id); 
	//check id access 
	if(empty($record_info->id)){ 
		redirect_to("view.php");} 
	}else{ 
		redirect_to("view.php"); 
	} 
	require_once("../layout/header.php"); 
?> 
  <!--header end-->  
  <!--sidebar start--> 
  <?php require_once("../layout/navigation.php");?> 
  <!--sidebar end-->  
  <!--main content start--> 
  <section id="main-content"> 
    <section class="wrapper site-min-height"> 
      <h4> Poll Options  Module</h4>  
      <!-- page start--> 
      <div class="row"> 
        <aside class="col-lg-8"> 
          <section> 
            <div class="panel"> 
              <div class="panel-heading"> Poll Option Info</div> 
              <div class="panel-body"> 
                <form class="form-horizontal tasi-form" role="form"> 
                
                  <div class="form-group"> 
                    <label  class="col-lg-4 ">Option:</label> 
                    <div class="col-lg-6" > 
                    <?php echo $record_info->poll_option?> 
                    </div> 
                  </div> 
                  <div class="form-group"> 
                    <label  class="col-lg-4 "> Option Counter :</label> 
                    <div class="col-lg-6" > 
                    <?php echo $record_info->option_counter?> 
                    </div> 
                  </div> 
                  <div class="form-group"> 
                  <div class="col-lg-offset-2 col-lg-10"> 
                    <button type="button" class="btn btn-info"   
                        onClick="window.location.href = 'update.php?id='+<?php echo $record_id?>" > <li class="icon-edit-sign"></li> Update </button> 
                        </div> 
                  </div> 
                </form> 
              </div> 
            </div> 
          </section> 
        </aside> 
        <div class="col-lg-3"> 
          <section class="panel panel-primary"> 
            <header class="panel-heading"> Entry Information: </header> 
            <div class="panel-body"> 
              <div id="list_info"> 
                <ul> 
                  <li><span style="color:#428bca">> Created By:</span> <?php echo $record_info->inserted_by?></li> 
                  <li><span style="color:#428bca">> Created Date:</span> <?php echo $record_info->inserted_date?></li> 
                  <li><span style="color:#428bca">> Last Update By:</span> <?php if($record_info->update_by != ""){ 
					  echo $record_info->update_by; 
				      }else{ 
						  echo "--";} 
					  ?></li> 
                  <li><span style="color:#428bca">> Last Update Date:</span> <?php if($record_info->last_update != "0000-00-00 00:00:00"){ 
					  echo $record_info->last_update; 
				      }else{ 
						  echo "--";} 
					  ?></li> 
                </ul> 
              </div> 
            </div> 
          </section> 
           
        </div> 
      </div> 
       
      <!-- page end-->  
    </section> 
  </section> 
  <!--main content end-->  
  <!--footer start--> 
  <?php require_once("../layout/footer.php");?>