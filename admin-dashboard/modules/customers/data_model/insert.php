<?php 
require_once('../../../../classes/Session.php'); 
require_once('../../../../classes/Functions.php'); 
require_once('../../../../classes/Profile.php'); 
require_once('../../../../classes/Users.php'); 
require_once('../../../../classes/MysqlDatabase.php'); 
require_once('../../../../classes/CustomerInfo.php'); 

//check  session user  log in 
if($session->is_logged() == false){ 
	redirect_to("../../../index.php"); 
} 
// get user profile   
$user_data = Users::find_by_id($session->user_id); 
// get user profile data 
$user_profile  = Profile::Find_by_id($user_data->user_profile); 
// check if the user profile block 
if($user_profile->profile_block == "yes"){ 
   redirect_to("../../../index.php");	 
} 
if(!empty($_POST["task"]) && $_POST["task"] == "insert"){ 
header('Content-Type: application/json'); 
//check rqquierd fields 
	$required_fields = array('first_name'=>"- Insert First Name",'email'=>'- Insert email','password'=>'- Insert password' ); 
	$check_required_fields = check_required_fields($required_fields); 
	if(count($check_required_fields) == 0){ 
	    $email = $_POST["email"];
		$check_email = CustomerInfo::find_by_custom_filed('email', $email);
		if(empty($check_email)){
			if (filter_var($email, FILTER_VALIDATE_EMAIL)) { 
				$add = new CustomerInfo(); 

				$add->first_name = $_POST["first_name"];
				$add->last_name = $_POST["last_name"];
				$add->city = $_POST["city"];
				$add->area = $_POST["area"];
				$add->street = $_POST["street"];
				$add->last_name = $_POST["last_name"];

				$add->email = $_POST["email"];
			    $add->password =  md5($_POST["password"]);
				$add->gender = $_POST["gender"]; 
				$add->birth_date = $_POST["birthday"];		 
			    $add->registeration_date = date_now();
				$add->account_status = $_POST["account_status"];
				$insert = $add->insert();
				$inserted_id = $add->id; 
				if($insert){ 
									 
					$data  = array("status"=>"work"); 
					echo json_encode($data); 
				}else{ 
					$data  = array("status"=>"error"); 
					echo json_encode($data); 
				}
			}else{
				  $data  = array("status"=>"email_error");
		          echo json_encode($data);
			}
	  }else{
		  $data = array("status"=>"email_exist"); 
				echo json_encode($data); 
	  }			 
	}else{ 
		  //validation error 
		  $comma_separated = implode("<br>", $check_required_fields); 
		  $data  = array("status"=>"valid_error", "fileds"=>$comma_separated); 
		  echo json_encode($data); 
	} 
} 
//close connection 
if(isset($database)){ 
$database->close_connection(); 
} 
?>