<?php 
require_once 'CRUD.php'; 
class NodesSelectedTaxonomies extends CRUD{ 
   //calss attributes 
   public $id; 
   public $node_id;  
   public $node_type;   
   public $taxonomy_id; 
   public $taxonomy_type; 
   //relation table attribute 
   public $taxonomy_name; 
   public $node_alias;
   public $node_title; 
   
   //push attributes for relational tables 
   public function enable_relation(){ 
		array_push(static::$primary_fields , 'taxonomy_name','node_alias','node_title'); 
   }    
   //define table name and fields 
	protected static $table_name = 'nodes_selected_taxonomies'; 
	protected static $primary_fields = array('id', 'taxonomy_id','taxonomy_type','node_id','node_type'); 
	//get all nodes in this texonomy  
	 public function return_taxonomy_nodes($taxonomy_id,$type,$taxonomy_type,$retrieve = 'one',$lang){ 
	   	$sql = "SELECT nodes_selected_taxonomies.node_id AS node_id, 
		        nodes_content.alias AS node_alias , nodes_content.title AS node_title
				FROM  nodes_selected_taxonomies,nodes_content,nodes
				WHERE nodes_selected_taxonomies.taxonomy_id = '$taxonomy_id' 
				AND nodes_content.node_id = nodes_selected_taxonomies.node_id 
				AND nodes_content.lang_id = '$lang'
				AND nodes_selected_taxonomies.node_type = 'page' 
				AND  nodes_selected_taxonomies.taxonomy_type = '$taxonomy_type'
        AND nodes.node_type = nodes_selected_taxonomies.node_type ";
				if($retrieve == 'one'){ 
		           $result_array = static::find_by_sql($sql); 
				   return !empty($result_array)? array_shift($result_array) : false;				 
				}else{ 
				   return self::find_by_sql($sql);  
				}	 
   } 
	 
	 
   //get the  all ids of taxonomy in specific module 
  public function return_node_taxonomy($node_id,$type,$taxonomy_type,$retrieve = 'one',$lang = null){ 
	   	$sql = "SELECT taxonomies_content.name AS taxonomy_name,taxonomies.id AS id 
				FROM  nodes_selected_taxonomies, taxonomies,taxonomies_content 
				WHERE nodes_selected_taxonomies.node_id = '$node_id' 
                AND nodes_selected_taxonomies.taxonomy_id = taxonomies.id  
				AND nodes_selected_taxonomies.node_type = '$type' 
				AND taxonomies_content.taxonomy_id = taxonomies.id 
				AND  nodes_selected_taxonomies.taxonomy_type = '$taxonomy_type' 
				AND taxonomies_content.lang_id = '$lang' "; 
				if($retrieve == 'one'){ 
		           $result_array = static::find_by_sql($sql); 
				   return !empty($result_array)? array_shift($result_array) : false;				 
				}else{ 
				   return self::find_by_sql($sql);  
				}	 
   } 
   //check category exist for this page 
   public static function check_taxonomy_exist($node_id,$taxonomy_id,$type,$taxonomy_type){ 
		$sql = "SELECT * FROM nodes_selected_taxonomies  
		        WHERE node_id = '$node_id'  
		        AND taxonomy_id = '$taxonomy_id'  
		        AND node_type = '$type' 
				AND taxonomy_type = '$taxonomy_type' ";  
				if($taxonomy_type == 'author'){ 
				   $result_array = static::find_by_sql($sql); 
				   return !empty($result_array)? array_shift($result_array) : false;		 
				}else{ 
		          $result_array = static::find_by_sql($sql); 
		          return self::find_by_sql($sql);  
				} 
   } 
   //return deleted_taxonomy in node 
   public static function return_deleted_taxonomy($node_id, $node_type, $taxonomies, $taxonomy_type){ 
$sql = "SELECT taxonomy_id FROM nodes_selected_taxonomies WHERE node_id = '$node_id'  
		  AND taxonomy_id NOT IN ($taxonomies) 
		  AND node_type = '$node_type' 
		  AND taxonomy_type = '$taxonomy_type' ";   
 return self::find_by_sql($sql); 		   
}    
    
    
} 
?>
