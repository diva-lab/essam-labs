<?php  
	require_once("../layout/initialize.php"); 
	$records = SocialSuggestionTopics::find_all('inserted_date', 'DESC'); 
	require_once("../layout/header.php"); 
?> 
<!--header end--> 
<!--sidebar start--> 
<?php require_once("../layout/navigation.php");?> 
<!--sidebar end-->  
<!--main content start--> 
<section id="main-content"> 
  <section class="wrapper site-min-height"> 
    <h4> Social Suggestion Topics Module</h4> 
    <!-- page start--> 
    <section class="panel"> 
      <header class="panel-heading"> View Suggestion Topics </header> 
      <div class="panel-body"> 
        <div class="adv-table editable-table ">  
          <!--<div class="clearfix"> 
                              <div class="btn-group"> 
                                  <button id="editable-sample_new" class="btn green"> 
                                      Add New <i class="icon-plus"></i> 
                                  </button> 
                              </div> 
                              <div class="btn-group pull-right"> 
                                  <button class="btn dropdown-toggle" data-toggle="dropdown">Tools <i class="icon-angle-down"></i> 
                                  </button> 
                                  <ul class="dropdown-menu pull-right"> 
                                      <li><a href="#">Print</a></li> 
                                      <li><a href="#">Save as PDF</a></li> 
                                      <li><a href="#">Export to Excel</a></li> 
                                  </ul> 
                              </div> 
                          </div>--> 
          <div class="space15"></div> 
          <table class="table table-striped table-hover table-bordered" id="editable-sample"> 
            <thead> 
              <tr> 
                <th>#</th> 
                <th><i class=""></i> User Name</th> 
                <th><i class=""></i> Topic Title</th> 
                <th><i class=""></i>Created Date</th> 
                <th>Action</th> 
              </tr> 
            </thead> 
            <tbody> 
              <?php   
				  $serialize=1; 
				  foreach($records as $record){ 
					  echo "<tr> 
					   <td>{$serialize}</td> 
					   <td>{$record->user_name}</td> 
					   <td><a href='full_info.php?id={$record->id}'>{$record->title}</a></td> 
					   <td>{$record->inserted_date}</td> 
						<td>"; 
						$opened_module_delete = 'cms_modules/delete'; 
						echo "									<a href='full_info.php?id={$record->id}' class='btn btn-primary btn-xs tooltips' data-placement='top' data-toggle='tooltip'  
									  data-original-title='Full info' >&nbsp;<i class='icon-info'></i>&nbsp;</a>"; 
						if(!in_array($opened_module_delete, $user_allowed_page_array)){ 
							echo " <a href= '' class='btn btn-primary btn-xs tooltips' data-placement='top' data-toggle='tooltip' data-original-title='Delete' 
							 disabled ><i class='icon-remove'></i></a>"; 
						}else{ 
							if($user_profile->global_delete == 'all_records' ){ 
								echo " <a href='#my{$record->id}' data-toggle='modal' class='btn btn-primary btn-xs tooltips'  
								data-placement='top' data-original-title='Delete'  >  <i class='icon-remove'></i></a>"; 
							}else{ 
								echo " <a href= '#myModal'  class='btn btn-primary btn-xs tooltips'  
								data-placement='top' data-toggle='tooltip' data-original-title='Delete'  disabled >  <i class='icon-remove'></i></a>"; 
							} 
						} 
						echo " 
									</td> 
								</tr>";   //delete dialoge  
					   echo   "<div class='modal fade' id='my{$record->id}' tabindex='-1' role='dialog' aria-labelledby='myModalLabel' aria-hidden='true'> 
														  <div class='modal-dialog'> 
															  <div class='modal-content'> 
																  <div class='modal-header'> 
																	  <button type='button' class='close' data-dismiss='modal' aria-hidden='true'>&times;</button> 
																	  <h4 class='modal-title'>Delete</h4> 
																  </div> 
																  <div class='modal-body'> 
																   <p> Are you sure you want delete  $record->title?</p> 
																  </div> 
																  <div class='modal-footer'> 
																	  <button class='btn btn-warning' type='button'  onClick=\"window.location.href = 'data_model/delete.php?task=delete&id={$record->id}'\"/>  
																	  Confirm</button>  <button data-dismiss='modal' class='btn btn-default' type='button'>cancle</button> 
																  </div> 
															  </div> 
														  </div> 
													  </div>"; 
													   $serialize++; 
							   }?> 
            </tbody> 
          </table> 
        </div> 
      </div> 
    </section> 
    <!-- page end-->  
  </section> 
</section> 
<!--main content end-->  
<!--footer start--> 
<?php require_once("../layout/footer.php");?> 
