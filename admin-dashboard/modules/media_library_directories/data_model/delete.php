<?php 
require_once('../../../../classes/Functions.php'); 
require_once('../../../../classes/MysqlDatabase.php'); 
require_once('../../../../classes/Session.php'); 
if(!empty($_GET["task"]) && $_GET["task"] == "delete"){ 
  $folder_name = $_GET['title']; 
  $path = "../../../../media-library/"; 
  $path_thumb = "../../../../media-library-thumb/"; 
   
   
//create folder 
if(file_exists($path.$folder_name)){ 
	//delete all file to delete folder 
	$folder_file = array_diff(scandir($path.$folder_name), array('..', '.'));  
	foreach($folder_file as $file){ 
		    $file_path = $path.$folder_name."/"; 
		     unlink($file_path.$file); 
	} 
	 
	$delete = rmdir($path.$folder_name); 
	if($delete){ 
		$folders_thumb = array('large','medium','small'); 
		foreach($folders_thumb as $folder){ 
		  $thumb_files = array_diff(scandir($path_thumb.$folder_name."/".$folder), array('..', '.'));  
		  foreach($thumb_files as $file){ 
			  $file_path = $path_thumb.$folder_name."/".$folder."/"; 
			    unlink($file_path.$file); 
		  } 
		     rmdir($path_thumb.$folder_name."/".$folder); 
		  } 
	   rmdir($path_thumb.$folder_name); 
	   redirect_to("../view.php"); 
	}else{ 
		redirect_to("../view.php"); 
	} 
  }else{ 
		redirect_to("../view.php");  
	 } 
}else{ 
	 redirect_to("../view.php");  
	 } 
  
?> 
