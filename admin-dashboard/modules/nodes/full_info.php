<?php  
require_once("../layout/initialize.php");	 
//check id access 
if(isset($_GET['id']) && is_numeric($_GET['id'])){ 
	$record_id = $_GET['id']; 
	$define_class = new Nodes; 
	$define_class->enable_relation(); 
	$record_info = $define_class->node_data(null,$record_id) ;  
	$type = $record_info->node_type;
	if(empty($record_info->id)){ 
		redirect_to("view.php");	 
	}else{ 
		$define_node_selected_taxonomy = new NodesSelectedTaxonomies(); 
		$define_node_selected_taxonomy->enable_relation(); 
		$event_categories = $define_node_selected_taxonomy->return_node_taxonomy($record_id,$type,'category','many',$general_setting_info->translate_lang_id);      
		 //get node model 
		$event_model = $define_class->get_model($record_id); 
		//event details 
		 $event_details  = array();
		  if($record_info->node_type == "event"){
		   $event_details = EventDetails::find_by_custom_filed('event_id',$record_id);	 
		  }
		$image_gallery = NodesImageGallery::find_all_by_custom_filed("related_id",$record_id,'sort','asc'); 
		//event tags 
		$tags = $define_node_selected_taxonomy->return_node_taxonomy($record_id,$type,'tag','many',0); 
	} 
}else{ 
	redirect_to("view.php");	 
} 
		 
require_once("../layout/header.php"); 
?> 
<!--header end--> 
<!--sidebar start--> 
<?php require_once("../layout/navigation.php");?> 
<!--sidebar end-->  
<!--main content start--> 
<section id="main-content"> 
<section class="wrapper site-min-height"> 
<h4>Nodes Module</h4> 
<div class="row"> 
  <aside class="col-lg-8"> 
	<section> 
	  <div class="panel"> 
		<div class="panel-heading"> Node Info</div> 
		<div class="panel-body"> 
		<form class="form-horizontal tasi-form" role="form" id="form_crud" action=""> 
			<section class="panel "> 
			 <header class="panel-heading tab-bg-dark-navy-blue"> 
			 <ul class="nav nav-tabs"> 
				 <li class=" center-block active" > <a data-toggle="tab" href="#main_info" class="text-center"><strong> Main Info</strong></a></li> 
				 <li> <a data-toggle="tab" href="#model_images" class="text-center"><strong> Gallery & Images </strong> </a> </li> 
				 <li> <a data-toggle="tab" href="#event_drtails" class="text-center"><strong>Nodes Details </strong> </a> </li> 
				 <li> <a data-toggle="tab" href="#taxonomies" class="text-center"><strong> Taxonomies</strong> </a> </li> 
			 </ul> 
		</header> 
		<div class="panel-body"> 
		  <div class="tab-content"> 
		  <div id="main_info" class="tab-pane active "> 
			 <section class="panel col-lg-9"> 
				<header class="panel-heading tab-bg-dark-navy-blue "> 
				<ul class="nav nav-tabs"> 
				  <?php 
					//create tabs for all available languages  
					$languages = Localization::find_all('id','desc'); 
					$serial_tabs = 1; 
					foreach($languages as $language){ 
						$lang_tab_header = ucfirst($language->name); 
						echo "<li class='";if($serial_tabs == 1){ echo " active ";}  echo"'> <a data-toggle='tab' href='#$language->name'> 
						<strong>$lang_tab_header</strong></a></li>"; 
						$serial_tabs++; 
					} 
				  ?> 
				</ul> 
				</header> 
				<div class="panel-body"> 
				<div class="tab-content"> 
				  <?php 
					$serial_tabs_content = 1; 
					foreach($languages as $language){ 
						//get data by language 
						$main_content = $define_class->get_node_content($record_id, $language->id); 
								echo " 
								<div id='$language->name' class='tab-pane"; if($serial_tabs_content == 1){ echo " active ";} echo"'>"; 
								if($main_content){ 
									echo " 
									<div class='form-group'> 
									<label  class='col-lg-2'>Title:</label> 
									<div class='col-lg-9'>$main_content->title</div> 
								  </div> 
								  <div class='form-group'> 
									<label class='col-lg-2'>Alias:</label> 
									<div class='col-lg-9'>$main_content->alias</div> 
								  </div> 
								  <div class='form-group'> 
									<label  class='col-lg-2'>Summary:</label> 
									<div class='col-lg-9'>$main_content->summary</div> 
								  </div> 
								  <div class='form-group'> 
									<label class='col-lg-2'>Body:</label> 
									<div class='col-lg-9'>$main_content->body</div> 
								  </div> 
								  <div class='form-group'> 
									<label  class='col-lg-2'>Meta Keys:</label> 
									<div class='col-lg-9'>$main_content->meta_keys</div> 
								  </div> 
								  <div class='form-group'> 
									<label  class='col-lg-2'>Meta Description:</label> 
									<div class='col-lg-9'>$main_content->meta_description</div> 
								  </div>"; 
									  } 
								echo "</div>"; 
						$serial_tabs_content++; 
					} 
				  ?> 
				</div> 
				</div> 
				</section> 
		    
		  </div> 
		  <div id="model_images" class="tab-pane "> 
			 <div class="form-group"> 
			  <label  class="col-lg-2">Image Cover:</label>
			  <div class="col-lg-8"> 
			  <?php if(!empty($record_info->cover_image) ){
                echo   "<div  id='imageShow'>
                     <img src='../../../media-library/{$record_info->cover_image}' id='imageSrc' style='width:200px; height:100px;'/>
                     </div>";
              }else{
                echo "No image was selected";

              }
                 ?>
			  </div> 
			  </div> 
		  <div class="form-group"> 
			  <label  class="col-lg-2">Slider Cover:</label>
			  <div class="col-lg-8">
                  <?php if(!empty($record_info->slider_cover) ){
                      echo   "<div  id='imageShow'>
                    <img src='../../../media-library/slider/{$record_info->slider_cover}' id='imageSrcSlider' style='width:200px; height:100px'/>
                     </div>";
                  }else{
                      echo "No Slider image was selected";

                  }
                  ?>
			   </div>
			  </div>
              <div class="form-group">
                  <label  class="col-lg-2">Gallery images:</label>
                  <div class="adv-table editable-table col-lg-10">
                      <table class="table table-striped table-hover table-bordered image_gallery_tbl">
                        <thead>
                          <tr>
                            <th class="name">Image</th>
                            <th class="sort" >Sort</th>
                            <th class="sort">Caption</th>
                          </tr>
                        </thead>
                        <tbody>
                        <?php
                        if($image_gallery){
                          foreach($image_gallery as $record){
                              echo "
                              <tr>
                                  <td><img src='../../../media-library/$record->image' id='imageSrc1' style='width:80px; height:80px;'></td>
                                  <td>$record->sort</td>
                                  <td>$record->caption</td>
                              </tr>
                              ";
                          }
                        }
                        ?>
                        </tbody>
                      </table>
                    </div>
                    </div>
              </div>
                <div id="event_drtails" class="tab-pane "> 
                 <div class="form-group"> 
                      <label  class="col-lg-2">Type:</label> 
                      <div class="col-lg-8"> 
                      <?php echo $record_info->node_type?> 
                      </div> 
                    </div> 
                  <div class="form-group  <?php if($record_info->node_type != "event"){echo "hide";} ?> "> 
                      <label  class="col-lg-2">Place:</label> 
                      <div class="col-lg-8"> 
                      <?php echo $event_details->place?> 
                      </div> 
                    </div> 
                     <div class="form-group  <?php if($record_info->node_type != "event"){echo "hide";} ?>"> 
                   <label class="col-lg-2">Start Date:</label>
                   <div class="col-lg-8"> 
                    <?php echo $event_details->start_date?> 
                  </div> 
                </div> 
                <div class="form-group  <?php if($record_info->node_type != "event"){echo "hide";} ?>"> 
                  <label class="col-lg-2">End Date:</label>
                   <div class="col-lg-8"> 
                   <?php echo $event_details->end_date?> 
                </div> 
                 </div> 
                 </div> 
                <div id="taxonomies" class="tab-pane "> 
                 <div class="form-group"> 
                    <label class="col-lg-2">Categories:</label>
                    <div class="col-lg-8"> 
                     <ul> 
                  <?php  
                 if(!empty($event_categories))
                 {
                     foreach($event_categories as $category){
                         echo "<li>- $category->taxonomy_name</li>";
                     }

                echo "</ul>";
                }else{
                echo "No Categories were selected";
                }
                ?>
               </div> 
             </div> 
           </div> 
          </div> 
         </div> 
          </section> 
          <div class="form-group"> 
              <div class="col-lg-offset-2 col-lg-10"> 
             <button type="button" class="btn btn-info" onClick="window.location.href = 'update.php?id=<?php echo $record_id?>'" ><i class="icon-edit-sign"></i>Update </button> 
             
                     
              </div> 
            </div> 
			 
	</form> 
		   
		</div> 
	  </div> 
	</section> 
  </aside>
  <div class="col-lg-4">
  <section class="panel ">
  <header class=" panel-heading  tab-bg-dark-navy-blue">
    <ul class="nav nav-tabs">
      <li class="center-block active"><a data-toggle="tab" href="#op1" class="text-center"> <i class=" icon-check"></i><strong> Entry Info</strong></a></li>
      <li > <a data-toggle="tab" href="#op2" class="text-center"> <i class=" icon-check"></i> <strong> Options</strong> </a> </li>
      <li > <a data-toggle="tab" href="#op3" class="text-center"> <i class=" icon-calendar "></i> <strong> Publish Date</strong> </a> </li>
    </ul>
  </header>
  <div class="panel-body">
    <div id="list_info">
      <ul>
        <div class="tab-content">
          <div id="op1" class="tab-pane active "> <br />
            <li><span style="color:#428bca">> Created By:</span> <?php echo $record_info->inserted_by?></li>
            <li><span style="color:#428bca">> Created Date:</span> <?php echo $record_info->inserted_date?></li>
            <li><span style="color:#428bca">> Last Update By:</span>
              <?php 
				if($record_info->update_by!=""){
					echo $record_info->update_by;
				}else{
					echo "--";
				}
			  ?>
            </li>
            <li><span style="color:#428bca">> Last Update Date:</span>
              <?php 
				if($record_info->last_update != "0000-00-00 00:00:00"){
					echo $record_info->last_update;
				}else{
					echo "--";
				}
			  ?>
            </li>
          </div>
          <div id="op2" class="tab-pane"> <br />
            <li><span style="color:#428bca">> Status:</span> <?php echo $record_info->status?></li>
            <li><span style="color:#428bca">> Enable Comment:</span> <?php echo $record_info->enable_comments?></li>
            <li><span style="color:#428bca">> Enable Summary:</span> <?php echo $record_info->enable_summary?></li>
            <li><span style="color:#428bca">> Front Page:</span> <?php echo $record_info->front_page ?></li>
            <li><span style="color:#428bca">> Slide Show:</span> <?php echo $record_info->slide_show ?></li>
          </div>
          <div id="op3"  class="tab-pane  ">
            <li><span style="color:#428bca">> Start Publish:</span> <?php echo $record_info->start_publishing ?></li>
            <li><span style="color:#428bca">> End Publish:</span>
              <?php
					if($record_info->end_publishing != "0000-00-00 00:00:00"){
						echo $record_info->end_publishing;
					}else{
						echo "--";
					}
			  ?>
            </li>
          </div>
        </div>
      </ul>
    </div>
    </section>
    <section class="panel panel-primary">
            <header class="panel-heading"> Layout Model: </header>
             <div class="panel-body">
               <ul> 
                <li><span style="color:#428bca">> Theme </span><?php echo $event_model->theme_name?></li> 
                <li><span style="color:#428bca">> Layout </span><?php echo $event_model->model_name?></li> 
                <li><span style="color:#428bca">> ShortCut Link </span><?php echo $record_info->shortcut_link?></li> 
                </ul> 
               </div>
            
          </section>
        </div> 
   
</div> 
<!-- page end-->  
</section> 
</section> 
<!--main content end-->  
<!--footer start--> 
<?php require_once("../layout/footer.php");?> 
