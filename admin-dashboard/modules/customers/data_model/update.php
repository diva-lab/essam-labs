<?php 
require_once('../../../../classes/Session.php'); 
require_once('../../../../classes/Functions.php'); 
require_once('../../../../classes/MysqlDatabase.php'); 
require_once('../../../../classes/CustomerInfo.php'); 
//check log in  
if($session->is_logged() == false){ 
	redirect_to("../../index.php"); 
} 
if(!empty($_POST["task"]) && $_POST["task"] == "update"){ 
	header('Content-Type: application/json'); 
	$id = $_POST["record"]; 
//check rqquierd fields 
	$required_fields = array('first_name'=>"- Insert First Name",'email'=>'- Insert email','password'=>'- Insert Password' );  
	$check_required_fields = check_required_fields($required_fields); 
	if(count($check_required_fields) == 0){ 
	$email = $_POST["email"];
	if (filter_var($email, FILTER_VALIDATE_EMAIL)) { 
	

		$edit = CustomerInfo::find_by_id($id); 
		$edit->password =  md5($_POST["password"]);	
		$edit->first_name = $_POST["first_name"];
		$edit->last_name = $_POST["last_name"];
		$edit->email = $_POST["email"];
		$edit->city = $_POST["city"]; 
		$edit->area = $_POST["area"]; 
		$edit->street = $_POST["street"]; 
		$edit->gender = $_POST["gender"]; 
		$edit->birthday = $_POST["birthday"];
		$edit->account_status = $_POST["account_status"];
		$update = $edit->update();	

		if($update){ 
			$data  = array("status"=>"work"); 
			echo json_encode($data); 
		}else{ 
			$data  = array("status"=>"error"); 
			echo json_encode($data); 
		}

	 }else{

	 	$data  = array("status"=>"password_error");
			echo json_encode($data);
	 }
	 //end if for checking mail 
	 }else{
			$data  = array("status"=>"email_error");
			echo json_encode($data);
	   }				 
	}else{ 
		  //validation error 
		  $comma_separated = implode("<br>", $check_required_fields); 
		  $data  = array("status"=>"valid_error", "fileds"=>$comma_separated); 
		  echo json_encode($data); 
	 
} 
//close connection 
if(isset($database)){ 
$database->close_connection(); 
} 
?>