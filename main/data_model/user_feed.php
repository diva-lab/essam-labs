<?php
require_once('../../classes/FrontSession.php');
require_once('../../classes/Functions.php');
require_once('../../classes/MysqlDatabase.php');
require_once('../../classes/Nodes.php');
require_once('../../classes/NodesContent.php');
require_once("../../classes/UsersFeeds.php");
require_once("../../classes/Localization.php");
require_once('../../localization/'.$_POST["lang"].'/validation_label.php');
require_once('../../localization/'.$_POST["lang"].'/form_label.php');
//phpMailer code
header('Content-Type: application/json');
$lang = $_POST["lang"];
$lang_info = Localization::find_by_custom_filed("label",$lang);
$required_fields = array();
$check_required_fields = check_required_fields($required_fields);
	$add = new Nodes();
	$add->node_type = "feed";
	$add->status ="publish";
	$add->inserted_date = date_now();
	$add->inserted_by = 1;
    $insert = $add->insert();
	$inserted_id = $add->id;
	if($insert){
		$add_content = new NodesContent();
		$add_content->title = $_POST["title"];
		$add_content->body = $_POST["fbody"];
		$add_content->summary = $_POST["summary"];
		$add_content->node_id = $inserted_id;
		$add_content->lang_id = $lang_info->id;
		$insert_content = $add_content->insert();
		//insert User feed
		$add_feed = new UsersFeeds();
		$add_feed->resource_id = $inserted_id;
		$add_feed->custome_info_id =  $front_session->user_id;
		$add_feed->inserted_date = date_now();
		$add_feed->resource_type = "feed";
		$add_feed->insert();
		$data = array("status"=>"work","id"=>$inserted_id);
		echo json_encode($data);
		}else{
			$data = array("status"=>"not_work","message"=>$verror_from_system);
			echo json_encode($data);
		}
						
?>
